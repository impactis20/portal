﻿<%@ Page Title="Portal Proveedores Colmena" Language="C#" MasterPageFile="~/headerFooter.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="PortalProveedores.App.UI.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div id="loginbox" style="margin-top: 50px;" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="text-center">
                        <h4>Ingreso a Portal Proveedores</h4>
                    </div>
                </div>
            </div>

            <div style="padding-top: 30px" class="panel-body">
                <div id="error" runat="server" class="alert alert-danger col-sm-12" visible="false"></div>

                <div id="loginform" class="form-horizontal">

                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control" MaxLength="10" placeholder="Ingrese RUT..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" autocomplete="off"></asp:TextBox>
                            </div>
                            <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="ingresar"></asp:CustomValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Ingrese su password..."></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Ingrese su contraseña..." ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="True" CssClass="help-block" ValidationGroup="ingresar" />
                        </div>
                    </div>

                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <asp:CheckBox ID="cbMantenerSesionIniciada" runat="server" Text="Mantener sesión iniciada" CssClass="checkbox" />
                            </label>
                        </div>
                    </div>

                    <br style="margin-top: 30px" class="form-group">
                        <!-- Button -->

                        <div class="col-sm-12">
                            <asp:Button ID="btnEntrar" runat="server" Text="Entrar" CssClass="btn btn-primary btn-block" OnClick="btnEntrar_Click" ValidationGroup="ingresar" />
                            <%--<input name="btnEntrar" type="button" class="boton" onClick="btnEntrar_Click();" ValidationGroup="ingresar" />--%>
                            <div class="text-center" style="margin-top:15px">
                                <a href="recuperar-clave.aspx" target="_self">Recordar contraseña | Clave Inicial</a>
                            </div>
                              <div class="text-center" style="margin-top:15px">
                                <a href="/PortalProveedoresColmena.pdf" target="_blank">Descarga Manual de Usuario</a>
                            </div>
                            <div class="text-center" style="margin-top:15px">
                                <a href="sugerencias-externas.aspx" target="_self">Contactenos <i class="fa fa-envelope-o" ></i></a>
                            </div>
                        </div>
                    
                    </div>
                    <br>
                  
                </div>
            </div>
        </div>


    
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>
