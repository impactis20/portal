﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;


namespace PortalProveedores.App.UI.App_Code
{
    public class Util
    {


        public static string pass = "_C_OLMEN_A_";
        public static string saltValue = "CL";
        /// <summary>
        /// Método para encriptar un texto plano usando el algoritmo (Rijndael). Este es el mas simple posible, muchos de los datos necesarios los definimos como constantes.
        /// </summary>
        /// <param name="textoQueEncriptaremos">texto a encriptar</param>
        /// <returns>Texto encriptado</returns>
        public static string Encriptar(string textoQueEncriptaremos)
        {
            return Encriptar(textoQueEncriptaremos, pass, saltValue, "MD5", 1, "@1B2c3D4e5F6g7H8", 128);
        }
        /// <summary>
        /// Método para encriptar un texto plano usando el algoritmo (Rijndael) x
        /// </summary>
        /// <returns>Texto encriptado</returns>
        private static string Encriptar(string textoQueEncriptaremos, string passBase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(textoQueEncriptaremos);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string cipherText = Convert.ToBase64String(cipherTextBytes);
            return cipherText;
        }

        /// <summary>
        /// Método para desencriptar un texto encriptado.
        /// </summary>
        /// <returns>Texto desencriptado</returns>
        public static string Desencriptar(string textoEncriptado)
        {
            return Desencriptar(textoEncriptado, pass, saltValue, "MD5", 1, "@1B2c3D4e5F6g7H8", 128);
        }

        /// <summary>
        /// Método para desencriptar un texto encriptado (Rijndael)
        /// </summary>
        /// <returns>Texto desencriptado</returns>
        private static string Desencriptar(string textoEncriptado, string passBase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] cipherTextBytes = Convert.FromBase64String(textoEncriptado.Replace(" ", "+"));
            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };

            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            return plainText;
        }

        public static string Initcap(string texto)
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(texto.ToLower());
        }

        public static bool EsEmailValido(string email)
        {
            return Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        public static bool RutValido(string Rut)
        {
            if (string.IsNullOrEmpty(Rut))
                return false;
            else
            {
                string rutLimpio = string.Empty;
                string digitoVerificador = string.Empty;
                int suma = 0;
                int contador = 2;
                bool valida = true;

                rutLimpio = Rut.Replace(".", string.Empty);
                rutLimpio = rutLimpio.Replace("-", string.Empty);
                rutLimpio = rutLimpio.Replace(" ", string.Empty);
                rutLimpio = rutLimpio.Substring(0, (rutLimpio.Length - 1));

                digitoVerificador = Rut.Substring((Rut.Length - 1), 1).ToUpper();

                int i = 0;
                for (i = (rutLimpio.Length - 1); i >= 0; i = (i + -1))
                {
                    if ((contador > 7))
                    {
                        contador = 2;
                    }

                    try
                    {
                        suma = (suma + (int.Parse(rutLimpio[i].ToString()) * contador));
                        contador++;
                    }
                    catch (Exception)
                    {
                        valida = true;
                    }
                }

                if (valida)
                {
                    int dv = (11 - (suma % 11));
                    string DigVer = dv.ToString();

                    if ((DigVer == "10"))
                    {
                        DigVer = "K";
                    }
                    if ((DigVer == "11"))
                    {
                        DigVer = "0";
                    }
                    if ((DigVer == digitoVerificador))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static string GetMensajeAlerta(string TipoAlerta, string Titulo, string Mensaje)
        {
            string Estilo = string.Empty;

            switch (TipoAlerta)
            {
                case "error":
                    Titulo = "<i class=\"fa fa-frown-o\" aria-hidden=\"true\"></i>&nbsp;" + Titulo;
                    Estilo = "text-danger";
                    break;

                case "success":
                    Titulo = "<i class=\"fa fa-smile-o\" aria-hidden=\"true\"></i>&nbsp;" + Titulo;
                    break;

                case "warning":
                    Titulo = "<i class=\"fa fa-meh-o\" aria-hidden=\"true\"></i>&nbsp;" + Titulo;
                    break;
            }

            return string.Format("alerta('{0}', '{1}', '{2}');", Titulo, Mensaje, Estilo);
        }
        public static string GetError(string Error)
        {
            return string.Format("Se produjo un problema al realizar la operación.<br><br><b>Error:</b> {0}", Error);
        }
    }
}