﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using PortalProveedores.BL;


namespace PortalProveedores.App.UI.App_Code
{
    public class Seguridad : IHttpModule
    {
        public Seguridad() { }
        private HttpApplication _app;

        public void Init(HttpApplication oHttpApp)
        {
            _app = oHttpApp;
            _app.AuthorizeRequest += new EventHandler(AuthorizeRequest);
        }

        public void Dispose() { }

        private void AuthorizeRequest(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.Identity is FormsIdentity)
                {

                    string paginaActual = HttpContext.Current.Request.Url.AbsolutePath;//HttpContext.Current.Request.RawUrl;
                    if (paginaActual != "/app/no-autorizado.aspx" && !paginaActual.Contains("__browserLink") && !paginaActual.Contains("ScriptResource") && !paginaActual.Contains("WebResource"))
                    {

                        //Modificado por #GloriaMolina 11-01-2018
                        //Entities.UsuarioEntity usuario = new Entities.UsuarioEntity(UsuarioBL.GetRol(GetRolUsuarioLogueado()));
                        string usuario = GetRolUsuarioLogueado();

                        if (usuario != null)
                        {
                            //obtiene las páginas restringuidas según rol
                            string rut = GetRutUsuarioLogueado();
                            List<Entities.SistemaEntity.Pagina> lstPaginaRol = UsuarioBL.GetPaginasRol(rut);

                            //Usuarios sin accesos serán transferido a página personalizada
                            if (!lstPaginaRol.Exists(x => x.path == paginaActual))
                            {
                                if (paginaActual == "/login.aspx")

                                { HttpContext.Current.Response.Redirect(FormsAuthentication.DefaultUrl); }

                                HttpContext.Current.Server.Transfer("~/app/no-autorizado.aspx");

                            }

                        }
                    }
                }
            }
            catch (HttpCompileException)
            {
                HttpContext.Current.Server.Transfer("~/app/no-autorizado.aspx");
            }
        }

        public static string GetRutUsuarioLogueado()
        {
            try
            {
                return GetCurrentUserData().UserData.Split('*')[0];
            }
            catch
            {
                CerrarSesion();
                return string.Empty;
            }
        }

        public static string GetClaveUsuarioLogueado()
        {
            try
            {
#pragma warning disable CS0436 // El tipo entra en conflicto con un tipo importado
                return Util.Desencriptar(GetCurrentUserData().UserData.Split('*')[1]);
#pragma warning restore CS0436 // El tipo entra en conflicto con un tipo importado
            }
            catch
            {
                CerrarSesion();
                return string.Empty;
            }
        }

        public static string GetRolUsuarioLogueado()
        {
            try
            {
                return GetCurrentUserData().UserData.Split('*')[2];
            }
            catch
            {
                CerrarSesion();
                return string.Empty;
            }
        }

        public static string GetSociedadUsuarioLogueado()
        {
            try
            {
                return GetCurrentUserData().UserData.Split('*')[3];
            }
            catch
            {
                CerrarSesion();
                return string.Empty;
            }
        }

        public static string GetEmailUsuarioLogueado()
        {
            try
            {
                return GetCurrentUserData().UserData.Split('*')[4];
            }
            catch
            {
                CerrarSesion();
                return string.Empty;
            }
            
        }

        public static string GetNombreUsuarioLogueado()
        {
            try
            {
                return GetCurrentUserData().Name;
            }
            catch
            {
                CerrarSesion();
                return string.Empty;
            }
        }

        private static FormsAuthenticationTicket GetCurrentUserData()
        {
            FormsIdentity _identity = (FormsIdentity)HttpContext.Current.User.Identity;
            FormsAuthenticationTicket ticket = _identity.Ticket;

            return ticket;
        }

        public static void AddCookieAutenticacion(string rut, string clave, string idRol, string idSociedad, string nombre, string email)
        {
#pragma warning disable CS0436 // El tipo entra en conflicto con un tipo importado
            string userData = string.Format("{0}*{1}*{2}*{3}*{4}", rut, Util.Encriptar(clave), idRol, idSociedad, email);
#pragma warning restore CS0436 // El tipo entra en conflicto con un tipo importado
#pragma warning disable CS0436 // El tipo entra en conflicto con un tipo importado
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2, Util.Initcap(nombre), DateTime.Now, DateTime.Now.AddDays(2), false, userData, FormsAuthentication.FormsCookiePath);
#pragma warning restore CS0436 // El tipo entra en conflicto con un tipo importado
            string ticketEncriptado = FormsAuthentication.Encrypt(ticket);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, ticketEncriptado));
        }

        public static void CerrarSesion(params StringBuilder[] parametrosLoginURL)
        {
            if (HttpContext.Current.Request.Cookies["dReUser_"] != null)
            {
                HttpContext.Current.Request.Cookies["dReUser_"].Expires = DateTime.Now;
                HttpContext.Current.Response.Cookies.Add(HttpContext.Current.Request.Cookies["dReUser_"]);
            }

            FormsAuthentication.SignOut();
            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session.Abandon();

            // limpia la cookie de autenticación
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty);
            cookie1.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie1);

            // limpia cookie de sessión (no es necesario)
            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, string.Empty);
            cookie2.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie2);

            StringBuilder parametros = new StringBuilder();
            foreach (StringBuilder valor in parametrosLoginURL)
                parametros.Insert(parametros.Length, valor);

            //FormsAuthentication.RedirectToLoginPage();
            HttpContext.Current.Response.Redirect(string.Format("{0}?{1}", FormsAuthentication.LoginUrl, parametros), false);
        }

        public static void DejarSesionIniciada(string rut, string clave)
        {
            string datosUsuario = string.Format("{0}|{1}", rut, clave);
#pragma warning disable CS0436 // El tipo entra en conflicto con un tipo importado
            HttpCookie Datos = new HttpCookie("dReUser_", App_Code.Util.Encriptar(datosUsuario));
#pragma warning restore CS0436 // El tipo entra en conflicto con un tipo importado
            Datos.Expires = DateTime.Now.AddDays(7);
            HttpContext.Current.Response.Cookies.Add(Datos);
        }

        public static void SetCookieAutenticacion(string clave)
        {
            string rut = GetRutUsuarioLogueado();
            string idRol = GetRolUsuarioLogueado();
            string nombre = GetNombreUsuarioLogueado();
            string email = GetEmailUsuarioLogueado();
            string idSociedad = GetSociedadUsuarioLogueado();

            AddCookieAutenticacion(rut, clave, idRol, idSociedad, nombre, email);
        }

        public static void SetCookieAutenticacion(string nombre, string email)
        {
            string rut = GetRutUsuarioLogueado();
            string idRol = GetRolUsuarioLogueado();
            string clave = GetClaveUsuarioLogueado();
            string idSociedad = GetSociedadUsuarioLogueado();

            AddCookieAutenticacion(rut, clave, idRol, idSociedad, nombre, email);
        }
    }
}