﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using System.Web;

namespace PortalProveedores.App.UI
{
    /// <summary>
    /// Descripción breve de ejemplo
    /// </summary>
    public class ejemplo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //En este controlador tenemos que recibir la ruta del pdf... cómo? recibiéndola por parámetro de url
            if (context.Request.QueryString["id"] != null) //verifica si trae el parámetro "id"
            {
                // Get the object used to communicate with the server.
                String ruta = App_Code.Util.Desencriptar(context.Request.QueryString["id"].ToString());
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(ruta);
                //ftpRequest.Credentials = new NetworkCredential("user345", "pass234");
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());
                List<string> directories = new List<string>();

                string line = streamReader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    directories.Add(line);
                    line = streamReader.ReadLine();
                }
                streamReader.Close();


                using (WebClient ftpClient = new WebClient())
                {
                    //ftpClient.Credentials = new System.Net.NetworkCredential("user345", "pass234");

                    for (int i = 0; i <= directories.Count - 1; i++)
                    {
                        if (directories[i].Contains("."))
                        {

                            string path = ruta + directories[i].ToString();
                            string trnsfrpth = context.Server.MapPath("/app/documento/temp/") + directories[i].ToString();
                            ftpClient.DownloadFile(path, trnsfrpth);
                        }
                    }
                }

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}