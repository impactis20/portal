﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.Security;
using PortalProveedores.BL;
using System.Web.UI.WebControls;

namespace PortalProveedores.App.UI
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowAmbiente();

                if (Request.QueryString["t"] != null && Request.QueryString["m"] != null)
                {
                    if (!ClientScript.IsStartupScriptRegistered("sMM"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "sMM", "<script type=\"text/javascript\" src=\"js/app/bootstrap-showMessageModal.v1.js\"></script>");
                    }

                    DateTime fecha = Convert.ToDateTime(App_Code.Util.Desencriptar(Request.QueryString["t"]));
                    bool fechaExpirada = fecha > DateTime.Now ? false : true;

                    if (!fechaExpirada)
                    {
                        string mensaje = App_Code.Util.Desencriptar(Request.QueryString["m"]);
                        ScriptManager.RegisterStartupScript(this, GetType(), "msj309", string.Format("getMessage('{0}', '{1}')", mensaje, "0"), true);
                    }
                }
            }

            //loguea automáticamente al usuario si la última vez checkeó "No cerrar sesión" { Si usted cierra el navegador sin cerrar sesión, la próxima vez que ingrese al Portal, entrará automáticamente sin ingresar Email y Contraseña }
            if (Request.Cookies["dReUser_"] != null)
                ValidaUsuario(new Entities.UsuarioEntity(App_Code.Util.Desencriptar(Request.Cookies["dReUser_"].Value).Split('|')[0], App_Code.Util.Desencriptar(Request.Cookies["dReUser_"].Value).Split('|')[1]), true);
        }


        protected void contraseña(object sender, System.EventArgs e)
        {
            HyperLink urlDoc = FindControl("recuperar_clave.aspx") as HyperLink;
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {

            string rut = txtRutProveedor.Text.Trim();
            string clave = txtPassword.Text.Trim();

            if (App_Code.Util.RutValido(rut))
            {
                ValidaUsuario(new Entities.UsuarioEntity(rut, clave), cbMantenerSesionIniciada.Checked);
            }
            else
            {
                error.Visible = true;
                error.InnerHtml = string.Format("<p><i class='fa fa-exclamation-triangle'>&nbsp;&nbsp;</i>{0}</p>", "Ingrese un rut válido.");
            }

        }


        private void ShowAmbiente()
        {
            string ambiente = ConfigurationManager.AppSettings["ambiente"];

            if (ambiente == "qas" || ambiente == "des")
            {
                //mensaje_ambiente.Visible = true;
            }
        }


        private void ValidaUsuario(Entities.UsuarioEntity usuario, bool MantieneSesionIniciada)
        {
            Entities.ResponseWSEntity respWS = UsuarioBL.GetAutenticacionUsuario(ref usuario);

            if (respWS.codigo == "0")
            {
                if (MantieneSesionIniciada)
                    App_Code.Seguridad.DejarSesionIniciada(usuario.rut, usuario.clave);

                //Crea cookie de autenticación en navegador
                App_Code.Seguridad.AddCookieAutenticacion(usuario.rut, usuario.clave, usuario.rol.id, usuario.sociedad.id, usuario.nombre, usuario.email);

                Response.Redirect(FormsAuthentication.GetRedirectUrl(usuario.nombre, false));
            }
            else
            {
                error.Visible = true;
                error.InnerHtml = string.Format("<p><i class='fa fa-exclamation-triangle'>&nbsp;&nbsp;</i>{0}</p>", respWS.mensaje);
            }
        }
    }
}