﻿
//Ejemplos de entrada y salida
//"10452369-5" = "10.452.369-5"
//"104523695" = "10.452.369-5"
//"104523695-" = "10.452.369-5"
//"" = ""

function textboxRutFormat(textbox) {
    var parteEntera = textbox.value.split('-')[0];
    var dv = '';
    numeral.locale('es');

    if (textbox.value.length > 1 && textbox.value.substring(textbox.value.length - 1, textbox.value) !== ".") {
        if (textbox.value.indexOf("-") !== -1) {
            //cuando trae guión
            dv = '-' + textbox.value.split('-')[1].substring(0, 1);

            if (dv === "-") {
                dv = "";
                textbox.value = numeral(parteEntera).format('0,0') + dv;
                textboxRutFormat(textbox);
                return;
            }
        }
        else {
            //cuando no trae guión
            dv = "-" + parteEntera.substring(parteEntera.length, parteEntera.length - 1);
            parteEntera = parteEntera.substring(0, parteEntera.length - 1);
        }

        var valor = numeral(parteEntera).format('0,0') + dv;
        textbox.value = valor;
    }
}

function textboxRutOnKeyUp(textbox, event) {    
    var e = window.event;
    if (e) {
        var key = e.keyCode;
        if (!((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key === 16 || key === 86 || key === 189 || key === 109 || key === 75 || key === 36 || key === 35 || key === 37 || key === 39)) {
            if (key === 190) //.
                textbox.value = textbox.value.replace(/\./g, '');
            else
                textbox.value = textbox.value.replace(new RegExp(window.event.key, 'g'), '');
        }            
    }
}

function textboxRutOnFocus(textbox)
{
    //limpia caracter con "."
    textbox.value = textbox.value.replace(/\./g, '');
}

function customValidatorRutObligatorio(source, args) {
    var textboxValidate = document.getElementById(source.controltovalidate);
    args.IsValid = validarRut(textboxValidate);
}

function customValidatorRut(source, args) {
    var textboxValidate = document.getElementById(source.controltovalidate);

    if (textboxValidate.value === "")
        args.IsValid = true;
    else
        args.IsValid = validarRut(textboxValidate);
}

function validarRut(rut) {
    var valor = rut.value.replace(/\./g, '');
    if (valor === "")
        return false;
    else {
        valor = valor.replace('-', '');
        cuerpo = valor.slice(0, -1);
        dv = valor.slice(-1).toUpperCase();
        suma = 0;
        multiplo = 2;
        for (i = 1; i <= cuerpo.length; i++) {
            index = multiplo * valor.charAt(cuerpo.length - i);
            suma = suma + index;
            if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
        }
        dvEsperado = 11 - (suma % 11);
        dv = (dv == 'K') ? 10 : dv;
        dv = (dv == 0) ? 11 : dv;
        return dvEsperado == dv;
    }
}