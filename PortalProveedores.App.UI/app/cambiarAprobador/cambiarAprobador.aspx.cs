﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app.cambiarAprobador
{
    public partial class cambiarAprobador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            
            }
        }
        protected void btnCambiar_ServerClick(object sender, EventArgs e)
        {
                string rutAnterior = txtRutAnterior.Text;
                string rutAnt = rutAnterior.Replace(".", string.Empty);
                string rutNuevo = txtRutNuevo.Text;
                string rutNue = rutNuevo.Replace(".", string.Empty);
                string FDesdeO = txtFechaDesde.Text;
                string FHastaO = txtFechaHasta.Text;

                if (FDesdeO != String.Empty && FHastaO != String.Empty)
                {
                    FDesdeO = Convert.ToDateTime(txtFechaDesde.Text).ToString("yyyyMMdd");
                    FHastaO = Convert.ToDateTime(txtFechaHasta.Text).ToString("yyyyMMdd");
                }
                
                string dateFechaDesde = FDesdeO;
                string dateFechaHasta = FHastaO;
                string idParametro = string.Empty;
       
                ViewState["ra"] = rutAnt;
                ViewState["rn"] = rutNue;
                ViewState["fd"] = dateFechaDesde;
                ViewState["fh"] = dateFechaHasta;
            
                if (rbCambiar.SelectedIndex == 0)
                {
                    idParametro = "2";
                }
                else if (rbCambiar.SelectedIndex == 1)
                {
                    idParametro = "1";                  
                }
                 Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
                 Entities.ResponseWSEntity resp = CambiarAprobadorBL.GetCambiarAprobador(rutAnt, rutNue, idParametro, dateFechaDesde, dateFechaHasta);
                ScriptManager.RegisterStartupScript(this, GetType(), "msj8", string.Format("getMessage('{0}', '{1}')", resp.codigo, resp.mensaje), true);
                limpiarCampos();
        }
        private void limpiarCampos()
        {
            txtRutAnterior.Text = string.Empty;
            txtRutNuevo.Text = string.Empty;
            txtFechaDesde.Text = string.Empty;
            txtFechaHasta.Text = string.Empty;
            rbCambiar.SelectedIndex =0;

        }
    }
}