﻿<%@ Page Title="Consultar usuario" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="consultar.aspx.cs" Inherits="PortalProveedores.App.UI.app.usuario.consultar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script> 
    <script src='<%= ResolveUrl("~/js/fileinput.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });

            $('[id*=calendarioFechaDesde]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                //minDate: moment().add(-3, 'month'),
                showClear: true,
                ignoreReadonly: false
            });

            $('[id*=calendarioFechaHasta]').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'),
                showClear: true,
                ignoreReadonly: false
            });

            $("[id*=calendarioFechaDesde]").on("dp.change", function (e) {
                $('[id*=calendarioFechaHasta]').data("DateTimePicker").minDate(e.date);
            });

            $("[id*=calendarioFechaHasta]").on("dp.change", function (e) {
                $('[id*=calendarioFechaDesde]').data("DateTimePicker").maxDate(e.date);
            });

            $('.fechafiltro').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: moment().add(-2, 'year'),
                showClear: true,
                ignoreReadonly: false
            });

        });
    </script>

    <h2 class="page-header">Consultar Usuarios</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                            <p>Para realizar una búsqueda debe rellenar los campos con asterisco. </p>                           
                            <div id="filtroRut" runat="server" class="col-lg-2">
                                <div class="row">
                                        </div>	       
				                            <div class="form-group">
											<label class="control-label"> Rut</label>
											<br />
										    <asp:TextBox ID="TextRutUsuarioc" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);"></asp:TextBox>
											<%--<asp:CustomValidator ID="CustomRutUsuarioc" runat="server" CssClass="help-block" ControlToValidate="TextRutUsuarioc" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" validationgroup="busr" ></asp:CustomValidator>--%>
										</div>
                                </div>
                            </div>                     
                          <div  runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
											<label class="control-label"> Rol</label>
											<br />
											<asp:ListBox ID="lstRol" runat="server" CssClass="selectpicker" SelectionMode="Single" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
											<%--<asp:RequiredFieldValidator ID="rfvrol" runat="server"  Display="Dynamic" ControlToValidate="lstRol" validationgroup="busr"/>--%>
										</div>
                                 </div>
                           </div>
                            <div id="filtroFechaDesde" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                       <label for="txtFechaDesde" class="control-label">Fecha desde (Creación)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaDesde">
                                            <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div id="filtroFechaHasta" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                      <label for="txtFechaHasta" class="control-label">Fecha hasta (Creación)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaHasta">
                                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                     </div>
                                </div>
                            </div>
                                            
                         </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    <div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                            <button id="btnBuscar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnBuscar_ServerClick">
                                <i class="fa fa-search"></i>
                                &nbsp;&nbsp;Buscar
                            </button>
                            </div>
                            </div>
                        </div>
					<div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                 <button type="button" class="btn btn-primary btn-sm"  id="btncUsuario">
                                     <i class="fa fa-search"></i>
                                     &nbsp;&nbsp;Crear Usuario
                                 </button>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
   </div>
   <%--Tabla usuarios --%>   
  <div class="table-responsive small">
        <asp:GridView ID="gvUsuario" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Nombre del usuario.">Nombre</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 150px;" >
                       <%# Eval("nombre") %>
                    </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rut del usuario.">Rut</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("rut") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rol del usuario.">Rol</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("nombreRol") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Correo electrónico del usuario.">Correo</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("correo") %>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Creación del usuario.">Fecha Creación</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <%# Eval("fechac") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Estatus del usuario.">Estatus</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("estado") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=" ">
                   <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número correlativo del documento.">Acciones</div>
                    </HeaderTemplate>
                     <ItemTemplate>
                         <div class="tooltip-wrapper-header-table pull-left" title="Editar usuario">
                         <a style="padding-right:25px;font-size:15px;" id="editar" runat="server" href="#" data-toggle="modal" data-target=".modalEditUsuario" data-rut='<%# Eval("rut") %>' data-nombre='<%# Eval("nombre") %>' data-correo='<%# Eval("correo") %>' data-rol='<%# Eval("rol.nombre") %>' data-fechac='<%# Eval("fechac") %>' data-fecham='<%# Eval("fecham") %>' class="fa fa-edit"></a>
                         </div> 
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <div class="tooltip-wrapper-header-table pull-left" title="Inactivar usuario">
                         <a style="padding-right:25px;font-size:15px;"  id="inact" runat="server" onclick='<%# "inactivar(" +(((GridViewRow) Container).RowIndex) + " );" %>'  data-id="<%# ((GridViewRow) Container).RowIndex %>" data-rut='<%# Eval("rut") %>' class="fa fa-power-off"></a>
                         </div>
                      </ItemTemplate>  
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
     
    
    <%--Modal crear usuario --%>
    <div class="modal fade" id="modalUsuario" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="creacion" runat="server" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title">Crear Usuario</h2>
                        <p>Complete todos los campos para crear un usuario.</p>
						</div>  
							<div class="modal-body">
							<div class="row">
							   <div class="form-group">
								<div class="col-lg-6">
									   <div class="form-group">
											<label class="control-label"> Nombre</label>
											<br />
											<asp:TextBox ID="txtnombreUsuario" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvnombreUsuario" runat="server" CssClass="help-block" ErrorMessage="Ingrese nombre..." Display="Dynamic" ControlToValidate="txtnombreUsuario" SetFocusOnError="True" ValidationGroup="crear" />
										</div>	       
				                            <div class="form-group">
											<label class="control-label"> Rut</label>
											<br />
										    <asp:TextBox ID="txtrutUsuario" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);"></asp:TextBox>
											<asp:CustomValidator ID="cvrutUsuario" runat="server" CssClass="help-block" ControlToValidate="txtrutUsuario" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="crear"></asp:CustomValidator>
										</div>
                                        <div class="form-group">
											<label class="control-label"> Correo</label>
											<br />
											<asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control input-sm" MaxLength="60" autocomplete="off" placeholder="Ingrese..."  ></asp:TextBox>
											<asp:CustomValidator ID="cvCorreo" runat="server" CssClass="help-block" ControlToValidate="txtCorreo" ErrorMessage="Ingrese un <strong>CORREO</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorCorreoValido" ValidateEmptyText="true" ValidationGroup="crear"></asp:CustomValidator>
                                        </div>
										<div class="form-group">
											<label class="control-label"> Rol</label>
											<br />
											<asp:ListBox ID="lstRoles" runat="server" CssClass="selectpicker" SelectionMode="Single" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
											<asp:RequiredFieldValidator ID="rfvRoles" runat="server" CssClass="help-block" ErrorMessage="Ingrese rol..." Display="Dynamic" ControlToValidate="lstRoles" SetFocusOnError="True" ValidationGroup="crear" />
										</div>
											<div class="pull-right">
									</div>
									</div>
								</div>
							</div>
						</div>
						</div>
                        <div id="modalfooterc" runat="server" class="modal-footer">
                       		<button id="btncrearUsuario" runat="server" type="button" class="btn btn-primary btn-sm"  onserverclick="btncrearUsuario_ServerClick" ValidationGroup="crear">
							<i class="fa fa-plus"></i>&nbsp;&nbsp;Crear</button>
                            <button id="btnCerrar" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancelar</button>
                       </div>
                  </div>
             </div>
        </div>

 <%--Modal Editar usuario --%>
    <div class="modal modalEditUsuario fade" role="dialog" aria-labelledby="tituloDoc" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="edicion" runat="server" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="tituloDoc"></h2>
                        <p>Modifique todos los campos a editar.</p>
					</div>  
					<div class="modal-body">
						<div class="row">
							<div class="form-group">
								<div class="col-lg-6">
									   <div class="form-group">
											<label class="control-label"> Nombre</label>
											<br />
											<asp:TextBox ID="txtNombreEditar" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvNombreEditar" runat="server" CssClass="help-block" ErrorMessage="Ingrese nombre..." Display="Dynamic" ControlToValidate="txtNombreEditar" SetFocusOnError="True" ValidationGroup="editar" />
										</div>	       
										<div class="form-group">
											<label class="control-label"> Rut</label>
											<br />
											<asp:TextBox ID="txtRutUsuarioEditar" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" ></asp:TextBox>
											<asp:CustomValidator ID="cvRutUsuarioEditar" runat="server" CssClass="help-block" ControlToValidate="txtRutUsuarioEditar" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="editar"></asp:CustomValidator>
										</div>
										<div class="form-group">
											<label class="control-label"> Correo</label>
											<br />
											<asp:TextBox ID="txtCorreoEditar" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:CustomValidator ID="cvCorreoEditar" runat="server" CssClass="help-block" ErrorMessage="Ingrese correo válido..." Display="Dynamic" ControlToValidate="txtCorreoEditar" SetFocusOnError="True" ClientValidationFunction="customValidatorCorreoValido" ValidationGroup="editar" />
										</div>
										<div class="form-group">
											<label for="lstEditarRolUsu" class="control-label"> Rol</label>
											<br />
											<asp:ListBox ID="lstEditarRolUsu" runat="server" CssClass="selectpicker" ViewStateMode="Enabled" EnableViewState="true" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
											<asp:RequiredFieldValidator ID="rfvEditarRolUsu" runat="server" CssClass="help-block" ErrorMessage="Ingrese rol..." Display="Dynamic" ControlToValidate="lstEditarRolUsu" SetFocusOnError="True" ValidationGroup="editar" />
                                        </div>
										<div class="form-group">
											<label class="control-label"> Fecha Creación</label>
											<br />
											<asp:TextBox ID="txtFechaC" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<%--<asp:RequiredFieldValidator ID="rfvFechaC" runat="server" CssClass="help-block" ErrorMessage="Ingrese fecha..." Display="Dynamic" ControlToValidate="txtFechaC" ValidationGroup="editar" SetFocusOnError="True" />--%>
										</div>
                                        <div class="form-group">
											<label class="control-label"> Fecha Modificación</label>
											<br />
											<asp:TextBox ID="txtFechaM" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<%--<asp:RequiredFieldValidator ID="rfvFechaM" runat="server" CssClass="help-block" ErrorMessage="Ingrese fecha..." Display="Dynamic" ControlToValidate="txtFechaM" ValidationGroup="editar" SetFocusOnError="True" />--%>
										</div>
										
										<div class="pull-right">
									</div>
									</div>
								</div>
							</div>
						</div>
						</div>
                        <div id="modalfooterE" runat="server" class="modal-footer">
                       	<button id="btnGuardar" runat="server" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Editando documento..." ValidationGroup="editar">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
                        </button>
                        <button id="btnCerrarE" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
  

<%--Abre modal crear usuario--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btncUsuario").click(function () {
            $("#modalUsuario").modal("toggle");
        });
    });
</script>

<%--Inactivar usuario--%>
<script type='text/javascript'>

    function inactivar(id) {

        var rutObject = $("#contenido_gvUsuario_inact_" + id).attr("data-rut");

        $.ajax({
            type: "POST",
            url: "consultar.aspx/SetEditarEstado",
            data: '{rut: "' + rutObject + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                setTimeout(function () {
                    if (response.d.codigo === "0") {

                        document.getElementById('<%=btnBuscar.ClientID%>').click();
                    }
                    else {
                        getMessage(response.d.mensaje, response.d.codigo);
                    }

                }, 4000);
            },
            error: function (response) {
                setTimeout(function () {
                    getMessage("No tiene Permisos para realizar esta acción", "2");
                }, 1000);
            }
        });

    }
</script>


<%--Abre modal editar usuario--%>
    <script type="text/javascript">
        $(document).ready(function () {

            //carga modal al abrirlo
            $('.modalEditUsuario').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var nombre = button.data('nombre');
                var correo = button.data('correo');
                var rol = button.data('rol');
                var rut = button.data('rut');
                var fechac = button.data('fechac');
                var fecham = button.data('fecham');
                var modal = $(this);

                //establece título
                $('#tituloDoc').text('Editar usuario: ' + nombre);

                ////bloquea formulario

                $('[id*=txtRutUsuarioEditar]').prop('disabled', true);
                $('[id*=txtFechaC]').prop('disabled', true);
                $('[id*=txtFechaM]').prop('disabled', true);

                //almacena valores antes de editar el formulario
                $(<%= hfRowId.ClientID %>).val(button.context.id)
                $(<%= hfRut.ClientID %>).val(rut);

                //asigna valores al formulario
                modal.find('#<%= txtRutUsuarioEditar.ClientID%>').val(rut);
                textboxRutFormat(<%= txtRutUsuarioEditar.ClientID %>); <%-- le da formato a rut en el textbox --%>
                modal.find('#<%= txtNombreEditar.ClientID%>').val(nombre);
                modal.find('#<%= txtCorreoEditar.ClientID%>').val(correo);
                modal.find('#<%= txtFechaC.ClientID%>').val(fechac);
                modal.find('#<%= txtFechaM.ClientID%>').val(fecham);
                $('#<%= lstEditarRolUsu.ClientID %>').selectpicker('val', rol);

            });

            $('.modalMessage').on('hidden.bs.modal', function (e) {
                var accion = $(<%= hfModalAccion.ClientID %>).val();

                if (accion !== "") {
                    var rowAfectada = $("#" + $(<%= hfRowAfectada.ClientID %>).val());


                    if (accion == "Editar") {
                        $(rowAfectada).addClass("bg-info").fadeOut(200).fadeIn(1000);

                        //datos nuevos de la edición

                        var nombre = $("[id*=txtNombreEditar]").val();
                        var correo = $("[id*=txtCorreoEditar]").val();
                        var rol = $("[id*=lstEditarRolUsu]").selectpicker('val');

                        $(rowAfectada).find('td:eq(0)').text(nombre); <%--Fecha emisión--%>
                        $(rowAfectada).find('td:eq(3)').text(correo); <%--Monto--%>
                        $(rowAfectada).find('td:eq(2)').text($("[id*=lstEditarRolUsu]").find("option:selected").text()); <%--Estado--%>
                    }

                    $(<%= hfModalAccion.ClientID %>).val("");
                }
            });


            //función actualiza documento y fila de gridview
            $('#<%= btnGuardar.ClientID %>').on('click', function () {
                var $this = $(this);
                var validationGroup = $this.attr("validationgroup");
                if (Page_ClientValidate(validationGroup)) {
                    $this.button('loading');
                    //lógica
                    $.ajax({
                        type: "POST",
                        url: "consultar.aspx/SetEditarUsuario",
                        data: "{'rut': '" + $(<%= hfRut.ClientID %>).val() + "', 'nombre': '" + $("#<%=txtNombreEditar.ClientID%>").val() + "', 'correo': '" + $("#<%=txtCorreoEditar.ClientID%>").val() + "', 'rol': '" + $("#<%=lstEditarRolUsu.ClientID%>").selectpicker('val') + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            setTimeout(function () {
                                $this.button('reset');
                                $('.modalEditUsuario').modal('hide');

                                if (response.d.codigo === "0") {
                                    <%--var idLink = $("#<%= hfRowId.ClientID %>").val();
                                    $(<%= hfRowAfectada.ClientID %>).val(idLink);
                                    $(<%= hfModalAccion.ClientID %>).val("Editar");--%>
                                    document.getElementById('<%=btnBuscar.ClientID%>').click();
                                }
                                else {
                                    getMessage(response.d.mensaje, response.d.codigo);
                                }

                            }, 4000);
                        },
                        error: function (response) {
                            setTimeout(function () {

                                $this.button('reset');
                                //getMessage(response.responseText, "2");
                                getMessage("No tiene Permisos para realizar esta acción", "2");
                            }, 1000);
                        }
                    });

                    //evita postback
                    return false;
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTable.ext.order['dom-text-numeric'] = function (settings, col) {
                return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
                    var value = $('a, span', td).text().replace(/\./g, '') * 1
                    return value;
                });
            }

            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-euro-pre": function (a) {
                    var x;

                    if ($.trim(a) !== '') {
                        var frDatea = $.trim(a).split(' ');
                        var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00, 00, 00];
                        var frDatea2 = frDatea[0].split('-');
                        x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + ((undefined != frTimea[2]) ? frTimea[2] : 0)) * 1;
                    }
                    else {
                        x = Infinity;
                    }
                    return x;
                },
                "date-euro-asc": function (a, b) {
                    return a - b;
                },
                "date-euro-desc": function (a, b) {
                    return b - a;
                }
            });

            //datatable
            $('#<%=gvUsuario.ClientID%>').DataTable({
                searching: false,
                paging: false,
                info: false,
                "columns": [
                    null,
                    { "orderDataType": "dom-text-numeric" },
                    null,
                    { "orderDataType": "dom-text-numeric" },
                    null,
                    null,
                    null,
                ],
                aoColumnDefs: [
                    { aTargets: [0], sType: "date-euro" },
                    { aTargets: [1], sType: "currency" },
                    { aTargets: [3], sType: "currency" },
                    { aTargets: [4], sType: "date-euro" },
                    { aTargets: [5], sType: "date-euro" }
                ]
            });
        });
    </script>

    <asp:HiddenField id="hfRowId" runat="server"/>
    <asp:HiddenField id="hfRut" runat="server"/> 
    <asp:HiddenField id="hfModalAccion" runat="server"/> 
    <asp:HiddenField id="hfRowAfectada" runat="server"/>
<script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
<script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>

 </asp:Content>