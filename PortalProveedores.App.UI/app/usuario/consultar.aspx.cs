﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;
using System.Web.Services;
using System.Net;
using PortalProveedores.App.UI.app;



namespace PortalProveedores.App.UI.app.usuario
{
    public partial class consultar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //GetRoles();
                GetRol();
                //GetRoledit();
            }
        }

        private void GetRol()

        {

            List<Entities.UsuarioEntity.Rol> lst = UsuarioBL.GetListRol();

            //Bindea list del formulario de búsqueda
            lstRol.DataValueField = "id";
            lstRol.DataValueField = "nombre";
            lstRol.DataSource = lst;
            lstRol.DataBind();
            lstRol.Items.Insert(0, "");

            //Modificado por #Gloria Molina 12-01-2018
            lstEditarRolUsu.DataValueField = "id";
            lstEditarRolUsu.DataValueField = "nombre";
            lstEditarRolUsu.DataSource = lst;
            lstEditarRolUsu.DataBind();

            lstRoles.DataValueField = "id";
            lstRoles.DataValueField = "nombre";
            lstRoles.DataSource = lst;
            lstRoles.DataBind();
            //lstRol.SelectedIndex = 0;
        }

        //Modificado por GloriaMolina
        //private void GetRoledit()
        //{

        //    List<Entities.UsuarioEntity.Rol> lst = UsuarioBL.GetListRol();

        //    //Bindea list del formulario de búsqueda
        //    lstEditarRolUsu.DataValueField = "id";
        //    lstEditarRolUsu.DataValueField = "nombre";
        //    lstEditarRolUsu.DataSource = lst;
        //    lstEditarRolUsu.DataBind();
        //}

        //public void GetRoles()

        //{
        //    List<Entities.UsuarioEntity.Rol> lst = UsuarioBL.GetListRol();

        //    //Bindea list del formulario de búsqueda
        //    lstRoles.DataValueField = "id";
        //    lstRoles.DataValueField = "nombre";
        //    lstRoles.DataSource = lst;
        //    lstRoles.DataBind();
        //}


        private void BindGridView(string rut)
        {
            rut = filtroRut.Visible ? rut : string.Empty;
            string rol = lstRol.SelectedItem != null ? lstRol.SelectedItem.Value : string.Empty;
            string fechai = filtroFechaDesde.Visible ? txtFechaDesde.Text.Trim() : string.Empty;
            string fechaf = filtroFechaHasta.Visible ? txtFechaHasta.Text.Trim() : string.Empty;
            string accion = "1";

            ViewState["r"] = rut;
            ViewState["ro"] = rol;
            ViewState["fi"] = fechai;
            ViewState["ff"] = fechaf;

            string rutm = App_Code.Seguridad.GetRutUsuarioLogueado();
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            gvUsuario.DataKeyNames = new string[1] { "rut" };
            gvUsuario.DataSource = UsuarioBL.GetUsuario(rut, rol, accion, fechai, fechaf, rutm, ref respWS);
            gvUsuario.DataBind();

            lblInfo.Visible = true;

            if (gvUsuario.Rows.Count > 0)
            {
                gvUsuario.UseAccessibleHeader = true;
                gvUsuario.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvUsuario.Visible = true;

                string contador = string.Empty;
                if (gvUsuario.Rows.Count == 1)
                    contador = "encontró <strong>1 usuario</strong>";
                else
                    contador = string.Format("encontraron <strong>{0} usuarios</strong>", gvUsuario.Rows.Count);

                lblInfo.Text = string.Format("Se {0} según parámetros ingresados.", contador);
                lblInfo.CssClass = "alert-info";

            }
            else
            {
                lblInfo.Text = respWS.mensaje;
                lblInfo.CssClass = "alert-danger";
                gvUsuario.Visible = false;
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            string rut = TextRutUsuarioc.Text.Trim();

            if (!string.IsNullOrEmpty(rut))
            {
                if (App_Code.Util.RutValido(rut))
                {
                    BindGridView(rut);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj01", string.Format("getMessage('{0}', '{1}')", "Ingrese un rut válido.", "1"), true);
                    gvUsuario.Visible = false;
                    lblInfo.Visible = false;
                }
            }
            else
            {
                BindGridView(string.Empty);
            }
        }

        protected void gvUsuario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.UsuarioEntity Usu = (Entities.UsuarioEntity)r.DataItem;
            }
        }


        protected void btncrearUsuario_ServerClick(object sender, EventArgs e)
        {
            string rut = txtrutUsuario.Text.Trim();
            rut = rut.Replace(".", string.Empty);

            if (App_Code.Util.RutValido(rut))
            {
                //crea los caracteres númericos de la contraseña
                Random aleatorio = new Random();
                string nume = "";
                string[] vals = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                for (int i = 0; i <= 3; i++)
                {
                    nume = nume + vals[aleatorio.Next(vals.GetUpperBound(0) + 1)];
                }
                //crea los caracteres alfabéticos de la contraseña
                Random aleatorio2 = new Random();
                string letr = "";
                string[] vals2 = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                for (int i = 0; i <= 1; i++)
                {
                    letr = letr + vals2[aleatorio2.Next(vals2.GetUpperBound(0) + 1)];
                }

                string rutc = rut.Substring(0, 4);
                string clave = letr + rutc + nume;
                string nombre = txtnombreUsuario.Text.Trim();
                string rol = lstRoles.SelectedItem.Value;
                string correo = txtCorreo.Text.Trim();
                string accion = "2";

                string usuario = App_Code.Seguridad.GetRutUsuarioLogueado();
                Entities.ResponseWSEntity resp = UsuarioBL.AddUsuario(rut, nombre, correo, clave, rol, accion);

                //limpia los datos del modal luego de crear el usuario
                if (resp.codigo == "0")
                {
                    txtnombreUsuario.Text = string.Empty;
                    txtrutUsuario.Text = string.Empty;
                    txtCorreo.Text = string.Empty;
                    lstRoles.ClearSelection();
                    BindGridView(string.Empty);
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj2", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
                }
                //limpia los datos del modal si la creación no fue exitosa
                else if (resp.codigo == "3")
                {
                    txtnombreUsuario.Text = string.Empty;
                    txtrutUsuario.Text = string.Empty;
                    txtCorreo.Text = string.Empty;
                    lstRoles.ClearSelection();
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj3", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);

                }
                else if (resp.codigo == null)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj4", "getMessage('No se pudo guardar, los campos son incorrectos', '1')", true);
                }
            }
        }

        [WebMethod]
        public static Entities.ResponseWSEntity SetEditarUsuario(string nombre, string rut, string rol, string correo)
        {
            string accion = "4";
            string usuModf = App_Code.Seguridad.GetRutUsuarioLogueado();
            return UsuarioBL.SetEditarUsuario(nombre, rut, rol, correo, usuModf, accion);
        }

        [WebMethod]
        public static Entities.ResponseWSEntity SetEditarEstado(string rut)
        {
            string accion = "3";
            //string rutUsuarioMod = App_Code.Seguridad.GetRutUsuarioLogueado();
            return UsuarioBL.SetEditarEstado(rut, accion);
        }
    }
}