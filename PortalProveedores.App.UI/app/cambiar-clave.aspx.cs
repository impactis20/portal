﻿using System;
using System.Text;
using System.Web.UI;
using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app
{
    public partial class cambiar_clave : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if(!IsPostBack)
            //{
                
            //}
        }

        protected void btnAgregar_ServerClick(object sender, EventArgs e)
        {
            string claveActual = txtClave.Text;
            string claveNueva = txtNuevoClave.Text;
            string claveNuevaRepetida = txtRepiteNuevoClave.Text;

            if(!string.IsNullOrEmpty(claveActual) && !string.IsNullOrEmpty(claveNueva) && !string.IsNullOrEmpty(claveNuevaRepetida))
            {
                string usuario = App_Code.Seguridad.GetRutUsuarioLogueado();
                Entities.ResponseWSEntity resp = UsuarioBL.SetClave(usuario, claveActual, claveNueva, claveNuevaRepetida);

                if (resp.codigo == "0")
                {
                    StringBuilder parametrosUrl = new StringBuilder();
                    parametrosUrl.AppendFormat("t={0}&m={1}", App_Code.Util.Encriptar(DateTime.Now.AddSeconds(20).ToString()), App_Code.Util.Encriptar(resp.mensaje));
                    App_Code.Seguridad.CerrarSesion(parametrosUrl);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj5", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
                }
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "msj5", string.Format("getMessage('{0}', '{1}')", "1", "Complete los campos del formulario para cambiar su clave."), true);
        }
    }
}