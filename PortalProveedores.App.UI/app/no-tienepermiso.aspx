﻿<%@ Page Title="Solicitud no autorizada" Language="C#" AutoEventWireup="true" MasterPageFile="~/app/entornoMenu.Master" CodeBehind="no-tienepermiso.aspx.cs" Inherits="PortalProveedores.App.UI.app.no_tienepermiso" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <h2 class="page-header">No autorizado... <i class="fa fa-frown-o" aria-hidden="true"></i></h2>
    
    <p>Usted no tiene permisos para realizar esta solicitud. Para mayor información contacte a su administrador.</p>
    <small id="error" runat="server"><strong>Descripción: </strong></small>
    <%--<asp:Button id="btnGoHome" runat="server" Text="Volver al inicio..." CssClass="btn btn-primary" OnClick="btnGoHome_Click"/>--%>
    <div class="row">
        <asp:Button ID="btnInicio" runat="server" CssClass="btn btn-link btn-primary" Text="Volver al inicio..." OnClick="btnInicio_Click" />
    </div>
</asp:Content>
