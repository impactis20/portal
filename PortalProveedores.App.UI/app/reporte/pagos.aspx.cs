﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app.reporte
{
    public partial class pagos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindSociedades();
        }

        private void BindSociedades()
        {
            Entities.SociedadEntity sociedad = SociedadBL.GetSociedad("9000");
            ListItem Item = new ListItem(sociedad.nombre, sociedad.id);
            Item.Selected = true;
            lstSociedades.Items.Add(Item);
            lstSociedades.DataBind();
        }

        private void BindGridView(string rut)
        {
            string nroDoc = txtNroDocumento.Text.Trim();

            ViewState["r"] = rut;
            ViewState["nd"] = nroDoc;

            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            gvPago.DataSource = DocumentoBL.GetPago(lstSociedades.Items[0].Value, rut, nroDoc, ref respWS);
            gvPago.DataBind();

            lblInfo.Visible = true;

            if (gvPago.Rows.Count > 0)
            {
                gvPago.UseAccessibleHeader = true;
                gvPago.HeaderRow.TableSection = TableRowSection.TableHeader;
                btnExportarDataExcel.Visible = true;
                gvPago.Visible = true;

                string contador = string.Empty;
                if (gvPago.Rows.Count == 1)
                    contador = "encontró <strong>1 documento</strong>";
                else
                    contador = string.Format("encontraron <strong>{0} documentos</strong>", gvPago.Rows.Count);

                lblInfo.Text = string.Format("Se {0} según parámetros ingresados.", contador);
                lblInfo.CssClass = "alert-info";
            }
            else
            {
                lblInfo.Text = respWS.mensaje;
                lblInfo.CssClass = "alert-danger";
                btnExportarDataExcel.Visible = false;
                gvPago.Visible = false;
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            string rut = txtRutProveedor.Text.Trim();

            if (!string.IsNullOrEmpty(rut))
            {
                if (App_Code.Util.RutValido(rut))
                {
                    BindGridView(rut);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj109", string.Format("getMessage('{0}', '{1}')", "Ingrese un rut válido.", "1"), true);
                    gvPago.Visible = false;
                    lblInfo.Visible = false;
                }
            }
            else
            {
                BindGridView(string.Empty);
            }

            BindSociedades();
        }

        protected void btnExportarDataExcel_ServerClick(object sender, EventArgs e)
        {
            string idSociedad = lstSociedades.Items[0].Value;
            string rut = ViewState["r"].ToString();
            string nroDoc = ViewState["nd"].ToString();

            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.DocumentoEntity> lstData = DocumentoBL.GetPago(lstSociedades.Items[0].Value, rut, nroDoc, ref respWS);
            gvPago.DataSource = lstData;
            gvPago.DataBind();

            if(respWS.codigo == "0")
            {
                try
                {
                    ExportToExcel(lstData);
                }
                catch
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 422</strong> No se pudo generar el archivo excel debido a un error interno.", "2"), true);

                    lblInfo.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 421</strong> No se pudo descargar el archivo excel debido a que no se pudieron obtener los documentos.", "2"), true);

                lblInfo.Visible = false;
            }
        }

        private void ExportToExcel(List<Entities.DocumentoEntity> lstData)
        {
            string filename = "pago_portalproveedores_colmena_" + DateTime.Now.ToShortDateString();

            DataTable dt = new DataTable();
            dt.Columns.Add("Factura", typeof(string));
            dt.Columns.Add("Fecha Documento", typeof(DateTime));
            dt.Columns.Add("Clase Documento", typeof(string));
            dt.Columns.Add("Denominacióm.", typeof(string));
            dt.Columns.Add("Importe Documento.", typeof(double));
            dt.Columns.Add("N° Doc. Pago", typeof(string));
            dt.Columns.Add("Bco. Prop.", typeof(string));
            dt.Columns.Add("Fecha Pago Documento", typeof(DateTime));
            dt.Columns.Add("Importe Pag.", typeof(double));
            dt.Columns.Add("Estado", typeof(string));
            dt.Columns.Add("Fecha Cobro", typeof(DateTime));

            foreach (Entities.DocumentoEntity doc in lstData)
            {
                DataRow r = dt.NewRow();
                r[0] = doc.nroDocumento;

                if(!string.IsNullOrEmpty(doc.fechaEmision))
                    r[1] = Convert.ToDateTime(doc.fechaEmision).ToShortDateString();

                r[2] = doc.tipoDocumento.descripcion;
                r[3] = doc.tipoDocumento.id;

                if (!string.IsNullOrEmpty(doc.monto))
                    r[4] = Convert.ToDouble(doc.monto);

                r[5] = doc.nroDocumentoPago;
                r[6] = doc.banco;
                r[7] = doc.nroCheque;

                if (!string.IsNullOrEmpty(doc.fechaPago))
                    r[8] = Convert.ToDateTime(doc.fechaPago).ToShortDateString();
                
                if (!string.IsNullOrEmpty(doc.monto2))
                    r[9] = Convert.ToDouble(doc.monto2);

                r[10] = doc.status.descripcion;

                if (!string.IsNullOrEmpty(doc.fechaCobro))
                    r[11] = Convert.ToDateTime(doc.fechaCobro).ToShortDateString();
                dt.Rows.Add(r);
            }

            DataGrid dg = new DataGrid();
            dg.AllowPaging = false;
            dg.UseAccessibleHeader = true;
            dg.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#249aef");
            dg.HeaderStyle.Font.Bold = true;
            dg.DataSource = dt;
            dg.DataBind();

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Default;
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ".xls");

            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(stringWriter);
            dg.RenderControl(HtmlTextWriter);
            HttpContext.Current.Response.Write(stringWriter.ToString());
            HttpContext.Current.Response.End();
        }
    }
}