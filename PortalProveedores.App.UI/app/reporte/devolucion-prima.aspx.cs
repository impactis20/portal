﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app.reporte
{
    public partial class devolucion_prima : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSociedades();
                BindFiltros();
                BindEstados();

                lblFechaDesdeR.Text = "Fecha R(desde)";
                lblFechaDesdeV.Text = "Fecha V(desde)";
            

            }
        }

        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = new List<Entities.SociedadEntity>();
            lstData.Add(SociedadBL.GetSociedad("9000"));

            //Bindea list del formulario de búsqueda
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";
            lstSociedades.DataSource = lstData;
            lstSociedades.DataBind();

            lstSociedades.Items[0].Selected = true;
        }

        private void BindEstados()
        {
            lbEstadoDoc.Items.Insert(0, "");
            lbEstadoDoc.Items.Insert(1, "GIRADO");
            lbEstadoDoc.Items.Insert(2, "RETIRADO");
            lbEstadoDoc.Items.Insert(3, "VENCIDO");
            lbEstadoDoc.Items.Insert(3, "INGRESADO");

        }

        private void BindGridView()
        {
            Ingrese.Visible = false;
            txtFechaRecepcionD.BorderColor = System.Drawing.Color.LightGray;
            lblFechaDesdeR.ForeColor = System.Drawing.Color.Black;
            txtFechaDvencimiento.BorderColor = System.Drawing.Color.LightGray;
            lblFechaDesdeV.ForeColor = System.Drawing.Color.Black;

            string idSociedad = lstSociedades.Items[0].Value;
            string año = txtAño.Text;
            string fechaDesdeR = filtroFechaRecepcionD.Visible ? txtFechaRecepcionD.Text : string.Empty;
            string fechaHastaR = filtroFechaRecepcionH.Visible ? txtFechaRecepcionH.Text : string.Empty;
            string fechaDesdeV = filtroFechaVencimientoD.Visible ? txtFechaDvencimiento.Text : string.Empty;
            string fechaHastaV = filtroFechaVencimientoH.Visible ? txtFechaHvencimiento.Text : string.Empty;
            string rut = filtroRut.Visible ? txtRutProveedor.Text : string.Empty;
            string estado = filtroEstadoDoc.Visible ? lbEstadoDoc.Text : string.Empty;

            if (fechaHastaR != "" & fechaDesdeR == "" || fechaHastaV != "" & fechaDesdeV == "")
            {
                if (fechaHastaR != "" & fechaDesdeR == "")
                {
                    Ingrese.Visible = true;
                    txtFechaRecepcionD.BorderColor = System.Drawing.Color.Red;
                    lblFechaDesdeR.Text = "Fecha R(desde)";
                    lblFechaDesdeR.ForeColor = System.Drawing.Color.Red;
                    Ingrese.Text = "Por favor ingrese la fecha de desde";
                    Ingrese.ForeColor = System.Drawing.Color.Red;
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj219", "cargarElementoFiltros();", true);
                }
                if (fechaHastaV != "" & fechaDesdeV == "")
                {
                    Ingrese.Visible = true;
                    txtFechaDvencimiento.BorderColor = System.Drawing.Color.Red;
                    lblFechaDesdeV.Text = "Fecha V(desde)";
                    lblFechaDesdeV.ForeColor = System.Drawing.Color.Red;
                    Ingrese.Text = "Por favor ingrese la fecha de desde";
                    Ingrese.ForeColor = System.Drawing.Color.Red;
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj219", "cargarElementoFiltros();", true);
                }
            }
            else
            {
                if (fechaHastaR == "" & fechaDesdeR != "" || fechaHastaV == "" & fechaDesdeV != "")
                {
                    if (fechaHastaR == "" & fechaDesdeR != "")
                    {
                        fechaHastaR = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                    if (fechaHastaV == "" & fechaDesdeV != "")
                    {
                        fechaHastaV = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                }

                ViewState.Add("fd", fechaDesdeR);
                ViewState.Add("fh", fechaHastaR);
                ViewState.Add("fdv", fechaDesdeV);
                ViewState.Add("fhv", fechaHastaV);
                ViewState.Add("rt", rut);
                ViewState.Add("ed", estado);
                ViewState.Add("añ", año);

                Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();

                gvPrima.DataSource = PrimaBL.GetPrimas(idSociedad, año, fechaDesdeR, fechaHastaR, fechaDesdeV, fechaHastaV, rut, estado, ref respWS);
                gvPrima.DataBind();

                lblInfo.Visible = true;

                if (gvPrima.Rows.Count > 0)
                {
                    gvPrima.UseAccessibleHeader = true;
                    gvPrima.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvPrima.Visible = true;
                    btnExportarDataExcel.Visible = true;

                    string contador = string.Empty;

                    if (gvPrima.Rows.Count == 1)
                        contador = "encontró <strong>1 documento</strong>";
                    else
                        contador = string.Format("encontraron <strong>{0} documentos</strong>", gvPrima.Rows.Count);

                    lblInfo.Text = string.Format("Se encontraron {0} documentos según parámetros ingresados.", contador);
                    lblInfo.CssClass = "alert-info";
                }
                else
                {
                    lblInfo.Text = respWS.mensaje;
                    lblInfo.CssClass = "alert-danger";
                    gvPrima.Visible = false;
                    btnExportarDataExcel.Visible = false;
                }
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            BindGridView();
            BindSociedades();

        }

        protected void btnExportarDataExcel_ServerClick(object sender, EventArgs e)
        {
            string idSociedad = lstSociedades.Items[0].Value;
            string fechaDesdeR = ViewState["fd"].ToString();
            string fechaHastaR = ViewState["fh"].ToString();
            string fechaDesdeV = ViewState["fdv"].ToString();
            string fechaHastaV = ViewState["fhv"].ToString();
            string rut = ViewState["rt"].ToString();
            string estado = ViewState["ed"].ToString();
            string año = ViewState["añ"].ToString();

            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.PrimaEntity> lstData = PrimaBL.GetPrimas(idSociedad, año, fechaDesdeR, fechaHastaR, fechaDesdeV, fechaHastaV, rut, estado, ref respWS);
            gvPrima.DataSource = lstData;
            gvPrima.DataBind();

            if (respWS.codigo == "0")
            {
                try
                {
                    ExportToExcel(lstData);
                }
                catch
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 622</strong> No se pudo generar el archivo excel debido a un error interno.", "2"), true);
                    gvPrima.Visible = false;
                    lblInfo.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 621</strong> No se pudo descargar el archivo excel debido a que no se pudieron obtener los documentos.", "2"), true);
                gvPrima.Visible = false;
                lblInfo.Visible = false;
            }
        }

        private void ExportToExcel(List<Entities.PrimaEntity> lstData)
        {
            string filename = "devolución_prima_portalproveedores_colmena_" + DateTime.Now.ToShortDateString();

            DataTable dt = new DataTable();
            dt.Columns.Add("N° Documento", typeof(string));
            dt.Columns.Add("Empresa", typeof(string));
            dt.Columns.Add("Monto", typeof(double));
            dt.Columns.Add("Estado", typeof(string));
            dt.Columns.Add("Rut", typeof(string));
            dt.Columns.Add("Fecha de Recepción", typeof(string));
            dt.Columns.Add("Fecha de Vencimiento", typeof(string));


            foreach (Entities.PrimaEntity prima in lstData)
            {
                DataRow r = dt.NewRow();
                r[0] = prima.nroDocumento;
                r[1] = prima.empresa;

                if (!string.IsNullOrEmpty(prima.monto))
                    r[2] = Convert.ToDouble(prima.monto);

                r[3] = prima.estado;
                r[4] = prima.rut;
                r[5] = prima.fechaR;
                r[6] = prima.fechaV;

                dt.Rows.Add(r);
            }

            dt.AcceptChanges();

            DataGrid dg = new DataGrid();
            dg.AllowPaging = false;
            dg.UseAccessibleHeader = true;
            dg.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#249aef");
            dg.HeaderStyle.Font.Bold = true;
            dg.DataSource = dt;
            dg.DataBind();

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Default;
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ".xls");

            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(stringWriter);
            dg.RenderControl(HtmlTextWriter);
            HttpContext.Current.Response.Write(stringWriter.ToString());
            HttpContext.Current.Response.End();
        }

        private void BindFiltros()
        {
            txtAño.Text = DateTime.Now.ToString("yyyy");
            lbFiltro.Items.Add(new ListItem("Fecha de Recepción", "3"));
            lbFiltro.Items.Add(new ListItem("Rut", "4"));
            lbFiltro.Items.Add(new ListItem("Estado del Documento", "5"));
            lbFiltro.Items.Add(new ListItem("Fecha de Vencimiento", "6"));
            lbFiltro.DataBind();
        }

        protected void lbFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in lbFiltro.Items)
            {
                if (item.Value == "3")
                {
                    filtroFechaRecepcionD.Visible = item.Selected;
                    filtroFechaRecepcionH.Visible = item.Selected;
                }
                if (item.Value == "4")
                { filtroRut.Visible = item.Selected; }
                if (item.Value == "5")
                { filtroEstadoDoc.Visible = item.Selected; }
                if (item.Value == "6")
                {
                    filtroFechaVencimientoD.Visible = item.Selected;
                    filtroFechaVencimientoH.Visible = item.Selected;
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "msj219", "cargarElementoFiltros();", true);
        }
    }
}