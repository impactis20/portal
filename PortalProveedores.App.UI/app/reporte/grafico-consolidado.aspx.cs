﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

using PortalProveedores.App.UI;
namespace PortalProveedores.App.UI.app.reporte
{
    public partial class grafico_consolidado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Chart1.DataSource = GridView1;
            {
                Chart1.Series[0].YValueMembers = Double.Parse(GridView1.Columns[1].ToString()).ToString();
                    
                Chart1.Series[0].YValueMembers = Double.Parse(GridView2.Columns[2].ToString()).ToString();
                Chart1.DataBind();
            }
        }
        private static readonly List<Grafico> proveedores = new List<Grafico>() {
        new Grafico { Rut ="12345678-9", RazonSocial = "Aluminios Ltda", Correo = "contacto@aluminios.cl", Telefono = 22334566, FechaCreacion= "27-11-2017", UltimoAcceso="07-12-2017" },

        new Grafico { Rut ="6356787-6", RazonSocial = "Poco a Poco S.a.", Correo = "contacto@pocoapoco.cl", Telefono = 854345, FechaCreacion= "27-11-2017", UltimoAcceso="08-12-2017" },

        new Grafico { Rut ="15874878-0", RazonSocial = "Minera Cuatro Montes S.A.", Correo = "contacto@cuatromontes.cl", Telefono = 987654433, FechaCreacion= "27-11-2017", UltimoAcceso="15-12-2017" },
         new Grafico { Rut ="8786534-5", RazonSocial = "artecom Ltda.", Correo = "contacto@artecom.cl", Telefono = 765432434, FechaCreacion= "27-11-2017", UltimoAcceso="16-12-2017" },

        new Grafico { Rut ="74854311-0", RazonSocial = "logistics Chile", Correo = "contacto@logistics.cl", Telefono = 87453223, FechaCreacion= "27-11-2017", UltimoAcceso="20-12-2017" },

        new Grafico { Rut ="75777979-3", RazonSocial = "Lider", Correo = "contacto@lider.cl", Telefono = 754534443, FechaCreacion= "27-11-2017", UltimoAcceso="22-12-2017" },
       
        };

        public List<Grafico> ObtenerProveedores() {
            return proveedores;

        }

        private static readonly List<Grafico> proveedores2 = new List<Grafico>() {
       
        new Grafico { Rut ="69324354-7", RazonSocial = "Agricola SPA.", Correo = "contacto@agricola.cl", Telefono = 8987654, FechaCreacion= "27-11-2017", UltimoAcceso="" },

        new Grafico { Rut ="8776563-3", RazonSocial = "Seguro de vida Tres S.A.", Correo = "contacto@vidatres.cl", Telefono = 434354653, FechaCreacion= "27-11-2017", UltimoAcceso="" },

         new Grafico { Rut ="9876543-6", RazonSocial = "vtr.", Correo = "contacto@vtr.cl", Telefono = 98765433, FechaCreacion= "27-11-2017", UltimoAcceso="" },
   
        new Grafico { Rut ="94768432-1", RazonSocial = "Mercedes Venz", Correo = "contacto@lmercedesvenz.cl", Telefono = 98765433, FechaCreacion= "27-11-2017", UltimoAcceso="" },


        };
        public List<Grafico> ObtenerProveedores2()
        {
            return proveedores2;

        }
    }
       
       
}