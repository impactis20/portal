﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PortalProveedores.App.UI.app.reporte;
namespace PortalProveedores.App.UI.app.reporte
{
    public class Grafico
    {
        public string Rut { get; set; }
        public string RazonSocial { get; set; }
        public string Correo { get; set; }
        public int Telefono { get; set; }
        public string FechaCreacion { get; set; }
        public string UltimoAcceso { get; set; }

        public Grafico(string Rut, string RazonSocial, string Correo, int Telefono, string FechaCreacion, string UltimoAcceso )
        {
            this.Rut = Rut;
            this.RazonSocial = RazonSocial;
            this.Correo = Correo;
            this.Telefono = Telefono;
            this.FechaCreacion = FechaCreacion;
            this.UltimoAcceso = UltimoAcceso;


        }
        public Grafico() {

        }
    }
}