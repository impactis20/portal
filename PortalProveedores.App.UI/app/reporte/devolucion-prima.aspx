﻿<%@ Page Title="Devolución primas" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="devolucion-prima.aspx.cs" Inherits="PortalProveedores.App.UI.app.reporte.devolucion_prima" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
  
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>


    <script type="text/javascript">
        $(function () {

            //datatable
            $('#<%=gvPrima.ClientID%>').DataTable({
                searching: false,
                paging: false,
                info: false
            });

            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione una opción...',
                showTick: true
            });

            $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });
        });
    </script>
       <script type="text/javascript">
           $(function () {
               cargarElementoFiltros();

               $.fn.button.Constructor.DEFAULTS = {
                   loadingText: '<i class="fa fa-spin fa-spinner"></i> Chargement…'
               };

               $('[data-toggle="popover"]').popover({
                   html: true
               });

               //Función que cierra popover desde el boton "x"
               $(document).on("click", ".popover .close", function () {
                   $(this).parents(".popover").popover('hide');
               });

               $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });


           });
    </script>
    <script type="text/javascript">
           function cargarElementoFiltros() {
               $('.filtro').selectpicker({
                   title: 'Filtro...',
                   showTick: true,
                   countSelectedText: function (numSelected, numTotal) {
                       return (numSelected == 1) ? "Mostrando {0} filtro" : "Mostrando {0} filtros";
                   },
                   style: 'btn-default btn-xs'
               });
               $('.selectpicker').selectpicker({
                   title: 'Seleccione...',
                   showTick: true
               });
           }
    </script>

     
    

    <h2 class="page-header">Devolución primas</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <p>Para realizar una búsqueda debe rellenar los campos con asterisco. 
                        <asp:ListBox ID="lbFiltro" runat="server" CssClass="filtro" AutoPostBack="true" OnSelectedIndexChanged="lbFiltro_SelectedIndexChanged" data-selected-text-format="count > 0" data-width="135px" SelectionMode="Multiple">
                            <asp:ListItem Value="1" Selected="True">Sociedad</asp:ListItem>
                            <asp:ListItem Value="1" Selected="True">Año</asp:ListItem>
                        </asp:ListBox>
                    </p>
                    <div class="col-lg-2">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <label class="control-label">Sociedad</label>
                                <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2" >
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label class="control-label">Año</label>
                                        <asp:TextBox ID="txtAño" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                         <div id="filtroFechaRecepcionD" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="txtFechaRecepcionD" class="control-label"><asp:Label runat="server" ID="lblFechaDesdeR" /></label>
                                        <br />
                                        <div class="input-group date input-group fechafiltro" id="calendarioFechaDesde">
                                            <asp:TextBox ID="txtFechaRecepcionD" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"  ></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div id="filtroFechaRecepcionH" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="txtFechaRecepcionH" class="control-label">Fecha R(hasta)</label>
                                        <br />
                                        <div class="input-group date input-group fechafiltro" id="calendarioFechaHasta">
                                            <asp:TextBox ID="txtFechaRecepcionH" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off" ></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" ></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div id="filtroRut" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <%--<label class="sr-only">Ingrese rut de proveedor a consultar</label>--%>
                                        <label for="txtRutProveedor" class="control-label">Rut Proveedor</label>
                                        <br />
                                        <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" placeholder="Ingrese..."></asp:TextBox>
                                        <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true" ClientValidationFunction="customValidatorRut" ValidationGroup="buscar"></asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvRutProveedor" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." ControlToValidate="txtRutProveedor" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>

                      <div id="filtroEstadoDoc" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <label for="lbEstadoDoc" class="control-label">Estado del Documento</label>
                                        <br />
                                        <asp:ListBox ID="lbEstadoDoc" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                                    </div>
                                </div>
                            </div>

                          <div id="filtroFechaVencimientoD" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="txtFechaDvencimiento" class="control-label"><asp:Label runat="server" ID="lblFechaDesdeV" /></label>
                                        <br />
                                        <div class="input-group date input-group fechafiltro" id="calendarioFechaDesdeV">
                                            <asp:TextBox ID="txtFechaDvencimiento" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaVencimientoH" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="txtFechaHvencimiento" class="control-label">Fecha V(hasta)</label>
                                        <br />
                                        <div class="input-group date input-group fechafiltro" id="calendarioFechaHastaV">
                                            <asp:TextBox ID="txtFechaHvencimiento" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                   

                    <div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <button id="btnBuscar" runat="server" type="button" class="btn btn-primary btn-sm" onserverclick="btnBuscar_ServerClick" style="margin-bottom: 15px;" validationgroup="buscar">
                                <i class="fa fa-search"></i>
                                &nbsp;&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline" id="lblLabel" runat="server">
                    <p><asp:Label runat="server" ID="Ingrese" /></p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
      <div class="col-md-3 text-right">
          <button id="btnExportarDataExcel" runat="server" type="button" class="btn btn-success btn-xs" onserverclick="btnExportarDataExcel_ServerClick" visible="false"><i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp;&nbsp;Exportar a excel</button>
      </div>
    </div>
    <div class="table-responsive small">
        <asp:GridView ID="gvPrima" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None">
            <Columns>
                <asp:TemplateField HeaderText="N° Documento">
                    <ItemTemplate>
                        <%# Eval("nroDocumento") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Empresa">
                    <ItemTemplate>
                        <%# Eval("empresa") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Monto">
                    <ItemTemplate>
                        <%# Eval("monto") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Estado">
                    <ItemTemplate>
                        <%# Eval("estado") %>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Rut">
                    <ItemTemplate>
                        <%# Eval("rut") %>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="FEcha de Recepción">
                    <ItemTemplate>
                        <%# Eval("fechaR") %>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
                <asp:TemplateField HeaderText="Fecha de Vencimiento">
                    <ItemTemplate>
                        <%# Eval("fechaV") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
         <script >
             (function ($) {
                 $('#calendarioFechaDesde').datetimepicker({
                     useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                     format: 'DD-MM-YYYY',
                     maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                     //minDate: moment().add(-3, 'month'),
                     showClear: true,
                     ignoreReadonly: false
                 });

                 $('#calendarioFechaHasta').datetimepicker({
                     useCurrent: false, //Important! See issue #1075
                     format: 'DD-MM-YYYY',
                     maxDate: moment().add(1, 'days'),
                     showClear: true,
                     ignoreReadonly: false
                 });

                 $('#calendarioFechaDesde').on("dp.change", function (e) {
                     $('#calendarioFechaHasta').data("DateTimePicker").minDate(e.date);
                 });

                 $('#calendarioFechaHasta').on("dp.change", function (e) {
                     $('#calendarioFechaDesde').data("DateTimePicker").maxDate(e.date);
                 });

                 $('#calendarioFechaHastaV').datetimepicker({
                     useCurrent: false, //Important! See issue #1075
                     format: 'DD-MM-YYYY',
                     maxDate: moment().add(2, 'years'),
                     showClear: true,
                     ignoreReadonly: false
                 });

                 $('#calendarioFechaDesdeV').datetimepicker({
                     useCurrent: false, //Important! See issue #1075
                     format: 'DD-MM-YYYY',
                     maxDate: moment().add(2, 'years'),
                     showClear: true,
                     ignoreReadonly: false
                 });
                 $('#calendarioFechaDesdeV').on("dp.change", function (e) {
                     $('#calendarioFechaHastaV').data("DateTimePicker").minDate(e.date);
                 });

                 $('#calendarioFechaHastaV').on("dp.change", function (e) {
                     $('#calendarioFechaDesdeV').data("DateTimePicker").maxDate(e.date);
                 });

                 $('.fechafiltro').datetimepicker({
                     useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                     format: 'DD-MM-YYYY',
                     minDate: moment().add(-2, 'years'),
                     maxDate: moment().add(2, 'years'),
                     showClear: true,
                     ignoreReadonly: false
                 });
             }(jQuery));
    </script>
    </asp:Content>