﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app.solped
{
    public partial class agregar_material : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Verifica si usuario tiene sociedad
                if (string.IsNullOrEmpty(App_Code.Seguridad.GetSociedadUsuarioLogueado()))
                    Server.Transfer("crear.aspx");
            }
        }

        protected void gvMaterial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "agregar")
            {
                Button btnAgregar = (e.CommandSource as Button);
                string id = App_Code.Util.Encriptar(btnAgregar.Attributes["id-material"]);
                string desc = App_Code.Util.Encriptar(btnAgregar.Attributes["descripcion"]);

                string url = string.Format("ingresar-material.aspx?id={0}&desc={1}", id, desc);
                Response.Redirect(url);
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            string prefix = txtMaterial.Text.Trim();

            try
            {
                Entities.ResponseWSEntity resp = new Entities.ResponseWSEntity();
                List<Entities.SolpedEntity> lst = SolpedBL.GetMaterial(prefix, ref resp);
                
                if(resp.codigo != "1")
                {
                    //éxito
                    gvMaterial.DataKeyNames = new string[1] { "id" };
                    gvMaterial.DataSource = lst;
                    gvMaterial.DataBind();

                    if(lst.Count>0)
                    {
                        gvMaterial.UseAccessibleHeader = true;
                        gvMaterial.HeaderRow.TableSection = TableRowSection.TableHeader;
                        string contador = string.Empty;

                        if (lst.Count == 1)
                            contador = "Se encontró <strong>1 material</strong>";
                        else
                            contador = string.Format("Se encontraron <strong>{0} materiales</strong> según los parámetros ingresados.", lst.Count);

                        lblInfo.Text = string.Format("{0}", contador);
                        lblInfo.CssClass = "alert-info";

                        //lblInfo.Text = string.Format("Se encontraron <strong>{0} materiales</strong> según los parámetros ingresados.", lst.Count);
                        //lblInfo.CssClass = "alert-info";
                        gvMaterial.Visible = true;
                    }
                    else
                    {
                        lblInfo.Text = resp.mensaje;
                        lblInfo.CssClass = "alert-info";
                        gvMaterial.Visible = false;
                    }
                }
                else
                {
                    lblInfo.Visible = false;
                    gvMaterial.Visible = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj9", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
                }
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj9", string.Format("getMessage('{0}', '{1}')", "[Error 339] Ocurrió un error interno al intentar buscar los materiales. Si el error persiste repórtelo a su administrador.", "1"), true);
            }
        }

        protected void gvMaterial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.SolpedEntity item = (Entities.SolpedEntity)r.DataItem;
                string prefix = txtMaterial.Text.Trim();
                Label lblID = (r.Controls[0].FindControl("lblID") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Button btnAgregar = (r.Controls[0].FindControl("btnAgregar") as Button);
                
                //match text
                string idMatch = item.id.Contains(prefix) ? item.id.Replace(prefix, string.Format("<mark>{0}</mark>", prefix)) : item.id;
                string descMatch = item.descripcion.Contains(prefix) ? item.descripcion.Replace(prefix, string.Format("<mark>{0}</mark>", prefix)) : item.descripcion;

                lblID.Text = idMatch;
                lblDesc.Text = descMatch;
            }
        }
    }
}