﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="visualizarSolicitudesPendientes.aspx.cs" Inherits="PortalProveedores.App.UI.app.solped.visualizarSolicitudesPendientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>.table th, .table td { 
     border-top: none !important; 
 }
.negra{font-weight:bold;}
    </style>

    <script type="text/javascript">

        $(document).ready(function () {

            $('[id*=calendarioFechaDesde]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                //minDate: moment().add(-3, 'month'),
                showClear: true,
                ignoreReadonly: false
            });

            $('[id*=calendarioFechaHasta]').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'),
                showClear: true,
                ignoreReadonly: false
            });

            $("[id*=calendarioFechaDesde]").on("dp.change", function (e) {
                $('[id*=calendarioFechaHasta]').data("DateTimePicker").minDate(e.date);
            });

            $("[id*=calendarioFechaHasta]").on("dp.change", function (e) {
                $('[id*=calendarioFechaDesde]').data("DateTimePicker").maxDate(e.date);
            });

            $('.fechafiltro').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: moment().add(-2, 'year'),
                showClear: true,
                ignoreReadonly: false
            });

            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });
        });
    </script>

<h2 class="page-header">Historial solicitudes liberadas.</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline" id="lblLabel" runat="server">
                    <p>Estimado <asp:Label runat="server" ID="lblNombreUsuarioLogueado" />, usted puede buscar solicitudes liberadas con los siguientes filtros. </p>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group" id="search" runat="server">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                           

                            <div id="FiltroOC" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <%--<label class="sr-only">Ingrese rut de proveedor a consultar</label>--%>
                                        <label for="txtsolicitudC" class="control-label">Solicitud de Compra </label>
                                        <br />
                                        <asp:TextBox ID="txtsolicitudC" runat="server" CssClass="form-control input-sm" MaxLength="40" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                                        <asp:CustomValidator ID="cvsolicitudC" runat="server" CssClass="help-block" ControlToValidate="txtsolicitudC" ErrorMessage="Ingrese <strong>Orden de Compra</strong>..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true"  ValidationGroup="buscar"></asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvRutProveedor" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." ControlToValidate="txtRutProveedor" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>


                            <div id="filtroFechaDesde" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only">Ingrese fecha desde, para realizar la consultar</label>--%>
                                        <label for="txtFechaDesde" class="control-label">Fecha desde (Emisión)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaDesde">
                                            <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha desde..." Display="Dynamic" ControlToValidate="txtFechaDesde" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaHasta" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only">Ingrese fecha hasta, para realizar la consultar</label>--%>
                                        <label for="txtFechaHasta" class="control-label">Fecha hasta (Emisión)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaHasta">
                                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha hasta..." Display="Dynamic" ControlToValidate="txtFechaHasta" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                      
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <asp:button id="btnBuscar2" runat="server" type="submit" class="btn btn-primary btn-sm" OnClick="btnBuscar2_Click" validationgroup="buscar" Text="Buscar">
                            </asp:button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
   </div>
   <%--Tabla usuarios --%>   
    <div class="table-responsive small">

        <asp:GridView ID="gvSolicitud" AutoGenerateColumns="false" runat="server" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
            <Columns> 
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Detalle."></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkdetail" runat="server" Text="Detalle" OnCommand="lnkdetail_Click" CommandArgument='<%#Eval("ID_PEDIDO")%>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Orden de Comprao.">Solicitud de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("SOLPED") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rut proveedor.">Sociedad</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("NOMBRE_CENTRO") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Orden.">Fecha de Solicitud</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("FECHA_SOLPED")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Entrega.">Fecha de Entrega</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#  Eval("FECHA_ENTREGA") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Entrega.">Items</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#  Eval("ESTADO_LIB") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div runat="server" id="detalle">
        <div class="table-responsive small">
        
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Solicitud de Compra <asp:Label runat="server" id="lblordencompra2" /></h3>
              </div>
           </div>
            <table  class="table table-striped" border="0" style="width:35%;border-color: #FAFAFA;" >
                <tr><td class="negra" style="text-align:left;">Solicitud de Compra:</td><td style="text-align:left;"><asp:Label runat="server" id="lblID_PEDIDO" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Sociedad:</td><td style="text-align:left;"><asp:Label runat="server" id="lblNOMBRE_CENTRO" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Fecha Solicitud:</td><td style="text-align:left;"><asp:Label runat="server" id="lblFECHA_SOLPED" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Fecha de Entrega:</td><td style="text-align:left;"><asp:Label runat="server" id="lblFECHA_ENTREGA" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Observación:</td><td style="text-align:left;"><asp:Label runat="server" id="lblObservacion" /></td></tr>
            </table>
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Detalle</h3>
              </div>
           </div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Posición.">Posición</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("POSICION") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Material.">Material</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("MATERIAL") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción.">Descripción</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("DESCRIPCION") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción centro costo.">Centro Costo</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("CENTRO_COSTE") %> <%# Eval("DESC_CENT_COSTE") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Cantidad.">Cantidad</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("CANTIDAD") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Estatus del usuario.">Unidad Medida</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("UNIDAD_MEDIDA") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        
<div id="divreturnws" class="alert alert-info" role="alert" runat="server" visible="false">
  <asp:Label ID="lblreturnws" runat="server" Text="label"/>
  <asp:Button CssClass="btn btn-default btn-sm" ID="btnContinuar" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Continuar" />
</div>
        <div style="padding-top:20px; width:100%; text-align:right;">
             <asp:Button CssClass="btn btn-default btn-sm" ID="btnVolver" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Cancelar" />
             <asp:HiddenField ID="hfOrdenCompraIdLiberar" Value="" runat="server" />
        </div>
    </div>
<script>
    $(document).ready(function () {
        $('#contenido_gvSolicitud').DataTable({
            searching: false
        });
    });
</script>

    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>

    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>


</asp:Content>
