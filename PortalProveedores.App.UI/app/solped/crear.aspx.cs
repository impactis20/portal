﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app.solped
{
    public partial class crear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Verifica si usuario tiene sociedad
                if (string.IsNullOrEmpty(App_Code.Seguridad.GetSociedadUsuarioLogueado()))
                    Server.Transfer("~/app/no-autorizado.aspx?error=Su usuario no pertenece a una sociedad.");

                BindSociedades();
                BindData();
            }
        }

        //Usuarios sin sociedad no pueden acceder a este módulo


        private void BindData()
        {
            string rutUsuarioActual = App_Code.Seguridad.GetRutUsuarioLogueado();
            List<Entities.SolpedEntity> lstData = Session[rutUsuarioActual] as List<Entities.SolpedEntity>;
            gvMaterial.DataSource = lstData;
            gvMaterial.DataBind();

            if (lstData != null && lstData.Count > 0)
            {
                gvMaterial.UseAccessibleHeader = true;
                gvMaterial.HeaderRow.TableSection = TableRowSection.TableHeader;
                string contador = string.Empty;

                if (lstData.Count == 1)
                    contador = "Usted tiene <strong>1 material</strong>en la lista para ser solicitado.";
                else
                    contador = string.Format("Usted tiene <strong>{0} materiales</strong> en la lista para ser solicitados.", lstData.Count);

                lblInfo.Text = string.Format("{0}", contador);
                lblInfo.CssClass = "alert-info";


                //lblInfo.Text = string.Format("Usted tiene <strong>{0} materiales</strong> en la lista para ser solicitados.", lstData.Count);
                //lblInfo.CssClass = "alert-info";
                btnCrearSolped.Visible = true;
                obs.Visible = true;
            }
            else
            {
                lblInfo.Visible = false;
                btnCrearSolped.Visible = false;
                obs.Visible = false;
            }
        }

        private void BindSociedades()
        {
            Entities.SociedadEntity sociedad = SociedadBL.GetSociedad(App_Code.Seguridad.GetSociedadUsuarioLogueado());
            ListItem Item = new ListItem(sociedad.nombre, sociedad.id);
            Item.Selected = true;
            lstSociedades.Items.Add(Item);
            lstSociedades.DataBind();
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("buscar-material.aspx");
        }

        protected void btnCrearSolped_ServerClick(object sender, EventArgs e)
        {
            string rutUsuarioSolicitante = App_Code.Seguridad.GetRutUsuarioLogueado();
            List<Entities.SolpedEntity> lstData = Session[rutUsuarioSolicitante] as List<Entities.SolpedEntity>;
            string planta = lstSociedades.Items[0].Value;
           string observacion= txtObservacion.Text;
            Entities.ResponseWSEntity resp = SolpedBL.AddSolped(planta, observacion,lstData);
            resp.codigo = "0";
            if (resp.codigo == "0")
            {
                Session[rutUsuarioSolicitante] = null;
                BindData();
            }
           
            ScriptManager.RegisterStartupScript(this, GetType(), "msj5", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
        }

        protected void gvMaterial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Button btnAccion = (e.CommandSource as Button);
            string id = btnAccion.Attributes["id-material"];
            string rutUsuarioSolicitante = App_Code.Seguridad.GetRutUsuarioLogueado();
            List<Entities.SolpedEntity> lstData = Session[rutUsuarioSolicitante] as List<Entities.SolpedEntity>;
            
            if (e.CommandName == "editar")
            {
                Entities.SolpedEntity item = lstData.First(x => x.id == id);

                string url = string.Format("ingresar-material.aspx?a=edit&id={0}&desc={1}&cant={2}&cc={3}&fec={4}", App_Code.Util.Encriptar(item.id), App_Code.Util.Encriptar(item.descripcion), App_Code.Util.Encriptar(item.cantidad), App_Code.Util.Encriptar(item.centroCosto), App_Code.Util.Encriptar(item.fechaEntrega));

                Response.Redirect(url);
            }

            if (e.CommandName == "quitar")
            {
                Session[rutUsuarioSolicitante] = lstData.Where(x => x.id != id).ToList();
                BindData();
            }
        }
    }
}