﻿<%@ Page Title="Búsqueda de materiales" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="buscar-material.aspx.cs" Inherits="PortalProveedores.App.UI.app.solped.agregar_material" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    
    <script type="text/javascript">
        $(function () {
            $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });
        });
    </script>

    <h2 class="page-header">Búsqueda de materiales</h2>
    <div class="form-group">
        <div class="form-inline">
            <p>Realice una búsqueda ingresando el ID o Descripción del Material. <a class="pull-right" href="crear.aspx">Volver a Solped</a></p>
            <div class="col-lg-2">
                <div class="row">
                    <div class="form-group" style="margin-right: 7px">
                        <label class="control-label">* Material</label>
                        <br />
                        <asp:TextBox ID="txtMaterial" runat="server" CssClass="form-control input-sm" placeholder="Ingrese material..." autocomplete="off"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvMaterial" runat="server" CssClass="help-block" ErrorMessage="Ingrese material..." Display="Dynamic" ControlToValidate="txtMaterial" SetFocusOnError="True" ValidationGroup="buscar" />
                    </div>
                </div>
            </div>
            <div class="col-lg-2" style="margin-top:20px">
                <div class="row">
                    <button id="btnBuscar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnBuscar_ServerClick" validationgroup="buscar">
                        <i class="fa fa-search"></i>
                        &nbsp;&nbsp;Buscar
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <asp:Label ID="lblInfo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="table-responsive small">
        <asp:GridView ID="gvMaterial" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
             OnRowCommand="gvMaterial_RowCommand" OnRowDataBound="gvMaterial_RowDataBound">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="ID del material.">Material</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Descripción del material.">Descripción</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("descripcion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-CssClass="text-right">
                    <ItemTemplate>
                        <asp:Button ID="btnAgregar" runat="server" Text="Agregar" CssClass="btn btn-primary btn-xs" CommandName="agregar" CommandArgument='<%# Container.DataItemIndex %>' id-material='<%# Eval("id") %>' descripcion='<%# Eval("descripcion") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
</asp:Content>
