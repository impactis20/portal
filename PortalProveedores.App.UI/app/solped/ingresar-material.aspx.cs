﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalProveedores.App.UI.app.solped
{
    public partial class ingresar_material : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Verifica si usuario tiene sociedad
                if (string.IsNullOrEmpty(App_Code.Seguridad.GetSociedadUsuarioLogueado()))
                    Server.Transfer("crear.aspx");

                txtGrupoCompra.Text = "004";
                txtRutSolicitante.Text = App_Code.Seguridad.GetRutUsuarioLogueado();

                if(Request.QueryString["a"] != null && Request.QueryString["id"] != null && Request.QueryString["desc"] != null && Request.QueryString["cant"] != null && Request.QueryString["cc"] != null && Request.QueryString["fec"] != null)
                {
                    //[edición] recibe material por parámetros
                    titulo.InnerText = "Editar material";
                    string id = App_Code.Util.Desencriptar(Request.QueryString["id"]);
                    string desc = App_Code.Util.Desencriptar(Request.QueryString["desc"]);
                    string cantidad = App_Code.Util.Desencriptar(Request.QueryString["cant"]);
                    string centroCosto = App_Code.Util.Desencriptar(Request.QueryString["cc"]);
                    string fechaEntrega = App_Code.Util.Desencriptar(Request.QueryString["fec"]);

                    txtID.Text = id;
                    txtDescripcion.Text = desc;
                    txtCantidad.Text = cantidad;
                    txtCentroCosto.Text = centroCosto;
                    txtFechaEntrega.Text = fechaEntrega;

                    btnAgregar.Visible = false;
                    btnGuardar.Visible = true;
                }
                else if(Request.QueryString["id"] != null && Request.QueryString["desc"] != null)
                {
                    //[ingreso] recibe material por parámetros
                    titulo.InnerText = "Ingresar material";
                    string id = App_Code.Util.Desencriptar(Request.QueryString["id"]);
                    string desc = App_Code.Util.Desencriptar(Request.QueryString["desc"]);

                    txtID.Text = id;
                    txtDescripcion.Text = desc;

                    btnAgregar.Visible = true;
                    btnGuardar.Visible = false;
                }
                else
                {
                    //error al ingresar a la página
                }
            }
        }

        protected void btnAgregar_ServerClick(object sender, EventArgs e)
        {
            string rutUsuarioActual = App_Code.Seguridad.GetRutUsuarioLogueado();
            string cantidad = txtCantidad.Text.Trim();
            string centroCosto = txtCentroCosto.Text.Trim();
            string fechaEntrega = txtFechaEntrega.Text.Trim();

            List<Entities.SolpedEntity> lstSolped;
            Entities.SolpedEntity material = new Entities.SolpedEntity
            {
                id = txtID.Text,
                descripcion = txtDescripcion.Text,
                grupoCompra = txtGrupoCompra.Text,
                rutSolicitante = rutUsuarioActual,
                cantidad = cantidad,
                centroCosto = centroCosto,
                fechaEntrega = fechaEntrega
            };

            if (Session[rutUsuarioActual] != null)
            {
                lstSolped = Session[rutUsuarioActual] as List<Entities.SolpedEntity>;
                lstSolped.Add(material);

                Session[rutUsuarioActual] = lstSolped;
            }
            else
            {
                lstSolped = new List<Entities.SolpedEntity>();
                lstSolped.Add(material);

                Session.Add(rutUsuarioActual, lstSolped);
            }

            Response.Redirect("crear.aspx");
        }

        protected void btnGuardar_ServerClick(object sender, EventArgs e)
        {
            string rutUsuarioActual = App_Code.Seguridad.GetRutUsuarioLogueado();

            if(Session[rutUsuarioActual] != null)
            {
                List<Entities.SolpedEntity> lstSolped = Session[rutUsuarioActual] as List<Entities.SolpedEntity>;

                string id = txtID.Text;

                foreach(Entities.SolpedEntity item in lstSolped)
                {
                    if(item.id == id)
                    {
                        item.cantidad = txtCantidad.Text.Trim();
                        item.centroCosto = txtCentroCosto.Text.Trim();
                        item.fechaEntrega = txtFechaEntrega.Text.Trim();
                        break;
                    }
                }

                Session[rutUsuarioActual] = lstSolped;

                Response.Redirect("crear.aspx");
            }
            else
            {
                //ocurrió un error...
            }
        }
    }
}