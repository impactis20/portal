﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.Entities;
using PortalProveedores.BL;
using PortalProveedores.DAL.solicitudPedido;

namespace PortalProveedores.App.UI.app.solped
{
    public partial class visualizarSolicitudesPendientes : System.Web.UI.Page
    {
        public List<SolicitudEntity> listSolicitud { get; set; }
        public List<SolicitudEntity> listSolicitud2 = new List<SolicitudEntity>();
        private string nombreuser = App_Code.Seguridad.GetRutUsuarioLogueado();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                lblNombreUsuarioLogueado.Text = App_Code.Seguridad.GetNombreUsuarioLogueado();
                //BindSociedades();
                //buscarOrdenesPendientesPorUsuario();
                detalle.Visible = false;
            }
            else
            {

            }
        }

        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = SociedadBL.GetAllSociedades();

            //Bindea list del formulario de búsqueda
            detalle.Visible = false;
        }
        private void buscarOrdenesPendientesPorUsuario()
        {
            BindGridView();
        }

        private void BindGridView()
        {
            listSolicitud = OrdenBL.GetSolicitud(nombreuser, "L", txtFechaDesde.Text, txtFechaHasta.Text, txtsolicitudC.Text, "1");
            int cuenta;

            for (int i = 0; i < Convert.ToInt32(listSolicitud.Count); i++)
            {
                cuenta = 0;
                for (int j = 0; j < Convert.ToInt32(listSolicitud2.Count); j++)
                {
                    if (listSolicitud[i].SOLPED == listSolicitud2[j].SOLPED)
                    {
                        cuenta++;
                    }
                }
                if (cuenta == 0)
                {
                    listSolicitud2.Add(listSolicitud[i]);
                }
            }
            gvSolicitud.DataSource = listSolicitud2;
            gvSolicitud.DataBind();
            gvSolicitud.Visible = true;

            lblInfo.Visible = true;
            detalle.Visible = false;
            lblLabel.Visible = true;

            if (gvSolicitud.Rows.Count > 0)
            {
                gvSolicitud.UseAccessibleHeader = true;
                gvSolicitud.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvSolicitud.Visible = true;
                string contador = string.Empty;

                if (gvSolicitud.Rows.Count == 1)
                    contador = "encontró <strong>1 solicitud</strong>";
                else
                    contador = string.Format("encontraron <strong>{0} solicitudes</strong>", gvSolicitud.Rows.Count);

                lblInfo.Text = string.Format("Se {0} según los parámetros ingresados.", contador);
                lblInfo.CssClass = "alert-info";
            }
            else
            {
                //lblInfo.Text = respWS.mensaje;
                lblInfo.Visible = false;
                lblInfo.CssClass = "alert-danger";
                //gvorden.Visible = false;
            }
        }

        private void BindGridViewdetalle(int ordencompra)
        {
            listSolicitud = OrdenBL.GetSolicitud(nombreuser, "L", "", "", "", "1");

            List<DetalleSolicitudEntity> listDetalleSolicitud = listSolicitud.ElementAt(ordencompra).detallesolicitud;
            GridView1.DataSource = listDetalleSolicitud;
            GridView1.DataBind();
            SolicitudEntity solicitud = listSolicitud.ElementAt(ordencompra);
            lblID_PEDIDO.Text = solicitud.SOLPED;
            lblNOMBRE_CENTRO.Text = solicitud.NOMBRE_CENTRO;
            lblFECHA_SOLPED.Text = solicitud.FECHA_SOLPED;
            lblFECHA_ENTREGA.Text = solicitud.FECHA_ENTREGA;
            
            


            lblInfo.Visible = false;
            detalle.Visible = true;
            lblLabel.Visible = false;
            hfOrdenCompraIdLiberar.Value = solicitud.ID_PEDIDO;
        }
        public string formatearRut(string rut)
        {
            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }

        protected void lnkdetail_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string yourValue = btn.CommandArgument;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            int index = Convert.ToInt32(row.RowIndex);
            //BindGridViewdetalle(index);
            gvSolicitud.Visible = false;
            search.Visible = false;
            int ordencompra = 0;

            listSolicitud = OrdenBL.GetSolicitudD(nombreuser, "L", "", "", yourValue, "1");

            //List<DetalleSolicitudEntity> listDetalleSolicitud = listSolicitud.ElementAt(ordencompra).detallesolicitud;
            GridView1.DataSource = listSolicitud;
            GridView1.DataBind();
            SolicitudEntity solicitud = listSolicitud.ElementAt(ordencompra);
            lblID_PEDIDO.Text = solicitud.SOLPED;
            lblNOMBRE_CENTRO.Text = solicitud.NOMBRE_CENTRO;
            lblFECHA_SOLPED.Text = solicitud.FECHA_SOLPED;
            lblFECHA_ENTREGA.Text = solicitud.FECHA_ENTREGA;
            lblObservacion.Text = solicitud.COMENTARIO;

            lblInfo.Visible = false;
            detalle.Visible = true;
            lblLabel.Visible = false;
            hfOrdenCompraIdLiberar.Value = solicitud.ID_PEDIDO;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/app/solped/visualizarSolicitudesPendientes.aspx");
        }

        protected void btnBuscar2_Click(object sender, EventArgs e)
        {
            buscarOrdenesPendientesPorUsuario();
        }
    }
}