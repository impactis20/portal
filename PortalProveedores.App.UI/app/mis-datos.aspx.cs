﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app
{
    public partial class mis_datos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDatosUsuario();
            }
        }


        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);

            //disponibiliza controles según rol
            string rol = App_Code.Seguridad.GetRolUsuarioLogueado();
            if (rol.ToUpper() != "P")
                nombre.Visible = true;
        }

        protected void btnActualizar_ServerClick(object sender, EventArgs e)
        {
            HabilitarEdicion(true);
        }

        protected void btnCancelar_ServerClick(object sender, EventArgs e)
        {
            rfvEmailNuevo.Visible = false;
            HabilitarEdicion(false);
            GetDatosUsuario();
            txtEmailNuevo.Text = string.Empty;
        }

        protected void btnGuardar_ServerClick(object sender, EventArgs e)
        {
            string usuario = App_Code.Seguridad.GetRutUsuarioLogueado();
            string clave = App_Code.Seguridad.GetClaveUsuarioLogueado();
            string nombreActual = App_Code.Seguridad.GetNombreUsuarioLogueado();
            string nombreNuevo = txtNombre.Text.Trim();
            string email = App_Code.Seguridad.GetEmailUsuarioLogueado();
            string emailNuevo = txtEmailNuevo.Text.Trim();
            bool edicionValida = true;

            if(!string.IsNullOrEmpty(emailNuevo))
            {
                bool nuevoEmailValido = App_Code.Util.EsEmailValido(emailNuevo);

                if (nuevoEmailValido)
                {
                    //Remueve error textbox email nuevo
                    rfvEmailNuevo.Visible = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj6", string.Format("removerErrorControl('#{0}');", txtEmailNuevo.ClientID), true);
                }
                else
                {
                    //Genera error en textbox email nuevo
                    rfvEmailNuevo.Visible = true;
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj6", string.Format("errorControl('#{0}');", txtEmailNuevo.ClientID), true);

                    edicionValida = false;
                }
            }

            if (edicionValida)
            {
                Entities.ResponseWSEntity resp = UsuarioBL.SetMisDatos(usuario, clave, nombreNuevo, email, emailNuevo);

                if (resp.codigo == "0")
                {
                    //Éxito
                    HabilitarEdicion(false);

                    if (nombreNuevo != nombreActual)
                    {
                        //Modifica nombre de usuario en header
                        HtmlGenericControl nombreUsuario = (Master.FindControl("nombreUsuario") as HtmlGenericControl);
                        nombreUsuario.InnerHtml = nombreUsuario.InnerHtml.Replace(nombreActual, nombreNuevo);
                    }

                    if (string.IsNullOrEmpty(emailNuevo))
                    {
                        //Modifica cookie de usuario con datos actualizados
                        App_Code.Seguridad.SetCookieAutenticacion(nombreNuevo, email);
                    }
                    else
                    {
                        //Modifica email de usuario en header
                        HtmlGenericControl emailUsuario = (Master.FindControl("emailUsuario") as HtmlGenericControl);
                        emailUsuario.InnerHtml = emailUsuario.InnerHtml.Replace(email, emailNuevo);

                        txtEmailActual.Text = emailNuevo;

                        //Modifica cookie de usuario con datos actualizados
                        App_Code.Seguridad.SetCookieAutenticacion(nombreNuevo, emailNuevo);
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), "msj7", string.Format("getMessage('{0}', '{1}');", resp.mensaje, resp.codigo), true);
                }
            }
        }

        private void GetDatosUsuario()
        {
            txtNombre.Text = App_Code.Seguridad.GetNombreUsuarioLogueado();
            txtEmailActual.Text = App_Code.Seguridad.GetEmailUsuarioLogueado();
        }

        private void HabilitarEdicion(bool habilitaFormulario)
        {
            btnGuardar.Visible = habilitaFormulario;
            btnCancelar.Visible = habilitaFormulario;
            btnActualizar.Visible = !habilitaFormulario;
            nuevoEmail.Visible = habilitaFormulario;

            txtNombre.ReadOnly = !habilitaFormulario;
            //txtEmailActual.ReadOnly = !habilitaFormulario;
            txtEmailNuevo.ReadOnly = !habilitaFormulario;

            msj2.Visible = habilitaFormulario;
            msj1.Visible = !habilitaFormulario;
        }
    }
}