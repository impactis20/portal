﻿using PortalProveedores.BL;
using PortalProveedores.DAL.modificarOrden;
using PortalProveedores.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalProveedores.App.UI.app.ocompra
{
    public partial class liberarOrdenesPendientes : System.Web.UI.Page
    {
        public List<OrdenEntity> listOrden { get; set; }
        private string nombreuser = App_Code.Seguridad.GetRutUsuarioLogueado();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                lblNombreUsuarioLogueado.Text = App_Code.Seguridad.GetNombreUsuarioLogueado();
                buscarOrdenesPendientesPorUsuario();
            }
            else
            {
                
            }
        }
        private void buscarOrdenesPendientesPorUsuario()
        {
            BindGridView();
        }

        private void BindGridView()
        {
            listOrden = OrdenBL.GetOrden(nombreuser, "X");
            gvorden.DataSource = listOrden;
            gvorden.DataBind();
            gvorden.Visible = true;

            lblInfo.Visible = true;
            detalle.Visible = false;
            lblLabel.Visible = true;

            if (gvorden.Rows.Count > 0)
            {
                gvorden.UseAccessibleHeader = true;
                gvorden.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvorden.Visible = true;
                string contador = string.Empty;

                if (gvorden.Rows.Count == 1)
                    contador = "encontró <strong>1 orden</strong>";
                else
                    contador = string.Format("encontraron <strong>{0} órdenes</strong>", gvorden.Rows.Count);

                lblInfo.Text = string.Format("Se {0} según los parámetros ingresados.", contador);
                lblInfo.CssClass = "alert-info";
            }
            else
            {
                //lblInfo.Text = respWS.mensaje;
                lblInfo.Visible = false;
                lblInfo.CssClass = "alert-danger";
                //gvorden.Visible = false;
            }
        }

        private void BindGridViewdetalle(int ordencompra)
        {
            listOrden = OrdenBL.GetOrden(nombreuser, "X");

            List<DetalleOrdenEntity> listDetalleOrden = listOrden.ElementAt(ordencompra).detalleOrden;
            GridView1.DataSource = listDetalleOrden;
            GridView1.DataBind();
            OrdenEntity orden = listOrden.ElementAt(ordencompra);
            lblrutproveedor.Text = formatearRut(orden.rutproveedor);
            lblrazonsocial.Text = orden.razonsocial;
            lblordencompra.Text = orden.ordencompra;
            lblordencompra2.Text = orden.ordencompra;
            lblfechaordencompra.Text = orden.fechaordencompra.ToString().Substring(6, 2) + "/" + orden.fechaordencompra.ToString().Substring(4, 2) + "/" + orden.fechaordencompra.ToString().Substring(0, 4);

            lblprecioneto.Text = (Convert.ToDecimal(orden.precioneto)).ToString("C0");
            if (orden.recargo == "0") { trRecargo.Visible = false; }
            lblrecargo.Text = orden.recargo == "0" ? "" : (Convert.ToDecimal(orden.recargo)).ToString("C0");
            if (orden.dsctoimporte == "0") { trImporte.Visible = false; }
            lbldsctoimporte.Text = orden.dsctoimporte == "0" ? "" : (Convert.ToDecimal(orden.dsctoimporte)).ToString("C0");
            if (orden.dsctoporcentaje == "0") { trPorcentaje.Visible = false; }
            lbldsctoporcentaje.Text = orden.dsctoporcentaje == "0" ? "" : (Convert.ToDecimal(orden.dsctoporcentaje)).ToString("C0");
            lblivacompraclp.Text = (Convert.ToDecimal(orden.ivacompraclp)).ToString("C0");
            if (orden.retencion == "0") { trRetencion.Visible = false; }
            lblretencion.Text = orden.retencion == "0" ? "" : (Convert.ToDecimal(orden.retencion)).ToString("C0");
            lblvalorbruto.Text = (Convert.ToDecimal(orden.valorbruto)).ToString("C0");

            lblInfo.Visible = false;
            detalle.Visible = true;
            lblLabel.Visible = false;
            hfOrdenCompraIdLiberar.Value = orden.ordencompra;
        }
        public string formatearRut(string rut)
        {
            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }

        protected void lnkdetail_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string yourValue = btn.CommandArgument;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            int index = Convert.ToInt32(row.RowIndex);
            BindGridViewdetalle(index);
            gvorden.Visible = false;
        }

        protected void btnLiberar_Click(object sender, EventArgs e)
        {
            /**/lblreturnws.Text = "";
            divreturnws.Visible = false;
            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            ValidaUsuarioEntity validaUsuario = OrdenBL.GetValidaUsuario(App_Code.Seguridad.GetRutUsuarioLogueado(), App_Code.Seguridad.GetClaveUsuarioLogueado());
            OrdenModificarMsgResp modificaOrden = OrdenBL.GetModificaOrden(validaUsuario.grupoLiberadorId, hfOrdenCompraIdLiberar.Value, rut);
            //divreturnws.Visible = true;
            lblreturnws.Text = "Respuesta: "  + modificaOrden.cabecera.glosa;
            lblreturnws2.Text = modificaOrden.cabecera.glosa;
            //MessageBox.Show("Zomg " + e.FullPath + " has been changed!!");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "alert('" + lblreturnws.Text + "');window.location='/app/ocompra/liberarOrdenesPendientes.aspx';", true);
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "crearMandato();", true);

            
            btnLiberar.Enabled = false;
            //Response.Redirect("~/app/ocompra/visualizarHistorialLiberadas.aspx");
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/app/ocompra/liberarOrdenesPendientes.aspx");
        }
    }
}