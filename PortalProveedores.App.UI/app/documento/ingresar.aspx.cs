﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;
using System.Data;
using System.Net;
using System.IO;
using System.Web.Services;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.app.documento
{
    public partial class ingresar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSociedades();
                BindStatus();
                BindTipoDocumento();
                txtFechaEmision.Text = DateTime.Today.ToShortDateString();
            }
        }

        private void BindSociedades()
        {
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";

            lstSociedades.DataSource = SociedadBL.GetAllSociedades();
            lstSociedades.DataBind();
        }

        private void BindStatus()
        {
            List<Entities.DocumentoEntity.Status> lstData = DocumentoBL.GetStatus(true, false);

            //Bindea list del formulario de edición (status)
            //Agrega items de status de documento con iconos
            foreach (Entities.DocumentoEntity.Status status in lstData)
            {
                ListItem item = new ListItem(status.descripcion, status.id);
                item.Attributes.Add("data-icon", status.icono);
                lstStatus.Items.Add(item);
            }

            lstStatus.DataBind();
        }

        private void BindTipoDocumento()
        {
            //Bindea list de documento
            lstTipoDocumento.DataTextField = "descripcion";
            lstTipoDocumento.DataValueField = "id";
            lstTipoDocumento.DataSource = DocumentoBL.GetTipoDocumento().Where(x => x.seleccionable == true);
            lstTipoDocumento.DataBind();
        }




        protected void btnAgregarDocComercial_ServerClick(object sender, EventArgs e)
        {
            string rut = txtRutProveedor.Text;
            if (App_Code.Util.RutValido(rut))
            {
                string idSociedad = lstSociedades.SelectedItem.Value;
                string nroDoc = txtNroDocumento.Text;
                string idTipoDoc = lstTipoDocumento.SelectedItem.Value;
                string referencia = txtReferencia.Text;
                string fechaDoc = txtFechaEmision.Text;
                string monto = Convert.ToDouble(txtMonto.Text).ToString();
                string idEstatus = lstStatus.SelectedItem.Value;
                string observacion = txtObservacion.Text;
                //string rutNombre = rut.Replace(".", string.Empty);
                string rutNombre = rut.Replace(".", string.Empty);
                rutNombre = rutNombre.Replace("-", string.Empty);
                //string rutNombre = rutNomb[0] + rutNomb[1]
                //rutNombre = rutNombre.Substring(0, 7);
                string nameFile = rutNombre + "-" + nroDoc + ".pdf";
                string fullPathDoc = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp_" + ConfigurationManager.AppSettings["ambiente"]], nameFile);
                if (Convert.ToDouble(monto) > 0)
                {
                    string rutUsuarioResponsable = App_Code.Seguridad.GetRutUsuarioLogueado();
                    try
                    {
                       
                        //DocumentoBL.AddDocumento(nroDoc, rut, idTipoDoc, referencia, fechaDoc, monto, idSociedad, observacion, idEstatus, rutUsuarioResponsable, fullPathDoc);
                        SaveFile(fArchivo.PostedFile);
                        Entities.ResponseWSEntity resp= AddDocumento(nroDoc, rut, idTipoDoc, referencia, fechaDoc, monto, idSociedad, observacion, idEstatus, fullPathDoc);
                         
                        if (resp.codigo == "0")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msj2", "getMessage('Documento Ingresado Exitosamente.', '0')", true);
                            limpiarCampos();
                        }
                        else if (resp.codigo == "2")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msj3", "getMessage('El proveedor ingresado no existe.', '2')", true);
                            limpiarCampos();
                        }
                    }
                    catch
                    {


                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop23", App_Code.Util.GetMensajeAlerta("error", "Error...", App_Code.Util.GetError("Ha ocurrido un error al intentar adjuntar un documento. Favor de intentar más tarde.")), true);

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj1", "getMessage('El monto ingresado debe ser mayor a 0.', '1')", true);
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj4", "getMessage('Ingrese un rut válido', '1')", true);

            }

        }

        public void SaveFile(HttpPostedFile file)
        {

            string rut = txtRutProveedor.Text;
            string rutNombre = rut.Replace(".", string.Empty);
            rutNombre = rutNombre.Replace("-", string.Empty);
            //rutNombre = rutNombre.Substring(0, 7);
            string nroDoc = txtNroDocumento.Text;
            string fileName = rutNombre + "-" + nroDoc + ".pdf";
            //string fullPathDoc = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp_" + ConfigurationManager.AppSettings["ambiente"]], fileName);
            string fullPathDoc = Server.MapPath("temp/" + fileName);
            string fullFinal = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp_" + ConfigurationManager.AppSettings["ambiente"]], fileName);


            // Obtenga el nombre del archivo para cargar.


            // Cree la ruta y el nombre del archivo para verificar si hay duplicados.
            string pathToCheck = fullPathDoc + fileName;

            // Cree un nombre de archivo temporal para usar para verificar duplicados
            string tempfileName = "";

            //Verifique si ya existe un archivo con el
            // mismo nombre que el archivo para cargar.

            if (System.IO.File.Exists(pathToCheck))
            {
                int counter = 2;
                while (System.IO.File.Exists(pathToCheck))
                {
                    // si ya existe un archivo con este nombre,
                    // prefija el nombre del archivo con un número
                    tempfileName = counter.ToString() + fileName;
                    pathToCheck = fullPathDoc + tempfileName;
                    counter++;
                }

                fileName = tempfileName;

                // Notify the user that the file name was changed.
                //UploadStatusLabel.Text = "A file with the same name already exists." +
                //    "<br />Your file was saved as " + fileName;
            }
            else
            {
                // Notify the user that the file was saved successfully.
                //UploadStatusLabel.Text = "Your file was uploaded successfully.";
            }

            // Agregue el nombre del archivo para cargar en la ruta.

            fullPathDoc += fileName;

            // Llame al método SaveAs para guardar el archivo cargado
            // en el directorio especificado.
            fArchivo.SaveAs(fullPathDoc);

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            try
            {

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fullFinal);
                request.UseBinary = true;
                request.Method = WebRequestMethods.Ftp.UploadFile;

                byte[] b = File.ReadAllBytes(fullPathDoc);

                request.ContentLength = b.Length;
                using (Stream s = request.GetRequestStream())
                {
                    s.Write(b, 0, b.Length);
                }

                request.Abort();

            }
            finally
            {
                if ((File.Exists(fullFinal)))
                    File.Delete(fullFinal);
            }

        }

        [WebMethod]
        public static Entities.ResponseWSEntity AddDocumento(string nroDoc, string rut, string idTipoDoc, string referencia, string fechaDoc, string monto, string idSociedad, string observacion, string idEstatus, string fullPathDoc)
        {
            try
            {
                double num;
                if (double.TryParse(monto, out num))
                {
                    monto = monto.Replace(".", string.Empty);

                    string rutUsuarioResponsable = App_Code.Seguridad.GetRutUsuarioLogueado();
                Entities.ResponseWSEntity resp = DocumentoBL.AddDocumento(nroDoc, rut, idTipoDoc, referencia, fechaDoc, monto, idSociedad, observacion, idEstatus, rutUsuarioResponsable, fullPathDoc );

                ResponseWSEntity respWS = new ResponseWSEntity(resp.codigo, string.Format("{0};{1}", resp.mensaje, DateTime.Now.ToShortDateString()));
                return resp;
                }
                else
                    return new ResponseWSEntity("2", "El monto debe ser un valor numérico.");
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>Error 220.</strong> Se generó un problema interno al intentar agregar el documento. Favor contacte a su administrador.");
            }

        }

        public void limpiarCampos()
        {

            txtMonto.Text = string.Empty;
            txtReferencia.Text = string.Empty;
            txtObservacion.Text = string.Empty;
            txtNroDocumento.Text = string.Empty;
            txtRutProveedor.Text = string.Empty;
            lstSociedades.Items.Clear();
            lstStatus.Items.Clear();
            lstTipoDocumento.Items.Clear();
            BindSociedades();
            BindStatus();
            BindTipoDocumento();
            txtFechaEmision.Text = DateTime.Today.ToShortDateString();


        }
    }
}