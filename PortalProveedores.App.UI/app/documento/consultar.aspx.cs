﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PortalProveedores.BL;
using System.Net;


namespace PortalProveedores.App.UI.app.documento
{
    public partial class consultar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //Validate();
                BindSociedades();
                BindTipoDocumento();
                BindStatusFiltro();
                BindFiltros();
                BindTipoDoc();

                if (Cache.Get("umbralDocumentoVencimiento") == null)
                {
                    Entities.ResponseWSEntity respWS = DocumentoBL.GetUmbralDocumentoVencimiento();
                    DateTime expira = DateTime.Now.AddDays(1);//.AddMinutes(1);

                    if (respWS.codigo == "0")
                        Cache.Insert("umbralDocumentoVencimiento", respWS.mensaje, null, expira, TimeSpan.Zero);
                    else
                        //por defecto toma 4
                        Cache.Insert("umbralDocumentoVencimiento", "4", null, expira, TimeSpan.Zero);
                }

                if (App_Code.Seguridad.GetRolUsuarioLogueado() == "P")
                {
                    filtroRut.Visible = false;
                    modalFooter.Visible = false; //oculta edición, eliminación 

                }
      
            }
        }

        private void BindFiltros()
        {
            //controlado por rol a proveedores
            if (App_Code.Seguridad.GetRolUsuarioLogueado() != "P")
            {
                System.Web.UI.WebControls.ListItem itemRut = new System.Web.UI.WebControls.ListItem("Rut Proveedor", "2");
                itemRut.Selected = true;
                lbFiltro.Items.Add(itemRut);
            }

            System.Web.UI.WebControls.ListItem itemDesde = new System.Web.UI.WebControls.ListItem("Desde", "3");
            itemDesde.Selected = true;
            lbFiltro.Items.Add(itemDesde);
            System.Web.UI.WebControls.ListItem itemHasta = new System.Web.UI.WebControls.ListItem("Hasta", "4");
            itemHasta.Selected = true;
            lbFiltro.Items.Add(itemHasta);
            lbFiltro.Items.Add(new System.Web.UI.WebControls.ListItem("Folio", "5"));
            lbFiltro.Items.Add(new System.Web.UI.WebControls.ListItem("Estado", "6"));
            lbFiltro.Items.Add(new System.Web.UI.WebControls.ListItem("Fecha Recepción", "7"));
            lbFiltro.Items.Add(new System.Web.UI.WebControls.ListItem("Fecha Cobro", "8"));
            lbFiltro.Items.Add(new System.Web.UI.WebControls.ListItem("Tipo Documento", "9"));


            lbFiltro.DataBind();
        }

        private void BindTipoDoc()
        {
            //Bindea list de documento
            lbTipoDoc.DataTextField = "descripcion";
            lbTipoDoc.DataValueField = "id";
            lbTipoDoc.DataSource = DocumentoBL.GetTipoDocumento().Where(x => x.seleccionable == true);
            lbTipoDoc.DataBind();
        }

        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = SociedadBL.GetAllSociedades();

            //Bindea list del formulario de búsqueda
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";
            lstSociedades.DataSource = lstData;
            lstSociedades.DataBind();

            //Bindea list de edición 
            lstEditarSociedades.DataTextField = "nombre";
            lstEditarSociedades.DataValueField = "id";
            lstEditarSociedades.DataSource = lstData;
            lstEditarSociedades.DataBind();
        }

        private void BindStatusFiltro()
        {
            List<Entities.DocumentoEntity.Status> lstData = DocumentoBL.GetStatus(true);
            lbEstado.Items.Clear();

            //Bindea list del formulario de edición (status)
            foreach (Entities.DocumentoEntity.Status status in lstData)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem(status.descripcion, status.id);
                lbEstado.Items.Add(item);
            }

            lbEstado.DataBind();
        }

        private void BindStatusEditar()
        {
            List<Entities.DocumentoEntity.Status> lstData = DocumentoBL.GetStatus(true, false); //(quitar estados automáticos: Aprobada y Pagada

            lstEditarStatus.Items.Clear();

            //Bindea list del formulario de edición (status)
            foreach (Entities.DocumentoEntity.Status status in lstData)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem(status.descripcion, status.id);
                item.Attributes.Add("data-icon", status.icono);
                lstEditarStatus.Items.Add(item);
            }

            lstEditarStatus.DataBind();
        }


        private void BindStatusMostrar()
        {
            List<Entities.DocumentoEntity.Status> lstData1 = DocumentoBL.GetStatusMostrar(true, false); //(quitar estados automáticos: Aprobada y Pagada

            lstMostrarStatus.Items.Clear();

            //Bindea list del formulario de edición (status)
            foreach (Entities.DocumentoEntity.Status status in lstData1)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem(status.descripcion, status.id);
                item.Attributes.Add("data-icon", status.icono);
                lstMostrarStatus.Items.Add(item);
            }

            lstMostrarStatus.DataBind();
        }

        private void BindMotivo()
        {
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.DocumentoEntity.MotivoRechazo> lstData = DocumentoBL.GetMotivoDescripcion(ref respWS);

            lstMotivo.Items.Clear();

            //Bindea list del formulario de edición (status)
            foreach (Entities.DocumentoEntity.MotivoRechazo motivoRechazo in lstData)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem(motivoRechazo.id, motivoRechazo.descripcion);
                lstMotivo.Items.Add(item);
            }
            //lstMotivo.DataSource = lstData;
            lstMotivo.DataBind();
        }

        private void BindTipoDocumento()
        {
            lstEditarTipoDocumento.Items.Clear();
            //Bindea list de tipo de documentos
            lstEditarTipoDocumento.DataTextField = "descripcion";
            lstEditarTipoDocumento.DataValueField = "id";
            lstEditarTipoDocumento.DataSource = DocumentoBL.GetTipoDocumento();
            lstEditarTipoDocumento.DataBind();
        }

        private void BindGridView(string rut)
        {
            BindStatusEditar();
            BindStatusMostrar();
            BindMotivo();

            if (lstSociedades.SelectedItem != null)
            {
                //proveedor no tiene opción de filtrar por rut, por ende no es necesario consultar si filtro rut está vacío.
                if (App_Code.Seguridad.GetRolUsuarioLogueado() != "P")
                    rut = filtroRut.Visible ? rut : string.Empty;

                string fechaDesde = filtroFechaDesde.Visible ? txtFechaDesde.Text.Trim() : string.Empty;
                string fechaHasta = filtroFechaHasta.Visible ? txtFechaHasta.Text.Trim() : string.Empty;
                string idSociedad = lstSociedades.SelectedItem.Value; //es un filtro obligatorio
                string folio = filtroFolio.Visible ? txtFolio.Text.Trim() : string.Empty;
                string estado = lbEstado.SelectedItem != null ? lbEstado.SelectedItem.Value : string.Empty;
                string fechaRecepcion = filtroFechaRecepcion.Visible ? txtFechaRecepcion.Text : string.Empty;
                string fechaCobro = filtroFechaCobro.Visible ? txtFechaCobro.Text.Trim() : string.Empty;
                string tipoDoc = lbTipoDoc.SelectedItem != null ? lbTipoDoc.SelectedItem.Value : string.Empty;

                estado = filtroEstado.Visible ? estado : string.Empty;
                tipoDoc = filtroTipoDoc.Visible ? tipoDoc : string.Empty;

                ViewState["fd"] = fechaDesde;
                ViewState["fh"] = fechaHasta;
                ViewState["is"] = idSociedad;
                ViewState["r"] = rut;
                ViewState["fo"] = folio;
                ViewState["es"] = estado;
                ViewState["fr"] = fechaRecepcion;
                ViewState["fc"] = fechaCobro;
                ViewState["td"] = tipoDoc;

                //Se asigna fecha actual por si el usuario filtra solamente por sociedad. De esta manera no habría error de conversión de fecha
                DateTime dateFechaDesde = string.IsNullOrEmpty(fechaDesde) ? DateTime.Now : Convert.ToDateTime(fechaDesde);
                DateTime dateFechaHasta = string.IsNullOrEmpty(fechaHasta) ? DateTime.Now : Convert.ToDateTime(fechaHasta);

                //int diasRangoMaximoConsulta = Convert.ToInt16(ConfigurationManager.AppSettings["consulta_rango_fecha"]);

                //if ((dateFechaHasta - dateFechaDesde).TotalDays <= diasRangoMaximoConsulta)
                //{
                Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();

                gvDocumentos.DataSource = DocumentoBL.GetDocumento(idSociedad, rut, fechaDesde, fechaHasta, folio, estado, fechaRecepcion, fechaCobro, ref respWS, tipoDoc);
                gvDocumentos.DataBind();

                lblInfo.Visible = true;

                if (gvDocumentos.Rows.Count > 0)
                {
                    gvDocumentos.UseAccessibleHeader = true;
                    gvDocumentos.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvDocumentos.Visible = true;
                    btnExportarDataExcel.Visible = true;
                    string contador = string.Empty;

                    if (gvDocumentos.Rows.Count == 1)
                        contador = "encontró <strong>1 documento</strong>";
                    else
                        contador = string.Format("encontraron <strong>{0} documentos</strong>", gvDocumentos.Rows.Count);

                    lblInfo.Text = string.Format("Se {0} según parámetros ingresados.", contador);
                    lblInfo.CssClass = "alert-info";
                }
                else
                {
                    lblInfo.Text = respWS.mensaje;
                    lblInfo.CssClass = "alert-danger";
                    gvDocumentos.Visible = false;
                    btnExportarDataExcel.Visible = false;
                }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "msj209", "getMessage('No puede realizar una búsqueda con un rango de fechas mayor a 90 días.', '1')", true);

                //    lblInfo.Visible = false;
                //    gvDocumentos.Visible = false;
                //    btnExportarDataExcel.Visible = false;
                //}
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj210", "errorControl(" + lstSociedades.ClientID + ")", true);

                lblInfo.Visible = false;
                gvDocumentos.Visible = false;
                btnExportarDataExcel.Visible = false;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string rut = App_Code.Seguridad.GetRolUsuarioLogueado() == "P" ? App_Code.Seguridad.GetRutUsuarioLogueado() : txtRutProveedor.Text.Trim();

            if (!string.IsNullOrEmpty(rut))
            {
                if (App_Code.Util.RutValido(rut))
                {
                    BindGridView(rut);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "Ingrese un rut válido.", "2"), true);
                    btnExportarDataExcel.Visible = false;
                    gvDocumentos.Visible = false;
                    lblInfo.Visible = false;
                }
            }
            else
            {
                BindGridView(string.Empty);
            }

            //BindStatusEditar();
            //BindStatusMostrar();
            //BindMotivo();
        }

        protected void gvDocumentos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
                GridViewRow r = e.Row;

                if (r.RowType == DataControlRowType.DataRow)
                {
                    Entities.DocumentoEntity Dcto = (Entities.DocumentoEntity)r.DataItem;
                   
                try
                {
                    if (Dcto.urlDoc == "" || Dcto.urlOC == "")
                    {

                        //ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 411</strong> El registro no tiene documento asociado", "2"), true);

                    }
                    else
                    {
                        HyperLink urlDoc = r.Controls[0].FindControl("doc") as HyperLink;
                        urlDoc.NavigateUrl = string.Format("archivo.ashx?id={0}", App_Code.Util.Encriptar(Dcto.urlDoc)); //asigna url del documento encriptada
                        HyperLink urlOC = r.Controls[0].FindControl("ocs") as HyperLink;
                        urlOC.NavigateUrl = string.Format("archivo.ashx?id={0}", App_Code.Util.Encriptar(Dcto.urlOC)); //asigna url del documento encriptada
                    }
                  
                }
                catch (Exception ex)
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 411</strong> El registro no tiene documento asociado", "2"), true);
                }


                //cuando Bandera tiene valor "X", debe habilitar las opciones "Rechazar", "En revisión", "Eliminar" y "Editar" en el documento.
                //cuando Bandera tiene valor "" (string.Empty), debe habilitar el detalle del documento en un botón.

                DateTime fechaProbablePago;

                    if (string.IsNullOrEmpty(Dcto.fechaProbablePago))
                        fechaProbablePago = DateTime.MinValue;
                    else
                        fechaProbablePago = Convert.ToDateTime(Dcto.fechaProbablePago);

                    int umbralDiasVencimiento = Convert.ToInt16(Cache.Get("umbralDocumentoVencimiento"));

                    //marca documentos prontos a vencer según umbral (servicio)
                    if (DateTime.Now.AddDays(umbralDiasVencimiento) >= fechaProbablePago && fechaProbablePago >= DateTime.Now)
                        r.CssClass = "danger";
                }
        }

        private void ExportToExcel(List<Entities.DocumentoEntity> lstData)
        {
            string filename = "documentos_portalproveedores_colmena_" + DateTime.Now.ToShortDateString();

            DataTable dt = new DataTable();
            dt.Columns.Add("Sociedad", typeof(string));
            dt.Columns.Add("Rut", typeof(string));
            dt.Columns.Add("Razón Social", typeof(string));
            dt.Columns.Add("N. Doc.", typeof(string));
            dt.Columns.Add("Monto", typeof(double));
            dt.Columns.Add("F. Recepción", typeof(DateTime));
            dt.Columns.Add("F. Probable de pago", typeof(DateTime));
            dt.Columns.Add("Estado", typeof(string));
            dt.Columns.Add("Tipo Doc.", typeof(string));
            dt.Columns.Add("campo1", typeof(string));
            dt.Columns.Add("campo2", typeof(string));
            dt.Columns.Add("campo3", typeof(string));
            dt.Columns.Add("campo4", typeof(string));
            dt.Columns.Add("campo5", typeof(string));
            dt.Columns.Add("Observación", typeof(string));

            foreach (Entities.DocumentoEntity doc in lstData)
            {
                DataRow r = dt.NewRow();
                r[0] = SociedadBL.GetSociedad(doc.sociedad.id).nombre;
                r[1] = doc.rutProveedor;
                r[2] = doc.razonSocial;
                r[3] = doc.nroDocumento;

                if (!string.IsNullOrEmpty(doc.monto))
                    r[4] = Convert.ToDouble(doc.monto);

                if (!string.IsNullOrEmpty(doc.fechaRecepcion) && doc.fechaRecepcion != "00-00-0000")
                    r[5] = Convert.ToDateTime(doc.fechaRecepcion).ToShortDateString();

                if (!string.IsNullOrEmpty(doc.fechaProbablePago))
                    r[6] = Convert.ToDateTime(doc.fechaProbablePago).ToShortDateString();

                r[7] = doc.status.descripcion;
                r[8] = DocumentoBL.GetNombreTipoDocumento(doc.tipoDocumento.id);
                r[9] = doc.modalidadPago; //campo1
                r[10] = doc.fechaPago; //campo2
                r[11] = doc.banco; //campo3
                r[12] = doc.monto2; //campo4
                r[13] = doc.nroCuenta; //campo5
                r[14] = doc.observacion;
                dt.Rows.Add(r);
            }

            dt.AcceptChanges();

            DataGrid dg = new DataGrid();
            dg.AllowPaging = false;
            dg.UseAccessibleHeader = true;
            dg.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#249aef");
            dg.HeaderStyle.Font.Bold = true;
            dg.DataSource = dt;
            dg.DataBind();

            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.Buffer = true;
            System.Web.HttpContext.Current.Response.ContentEncoding = Encoding.Default;
            System.Web.HttpContext.Current.Response.Charset = "";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ".xls");

            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(stringWriter);
            dg.RenderControl(HtmlTextWriter);
            System.Web.HttpContext.Current.Response.Write(stringWriter.ToString());
            System.Web.HttpContext.Current.Response.End();
        }

        protected void lstEditarStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            string estatus = lstEditarStatus.SelectedItem != null ? lbEstado.SelectedItem.Value : string.Empty;
            if (estatus == "Rechazado")
            {
                lstMotivo.Visible = true;
            }
        }

        [WebMethod]
        public static Entities.ResponseWSEntity SetDocumento(string nroDocumentoAntiguo, string idTipoDocumentoAntiguo, string referenciaAntiguo, string rutProveedorAntiguo, string idSociedadAntigua, string montoAntiguo, string fechaDocumentoAntigua, string idEstadoAntiguo, string observacionAntiguo, string motivoAntiguo, string nroDocumentoNuevo, string idTipoDocumentoNuevo, string referenciaNuevo, string rutProveedorNuevo, string montoNuevo, string fechaDocumentoNuevo, string idSociedadNuevo, string idEstadoNuevo, string observacionNuevo, string motivoNuevo)
        {
            if (idEstadoNuevo != "Rechazada")
            {
                motivoNuevo = "";
            }

            Entities.DocumentoEntity documentoAntiguo = new Entities.DocumentoEntity(nroDocumentoAntiguo, idTipoDocumentoAntiguo, referenciaAntiguo, rutProveedorAntiguo, idSociedadAntigua, montoAntiguo, fechaDocumentoAntigua, idEstadoAntiguo, observacionAntiguo, motivoAntiguo);

            Entities.DocumentoEntity documentoNuevo = new Entities.DocumentoEntity(nroDocumentoNuevo, idTipoDocumentoNuevo, referenciaNuevo, rutProveedorNuevo, idSociedadNuevo, montoNuevo, fechaDocumentoNuevo, idEstadoNuevo, observacionNuevo, motivoNuevo);

            string rutUsuarioResponsable = App_Code.Seguridad.GetRutUsuarioLogueado();
            //El nombre del archivo no se modifica, siempre será el mismo que se asignó cuando se agregó el documento.
            //Por ende en la edición, siempre tendrá que sobreescribir el documento en la ruta señalada.

            return DocumentoBL.SetDocumento(documentoAntiguo, documentoNuevo, rutUsuarioResponsable);
        }

        [WebMethod]
        public static Entities.ResponseWSEntity SetEstadoDocumento(string idSociedad, string rut, string nroDocumento, string fechaDoc, string nombreDoc)
        {

            string rutUsuario = App_Code.Seguridad.GetRutUsuarioLogueado();
            string estado = "ELIMINAR";

            try
            {
                string rut2 = rut.Replace(".", string.Empty);
                rut2 = rut2.Replace("-", string.Empty);
                string extension = "pdf";
                string path = string.Format("{0}{1}{2}.{3}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp_" + ConfigurationManager.AppSettings["ambiente"]], nombreDoc, extension);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                FtpWebResponse responseFileDelete = (FtpWebResponse)request.GetResponse();
            }
            catch
            {
                //Response.Write(serializer.Serialize(new { error = "Error al elimminar el archivo. " + ex.Message }));
            }

            return DocumentoBL.SetEstadoDocumento(idSociedad, rut, nroDocumento, fechaDoc, estado, rutUsuario);
        }

        protected void btnExportarDataExcel_ServerClick(object sender, EventArgs e)
        {
            //Consulta realizada
            string fechaDesde = ViewState["fd"].ToString();
            string fechaHasta = ViewState["fh"].ToString();
            string idSociedad = ViewState["is"].ToString();
            string rut = ViewState["r"].ToString();
            string folio = ViewState["fo"].ToString();
            string estado = ViewState["es"].ToString();
            string fechaRecepcion = ViewState["fr"].ToString();
            string fechaCobro = ViewState["fc"].ToString();
            string tipoDoc = ViewState["td"].ToString();

            Entities.ResponseWSEntity resp = new Entities.ResponseWSEntity();
            List<Entities.DocumentoEntity> lstData = DocumentoBL.GetDocumento(idSociedad, rut, fechaDesde, fechaHasta, folio, estado, fechaRecepcion, fechaCobro, ref resp, tipoDoc);

            gvDocumentos.DataSource = lstData;
            gvDocumentos.DataBind();

            if (resp.codigo == string.Empty)
            {
                try
                {
                    ExportToExcel(lstData);
                }
                catch
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 411</strong> No se pudo generar el archivo excel debido a un error interno.", "2"), true);

                    lblInfo.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj209", string.Format("getMessage('{0}', '{1}')", "<strong>Error 410</strong> No se pudo descargar el archivo excel debido a que no se pudieron obtener los documentos.", "2"), true);

                lblInfo.Visible = false;
            }
        }

        protected void lbFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (System.Web.UI.WebControls.ListItem item in lbFiltro.Items)
            {
                if (item.Value == "2")
                    filtroRut.Visible = item.Selected;
                if (item.Value == "3")
                    filtroFechaDesde.Visible = item.Selected;
                if (item.Value == "4")
                    filtroFechaHasta.Visible = item.Selected;
                if (item.Value == "5")
                    filtroFolio.Visible = item.Selected;
                if (item.Value == "6")
                    filtroEstado.Visible = item.Selected;
                if (item.Value == "7")
                    filtroFechaRecepcion.Visible = item.Selected;
                if (item.Value == "8")
                    filtroFechaCobro.Visible = item.Selected;
                if (item.Value == "9")
                    filtroTipoDoc.Visible = item.Selected;
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "msj219", "cargarElementoFiltros();", true);
        }
    }
}