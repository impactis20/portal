﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app
{
    public partial class entornoMenu : System.Web.UI.MasterPage
    {
        public string rutUsuario = ""; 
        protected void Page_Load(object sender, EventArgs e)

        {
            if (!IsPostBack)

            {
                ShowAmbiente();
                nombreUsuario.InnerHtml +=  App_Code.Seguridad.GetNombreUsuarioLogueado();
                emailUsuario.InnerHtml += App_Code.Seguridad.GetEmailUsuarioLogueado();
                rutUsuario = App_Code.Seguridad.GetRutUsuarioLogueado();

            }

            GetMenu(UsuarioBL.GetMenu(App_Code.Seguridad.GetRutUsuarioLogueado()), menu);
        }

        private void GetMenu(List<Entities.SistemaEntity.Menu> lstMenuUsuario, HtmlGenericControl ul)
        {
            for(int i = 0; i < lstMenuUsuario.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl anchor = new HtmlGenericControl("a");


                if (!string.IsNullOrEmpty(lstMenuUsuario[i].url))
                    anchor.Attributes.Add("href", lstMenuUsuario[i].url);
                anchor.InnerHtml = lstMenuUsuario[i].descripcion;
                li.Controls.Add(anchor);

                if (lstMenuUsuario[i].esPadre)
                {
                    HtmlGenericControl ulHijo = new HtmlGenericControl("ul");
                    ulHijo.Attributes.Add("class", lstMenuUsuario[i].css);
                    int idItem = lstMenuUsuario[i].id;

                    GetMenu(lstMenuUsuario.Where(x => x.idPadre == idItem).ToList(), ulHijo);

                    lstMenuUsuario = lstMenuUsuario.Except(lstMenuUsuario.Where(x => x.idPadre == idItem).ToList()).ToList();
                    li.Controls.Add(ulHijo);
                }

                ul.Controls.Add(li);

                lstMenuUsuario.Remove(lstMenuUsuario[i]);
                i--;
            }
        }

        private void ShowAmbiente()
        {
            string ambiente = ConfigurationManager.AppSettings["ambiente"];

            if (ambiente == "qas" || ambiente == "des")
            {
                //mensaje_ambiente.Visible = true;
            }
        }

        protected void btnSalir_ServerClick(object sender, EventArgs e)
        {
            App_Code.Seguridad.CerrarSesion();
        }
    }
}