﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;
using System.Net;
using System.IO;
using System.Configuration;

namespace PortalProveedores.App.UI.app.sugerencias
{
    public partial class sugerencias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSociedades();
                BindSTipoSolicitud();
            }
        }
        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = SociedadBL.GetAllSociedades();

            //Bindea list del formulario de búsqueda
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";
            lstSociedades.DataSource = lstData;
            lstSociedades.DataBind();
        }
        private void BindSTipoSolicitud()
        {
            string accion = "2";
            string origen = "1";
            
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.SugerenciasEntity> lst = SugerenciasBL.GetTipoSugerencia(accion, origen , ref respWS);

            //Bindea list del formulario de búsqueda
            lstTipoSolicitud.DataTextField = "descripcion";
            lstTipoSolicitud.DataValueField = "id";
            lstTipoSolicitud.DataSource = lst;
            lstTipoSolicitud.DataBind();
        }
        protected void btnEnviar_ServerClick(object sender, EventArgs e)
        {
            string accion = "1";
            string rutProveedor = App_Code.Seguridad.GetRutUsuarioLogueado();
            string origen = "1";
            string idSoli = lstTipoSolicitud.SelectedItem.Value;
            string descripcion = txtDescripcion.Text;
            string razon = App_Code.Seguridad.GetNombreUsuarioLogueado();
            string idSociedad = lstSociedades.SelectedItem.Value;
            string rutNombre = rutProveedor.Replace(".", string.Empty);
            rutNombre = rutNombre.Replace("-", string.Empty);
            //string rutNombre = rutNomb[0] + rutNomb[1]
            //rutNombre = rutNombre.Substring(0, 7);
            string nroDoc = txtNombreDocumento.Text;
            string nameFile = rutNombre + "-" + nroDoc + ".jpeg";
            //string fullPathDoc = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["ImagenesSugerencias"], nameFile);
            string fullPathDoc = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["ImagenesSugerencias_" + ConfigurationManager.AppSettings["ambiente"]], nameFile);
            ViewState["rt"] = rutProveedor;
            ViewState["de"] = descripcion;
            ViewState["ra"] = razon;
            ViewState["so"] = idSociedad;
            ViewState["is"] = idSoli;
            try
            {
                SaveFile(fArchivo.PostedFile);
                Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
                SugerenciasBL.GetSugerencias(accion, rutProveedor, idSoli, origen, descripcion, razon, idSociedad, fullPathDoc, ref respWS);
                if (respWS.codigo == "0")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj2", "getMessage('Sus datos fueron recibidos exitosamente.', '0')", true);
                    limpiarCampos();
                }
                else if (respWS.codigo != "0")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj3", "getMessage('Error al enviar sus datos... Vuela a intentar.')", true);
                    limpiarCampos();
                }

            }
            catch
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "msj3", "getMessage('Se generó un problema interno al intentar enviar los datos. Favor contacte a su administrador.', '1')", true);
            }
        }
        public void SaveFile(HttpPostedFile file)
        {

            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            string rutNombre = rut.Replace(".", string.Empty);
            rutNombre = rutNombre.Replace("-", string.Empty);
            string nroDoc = txtNombreDocumento.Text;
            string fileName = rutNombre + "-" + nroDoc + ".jpeg";
            //string fullPathDoc = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp_" + ConfigurationManager.AppSettings["ambiente"]], fileName);
            string fullPathDoc = Server.MapPath("temp/" + fileName);
           string fullFinal = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["ImagenesSugerencias_" + ConfigurationManager.AppSettings["ambiente"]], fileName);
            //string fullFinal = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["ImagenesSugerencias" ], fileName);

            // Obtenga el nombre del archivo para cargar.

            // Cree la ruta y el nombre del archivo para verificar si hay duplicados.
            string pathToCheck = fullPathDoc + fileName;

            // Cree un nombre de archivo temporal para usar para verificar duplicados
            string tempfileName = "";

            //Verifique si ya existe un archivo con el
            // mismo nombre que el archivo para cargar.

            if (System.IO.File.Exists(pathToCheck))
            {
                int counter = 2;
                while (System.IO.File.Exists(pathToCheck))
                {
                    // si ya existe un archivo con este nombre,
                    // prefija el nombre del archivo con un número
                    tempfileName = counter.ToString() + fileName;
                    pathToCheck = fullPathDoc + tempfileName;
                    counter++;
                }

                fileName = tempfileName;

                // Notify the user that the file name was changed.
                //UploadStatusLabel.Text = "A file with the same name already exists." +
                //    "<br />Your file was saved as " + fileName;
            }
            else
            {
                // Notify the user that the file was saved successfully.
                //UploadStatusLabel.Text = "Your file was uploaded successfully.";
            }

            // Agregue el nombre del archivo para cargar en la ruta.

            fullPathDoc += fileName;

            // Llame al método SaveAs para guardar el archivo cargado
            // en el directorio especificado.
            fArchivo.SaveAs(fullPathDoc);

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            try
            {

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fullFinal);
                request.UseBinary = true;
                request.Method = WebRequestMethods.Ftp.UploadFile;

                byte[] b = File.ReadAllBytes(fullPathDoc);

                request.ContentLength = b.Length;
                using (Stream s = request.GetRequestStream())
                {
                    s.Write(b, 0, b.Length);
                }

                request.Abort();

            }
            finally
            {
                if ((File.Exists(fullFinal)))
                    File.Delete(fullFinal);
            }

        }
        private void limpiarCampos()
        {
            txtDescripcion.Text = string.Empty;
            lstTipoSolicitud.SelectedIndex = -1;
            lstSociedades.SelectedIndex = -1;
            txtNombreDocumento.Text = string.Empty;
        }
    }
}