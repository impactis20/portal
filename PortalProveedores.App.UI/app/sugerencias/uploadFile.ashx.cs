﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Net;
namespace PortalProveedores.App.UI.app.sugerencias
{
    /// <summary>
    /// Descripción breve de uploadFile
    /// </summary>
    public class uploadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/json";

            if (context.Request.Files.Count > 0 && context.Request.QueryString["nroDoc"] != null)
            {
                for (int i = 0; i < context.Request.Files.Count; i++)
                {
                    string nroDoc = context.Request.QueryString["nroDoc"];
                    string[] nrut = nroDoc.Split(',');
                    string numeDoc;

                    if (nrut.Length < 2)
                    //el nombre viene desde el modal editar docuemnto.
                    {
                        numeDoc = nroDoc;
                    }
                    //el nombre viene desde la opcion ingresar  docuemnto.
                    else
                    {
                        nroDoc = nroDoc.Replace(".", string.Empty);
                        nroDoc = nroDoc.Replace("-", string.Empty);
                        string[] nrut2 = nroDoc.Split(',');
                        numeDoc = nrut2[0] + "-" + nrut2[1];
                    }

                    //almacena de manera temporal
                    var file = context.Request.Files[i];
                    string path = context.Server.MapPath("temp/");
                    string nameFile = string.Format("{0}.{1}", numeDoc, file.FileName.Split('.')[1]);
                    string fullPath = string.Format("{0}{1}", path, nameFile);
                    file.SaveAs(fullPath);

                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    try
                    {
                        
                        string fullPathDoc = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["ImagenesSugerencias_" + ConfigurationManager.AppSettings["ambiente"]], nameFile);

                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fullPathDoc);
                        request.UseBinary = true;
                        request.Method = WebRequestMethods.Ftp.UploadFile;

                        byte[] b = File.ReadAllBytes(fullPath);

                        request.ContentLength = b.Length;
                        using (Stream s = request.GetRequestStream())
                        {
                            s.Write(b, 0, b.Length);
                        }

                        request.Abort();
                        //FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                        //response.Dispose();
                        //response.Close();

                        context.Response.Write(serializer.Serialize(new
                        {
                            success = "Listo"
                        }));
                    }
                    catch (WebException ex)
                    {
                        context.Response.Write(serializer.Serialize(new { error = "Error al subir imagen. " + ex.Message }));
                    }
                    finally
                    {
                        if ((File.Exists(fullPath)))
                            File.Delete(fullPath);
                    }
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}