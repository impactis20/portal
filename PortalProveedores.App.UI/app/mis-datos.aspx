﻿<%@ Page Title="Mis datos" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="mis-datos.aspx.cs" Inherits="PortalProveedores.App.UI.app.mis_datos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <h2 class="page-header">Mis datos</h2>

    <p id="msj1" runat="server">Si desea actualizar sus datos haga clic en el botón <span class="label label-primary"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Actualizar</span> y el formulario se habilitará para editar sus datos.</p>
    <p id="msj2" runat="server" visible="false"> Para guardar los cambios presione el botón <span class="label label-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</span> o de lo contrario presione <span class="label label-default"><i class="fa fa-remove"></i>&nbsp;&nbsp;Cancelar</span>. Para actualizar su correo electrónico debe completar el campo <strong><var>Correo electrónico nuevo</var></strong>, de lo contrario, deje el campo en blanco.</p>

    <div class="col-lg-4">
        <div class="row">
            <div id="nombre" runat="server" class="form-group" visible="false">
                <label class="control-label">* Nombre</label>
                <br />
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese su nombre..." ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNombre" runat="server" CssClass="help-block" ErrorMessage="Ingrese un nombre..." Display="Dynamic" ControlToValidate="txtNombre" SetFocusOnError="True" ValidationGroup="actualizar" />
            </div>
            <div class="form-group">
                <label class="control-label">Correo electrónico actual</label>
                <br />
                <asp:TextBox ID="txtEmailActual" runat="server" CssClass="form-control input-sm" Text="francisco@impactis.cl" MaxLength="100" autocomplete="off" ReadOnly="true"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="rfvEmailActual" runat="server" CssClass="help-block" ErrorMessage="Ingrese email actual..." Display="Dynamic" ControlToValidate="txtEmailActual" SetFocusOnError="True" ValidationGroup="actualizar" />--%>
            </div>
            <div id="nuevoEmail" runat="server" class="form-group" visible="false">
                <label class="control-label">Correo electrónico nuevo</label> <small>(opcional)</small>
                <br />
                <asp:TextBox ID="txtEmailNuevo" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese un nuevo email..." ReadOnly="true"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="rfvEmailNuevo" runat="server" CssClass="help-block" ErrorMessage= Display="Dynamic" ControlToValidate="txtEmailNuevo" SetFocusOnError="True" ValidationGroup="actualizar" />--%>
                <asp:Label ID="rfvEmailNuevo" runat="server" Text="Ingrese un email válido para actualizar su correo, de lo contrario deje este campo en blanco o cancele la operación..." CssClass="help-block" Visible="false"></asp:Label>
            </div>

            <div class="pull-right">
                <button id="btnActualizar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnActualizar_ServerClick">
                    <i class="fa fa-refresh"></i>
                    &nbsp;&nbsp;Actualizar
                </button>
                <button id="btnGuardar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnGuardar_ServerClick" validationgroup="actualizar" visible="false">
                    <i class="fa fa-save"></i>
                    &nbsp;&nbsp;Guardar
                </button>
                <button id="btnCancelar" runat="server" type="submit" class="btn btn-default btn-sm" onserverclick="btnCancelar_ServerClick" visible="false">
                    <i class="fa fa-remove"></i>
                    &nbsp;&nbsp;Cancelar
                </button>
            </div>
        </div>
    </div>

    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
</asp:Content>
