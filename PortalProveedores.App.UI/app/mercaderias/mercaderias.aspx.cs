﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using PortalProveedores.BL;
using System.Net;
using System.IO;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.app.mercaderias
{
    public partial class mercaderias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblfechahoy.Text = DateTime.Now.ToString("dd/MM/yyyy");
            if (!IsPostBack)
            {
                BindGridView();
                lblNombre.Text = App_Code.Seguridad.GetNombreUsuarioLogueado();
            }
        }

            private void BindGridView()
            {
                string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
                Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
                gvOrdenes.DataSource = MercaderiasBL.GetOrdenes(rut, ref respWS);
                gvOrdenes.DataBind();

                lblInfo.Visible = true;

                if (gvOrdenes.Rows.Count > 0)
                {
                    gvOrdenes.UseAccessibleHeader = true;
                    gvOrdenes.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvOrdenes.Visible = true;

                    string contador = string.Empty;
                    if (gvOrdenes.Rows.Count == 1)
                        contador = "encontró <strong>1 orden</strong>";
                    else
                        contador = string.Format("encontraron <strong>{0} órdenes</strong>", gvOrdenes.Rows.Count);

                    lblInfo.Text = string.Format("Se {0} según parámetros ingresados.", contador);
                    lblInfo.CssClass = "alert-info";
                }
                else
                {
                    lblInfo.Text = respWS.mensaje;
                    lblInfo.CssClass = "alert-danger";
                    gvOrdenes.Visible = false;
                }
            }

        protected void lnkdetail_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string orden = btn.CommandArgument;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            int index = Convert.ToInt32(row.RowIndex);
            gvOrdenes.Visible = false;
            //string rut = "16619948-4";
            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            gvDetalleOrdenes.DataSource = MercaderiasBL.GetDetalle(rut, orden, ref respWS);
            gvDetalleOrdenes.DataBind();

            lblInfo.Visible = true;

            if (gvDetalleOrdenes.Rows.Count > 0)
            {
                gvDetalleOrdenes.UseAccessibleHeader = true;
                gvDetalleOrdenes.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvDetalleOrdenes.Visible = true;

                //string contador = string.Empty;
                //if (gvDetalleOrdenes.Rows.Count == 1)
                //    contador = "encontró <strong>1 orden</strong>";
                //else
                //    contador = string.Format("encontraron <strong>{0} órdenes</strong>", gvDetalleOrdenes.Rows.Count);

                //lblInfo.Text = string.Format("Se {0} según parámetros ingresados.", contador);
                //lblInfo.CssClass = "alert-info";
                lblInfo.Visible = false;
            }
            else
            {
                lblInfo.Text = respWS.mensaje;
                lblInfo.CssClass = "alert-danger";
                gvDetalleOrdenes.Visible = false;
            }
        }



        protected void gvDetalleOrdenes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.MercaderiasEntity Merc = (Entities.MercaderiasEntity)r.DataItem;
            }
        }
        [WebMethod]
        public static Entities.ResponseWSEntity AddContabilizacion(string tipoM, String orden, string posicion, string cantidadR, string fechaD, string observaciones, string GuiaD)
        {
            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            string accion = "3";
            string fechaS = DateTime.Today.ToString("yyyy/MM/dd");
            fechaS = fechaS.Replace("-", "");
            string[] fechadoc = fechaD.Split('-');
            fechaD = fechadoc[2] + fechadoc[1] + fechadoc[0];
            return MercaderiasBL.AddContabilizacion( tipoM, orden, posicion, cantidadR, fechaD, observaciones, fechaS, accion, rut, GuiaD);
        }

    }
}