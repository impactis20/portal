﻿<%@ Page Title="Recepción de mercadería" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="mercaderias.aspx.cs" Inherits="PortalProveedores.App.UI.app.mercaderias.mercaderias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>
   
 <script type="text/javascript">
     $(function () {
         //inicia combobox por defecto
         $('.selectpicker').selectpicker({
             title: 'Seleccione...',
             showTick: true
         });

         $('[id*=calendarioFechaCont]').datetimepicker({
             useCurrent: false, //mantiene la fecha ingresa después de realizar postback
             format: 'DD-MM-YYYY',
             maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
             //minDate: moment().add(-3, 'month'),
             showClear: true,
             ignoreReadonly: false
         });

         $('[id*=calendarioFechaDoc]').datetimepicker({
             useCurrent: false, //Important! See issue #1075
             format: 'DD-MM-YYYY',
             maxDate: moment().add(1, 'days'),
             showClear: true,
             ignoreReadonly: false
         });


         $('.fechafiltro').datetimepicker({
             useCurrent: false, //mantiene la fecha ingresa después de realizar postback
             format: 'DD-MM-YYYY',
             minDate: moment().add(-2, 'year'),
             showClear: true,
             ignoreReadonly: false
         });

     });
    </script>

    <h2 class="page-header">Órdenes pendientes de recepción de mercadería</h2>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline" id="lblNombre1" runat="server">
                    <p>Estimado <asp:Label runat="server" ID="lblNombre" />, favor realizar recepción de mercadería de las siguientes Órdenes de Compra originadas de sus Solicitudes de Pedido. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
   </div>
    
   <%--Tabla Orden --%>   
  <div class="table-responsive small">
          <asp:GridView ID="gvOrdenes" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None">
            <Columns>
               <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Detalle de la orden de compra.">Detalle</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:LinkButton ID="lnkdetail" runat="server" Text="Detalle" OnCommand="lnkdetail_Click" CommandArgument='<%#Eval("orden")%>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número de la orden de compra.">Orden de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                     <%#Eval("orden")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de emisión de la orden de compra.">Fecha de Orden</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaO") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rut del proveedor.">Rut Proveedor</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("rutP") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Razón social del proveedor.">Razon Social Proveedor</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("razonP") %>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Precio neto.">Precio Neto</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <%# Eval("precioN") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="IVA.">IVA</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("iva") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Retención.">Retención</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("retencion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto Bruto.">Monto Bruto</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("montoB") %>
                    </ItemTemplate>
                </asp:TemplateField>
             </Columns>
        </asp:GridView>
    </div>
     
  <%--Tabla Detalle Orden --%>   
  <div class="table-responsive small">
        <asp:GridView ID="gvDetalleOrdenes" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número de la orden de compra.">Orden de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                     <%# Eval("orden") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Posición del ítem en la orden de compra.">Posición</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("posicion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Código del item.">Material</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("material") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Descripción del item.">Descripción</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("descripcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Cantidad solicitada.">Cantidad</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <%# Eval("cantidad") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Unidad de medida del item.">Unidad Medida</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("unidadM") %>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Cantidad de items recibidos.">Cantidad Recibida</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("cantidadR") %>
                    </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Cantidad de items pendientes.">Cantidad Pendiente</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("cantidadP") %>
                    </ItemTemplate>
                </asp:TemplateField>
               
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Descripción del tipo de material.">Descripción Tipo de Material</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("descripcionT") %>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Contabilizar recepción de mercadería."></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                       <div class="tooltip-wrapper-header-table pull-left" title="Contabilizar recepción de mercadería">
                         <a style="padding-right:25px;font-size:15px;" id="contb" runat="server" href="#" data-toggle="modal" data-target=".modalContabilizar" data-tipomat='<%# Eval("tipoM") %>' data-orden='<%# Eval("orden") %>' data-cantidad='<%# Eval("cantidad") %>' data-cantidadr='<%# Eval("cantidadR") %>' data-cantidadp='<%# Eval("cantidadP") %>' data-posicion='<%# Eval("posicion") %>'  class="fa fa-edit"></a>
                         </div> 
                    </ItemTemplate>
                </asp:TemplateField>
             </Columns>
        </asp:GridView>
    </div>

  <%--Modal Contabilizar Entrada de mercacias --%>
	<div class="modal modalContabilizar fade" role="dialog" aria-labelledby="contabRecep" data-backdrop="static">
		<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="contabilizacion" runat="server" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="contabRecep">Contabilizar Entrada de Mercancias</h2>
                    </div>  
					<div class="modal-body">
						        <div class="row">
							        <div class="col-lg-6">
									    <div class="form-group">
											<label class="control-label"> Orden de Compra</label>
											<br />
											<asp:TextBox ID="txtNguia" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvNguia" runat="server" CssClass="help-block" ErrorMessage="Ingrese número..." Display="Dynamic" ControlToValidate="txtNguia" SetFocusOnError="True" ValidationGroup="cont" />
									    </div>
                                    </div>
									 <div class="col-lg-6">	
                                        <div class="form-group">
											<label class="control-label"> Posición</label>
											<br />
											<asp:TextBox ID="TextPos" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvPos" runat="server" CssClass="help-block" ErrorMessage="Ingrese posición..." Display="Dynamic" ControlToValidate="TextPos" SetFocusOnError="True" ValidationGroup="cont" />
									    </div>
                                    </div>
								</div>
                        		<div class="row">
								<div class="col-lg-6">                     
									<div class="form-group">
										   <label for="txtFechaCont" class="control-label">Fecha Contable</label>
											<br />
										<div class="input-group date input-group" id="calendarioFechaCont">
											<asp:TextBox ID="lblfechahoy" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
											<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-lg-6">  
									<div class="form-group">
										  <label for="txtFechaDoc" class="control-label">Fecha Documento</label>
											<br />
										<div class="input-group date input-group" id="calendarioFechaDoc">
											<asp:TextBox ID="txtFechaDoc" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
											<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
											</span>
										</div>
                                         <asp:RequiredFieldValidator ID="rfvtxtFechaDoc" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha del documento..." Display="Dynamic" ControlToValidate="txtFechaDoc" SetFocusOnError="True" ValidationGroup="cont" />
									</div>
								</div>
						        </div>
								<div class="row">
								    <div class="col-lg-6">
                                       <div class="form-group">
											<label class="control-label"> Cantidad Solicitada</label>
											<br />
											<asp:TextBox ID="TextCants" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvCants" runat="server" CssClass="help-block" ErrorMessage="Ingrese número..." Display="Dynamic" ControlToValidate="TextCants" SetFocusOnError="True" ValidationGroup="cont" />
									    </div>	
                                    </div>	
                                   <div class="col-lg-6">            
                                    <div class="form-group">
										<label class="control-label"> Cantidad Pendiente</label>
										<br />
										<asp:TextBox ID="TextcantP" runat="server" CssClass="form-control input-sm" MaxLength="250" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
										<asp:RequiredFieldValidator ID="rfvcantP" runat="server" CssClass="help-block" ErrorMessage="Ingrese observaciones..." Display="Dynamic" ControlToValidate="TextcantP" SetFocusOnError="True"  ValidationGroup="cont" />
									</div>	
                                </div>
                             </div>
                            <div class="row">
							    <div class="col-lg-6">
										<div class="form-group">
											<label class="control-label"> Cantidad Recibida</label>
											<br />
											<asp:TextBox ID="TextCantr" runat="server" CssClass="form-control input-sm" MaxLength="30" autocomplete="off" placeholder="Ingrese cantidad..."  />
                                            <asp:RequiredFieldValidator ID="rfvCantr" runat="server" CssClass="help-block" ErrorMessage="Ingrese cantidad recibida..." Display="Dynamic" ControlToValidate="TextCantr" SetFocusOnError="True"  ValidationGroup="cont" />
									    </div>
                                    </div>	
                                 <div class="col-lg-6">
										<div class="form-group">
											<label class="control-label"> N° Guía de Despacho</label>
											<br />
											<asp:TextBox ID="TextGuiaD" runat="server" CssClass="form-control input-sm" MaxLength="30" autocomplete="off" placeholder="Ingrese cantidad..."  />
                                            <asp:RequiredFieldValidator ID="rfvGuiaD" runat="server" CssClass="help-block" ErrorMessage="Ingrese cantidad recibida..." Display="Dynamic" ControlToValidate="TextGuiaD" SetFocusOnError="True"  ValidationGroup="cont" />
									    </div>
                                    </div>	
                             </div>

                        <div class="row">
							<div class="form-group">
								<div class="col-lg-12">            
                                    <div class="form-group">
										<label class="control-label"> Observaciones</label>
										<br />
										<asp:TextBox ID="TextObser" runat="server" CssClass="form-control input-sm" MaxLength="250" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
										<asp:RequiredFieldValidator ID="rfvObser" runat="server" CssClass="help-block" ErrorMessage="Ingrese observaciones..." Display="Dynamic" ControlToValidate="TextObser" SetFocusOnError="True" ValidationGroup="cont" />
									</div>	
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
					    </div>
				    </div>
                 </div>
						<div id="modalfooter" runat="server" class="modal-footer" >
							<button id="btnGuardar" runat="server" type = "button"    class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Contabilizando..." validationgroup="cont">
							<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
							</button>
							<button id="btnCerrarE" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						</div>
				</div>
			</div>
		</div>

             
   <%--Abre modal--%>
    <script type="text/javascript">
        $(document).ready(function () {

            //carga modal al abrirlo
            $('.modalContabilizar').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var tipoM = button.data('tipomat');
                var nOrden = button.data('orden');
                var cant = button.data('cantidad');
                var posicion = button.data('posicion');
                var cantP = button.data('cantidadp');

                
                var modal = $(this);
                             
                ////bloquea formulario
                $('[id*=txtNguia]').prop('disabled', true);
                $('[id*=TextPos]').prop('disabled', true);
                $('[id*=TextCants]').prop('disabled', true);
                $('[id*=lblfechahoy]').prop('disabled', true);
                $('[id*=TextcantP]').prop('disabled', true);

                //almacena valores antes de editar el formulario
                $(<%= hfRowId.ClientID %>).val(button.context.id)
                $(<%= hftipoM.ClientID %>).val(tipoM);
             
                //asigna valores al formulario
                modal.find('#<%= txtNguia.ClientID%>').val(nOrden);
                modal.find('#<%= TextPos.ClientID%>').val(posicion);
                modal.find('#<%= TextCants.ClientID%>').val(cant);
                modal.find('#<%= TextcantP.ClientID%>').val(cantP);
                
            });

                    //función actualiza documento y fila de gridview
            $('#<%= btnGuardar.ClientID %>').on('click', function () {
                var $this = $(this);
                var validationGroup = $this.attr("validationgroup");
                if (Page_ClientValidate(validationGroup)) {
                    $this.button('loading');
                    //lógica
                    $.ajax({
                        type: "POST",
                        url: "mercaderias.aspx/AddContabilizacion",
                        data: "{'tipoM': '" + $(<%= hftipoM.ClientID %>).val() + "','orden': '" + $(<%= txtNguia.ClientID %>).val() + "', 'posicion': '" + $("#<%=TextPos.ClientID%>").val() + "', 'cantidadR': '" + $("#<%=TextCantr.ClientID%>").val() + "','fechaD': '" + $(<%= txtFechaDoc.ClientID %>).val() + "','observaciones': '" + $(<%= TextObser.ClientID %>).val() + "', 'GuiaD': '" + $("#<%=TextGuiaD.ClientID%>").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            setTimeout(function () {
                                $this.button('reset');
                                $('.modalContabilizar').modal('hide');
                                   if (response.d.codigo === "0") {
                               
                                       getMessage(response.d.mensaje, response.d.codigo);
                                       window.location.reload()
                                }
                                else {
                                       getMessage(response.d.mensaje, response.d.codigo);
                                
                                }

                            }, 4000);
                        },
                        error: function (response) {
                            setTimeout(function () {

                                $this.button('reset');
                                getMessage("No tiene Permisos para realizar esta acción", "2");
                            }, 1000);
                        }
                    });

                    return false;
                }
            });

        });
    </script>




    <asp:HiddenField id="hfRowId" runat="server"/>
    <asp:HiddenField id="hfRut" runat="server"/> 
    <asp:HiddenField id="hfModalAccion" runat="server"/> 
    <asp:HiddenField id="hfRowAfectada" runat="server"/>
    <asp:HiddenField id="hftipoM" runat="server"/>
 
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/CantidadRecibida.js") %>'></script>

 </asp:Content>