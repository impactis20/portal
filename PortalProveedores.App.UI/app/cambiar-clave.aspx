﻿<%@ Page Title="Cambiar clave" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="cambiar-clave.aspx.cs" Inherits="PortalProveedores.App.UI.app.cambiar_clave" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <h2 class="page-header">Cambiar clave</h2>
    
    <p>Para cambiar su clave debe rellenar todos los campos según corresponda. Para finalizar presione <span class="label label-primary"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Actualizar</span>, una vez realizado el cambio tendrá que volver a iniciar sesión con su nueva clave.</p>
    <div class="col-lg-4">
        <div class="row">
            <div class="form-group">
                <%--<label class="sr-only">Ingrese el rut del proveedor</label>--%>
                <label class="control-label">* Clave actual</label>
                <br />
                <asp:TextBox ID="txtClave" runat="server" CssClass="form-control input-sm" TextMode="Password" MaxLength="10" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvClave" runat="server" CssClass="help-block" ErrorMessage="Ingrese su clave actual..." Display="Dynamic" ControlToValidate="txtClave" SetFocusOnError="True" ValidationGroup="actualizar" />
            </div>
            <div class="form-group">
                <%--<label class="sr-only">Ingrese el rut del proveedor</label>--%>
                <label class="control-label">* Nueva clave</label>
                <br />
                <asp:TextBox ID="txtNuevoClave" runat="server" CssClass="form-control input-sm" TextMode="Password" MaxLength="10" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNuevoClave" runat="server" CssClass="help-block" ErrorMessage="Ingrese su nueva clave..." Display="Dynamic" ControlToValidate="txtNuevoClave" SetFocusOnError="True" ValidationGroup="actualizar" />
            </div>
            <div class="form-group">
                <%--<label class="sr-only">Ingrese el rut del proveedor</label>--%>
                <label class="control-label">* Repetir nueva clave</label>
                <br />
                <asp:TextBox ID="txtRepiteNuevoClave" runat="server" CssClass="form-control input-sm" TextMode="Password" MaxLength="10" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvRepiteNuevoClave" runat="server" CssClass="help-block" ErrorMessage="Vuelva a ingresar su nueva clave..." Display="Dynamic" ControlToValidate="txtRepiteNuevoClave" SetFocusOnError="True" ValidationGroup="actualizar" />
            </div>

            <div class="pull-right">
                <button id="btnAgregar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnAgregar_ServerClick" validationgroup="actualizar">
                    <i class="fa fa-refresh"></i>
                    &nbsp;&nbsp;Actualizar
                </button>
            </div>
        </div>
    </div>
    
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
</asp:Content>
