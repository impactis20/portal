﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="monitor.aspx.cs" Inherits="PortalProveedores.App.UI.app.monitor.monitor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>
    <link href="../../css/style.css" rel="stylesheet" />
    <style>
  /*.tr, td {
            padding: 8px;
            text-align: left;
            border: black 1px solid;
            
        }

        .th {
             padding: 8px;
            text-align: left;
            border: black 1px solid;
        }*/

    </style>
   

       <script type="text/javascript">
                  $(document).ready(function () {

                      //datatable
                      $('#<%=gvMonitorDocumentos.ClientID%>').DataTable({
                          searching: false,
                          paging: false,
                          info: false
                      });

                      //inicia combobox por defecto
                      $('.selectpicker').selectpicker({
                          title: 'Seleccione una opción...',
                          showTick: true
                      });

                      $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });
                  });
    </script>
       <script type="text/javascript">
           $(function () {
               cargarElementoFiltros();

               $.fn.button.Constructor.DEFAULTS = {
                   loadingText: '<i class="fa fa-spin fa-spinner"></i> Chargement…'
               };

               $('[data-toggle="popover"]').popover({
                   html: true
               });

               //Función que cierra popover desde el boton "x"
               $(document).on("click", ".popover .close", function () {
                   $(this).parents(".popover").popover('hide');
               });

               $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });


           });
    </script>
    <script type="text/javascript">
           function cargarElementoFiltros() {
               $('.filtro').selectpicker({
                   title: 'Filtro...',
                   showTick: true,
                   countSelectedText: function (numSelected, numTotal) {
                       return (numSelected == 1) ? "Mostrando {0} filtro" : "Mostrando {0} filtros";
                   },
                   style: 'btn-default btn-xs'
               });
               $('.selectpicker').selectpicker({
                   title: 'Seleccione...',
                   showTick: true
               });
           }
    </script>


<h2 class="page-header">Monitor de Documentos</h2>

<div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                        <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                                 <p>Para realizar una búsqueda debe rellenar los campos con asterisco. 
                                    <asp:ListBox ID="lbFiltro" runat="server" CssClass="filtro" AutoPostBack="true" data-selected-text-format="count > 0" data-width="135px" SelectionMode="Multiple" OnSelectedIndexChanged="lbFiltro_SelectedIndexChanged1">
                                     <asp:ListItem Value="1" Selected="True">Sociedad</asp:ListItem>
                                     </asp:ListBox>
                                 </p>
                                 <div class="col-lg-2">
                                     <div class="row">
                                         <div class="form-group" style="margin-right: 7px">
                                             <label class="control-label">* Sociedad</label>
                                             <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count &gt; 1" data-width="100%"></asp:ListBox>
                                         <asp:RequiredFieldValidator ID="rfvSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstSociedades" SetFocusOnError="True" ValidationGroup="buscar" />
                                         </div>
                                     </div>
                                 </div>
                              <div id="filtroTipoDoc" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <label for="lbTipoDoc" class="control-label">Tipo Documento</label>
                                        <br />
                                        <asp:ListBox ID="lbTipoDoc" runat="server" CssClass="selectpicker" data-selected-text-format="count &gt; 1" data-width="100%">
                                            <asp:ListItem Value="1">Orden Compra</asp:ListItem>
                                            <asp:ListItem Value="2">Solped</asp:ListItem>
                                        </asp:ListBox>
                                    </div>
                                </div>
                            </div>
                                 <div id="filtroRut" runat="server" class="col-lg-2" visible="false">
                                     <div class="row">
                                         <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                             <label class="control-label" for="txtRutProveedor"><asp:Label ID="lblrutproveedor" runat="server" />Rut Proveedor
                                             </label>
                                             <br />
                                             <asp:TextBox ID="txtRutProveedor" runat="server" autocomplete="off" CssClass="form-control input-sm" MaxLength="10" onblur="javascript:textboxRutFormat(this);" onfocus="javascript:textboxRutOnFocus(this);" onkeyup="javascript:textboxRutOnKeyUp(this);" placeholder="Ingrese..."></asp:TextBox>
                                            <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true" ClientValidationFunction="customValidatorRut" ValidationGroup="buscar"></asp:CustomValidator>
                                         </div>
                                     </div>
                                 </div>
                             <div id="filtroId" runat="server" class="col-lg-2" visible="false">
                                     <div class="row">
                                         <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                             <label class="control-label" for="txtId"><asp:Label ID="lblId" runat="server" />ID
                                             </label>
                                             <br />
                                             <asp:TextBox ID="txtId" runat="server" autocomplete="off" CssClass="form-control input-sm" MaxLength="10" placeholder="Ingrese..."></asp:TextBox>
                                         </div>
                                     </div>
                                 </div>
                                 <div id="filtroFechaDesde" runat="server" class="col-lg-2">
                                     <div class="row">
                                         <div class="form-group" style="margin-right: 7px">
                                             <label class="control-label" for="txtFechaDesde"><asp:Label ID="lblFechaDesdeI" runat="server" />
                                             </label>
                                             <br />
                                             <div id="calendarioFechaDesde" class="input-group date input-group">
                                                 <asp:TextBox ID="txtFechaDesde" runat="server" autocomplete="off" CssClass="form-control input-sm" placeholder="Seleccione..."></asp:TextBox>
                                                 <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div id="filtroFechaHasta" runat="server" class="col-lg-2">
                                     <div class="row">
                                         <div class="form-group" style="margin-right: 7px">
                                             <label class="control-label" for="txtFechaHasta"><asp:Label ID="lblFechaHastaF" runat="server" />
                                             </label>
                                             <br />
                                             <div id="calendarioFechaHasta" class="input-group date input-group">
                                                 <asp:TextBox ID="txtFechaHasta" runat="server" autocomplete="off" CssClass="form-control input-sm" placeholder="Seleccione..."></asp:TextBox>
                                                 <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                            <button id="btnBuscar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnBuscar_Click" validationgroup="buscar">
                                <i class="fa fa-search"></i>
                                &nbsp;&nbsp;Buscar
                            </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
    </div>
    </br>
  <div class="table-responsive small">
        <asp:GridView ID="gvMonitorDocumentos" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"  OnRowDataBound="gvMonitorDocumentos_RowDataBound">
            <Columns>
                <asp:TemplateField >
                      <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" >ID</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                           <a id="editar" runat="server" href="#" data-toggle="modal" data-target=".detalle" data-idproceso='<%# Eval("idProceso") %>' data-idmoni='<%# Eval("idmoni") %>' data-idstatus='<%# Eval("idStatus") %>' data-idsociedad='<%# Eval("sociedad")%>' data-nrodoc='<%# Eval("nrodoc")%>' data-idsolped='<%# Eval("idsolped")%>' data-rutliberador='<%# Eval("rutliberador")%>' data-fechalib='<%# Eval("fechaLib")%>' data-horalib='<%# Eval("horaLib")%>' data-idorden='<%# Eval("idOrden")%>' data-liberadores='<%# Eval("liberadores")%>' data-folio='<%# Eval("folio")%>' data-fechaemision='<%# Eval("fechaEmision")%>' data-fechapago='<%# Eval("fechaPago")%>' data-tipopago='<%# Eval("tipoPago")%>' data-fechaproba='<%# Eval("fechaProba")%>'>
                            <%# Eval("idmoni") %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                      <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Nombre proveedor">RAZÓN SOCIAL</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("razon") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField >
                      <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha que se creó el documento.">F. CREACIÓN</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fecha") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                      <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto del documento.">MONTO</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("monto") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                      <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="OC">Orden de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <%# Eval("idCoti") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                      <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Solped">Solped</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                       <%# Eval("idMercaderia") %>
                    </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField>
                  <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Estado del documento.">ESTADO</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
       <br/>
      
                <%--Simboligía--%>
                <div>
                    <p style="font-size:16px;margin-right: 7px; font-weight:bold;">Leyenda</p>
                    <p><i class="fa fa-check" style="font-size:18px; color:#5FB456;margin-right: 7px;"></i>&nbsp;&nbsp;<asp:Label ID="Label1" runat="server" Text="Procesada"></asp:Label></p> 
                    <p><i class="fa fa-refresh" style="font-size:18px;color:#E2E960;margin-right: 7px;"></i>&nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Text="En Proceso"></asp:Label></p> 
                    <p><i class="fa fa-close" style="font-size:18px;color:#EC8572;margin-right: 7px;"></i>&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Text="Con Error"></asp:Label></p> 
                </div>

                        <%--Diseño modal Detalle--%>
                <div class="modal detalle fade" role="dialog" aria-labelledby="tituloDoc" data-backdrop="static">
                  <div class="modal-dialog"  role="document" style="width:95%;height:auto">
                     <div class="modal-content" style="margin-left:auto; margin-right:auto;" >
                         <div id="edicion" runat="server" >
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title" id="tituloDoc"></h2>
					        </div>
                            <!-- dialog body -->
                            <div class="modal-body" >
                               <div class="row">
                                    <div class="form-group" align="center"  runat="server">
                                    <div class="col-lg-11">
                                        <div class="col-lg-2">
                                            <div id="pointer" style="display:inline-block; margin:10px; "><p style="text-align:center;">Solped</p>
                                            <a id="solp" ></a>&nbsp;&nbsp;<a  href="#" data-toggle="collapse" data-target="#dtSolpedLiberacion" aria-expanded="true" data-idsolped='<%# Eval("idsolped") %>' aria-controls="dtSolpedLiberacion" class="fa fa-search" style="font-size:18px;display:inline-block; color:#0B396E;"></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div id="pointer2" style="display:inline-block;margin:10px; "><p style="text-align:center;">Cotización</p>
                                            <a id="coti"></a><i></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div id="pointer3" style="display:inline-block; margin:10px;"><p style="text-align:center;">Orden de Compra</p>
                                                <div id="ocC"><a id="oc" ></a>&nbsp;&nbsp;<a href="#" data-toggle="collapse" data-target="#dtOCLiberacion" aria-expanded="true" aria-controls="dtOCLiberacion" class="fa fa-search" style="font-size:18px;display:inline-block; color:#0B396E;"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div id="pointer4" style="display:inline-block; margin:10px;"><p style="text-align:right;">Entrada Mercancía</p>
                                                <a id="mer" ></a><i></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div id="pointer5" style="display:inline-block; margin:10px;"><p style="text-align:center;">Factura</p>
                                                <div id="fact"><a id="fac"></a>&nbsp;&nbsp;<a  href="#dtSolpedLiberacion" data-toggle="collapse" data-target="#dtFactLiberacion" aria-expanded="true" aria-controls="dtFactLiberacion" class="fa fa-search"  style="font-size:18px;display:inline-block; color:#0B396E;"></a></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div id="pointer6" style="display:inline-block; margin:10px;"><p style="text-align:center;">Pago</p>
                                                <div id="pago"><a id="pag"></a>&nbsp;&nbsp;<a  href="#dtPago" data-toggle="collapse" data-target="#dtPago" aria-expanded="true" aria-controls="dtPago" class="fa fa-search"  style="font-size:18px;display:inline-block; color:#0B396E;"></a></div>
                                            </div>
                                         </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                               <div class="row">
                                 <div class="col-lg-12" >
                                     <%-- modal Detalle dtSolpedLiberacion--%>
                                     <div class="col-lg-3">
                                           <div id="dtSolpedLiberacion" class="collapse" style="border: none; margin-left: 10px ;" data-parent="#detalle">
                                                <div class="panel-heading"><strong>Solped</strong></div>
                                                <ul class="list-group" style="border: none; ">
                                                    <li id="lblSolped" class="list-group-item" style="border: none;"></li>
                                                    <li id="lblLiberador" class="list-group-item" style="border: none;"></li>
                                                    <li id="lblFecha" class="list-group-item" style="border: none;"></li>
                                                    <li id="lblHora" class="list-group-item" style="border: none;"></li>
                                                </ul>
                                            </div>
                                     </div>
                                     <%-- modal Detalle dtOCLiberacion--%>
                                     <div class="col-lg-3" >
                                         <div id="dtOCLiberacion" class="collapse" style="border: none; margin-left: 70px ;" data-parent="#detalle">                                  
                                             <div class="panel-heading"><strong>Orden de Compra</strong></div>
                                                <ul class="list-group" style="border: none;">
                                                    <li id="lblidOrden" class="list-group-item" style="border: none;"></li>
                                                    <li id="lblliberador1" class="list-group-item" style="border: none;"></li>
                                                    <li id="lblliberador2" class="list-group-item" style="border: none;"></li>
                                                    <li id="lblliberador3" class="list-group-item" style="border: none;"></li>
                                                </ul>
                                           </div>
                                       </div>
                                      <%-- modal Detalle dtFactLiberacion --%>
                                     <div class="col-lg-3" >
                                         <div id="dtFactLiberacion" class="collapse" style="border: none; margin-left: 130px ;" data-parent="#detalle">                                  
                                             <div class="panel-heading"><strong>Factura</strong></div>
                                                    <ul class="list-group" style="border: none;" >
                                                        <li id="lblfolio" class="list-group-item" style="border: none;"></li>
                                                        <li id="lblfechaEmision" class="list-group-item" style="border: none;"></li>
                                                        <li id="lblFechaProba" class="list-group-item" style="border: none;"></li>
                                                    </ul>
                                            </div>
                                        </div>
                                    
                                     <div class="col-lg-3" >
                                         <div id="dtPago" class="collapse" style="border: none; margin-left: 5px ;" data-parent="#detalle">                                  
                                             <div class="panel-heading"><strong>Pago</strong></div>
                                                    <ul class="list-group" style="border: none;">
                                                        <li id="lblfolioFact" class="list-group-item" style="border: none;"></li>
                                                        <li id="lblFechaEmisi" class="list-group-item" style="border: none;"></li>
                                                        <li id="lblPago" class="list-group-item" style="border: none;"></li>
                                                        <li id="lblTipoPag" class="list-group-item" style="border: none;"></li>
                                                    </ul>
                                                </div>
                                           </div>
                                    </div>
                                </div>
                             </div>
                      </div>
               </div>
    
 <%--Abre modal Detalle--%>
<%--Abre modal Detalle--%>
    <script>
           //determina si debe activar el detalle de Solped
           function dtSolpedLiberacion($li, value) {
               if (value === '') {
                   $li.hide();
               }
               else {
                   $li.text(value);
                   $li.show();
               }
           }

           //determina si debe activar el detalle de Orden de Compra
           function dtOCLiberacion($li, value) {
               if (value === '') {
                   $li.hide();
               }
               else {
                   $li.text(value);
                   $li.show();
               }
           }

           //determina si debe activar el detalle de Orden de Compra
           function dtFactLiberacion($li, value) {
               if (value === '') {
                   $li.hide();
               }
               else {
                   $li.text(value);
                   $li.show();
               }
           }

           //determina si debe activar el detalle de Orden de Compra
           function dtPago($li, value) {
               if (value === '') {
                   $li.hide();
               }
               else {
                   $li.text(value);
                   $li.show();
               }
           }
    </script>
    <script type="text/javascript">
           $(document).ready(function () {
               //carga modal al abrirlo
               $('.detalle').on('show.bs.modal', function (event) {
                   var button = $(event.relatedTarget);
                   var idproceso = button.data('idproceso');
                   var idmoni = button.data('idmoni');
                   var idstatus = button.data('idstatus');
                   var idsociedad = button.data('idsociedad');
                   var nrodoc = button.data('nrodoc');
                   var idsolped = button.data('idsolped');
                   var rutliberador = button.data('rutliberador');
                   var fechalib = button.data('fechalib');
                   var horalib = button.data('horalib');
                   var idorden = button.data('idorden');
                   var liberadores = button.data('liberadores')
                   var liberadoresArray = liberadores.split(";");
                   var folio = button.data('folio');
                   var fechaemision = button.data('fechaemision');
                   var fechapago = button.data('fechapago');
                   var tipopago = button.data('tipopago');
                   var fechaproba = button.data('fechaproba');
                   var modal = $(this);

                   //establece título
                   $('#tituloDoc').text('ID: ' + idmoni);
                   
                   
                   //Detalle Solped
                   dtSolpedLiberacion(modal.find('#lblSolped'), "Id:" + idsolped);
                   dtSolpedLiberacion(modal.find('#lblLiberador'), "Rut:" + rutliberador);
                   dtSolpedLiberacion(modal.find('#lblFecha'), "Fecha:" + fechalib);
                   dtSolpedLiberacion(modal.find('#lblHora'), "Hora:" + horalib);

                   //Detalle Orden de Compra
                   dtOCLiberacion(modal.find('#lblidOrden'), "Id:" + idorden);
                   dtOCLiberacion(modal.find('#lblliberador1'), "Liberadores:" + liberadoresArray[0]);
                   dtOCLiberacion(modal.find('#lblliberador2'), "            " + liberadoresArray[1]);
                   dtOCLiberacion(modal.find('#lblliberador3'), "            " + liberadoresArray[2]);


                   //Detalle Factura
                   dtFactLiberacion(modal.find('#lblfolio'), "N°:" + folio);
                   dtFactLiberacion(modal.find('#lblfechaEmision'), "Fecha Emisión:" + fechaemision);
                   dtFactLiberacion(modal.find('#lblFechaProba'), "Fecha Probable:" + fechaproba);


                   //Detalle Pago
                   dtPago(modal.find('#lblfolioFact'), "N° Factura:" + folio);
                   dtPago(modal.find('#lblFechaEmisi'), "Fecha Emisión:" + fechaemision);
                   dtPago(modal.find('#lblPago'), "Pago:" + fechapago);
                   dtPago(modal.find('#lblTipoPag'), "Tipo Pago:" + tipopago);


                   if (idproceso == "S") {
                       switch (idstatus) {
                           case "A":
                               $("#solp").addClass("fa fa-check");
                               $("#solp").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                               break;
                           case "P":
                               $("#solp").addClass("fa fa-refresh");
                               $("#solp").attr("style", "font-size:18px; text-align:center; color:#E2E960;");
                               break;
                           case "R":
                               $("#solp").addClass("fa fa-close");
                               $("#solp").attr("style", "font-size:18px; text-align:center; color:#EC8572;");
                               break;

                       }
                       $("#ocC").hide();
                       $("#fact").hide();
                       $("#pago").hide();
                       $("#lblSol").show();
                   } else if (idproceso == "C") {

                       $("#solp").addClass("fa fa-check");
                       $("#solp").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                       switch (idstatus) {
                           case "A":
                               $("#coti").addClass("fa fa-check");
                               $("#coti").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                               break;
                           case "P":
                               $("#coti").addClass("fa fa-refresh");
                               $("#coti").attr("style", "font-size:18px; text-align:center; color:#E2E960;");
                               break;
                           case "R":
                               $("#coti").addClass("fa fa-close");
                               $("#coti").attr("style", "font-size:18px; text-align:center; color:#EC8572;");
                               break;
                       }
                       $("#ocC").hide();
                       $("#fact").hide();
                       $("#pago").hide();
                   } else if (idproceso == "O") {
                       $("#solp").addClass("fa fa-check");
                       $("#solp").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#coti").addClass("fa fa-check");
                       $("#coti").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       switch (idstatus) {
                           case "A":
                               $("#oc").addClass("fa fa-check");
                               $("#oc").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                               break;
                           case "P":
                               $("#oc").addClass("fa fa-refresh");
                               $("#oc").attr("style", "font-size:18px; text-align:center; color:#E2E960;");
                               break;
                           case "R":
                               $("#oc").addClass("fa fa-close");
                               $("#oc").attr("style", "font-size:18px; text-align:center; color:#EC8572;");
                               break;
                       }
                       $("#mer").hide();
                       $("#fact").hide();
                       $("#pago").hide();
                   } else if (idproceso == "E") {
                       $("#solp").addClass("fa fa-check");
                       $("#solp").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#coti").addClass("fa fa-check");
                       $("#coti").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#oc").addClass("fa fa-check");
                       $("#oc").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                       switch (idstatus) {
                           case "A":
                               $("#mer").addClass("fa fa-check");
                               $("#mer").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                               break;
                           case "P":
                               $("#mer").addClass("fa fa-refresh");
                               $("#mer").attr("style", "font-size:18px; text-align:center; color:#E2E960;");
                               break;
                           case "R":
                               $("#mer").addClass("fa fa-close");
                               $("#mer").attr("style", "font-size:18px; text-align:center; color:#EC8572;");
                               break;
                       }
                       $("#fact").hide();
                       $("#pago").hide();
                   } else if (idproceso == "F") {
                       $("#solp").addClass("fa fa-check");
                       $("#solp").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#coti").addClass("fa fa-check");
                       $("#coti").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#oc").addClass("fa fa-check");
                       $("#oc").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#mer").addClass("fa fa-check");
                       $("#mer").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       switch (idstatus) {
                           case "A":
                               $("#fac").addClass("fa fa-check");
                               $("#fac").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                               break;
                           case "P":
                               $("#fac").addClass("fa fa-refresh");
                               $("#fac").attr("style", "font-size:18px; text-align:center; color:#E2E960;");
                               break;
                           case "R":
                               $("#fac").addClass("fa fa-close");
                               $("#fac").attr("style", "font-size:18px; text-align:center; color:#EC8572;");
                               break;
                       }
                       $("#pago").hide();
                   } else if (idproceso == "P") {
                       $("#solp").addClass("fa fa-check");
                       $("#solp").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#coti").addClass("fa fa-check");
                       $("#coti").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#oc").addClass("fa fa-check");
                       $("#oc").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#mer").addClass("fa fa-check");
                       $("#mer").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       $("#fac").addClass("fa fa-check");
                       $("#fac").attr("style", "font-size:18px; text-align:center; color:#5FB456;");

                       switch (idstatus) {
                           case "A":
                               $("#pago").addClass("fa fa-check");
                               $("#pago").attr("style", "font-size:18px; text-align:center; color:#5FB456;");
                               break;
                           case "P":
                               $("#pago").addClass("fa fa-refresh");
                               $("#pago").attr("style", "font-size:18px; text-align:center; color:#E2E960;");
                               break;
                           case "R":
                               $("#pago").addClass("fa fa-close");
                               $("#pago").attr("style", "font-size:18px; text-align:center; color:#EC8572;");
                               break;
                       }
                       $("#fact").show();
                   }
                 

               });

           });    
        
           $(function () {
               //esconde el div acordeon cuando el modal detalle se cierra o se esconde
               $('.detalle').on('hidden.bs.modal', function (event) {
                   $("#dtSolpedLiberacion").collapse('hide');
                   $("#dtOCLiberacion").collapse('hide');
                   $("#dtFactLiberacion").collapse('hide');
                   $("#dtPago").collapse('hide');
               });
           });

           (function ($) {
               $('#calendarioFechaDesde').datetimepicker({
                   useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                   format: 'DD-MM-YYYY',
                   maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                   //minDate: moment().add(-3, 'month'),
                   showClear: true,
                   ignoreReadonly: false
               });

               $('#calendarioFechaHasta').datetimepicker({
                   useCurrent: false, //Important! See issue #1075
                   format: 'DD-MM-YYYY',
                   maxDate: moment().add(1, 'days'),
                   showClear: true,
                   ignoreReadonly: false
               });

               $('#calendarioFechaDesde').on("dp.change", function (e) {
                   $('#calendarioFechaHasta').data("DateTimePicker").minDate(e.date);
               });

               $('#calendarioFechaHasta').on("dp.change", function (e) {
                   $('#calendarioFechaDesde').data("DateTimePicker").maxDate(e.date);
               });


               $('.fechafiltro').datetimepicker({
                   useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                   format: 'DD-MM-YYYY',
                   minDate: moment().add(-2, 'years'),
                   maxDate: moment().add(2, 'years'),
                   showClear: true,
                   ignoreReadonly: false
               });
           }(jQuery));

    </script>



<script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
<script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
<script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
</asp:Content>
