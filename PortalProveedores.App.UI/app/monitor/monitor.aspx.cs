﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;


namespace PortalProveedores.App.UI.app.monitor
{
    public partial class monitor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!this.IsPostBack)
            {
                BindSociedades();
                BindFiltros();
                //lblrutproveedor.Text = "Rut Proveedor";
                lblFechaDesdeI.Text = "Fecha Desde";
                lblFechaHastaF.Text = "Fecha Hasta";

            }

        }

        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = SociedadBL.GetAllSociedades();

            //Bindea list del formulario de búsqueda
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";
            lstSociedades.DataSource = lstData;
            lstSociedades.DataBind();
        }
        private void BindFiltros()
        {
            ListItem itemTipoDoc = new ListItem("Tipo Documento", "2");
            itemTipoDoc.Selected = true;
            lbFiltro.Items.Add(itemTipoDoc);
            lbFiltro.Items.Add(new ListItem("Rut Proveedor", "3"));
            ListItem itemDesde = new ListItem("Desde", "4");
            itemDesde.Selected = true;
            lbFiltro.Items.Add(itemDesde);
            ListItem itemHasta = new ListItem("Hasta", "5");
            itemHasta.Selected = true;
            lbFiltro.Items.Add(itemHasta);
            lbFiltro.Items.Add(new ListItem("ID", "6"));
            lbFiltro.DataBind();
            
        }
        protected void lbFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BindGridView()
        {
            if (lstSociedades.SelectedItem != null)
            {
                string format;
                string idSociedad = lstSociedades.SelectedItem.Value;
                string tipoDoc = lbTipoDoc.SelectedItem.Value;
                string id= filtroId.Visible ? txtId.Text : string.Empty;
                string rut = filtroRut.Visible ? txtRutProveedor.Text : string.Empty;
                string fechaDesdeR = filtroFechaDesde.Visible ? txtFechaDesde.Text : string.Empty;
                string fechaHastaR = filtroFechaHasta.Visible ? txtFechaHasta.Text : string.Empty;
                string accion = "1";
                rut = rut.Replace(".", string.Empty);
                ViewState["so"] = idSociedad;
                ViewState["r"] = rut; 
                ViewState["fd"] = fechaDesdeR;
                ViewState["fh"] = fechaHastaR;
                //DateTime dateFechaDesde = string.IsNullOrEmpty(fechaDesdeR) ? DateTime.Now : Convert.ToDateTime(fechaDesdeR);
                //DateTime dateFechaHasta = string.IsNullOrEmpty(fechaHastaR) ? DateTime.Now : Convert.ToDateTime(fechaHastaR);

                Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();

                gvMonitorDocumentos.DataSource = MonitorBL.GetMonitor(idSociedad,  rut, fechaDesdeR, fechaHastaR, accion, tipoDoc, id, ref respWS);
                gvMonitorDocumentos.DataBind();

                lblInfo.Visible = true;

                if (gvMonitorDocumentos.Rows.Count > 0)
                {
                    gvMonitorDocumentos.UseAccessibleHeader = true;
                    gvMonitorDocumentos.HeaderRow.TableSection = TableRowSection.TableHeader;
                    gvMonitorDocumentos.Visible = true;
                    string contador = string.Empty;

                    if (gvMonitorDocumentos.Rows.Count == 1)
                        contador = "encontró <strong>1 orden</strong>";
                    else
                        contador = string.Format("encontraron <strong>{0} órdenes</strong>", gvMonitorDocumentos.Rows.Count);

                    lblInfo.Text = string.Format("Se {0} según los parámetros ingresados.", contador);
                    lblInfo.CssClass = "alert-info";
                }
                else
                {
                    lblInfo.Text = respWS.mensaje;
                    lblInfo.Visible = false;
                    lblInfo.CssClass = "alert-danger";
                    //gvorden.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj210", "errorControl(" + lstSociedades.ClientID + ")", true);

                lblInfo.Visible = false;
                gvMonitorDocumentos.Visible = false;
            }
        }
        protected void gvMonitorDocumentos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.MonitorEntity Moni = (Entities.MonitorEntity)r.DataItem;
                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);


                lblColor.Attributes.Add("rel", "tooltip");
                lblColor.Attributes.Add("data-placement", "top");
                lblColor.Attributes.Add("data-toggle", "tooltip");



                switch (Moni.idStatus)
                {
                    case "A":
                        lblColor.CssClass = "fa fa-check";
                        lblColor.Attributes.Add("style", "font-size:18px; text-align:center; color:#5FB456;");
                        break;
                    case "R":

                        lblColor.CssClass = "fa fa-close ";
                        lblColor.Attributes.Add("style", "font-size:18px; text-align:center; color:#E2E960;");
                        break;

                    case "P":
                        lblColor.CssClass = "fa fa-refresh";
                        lblColor.Attributes.Add("style", "font-size:18px; text-align:center; color:#EC8572;");
                        break;
                }

            }
        }
        protected void lbFiltro_SelectedIndexChanged1(object sender, EventArgs e)
        {
            foreach (ListItem item in lbFiltro.Items)
            {
                if (item.Value == "2")
                {
                    filtroTipoDoc.Visible = item.Selected;
                }
                if (item.Value == "3")
                {
                    filtroRut.Visible = item.Selected;
                }
                if (item.Value == "4")
                {
                    filtroFechaDesde.Visible = item.Selected;
                }
                if (item.Value == "5")
                {
                    filtroFechaHasta.Visible = item.Selected;
                }
                if (item.Value == "6")
                {
                    filtroId.Visible = item.Selected;
                }

            }

            ScriptManager.RegisterStartupScript(this, GetType(), "msj219", "cargarElementoFiltros();", true);
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BindGridView();
        }

        
    }
}