﻿<%@ Page Title="Crear proveedor" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="crear.aspx.cs" Inherits="PortalProveedores.App.UI.app.proveedor.crear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
   <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script type="text/javascript">
        $(function() {
            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });
        });
    </script>
    <h2 class="page-header">Crear proveedor</h2>
    <p>Complete todos los campos para crear un proveedor.</p>
    <div class="form-group">
        <div class="col-lg-4">
            <div class="row">
                <div class="form-group">
                    <label class="control-label">* Rut Proveedor</label>
                    <br />
                    <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);"></asp:TextBox>
                    <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="ingresar"></asp:CustomValidator>
                </div>
                <div class="form-group">
                    <label class="control-label">* Nombre</label>
                    <br />
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNombre" runat="server" CssClass="help-block" ErrorMessage="Ingrese nombre..." Display="Dynamic" ControlToValidate="txtNombre" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <label class="control-label">* Región</label>
                    <br />
                    <asp:ListBox ID="lstRegiones" runat="server" CssClass="selectpicker" SelectionMode="Single" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
                    <asp:RequiredFieldValidator ID="rfvRegiones" runat="server" CssClass="help-block" ErrorMessage="Ingrese región..." Display="Dynamic" ControlToValidate="lstRegiones" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <label class="control-label">* Comuna/Ciudad</label>
                    <br />
                    <asp:TextBox ID="txtPoblacion" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPoblacion" runat="server" CssClass="help-block" ErrorMessage="Ingrese población..." Display="Dynamic" ControlToValidate="txtPoblacion" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <label class="control-label">* Calle</label>
                    <br />
                    <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCalle" runat="server" CssClass="help-block" ErrorMessage="Ingrese calle..." Display="Dynamic" ControlToValidate="txtCalle" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                 <div class="form-group">
                    <label class="control-label">* Correo</label>
                    <br />
                    <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control input-sm" MaxLength="60" autocomplete="off" placeholder="Ingrese..."  ></asp:TextBox>
				    <asp:CustomValidator ID="cvCorreo" runat="server" CssClass="help-block" ControlToValidate="txtCorreo" ErrorMessage="Ingrese un <strong>CORREO</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorCorreoValido" ValidateEmptyText="true" ValidationGroup="ingresar"></asp:CustomValidator>
                </div>
                <div class="form-group">
                    <label class="control-label">* Sociedad</label>
                     <br />
                    <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                    <asp:RequiredFieldValidator ID="rfvSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstSociedades" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>

                <div class="pull-right">
                    <button id="btnAgregar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnAgregar_ServerClick" validationgroup="ingresar">
                        <%--validationgroup="buscar"--%>
                        <i class="fa fa-plus"></i>
                        &nbsp;&nbsp;Agregar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
</asp:Content>