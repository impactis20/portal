﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;

namespace PortalProveedores.App.UI.app.proveedor
{
    public partial class crear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GetRegiones();
                BindSociedades();
            }
        }
        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = SociedadBL.GetAllSociedades();

            //Bindea list del formulario de búsqueda
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";
            lstSociedades.DataSource = lstData;
            lstSociedades.DataBind();
        }

        private void GetRegiones()
        {
            List<Entities.RegionEntity> lst = RegionBL.GetRegion();

            //Bindea list del formulario de búsqueda
            lstRegiones.DataTextField = "descripcion";
            lstRegiones.DataValueField = "id";
            lstRegiones.DataSource = lst;
            lstRegiones.DataBind();
        }

        protected void btnAgregar_ServerClick(object sender, EventArgs e)
        {
            string rut = txtRutProveedor.Text.Trim();
           
            if (App_Code.Util.RutValido(rut))
            {
                    string nombre = txtNombre.Text.Trim();
                    string region = lstRegiones.SelectedItem.Value;
                    string poblacion = txtPoblacion.Text.Trim();
                    string calle = txtCalle.Text.Trim();
                    string idSociedad = lstSociedades.SelectedItem.Value;
                    string usuario = App_Code.Seguridad.GetRutUsuarioLogueado();
                    string correo = txtCorreo.Text.Trim();
                    Entities.ResponseWSEntity resp = DocumentoBL.AddProveedor(rut, nombre, region, poblacion, calle, idSociedad, correo);


                    if (resp.codigo == "0")
                    {
                        txtNombre.Text = string.Empty;
                        txtRutProveedor.Text = string.Empty;
                        txtCalle.Text = string.Empty;
                        txtPoblacion.Text = string.Empty;
                        lstRegiones.ClearSelection();
                        lstSociedades.SelectedIndex = -1;
                        txtCorreo.Text = string.Empty;
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), "msj7", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
            }
        }
    }

}