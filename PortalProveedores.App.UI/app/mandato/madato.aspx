﻿<%@ Page Title="Mandato" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="madato.aspx.cs" Inherits="PortalProveedores.App.UI.app.mandato.madato" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
    <%--<link href="../css/jquery.dataTables.min.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>


<script type="text/javascript">
    $(function () {
       //inicia combobox por defecto
       $('.selectpicker').selectpicker({
       title: 'Seleccione...',
       showTick: true
       });
          });
</script>
 <script>
      function crearMandato()
    {
        $("#editarMandato").hide();
    }

  </script>
  <script>
      function VerMandato() {
          $('[id*=lblcuenta]').prop('disabled', true);
          $('[id*=lstcorreoA]').prop('disabled', true);
          $('[id*=lsttipoCuentaE]').prop('disabled', true);
          $('[id*=lstBancosE]').prop('disabled', true);
          $("#crearMandato").hide();
          $("#BotonE").show();
          $("#btnCerrar").hide();
          $("#txtcorreoNu").hide();
          $("#<%= BotonG.ClientID %>").hide();
     }
</script>
<script>
      function editarMandato() {
          $("#crearMandato").hide();
          $('[id*=lblcuenta]').prop('disabled', false);
          $('[id*=lsttipoCuentaE]').prop('disabled', false);
          $('[id*=lsttipoCuentaE]').selectpicker('refresh');
          $('[id*=lstBancosE]').prop('disabled', false);
          $('[id*=lstBancosE]').selectpicker('refresh');
          $("#BotonE").hide();
          $("#<%= BotonG.ClientID %>").show();
          $("#btnCerrar").show();
          $("#txtcorreoNu").show();
      }

  </script>
<script>
      function EditarBanco() {

          $('[id*=lblcuenta]').val("");
          $('[id*=lsttipoCuentaE]').val('').selectpicker('refresh');

          }

</script>

<div id="crearMandato">
<h2 class="page-header">Ingreso Mandato</h2>
    <p>Estimado Proveedor, favor ingrese sus datos bancarios y correo para que le   <br/> notifiquemos de futuros pagos.</p>
                         <div class="row">
                           <div class="form-group"> 
                             <div class="col-lg-6">
                                <div class="form-group"> 
                                    <label for="lstBancos" class="control-label">Banco</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Seleccione un banco">
                                        <asp:ListBox ID="lstBancos" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvBancos" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstBancos" SetFocusOnError="True" ValidationGroup="crear" />
                                 </div>
                                 <div class="form-group">
                                    <label for="lsttipoCuenta" class="control-label">Tipo de cuenta</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Seleccione un tipo de cuenta">
                                        <asp:ListBox ID="lsttipoCuenta" runat="server" CssClass="selectpicker" SelectionMode="Single" data-selected-text-format="count > 1" data-size="5" data-width="100%">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvtipoCuenta" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lsttipoCuenta" SetFocusOnError="True" ValidationGroup="crear" />
                                </div>
                                <div class="form-group">
                                    <label for="txtnroCuenta" class="control-label">N° Cuenta</label>
                                    <asp:TextBox ID="lblcuentac" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="cvnroCuenta" runat="server" CssClass="help-block" ErrorMessage="Ingrese una cuenta igual o menor a 15 caracteres..." Display="Dynamic" ControlToValidate="lblcuentac" SetFocusOnError="True" ClientValidationFunction="customValidatorNumeroCuenta" ValidationGroup="crear" />
                                   <%-- <asp:RequiredFieldValidator ID="rfvnroCuenta" runat="server" CssClass="help-block" ErrorMessage="Ingrese Nro de cuenta..." Display="Dynamic" ControlToValidate="lblcuentac" SetFocusOnError="True" ValidationGroup="crear" />--%>
                                </div>
                                 <div class="row">
                                 <div class="col-lg-8">
                                 <div class="form-group">
                                    <label for="txtcorreo" class="control-label">Correo</label>
                                    <asp:TextBox ID="txtcorreo" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="cvcorreo" runat="server" CssClass="help-block" ErrorMessage="Ingrese un correo válido..." Display="Dynamic" ControlToValidate="txtcorreo" SetFocusOnError="True" ClientValidationFunction="customValidatorCorreoValido" ValidationGroup="crear" />
                                </div>
                                </div>
                               <div class="col-lg-4">
                               <div class="form-group">
                                  <button id="btnAgregar" runat="server" type="button" class="btn btn-primary btn btn-form-line"  onserverclick="btnagregarCorreo_ServerClick" validationgroup="crear">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Agregar Correo
                                  </button> 
                                </div>
                                </div>
                                </div>
                                 <div class="form-group">
                                    <label for="Listcc" class="control-label"></label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Correos a agregar">
                                        <asp:ListBox ID="Listcc" runat="server"  data-selected-text-format="count > 1" data-width="100%" style="width: 509px;">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvListcc" runat="server" CssClass="help-block" ErrorMessage="Agregue al menos un correo electrónico" Display="Dynamic" ControlToValidate="Listcc" SetFocusOnError="True" validationgroup="crea"/>
                                </div>    
                               <div class="form-group">
                             <button id="btnGuardar" runat="server" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Guardando mandato..." onserverclick="btncrearMandato_ServerClick" validationgroup="crear">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
                            </button>
                     </div>
                </div>
            </div>
      </div>
</div>

<div id="editarMandato">
<h2 class="page-header">Modificación Mandato</h2>
    <p>Estimado Proveedor, favor ingrese sus datos bancarios.</p>
           <div class="row">
                        <div class="form-group"> 
                           <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="lstBancosE" class="control-label">Banco</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Seleccione un banco">
                                        <asp:ListBox ID="lstBancosE" runat="server" CssClass="selectpicker" onChange="EditarBanco();" data-selected-text-format="count > 1" data-width="100%">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvtBancosE" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstBancosE" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                                <div class="form-group">
                                    <label for="lsttipoCuentaE" class="control-label">Tipo de cuenta</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Seleccione un tipo de cuenta">
                                        <asp:ListBox ID="lsttipoCuentaE" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvtipoCuentaE" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lsttipoCuentaE" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            <div class="form-group">
                                    <label for="txtnroCuentaE" class="control-label">N° Cuenta</label>
                                    <asp:TextBox ID="lblcuenta" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="rfvnroCuentaE" runat="server" CssClass="help-block" ErrorMessage="Ingrese Nro de cuenta..." Display="Dynamic" ControlToValidate="lblcuenta"  ClientValidationFunction="customValidatorNumeroCuenta" SetFocusOnError="True" ValidationGroup="editar" />--%>
                                    <asp:CustomValidator ID="cvnroCuentaE" runat="server" CssClass="help-block" ErrorMessage="Ingrese una cuenta igual o menor a 15 caracteres..." Display="Dynamic" ControlToValidate="lblcuenta" SetFocusOnError="True" ClientValidationFunction="customValidatorNumeroCuenta" ValidationGroup="editar" />
                               </div>
                               <div class="form-group" id= "txtcorreoNu">
                               <div  class="row">
                                 <div class="col-lg-8">
                               <div  class="form-group">
							    <label class="control-label"></label>
								<br />
								<asp:TextBox ID="txtcorreoN" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese correo a ser agregado..."></asp:TextBox>
								<asp:CustomValidator ID="cvcorreoN" runat="server" CssClass="help-block" ErrorMessage="Ingrese un correo válido..." Display="Dynamic" ControlToValidate="txtcorreoN" SetFocusOnError="True" ClientValidationFunction="customValidatorCorreoValido" ValidationGroup="correo" />
							 </div>
                             </div>
                               <div class="col-lg-4">
                               <div class="form-group">
                                  <button id="Button1" runat="server" type="button" class="btn btn-primary btn btn-form-line"  onserverclick="btnagregarCorreoEditar_ServerClick" validationgroup="correo">
                                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Agregar Correo
                                  </button> 
                                </div>
                                </div>
                                </div>
                               <div class="form-group">
                                    <label for="ListccE" class="control-label">Correos a agregar</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Correos a ser agregados al mandato">
                                        <asp:ListBox ID="ListccE" runat="server"  data-selected-text-format="count > 1" data-width="100%" style="width: 509px;">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvListccE" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="ListccE" SetFocusOnError="True" validationgroup="edi"/>
                                </div>
                                </div>
                                  <div class="form-group">
                                     <p>Los pagos serán notificados a los siguientes correos.</p>
                                      <label for="lstcorreoA" class="control-label">Correos</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Correos que posee actualmente el mandato">
                                        <asp:ListBox ID="lstcorreoA" runat="server"  data-selected-text-format="count > 1" data-width="100%" style="width: 509px;">
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvlstcorreoA" runat="server" CssClass="help-block"  Display="Dynamic" ControlToValidate="lstcorreoA" SetFocusOnError="True" ValidationGroup="edi"/>
                                </div>
                               <div>
                            
                               </div> 
                        <button id="BotonE" type="button"  onclick="editarMandato();" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Habilitando formulario...">
                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>&nbsp;&nbsp;Habilitar edición
                        </button>
                        <button id="BotonG" runat="server" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Guardando mandato..." onserverclick="btnEditarMandato_ServerClick" validationgroup="editar">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
                          </button>  
                        <button id="btnCerrar" type="button"  onclick="VerMandato();" class="btn btn-primary" >Cancelar</button>
                       <asp:HyperLink ID="doc"  runat="server" target="_blank" Text="<i class='fa fa-file-pdf-o fa-2x pull-right'  aria-hidden='true'></i>"></asp:HyperLink>
                       </div>     
                 </div>
        </div>
     </div>


  
    <script src='<%= ResolveUrl("~/js/app/textbox-formato-nucuenta.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
   
</asp:Content>
