﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PortalProveedores.BL;
using System.Net;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using PortalProveedores.Entities;
using System.Globalization;
using System.Threading;



namespace PortalProveedores.App.UI.app.mandato
{
    public partial class madato : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
                string accion = "2";
                Entities.ResponseWSEntity resp = new Entities.ResponseWSEntity();
                MandatoEntity mandato = MandatoBL.GetMandato(rut, accion, ref resp);
                string codigoc = resp.codigo;

                if (resp.codigo == "0")
                {
                    GetMandato();
                }
                else
                {
                    BindBanco();
                    GetCuenta();
                    ClientScript.RegisterStartupScript(this.GetType(), "myScript", "crearMandato();", true);
                }
            }
        }
        private void GetCuenta()
        {
            List<Entities.MandatoEntity.Tcuenta> lst = MandatoBL.GetNumero();

            lsttipoCuenta.DataValueField = "id";
            lsttipoCuenta.DataValueField = "nombre";
            lsttipoCuenta.DataSource = lst;
            lsttipoCuenta.DataBind();
        }
        private void BindBanco()
        {
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.MandatoEntity.Banco> lstData = MandatoBL.GetListBanco(ref respWS);

            lstBancos.Items.Clear();

            foreach (Entities.MandatoEntity.Banco banco in lstData)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem(banco.id, banco.descripcion);
                lstBancos.Items.Add(item);
            }
            lstBancos.DataBind();
        }
        private void GetMandato()
        {
            txtcorreoN.Text = string.Empty;
            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            string accion = "2";
            Entities.ResponseWSEntity resp = new Entities.ResponseWSEntity();
            MandatoEntity mandato = MandatoBL.GetMandato(rut, accion, ref resp);
            lblcuenta.Text = mandato.cuenta;

            string correo = mandato.correo;
            string[] correoA = correo.Split(';');
            List<string> list1 = new List<string>(correoA);
            lstcorreoA.DataSource = list1;
            lstcorreoA.DataBind();

            string codCuenta = mandato.tipoCuenta.nombre;
            string codBanco = mandato.desBanco;
            string rutapdfP = mandato.ruta;
            HyperLink urlDoc = doc.Parent.FindControl("doc") as HyperLink;
            urlDoc.NavigateUrl = string.Format("archivo.ashx?id={0}", App_Code.Util.Encriptar(rutapdfP));

            List<Entities.MandatoEntity.Tcuenta> lst = MandatoBL.GetNumero();

            lsttipoCuentaE.DataValueField = "id";
            lsttipoCuentaE.DataValueField = "nombre";
            lsttipoCuentaE.DataSource = lst;
            lsttipoCuentaE.DataBind();
            lsttipoCuentaE.SelectedValue = codCuenta;

            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.MandatoEntity.Banco> lstData = MandatoBL.GetListBanco(ref respWS);

            lstBancosE.Items.Clear();

            foreach (Entities.MandatoEntity.Banco banco in lstData)
            {
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem(banco.id, banco.descripcion);
                lstBancosE.Items.Add(item);
            }
            lstBancosE.SelectedValue = codBanco;
            lstBancosE.DataBind();

            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "VerMandato();", true);
        }


        protected void btnagregarCorreo_ServerClick(object sender, EventArgs e)
        {

            lstBancos.Text = lstBancos.SelectedItem.Value;
            lsttipoCuenta.Text = lsttipoCuenta.SelectedItem.Value;
            lblcuentac.Text = lblcuentac.Text.Trim();

            List<string> listAE = new List<string>();
            if (Listcc.Items.Count == 0)
            {
                listAE.Add(txtcorreo.Text.Trim());
            }
            else
            {
                listAE.Add(txtcorreo.Text.Trim());
                for (int i = 0; i < Listcc.Items.Count; i++)
                {
                    listAE.Add(Listcc.Items[i].Value.ToString());
                }
            }
            Listcc.DataSource = listAE;
            Listcc.DataBind();
            txtcorreo.Text = string.Empty;
            //Listcc.Visible = true;
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "crearMandato();", true);
        }
        protected void btncrearMandato_ServerClick(object sender, EventArgs e)
        {

            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            rut = rut.Replace(".", string.Empty);
            string ruti = rut.Replace("-", string.Empty);

            //crea los caracteres alfabéticos de la contraseña
            Random aleatorio2 = new Random();
            string letr = "";
            string[] vals2 = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i <= 1; i++)
            {
                letr = letr + vals2[aleatorio2.Next(vals2.GetUpperBound(0) + 1)];
            }

            string letra = letr.Substring(0, 1);
            string referencia = letra + ruti;
            string banco = lstBancos.SelectedItem.Value;
            string bancoPDF = lstBancos.SelectedItem.Text;
            string cuenta = lblcuentac.Text.Trim();
            string tipoCuenta = lsttipoCuenta.SelectedItem.Value;
            //string correo = txtcorreo.Text.Trim();
            string exte = "pdf";
            string ruta = string.Format("{0}{1}{2}.{3}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp2_" + ConfigurationManager.AppSettings["ambiente"]], referencia, exte);
            string accion = "1";
            string correoNue = "";
            int cantCorreos = 0;


            for (int i = 0; i < Listcc.Items.Count; i++)
            {
                String correo = Listcc.Items[i].Value.ToString();
                Entities.ResponseWSEntity resp = MandatoBL.AddMandato(rut, banco, cuenta, tipoCuenta, referencia, correo, ruta, accion);

                if (resp.codigo == "0")
                {
                    if (cantCorreos < 3)
                    {
                        if (cantCorreos == 0)
                        { correoNue = Listcc.Items[i].Value.ToString(); }
                        else
                        { correoNue = correoNue + "\n" + Listcc.Items[i].Value.ToString(); }
                    }
                    cantCorreos++;
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myScript", "crearMandato();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj3", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
                }
            }
            string nombre = App_Code.Seguridad.GetNombreUsuarioLogueado();
            string fecha1 = DateTime.Now.ToString("dd/MM/yyyy");
            string fecha2 = fecha1.Replace("-", ".");
            string rutepm = Server.MapPath("temp/");
            string rutepmar = rutepm + referencia + ".pdf";

            //Creación del PDF
            DateTime dt = DateTime.Now;
            string fechaD = dt.ToString("D", CultureInfo.CreateSpecificCulture("es-MX"));

            FileStream fs = new FileStream(rutepmar, FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document(PageSize.LETTER, 50f, 50f, 50f, 50f);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.AddTitle(referencia);
            doc.AddCreator("Portal de Proveedores Colmena");
            doc.Open();

            iTextSharp.text.Font titulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font subtitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font titulot = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font subtitulo2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            //iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("C:\\Users\\Johana\\Documents\\Visual Studio 2017\\Projects\\PortalProveedoresColmena_Johanaultimo\\PortalProveedores.App.UI\\img\\logo-colmena-salud.jpg");
            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("C:\\inetpub\\wwwroot\\PortalProveedores\\img\\logo-colmena-salud.jpg");
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_LEFT;
            float percentage = 0.0f;
            percentage = 150 / imagen.Width;
            imagen.ScalePercent(percentage * 100);
            doc.Add(imagen);
            // iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("C:\\Users\\Johana\\Desktop\\VersionesPortal\\Portal Proveedores Colmena Felipe12102017\\PortalProveedores.App.UI\\img\\check box.PNG");
            //  iTextSharp.text.Image imagen3 = iTextSharp.text.Image.GetInstance("C:\\Users\\Johana\\Desktop\\VersionesPortal\\Portal Proveedores Colmena Felipe12102017\\PortalProveedores.App.UI\\img\\box.PNG");
            iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("C:\\inetpub\\wwwroot\\PortalProveedores\\img\\check box.PNG");
            iTextSharp.text.Image imagen3 = iTextSharp.text.Image.GetInstance("C:\\inetpub\\wwwroot\\PortalProveedores\\img\\box.PNG");

            imagen2.ScalePercent(percentage * 5);
            imagen3.ScalePercent(percentage * 5);

            Paragraph p1 = new Paragraph("Solicitud y Autorización", subtitulo);
            p1.Alignment = Element.ALIGN_RIGHT;
            doc.Add(p1);

            Paragraph pa1 = new Paragraph("Servicio de Depósito Automático", subtitulo);
            pa1.Alignment = Element.ALIGN_RIGHT;
            doc.Add(pa1);
            doc.Add(Chunk.NEWLINE);

            iTextSharp.text.pdf.draw.LineSeparator line1 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            doc.Add(new Chunk(line1));

            Paragraph p = new Paragraph("SOLICITUD Y AUTORIZACIÓN SERVICIO DE DEPÓSITO AUTOMÁTICO", titulo);
            p.Alignment = Element.ALIGN_CENTER;
            doc.Add(p);
            doc.Add(Chunk.NEWLINE);

            Paragraph pa2 = new Paragraph("Fecha Suscripción: " + fecha2, subtitulo);
            pa2.Alignment = Element.ALIGN_RIGHT;
            doc.Add(pa2);
            doc.Add(Chunk.NEWLINE);

            Paragraph p4 = new Paragraph("Solicito y autorizo a las empresas del grupo Colmena, abonar a la cuenta que se indica a continuación cualquier pago que se presente por cualquier concepto.", subtitulo);
            p4.Alignment = Element.ALIGN_JUSTIFIED;
            doc.Add(p4);
            doc.Add(Chunk.NEWLINE);

            Paragraph pa3 = new Paragraph("Razon Social: " + nombre, subtitulo);
            pa3.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa3);

            PdfPTable table = new PdfPTable(new float[] { 2, 2 });
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.WidthPercentage = 100;
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase("RUT: " + rut, subtitulo));
            table.AddCell(new Phrase("Banco: " + bancoPDF, subtitulo));
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase("Tipo de cuenta:", subtitulo));
            table.AddCell(new Phrase("Nº Cuenta: " + cuenta, subtitulo));
            doc.Add(table);


            if (tipoCuenta == "Cuenta Corriente")
            {
                Paragraph pc1 = new Paragraph();
                pc1.Add(new Phrase("  ", subtitulo));
                pc1.Add(new Chunk(imagen2, 0, 0));
                pc1.Add(new Phrase("  Cuenta Corriente", subtitulo));
                doc.Add(pc1);
                Paragraph pc2 = new Paragraph();
                pc2.Add(new Phrase("  ", subtitulo));
                pc2.Add(new Chunk(imagen3, 0, 0));
                pc2.Add(new Phrase("  Cuenta Vista", subtitulo));
                doc.Add(pc2);
                Paragraph pc3 = new Paragraph();
                pc3.Add(new Phrase("  ", subtitulo));
                pc3.Add(new Chunk(imagen3, 0, 0));
                pc3.Add(new Phrase("  Cuenta Ahorro", subtitulo));
                doc.Add(pc3);
            }
            if (tipoCuenta == "Cuenta Vista")
            {
                Paragraph pv1 = new Paragraph();
                pv1.Add(new Phrase("  ", subtitulo));
                pv1.Add(new Chunk(imagen3, 0, 0));
                pv1.Add(new Phrase("  Cuenta Corriente", subtitulo));
                doc.Add(pv1);
                Paragraph pv2 = new Paragraph();
                pv2.Add(new Phrase("  ", subtitulo));
                pv2.Add(new Chunk(imagen2, 0, 0));
                pv2.Add(new Phrase("  Cuenta Vista", subtitulo));
                doc.Add(pv2);
                Paragraph pv3 = new Paragraph();
                pv3.Add(new Phrase("  ", subtitulo));
                pv3.Add(new Chunk(imagen3, 0, 0));
                pv3.Add(new Phrase("  Cuenta Ahorro", subtitulo));
                doc.Add(pv3);
            }
            if (tipoCuenta == "Cuenta Ahorro")
            {
                Paragraph ph1 = new Paragraph();
                ph1.Add(new Phrase("  ", subtitulo));
                ph1.Add(new Chunk(imagen3, 0, 0));
                ph1.Add(new Phrase("  Cuenta Corriente", subtitulo));
                doc.Add(ph1);
                Paragraph ph2 = new Paragraph();
                ph2.Add(new Phrase("  ", subtitulo));
                ph2.Add(new Chunk(imagen3, 0, 0));
                ph2.Add(new Phrase("  Cuenta Vista", subtitulo));
                doc.Add(ph2);
                Paragraph ph3 = new Paragraph();
                ph3.Add(new Phrase("  ", subtitulo));
                ph3.Add(new Chunk(imagen2, 0, 0));
                ph3.Add(new Phrase("  Cuenta Ahorro", subtitulo));
                doc.Add(ph3);
            }

            doc.Add(Chunk.NEWLINE);
            PdfPTable table2 = new PdfPTable(new float[] { 2, 2 });
            table2.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table2.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table2.DefaultCell.Border = Rectangle.NO_BORDER;
            table2.WidthPercentage = 100;
            table2.AddCell(new Phrase("Correo notificación: ", subtitulo));
            table2.AddCell(new Phrase(" ", subtitulo));
            doc.Add(table2);
            Paragraph pc = new Paragraph();
            pc.Add(new Phrase(correoNue.ToLower(), subtitulo));
            doc.Add(pc);
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            Paragraph pa5 = new Paragraph("INFORMACIÓN DE UTILIDAD", subtitulo);
            pa5.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa5);
            iTextSharp.text.pdf.draw.LineSeparator line2 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            doc.Add(new Chunk(line2));
            Paragraph pa6 = new Paragraph("Si por razones ajenas a las empresas del Grupo Colmena , el depósito es rechazado por el Banco. Se procederá a emitir un cheque por el monto del abono rechazado, el cual estará disponible en el Departamento de Tesorería y Finanzas ubicada en Los Militares 4777, oficina 501, Las Condes para COLMENA GOLDEN CROSS S.A.o en Luis Thayer Ojeda 166, piso 6, Providencia para COLMENA COMPAÑÍA DE SEGUROS DE VIDA S.A. , en un plazo de 5 días hábiles desde la fecha original de rechazo.", subtitulo2);
            pa6.Alignment = Element.ALIGN_JUSTIFIED;
            doc.Add(pa6);

            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);

            Paragraph pa7 = new Paragraph("Santiago, " + fechaD, subtitulo);
            pa7.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa7);
            doc.Add(Chunk.NEWLINE);

            iTextSharp.text.pdf.draw.LineSeparator line3 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            doc.Add(new Chunk(line3));
            Paragraph pa8 = new Paragraph("Folio:  " + referencia + "                                                          Solicitud y autorización Servicio de Depósito Automático", subtitulo);
            pa8.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa8);


            doc.Close();
            writer.Close();
            //Fin creacion del PDF
            try
            {
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(ruta);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                FileStream stream = File.OpenRead(rutepmar);
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                stream.Close();
                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Flush();
                reqStream.Close();
            }
            catch
            {

            }

            GetMandato();
            //ClientScript.RegisterStartupScript(this.GetType(), "myScript", "VerMandato();", true);
        }

        protected void btnagregarCorreoEditar_ServerClick(object sender, EventArgs e)
        {

            lstBancosE.Text = lstBancosE.SelectedItem.Value;
            lsttipoCuentaE.Text = lsttipoCuentaE.SelectedItem.Value;
            lblcuenta.Text = lblcuenta.Text.Trim();
            List<string> listE = new List<string>();
            if (ListccE.Items.Count == 0)
            {
                listE.Add(txtcorreoN.Text.Trim());
            }
            else
            {
                listE.Add(txtcorreoN.Text.Trim());
                for (int i = 0; i < ListccE.Items.Count; i++)
                {
                    listE.Add(ListccE.Items[i].Value.ToString());
                }
            }
            ListccE.DataSource = listE;
            ListccE.DataBind();
            txtcorreoN.Text = string.Empty;
            //Listcc.Visible = true;
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "editarMandato();", true);
        }
        protected void btnEditarMandato_ServerClick(object sender, EventArgs e)
        {
            string rut = App_Code.Seguridad.GetRutUsuarioLogueado();
            rut = rut.Replace(".", string.Empty);
            string ruti = rut.Replace("-", string.Empty);

            //crea los caracteres alfabéticos de la contraseña
            Random aleatorio2 = new Random();
            string letr = "";
            string[] vals2 = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i <= 1; i++)
            {
                letr = letr + vals2[aleatorio2.Next(vals2.GetUpperBound(0) + 1)];
            }

            string letra = letr.Substring(0, 1);
            string referencia = letra + ruti;
            string cuenta = lblcuenta.Text.Trim();
            string tipoCuenta = lsttipoCuentaE.SelectedItem.Value;
            string banco = lstBancosE.SelectedItem.Value;
            string bancoPDF = lstBancosE.SelectedItem.Text;
            string exte = "pdf";
            string ruta = string.Format("{0}{1}{2}.{3}", ConfigurationManager.AppSettings["ftp"], ConfigurationManager.AppSettings["carpeta_ftp2_" + ConfigurationManager.AppSettings["ambiente"]], referencia, exte);
            string accion = "1";
            string correoPDF;
            string correo = "";
            List<string> listcorreoNue = new List<string>();
            listcorreoNue.Clear();


            //Envio del mandato a SAP para modificación
            if (ListccE.Items.Count == 0)
            {
                Entities.ResponseWSEntity resp = MandatoBL.AddMandato(rut, banco, cuenta, tipoCuenta, referencia, correo, ruta, accion);
                if (resp.codigo != "0")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myScript", "VerMandato();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj5", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);

                }
                else
                {

                    ClientScript.RegisterStartupScript(this.GetType(), "myScript", "VerMandato();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "msj6", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
                }
            }
            else
            {
                for (int i = 0; i < ListccE.Items.Count; i++)
                {
                    correo = ListccE.Items[i].Value.ToString();
                    Entities.ResponseWSEntity resp = MandatoBL.AddMandato(rut, banco, cuenta, tipoCuenta, referencia, correo, ruta, accion);
                    if (resp.codigo != "0")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "VerMandato();", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "msj5", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
                    }
                    else
                    {
                        listcorreoNue.Add(correo);
                    }
                }
            }

            //Correos anteriores del mandato
            for (int i = 0; i < lstcorreoA.Items.Count; i++)
            {
                listcorreoNue.Add(lstcorreoA.Items[i].Value.ToString());
            }
            correoPDF = listcorreoNue[0] + "\n" + listcorreoNue[1] + "\n" + listcorreoNue[2];

            String[] substrings = correoPDF.Split('\n');
            string correoPDFF = substrings[0] + "\n  " + substrings[1] + "\n  " + substrings[2];
            string nombre = App_Code.Seguridad.GetNombreUsuarioLogueado();
            string fecha1 = DateTime.Now.ToString("dd/MM/yyyy");
            string fecha2 = fecha1.Replace("-", ".");
            string rutepm = Server.MapPath("temp/");
            string rutepmar = rutepm + referencia + ".pdf";

            //Creación del PDF
            DateTime dt = DateTime.Now;
            string fechaD = dt.ToString("D", CultureInfo.CreateSpecificCulture("es-MX"));

            FileStream fs = new FileStream(rutepmar, FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document(PageSize.LETTER, 50f, 50f, 50f, 50f);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.AddTitle(referencia);
            doc.AddCreator("Portal de Proveedores Colmena");
            doc.Open();

            iTextSharp.text.Font titulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font subtitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font titulot = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font subtitulo2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            //iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("C:\\Users\\Johana\\Desktop\\VersionesPortal\\PPC 21112017 version paralela documento\\PortalProveedores.App.UI\\img\\logo-colmena-salud.jpg");
            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("C:\\inetpub\\wwwroot\\PortalProveedores\\img\\logo-colmena-salud.jpg");
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_LEFT;
            float percentage = 0.0f;
            percentage = 150 / imagen.Width;
            imagen.ScalePercent(percentage * 100);
            doc.Add(imagen);
            // iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("C:\\Users\\Johana\\Desktop\\VersionesPortal\\PPC 21112017 version paralela documento\\PortalProveedores.App.UI\\img\\check box.PNG");
            // iTextSharp.text.Image imagen3 = iTextSharp.text.Image.GetInstance("C:\\Users\\Johana\\Desktop\\VersionesPortal\\PPC 21112017 version paralela documento\\PortalProveedores.App.UI\\img\\box.PNG");
            iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("C:\\inetpub\\wwwroot\\PortalProveedores\\img\\check box.PNG");
            iTextSharp.text.Image imagen3 = iTextSharp.text.Image.GetInstance("C:\\inetpub\\wwwroot\\PortalProveedores\\img\\box.PNG");

            imagen2.ScalePercent(percentage * 5);
            imagen3.ScalePercent(percentage * 5);

            Paragraph p1 = new Paragraph("Solicitud y Autorización", subtitulo);
            p1.Alignment = Element.ALIGN_RIGHT;
            doc.Add(p1);

            Paragraph pa1 = new Paragraph("Servicio de Depósito Automático", subtitulo);
            pa1.Alignment = Element.ALIGN_RIGHT;
            doc.Add(pa1);
            doc.Add(Chunk.NEWLINE);

            iTextSharp.text.pdf.draw.LineSeparator line1 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            doc.Add(new Chunk(line1));

            Paragraph p = new Paragraph("SOLICITUD Y AUTORIZACIÓN SERVICIO DE DEPÓSITO AUTOMÁTICO", titulo);
            p.Alignment = Element.ALIGN_CENTER;
            doc.Add(p);
            doc.Add(Chunk.NEWLINE);

            Paragraph pa2 = new Paragraph("Fecha Suscripción: " + fecha2, subtitulo);
            pa2.Alignment = Element.ALIGN_RIGHT;
            doc.Add(pa2);
            doc.Add(Chunk.NEWLINE);

            Paragraph p4 = new Paragraph("Solicito y autorizo a las empresas del grupo Colmena, abonar a la cuenta que se indica a continuación cualquier pago que se presente por cualquier concepto.", subtitulo);
            p4.Alignment = Element.ALIGN_JUSTIFIED;
            doc.Add(p4);
            doc.Add(Chunk.NEWLINE);

            Paragraph pa3 = new Paragraph("Razon Social: " + nombre, subtitulo);
            pa3.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa3);

            PdfPTable table = new PdfPTable(new float[] { 2, 2 });
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.WidthPercentage = 100;
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase("RUT: " + rut, subtitulo));
            table.AddCell(new Phrase("Banco: " + bancoPDF, subtitulo));
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase(" ", subtitulo));
            table.AddCell(new Phrase("Tipo de cuenta:", subtitulo));
            table.AddCell(new Phrase("Nº Cuenta: " + cuenta, subtitulo));
            doc.Add(table);

            if (tipoCuenta == "Cuenta Corriente")
            {
                Paragraph pc1 = new Paragraph();
                pc1.Add(new Phrase("  ", subtitulo));
                pc1.Add(new Chunk(imagen2, 0, 0));
                pc1.Add(new Phrase("  Cuenta Corriente", subtitulo));
                doc.Add(pc1);
                Paragraph pc2 = new Paragraph();
                pc2.Add(new Phrase("  ", subtitulo));
                pc2.Add(new Chunk(imagen3, 0, 0));
                pc2.Add(new Phrase("  Cuenta Vista", subtitulo));
                doc.Add(pc2);
                Paragraph pc3 = new Paragraph();
                pc3.Add(new Phrase("  ", subtitulo));
                pc3.Add(new Chunk(imagen3, 0, 0));
                pc3.Add(new Phrase("  Cuenta Ahorro", subtitulo));
                doc.Add(pc3);
            }
            if (tipoCuenta == "Cuenta Vista")
            {
                Paragraph pv1 = new Paragraph();
                pv1.Add(new Phrase("  ", subtitulo));
                pv1.Add(new Chunk(imagen3, 0, 0));
                pv1.Add(new Phrase("  Cuenta Corriente", subtitulo));
                doc.Add(pv1);
                Paragraph pv2 = new Paragraph();
                pv2.Add(new Phrase("  ", subtitulo));
                pv2.Add(new Chunk(imagen2, 0, 0));
                pv2.Add(new Phrase("  Cuenta Vista", subtitulo));
                doc.Add(pv2);
                Paragraph pv3 = new Paragraph();
                pv3.Add(new Phrase("  ", subtitulo));
                pv3.Add(new Chunk(imagen3, 0, 0));
                pv3.Add(new Phrase("  Cuenta Ahorro", subtitulo));
                doc.Add(pv3);
            }
            if (tipoCuenta == "Cuenta Ahorro")
            {
                Paragraph ph1 = new Paragraph();
                ph1.Add(new Phrase("  ", subtitulo));
                ph1.Add(new Chunk(imagen3, 0, 0));
                ph1.Add(new Phrase("  Cuenta Corriente", subtitulo));
                doc.Add(ph1);
                Paragraph ph2 = new Paragraph();
                ph2.Add(new Phrase("  ", subtitulo));
                ph2.Add(new Chunk(imagen3, 0, 0));
                ph2.Add(new Phrase("  Cuenta Vista", subtitulo));
                doc.Add(ph2);
                Paragraph ph3 = new Paragraph();
                ph3.Add(new Phrase("  ", subtitulo));
                ph3.Add(new Chunk(imagen2, 0, 0));
                ph3.Add(new Phrase("  Cuenta Ahorro", subtitulo));
                doc.Add(ph3);
            }

            doc.Add(Chunk.NEWLINE);
            PdfPTable table2 = new PdfPTable(new float[] { 2, 2 });
            table2.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table2.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table2.DefaultCell.Border = Rectangle.NO_BORDER;
            table2.WidthPercentage = 100;
            table2.AddCell(new Phrase("Correo notificación: ", subtitulo));
            table2.AddCell(new Phrase(" ", subtitulo));
            doc.Add(table2);
            Paragraph pc = new Paragraph();
            pc.Add(new Phrase("  " + correoPDFF.ToLower(), subtitulo));
            doc.Add(pc);
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            Paragraph pa5 = new Paragraph("INFORMACIÓN DE UTILIDAD", subtitulo);
            pa5.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa5);
            iTextSharp.text.pdf.draw.LineSeparator line2 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            doc.Add(new Chunk(line2));
            Paragraph pa6 = new Paragraph("Si por razones ajenas a las empresas del Grupo Colmena , el depósito es rechazado por el Banco. Se procederá a emitir un cheque por el monto del abono rechazado, el cual estará disponible en el Departamento de Tesorería y Finanzas ubicada en Los Militares 4777, oficina 501, Las Condes para COLMENA GOLDEN CROSS S.A.o en Luis Thayer Ojeda 166, piso 6, Providencia para COLMENA COMPAÑÍA DE SEGUROS DE VIDA S.A. , en un plazo de 5 días hábiles desde la fecha original de rechazo.", subtitulo2);
            pa6.Alignment = Element.ALIGN_JUSTIFIED;
            doc.Add(pa6);

            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);

            Paragraph pa7 = new Paragraph("Santiago, " + fechaD, subtitulo);
            pa7.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa7);
            doc.Add(Chunk.NEWLINE);

            iTextSharp.text.pdf.draw.LineSeparator line3 = new iTextSharp.text.pdf.draw.LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            doc.Add(new Chunk(line3));
            Paragraph pa8 = new Paragraph("Folio:  " + referencia + "                                                          Solicitud y autorización Servicio de Depósito Automático", subtitulo);
            pa8.Alignment = Element.ALIGN_LEFT;
            doc.Add(pa8);

            doc.Close();
            writer.Close();
            //Fin creacion del PDF

            try
            {
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(ruta);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                FileStream stream = File.OpenRead(rutepmar);
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                stream.Close();
                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Flush();
                reqStream.Close();
            }
            catch
            {

            }
            ListccE.Items.Clear();
            GetMandato();
        }
    }

}