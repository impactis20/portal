﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using System.Web;

namespace PortalProveedores.App.UI.app.mandato

{
    /// <summary>
    /// Summary description for documento
    /// </summary>
    public class archivo : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
           
                context.Response.ContentType = "application/pdf";
                //En este controlador tenemos que recibir la ruta del pdf... cómo? recibiéndola por parámetro de url
                if (context.Request.QueryString["id"] != null) //verifica si trae el parámetro "id"
                {
                    context.Response.ContentType = "application/pdf";
                    // Get the object used to communicate with the server.
                    String ruta = App_Code.Util.Desencriptar(context.Request.QueryString["id"]);
                    WebClient client = new WebClient();
                    Byte[] buffer = client.DownloadData(ruta);
                    context.Response.AddHeader("content-disposition", string.Format("inline;filename={0}", Path.GetFileName(ruta)));
                    context.Response.AddHeader("content-length", buffer.Length.ToString());
                    context.Response.BinaryWrite(buffer);
                    context.Response.Flush();
                    context.Response.Close();
                    context.Response.End();
                }
          
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

//Create FTP Request.
//FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ruta);
//request.Method = WebRequestMethods.Ftp.DownloadFile;

//        //Enter FTP Server credentials.
//        request.UsePassive = true;
//        request.UseBinary = true;
//        request.EnableSsl = false;

//        //Fetch the Response and read it into a MemoryStream object.
//        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
//        using (MemoryStream stream = new MemoryStream())
//        {
//            //Download the File.
//            response.GetResponseStream().CopyTo(stream);
//Response.AddHeader("content-disposition", "attachment;filename=archivoDocumento.pdf");
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);
//            Response.BinaryWrite(stream.ToArray());
//            Response.End();
//        }