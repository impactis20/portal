﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalProveedores.App.UI.app
{
    public partial class no_autorizado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           //Response.Redirect("~/app/no-autorizado.aspx");
           //if (Request.QueryString["error"] != null)
           //{
           error.InnerHtml += Request.QueryString["error"];
           error.Visible = true;
           //}
        }

        protected void btnInicio_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/app/inicio.aspx");
        }
    }
}