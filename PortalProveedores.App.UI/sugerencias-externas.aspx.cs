﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;
namespace PortalProveedores.App.UI
{
    public partial class sugerencias_externas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindSociedades();
                BindSTipoSolicitud();
            }
        }
        private void BindSTipoSolicitud()
        {
            string accion = "2";
            string origen = "1";

            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
            List<Entities.SugerenciasEntity> lst = SugerenciasBL.GetTipoSugerencia(accion, origen, ref respWS);

            //Bindea list del formulario de búsqueda
            lstTipoSolicitud.DataTextField = "descripcion";
            lstTipoSolicitud.DataValueField = "id";
            lstTipoSolicitud.DataSource = lst;
            lstTipoSolicitud.DataBind();
        }
        private void BindSociedades()
        {
            List<Entities.SociedadEntity> lstData = SociedadBL.GetAllSociedades();

            //Bindea list del formulario de búsqueda
            lstSociedades.DataTextField = "nombre";
            lstSociedades.DataValueField = "id";
            lstSociedades.DataSource = lstData;
            lstSociedades.DataBind();
        }
        protected void btnEnviar_ServerClick(object sender, EventArgs e)
        {
            string rut = txtRutProveedor.Text.Trim();

            if (App_Code.Util.RutValido(rut))
            {
                string accion = "1";
                string rutProveedor = rut.Replace(".", string.Empty);
                string origen = "2";
                string idSoli = lstTipoSolicitud.SelectedItem.Value;
                string descripcion = txtDescripcion.Text;
                string razon = txtNombre.Text;
                string idSociedad = lstSociedades.SelectedItem.Value;
                ViewState["rt"] = rutProveedor;
                ViewState["de"] = descripcion;
                ViewState["ra"] = razon;
                ViewState["so"] = idSociedad;
                ViewState["is"] = idSoli;
                try
                {
                    Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity();
                    SugerenciasBL.GetSugerenciasExternas(accion, rutProveedor, idSoli, origen, descripcion, razon, idSociedad, ref respWS);
                    if (respWS.codigo == "0")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msj2", "getMessage('Sus datos fueron recibidos exitosamente.', '0')", true);
                        limpiarCampos();
                    }
                    else if (respWS.codigo != "0")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msj3", "getMessage('Error al enviar sus datos... Vuela a intentar.')", true);
                        limpiarCampos();
                    }

                }
                catch
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "msj3", "getMessage('Se generó un problema interno al intentar enviar los datos. Favor contacte a su administrador.', '1')", true);
                }
            }
        }

        private void limpiarCampos()
        {
            txtDescripcion.Text = string.Empty;
            lstTipoSolicitud.SelectedIndex = -1;
            lstSociedades.SelectedIndex = -1;
            txtNombre.Text = string.Empty;
            txtRutProveedor.Text = string.Empty;

        }

    }
}