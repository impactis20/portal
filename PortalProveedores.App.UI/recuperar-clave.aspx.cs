﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalProveedores.BL;

namespace PortalProveedores.App.UI
{
    public partial class recuperar_clave : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            string rut = txtRutProveedor.Text.Trim();
            rut = rut.Replace(".", string.Empty);

            if (!string.IsNullOrEmpty(rut) && App_Code.Util.RutValido(rut))
            {

                Entities.ResponseWSEntity resp = UsuarioBL.GetClave(rut);

                if (resp.codigo == "0")
                {
                    txtRutProveedor.Text = string.Empty;
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "msj90", string.Format("getMessage('{0}', '{1}')", resp.mensaje, resp.codigo), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msj91", string.Format("getMessage('{0}', '{1}')", "Ingrese un rut válido.", "1"), true);
            }
        }
    }
}