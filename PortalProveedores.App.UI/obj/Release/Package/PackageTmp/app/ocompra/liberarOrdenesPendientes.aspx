﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="liberarOrdenesPendientes.aspx.cs" Inherits="PortalProveedores.App.UI.app.ocompra.liberarOrdenesPendientes" %>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>.table th, .table td { 
     border-top: none !important; 
 }
.negra{font-weight:bold;}
    </style>
    
<h2 class="page-header">Liberar órdenes de compra pendientes.</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline" id="lblLabel" runat="server">
                    <p>Estimado <asp:Label runat="server" ID="lblNombreUsuarioLogueado" />, favor revisar y liberar las órdenes de compra asignadas a su usuario. </p>
                </div>
            </div>
        </div>
    </div>

  <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
   </div>
   <%--Tabla usuarios --%>   
    <div class="table-responsive small">

        <asp:GridView ID="gvorden" AutoGenerateColumns="false" runat="server" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
            <Columns> 
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Detalle."></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkdetail" runat="server" Text="Detalle" OnCommand="lnkdetail_Click" CommandArgument='<%#Eval("ordencompra")%>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Orden de Comprao.">Orden de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("ordencompra") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Orden.">Fecha de Orden</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaordencompra").ToString().Substring(6,2)+"/"+Eval("fechaordencompra").ToString().Substring(4,2)+"/"+Eval("fechaordencompra").ToString().Substring(0,4)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Entrega.">Fecha de Entrega</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#  Eval("fechaentrega").ToString().Substring(6,2)+"/"+Eval("fechaentrega").ToString().Substring(4,2)+"/"+Eval("fechaentrega").ToString().Substring(0,4) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rut proveedor.">Rut Proveedor</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# formatearRut(Eval("rutproveedor").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Razon social proveedor.">Razon Social Proveedor</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <%# Eval("razonsocial") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto bruto.">Monto Bruto</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# (Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"valorbruto"))).ToString("C0") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Texto cabecera.">Texto Cabecera</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("textocabecera") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div runat="server" id="detalle">
        <div class="table-responsive small">
        
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Orden de compra <asp:Label runat="server" id="lblordencompra2" /></h3>
              </div>
           </div>
            <table  class="table table-striped" border="0" style="width:35%;border-color: #FAFAFA;" >
                <tr><td class="negra" style="text-align:left;">Rut Proveedor:</td><td style="text-align:left;"><asp:Label runat="server" id="lblrutproveedor" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Razon Social Proveedor:</td><td style="text-align:left;"><asp:Label runat="server" id="lblrazonsocial" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Orden de Compra:</td><td style="text-align:left;"><asp:Label runat="server" id="lblordencompra" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Fecha de Orden:</td><td style="text-align:left;"><asp:Label runat="server" id="lblfechaordencompra" /></td></tr>
            </table>
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Detalle</h3>
              </div>
           </div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Posición.">Posición</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("posicion") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Material.">Material</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("material") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción.">Descripción</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("descripcion") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción centro costo.">Centro Costo</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("descripcentrocoste") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Cantidad.">Cantidad</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("cantidad") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Estatus del usuario.">Unidad Medida</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("um") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Precio neto unidad.">Precio Neto Unidad</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#  (Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"pnetounidad"))).ToString("C0") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Valor neto.">Valor Neto</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#  (Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"totalbruto"))).ToString("C0") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                <table class="table table-striped pull-right" border="0" style="width:35%;border-color: #FAFAFA;">
                    <tr>
                        <td class="negra" style="text-align:left;">Precio Neto</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblprecioneto" /></td>
                    </tr>
                    <tr runat="server" id="trRecargo">
                        <td class="negra" style="text-align:left;">Recargo</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblrecargo" /></td>
                    </tr>
                    <tr runat="server" id="trImporte">
                        <td class="negra" style="text-align:left;">Descuento Importe</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lbldsctoimporte" /></td>
                    </tr>
                    <tr runat="server" id="trPorcentaje">
                        <td class="negra" style="text-align:left;">Descuento Porcentaje</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lbldsctoporcentaje" /></td>
                    </tr>
                    <tr>
                        <td class="negra" style="text-align:left;">Iva</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblivacompraclp" /></td>
                    </tr>
                    <tr runat="server" id="trRetencion">
                        <td class="negra" style="text-align:left;">Retención</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblretencion" /></td>
                    </tr>
                    <tr>
                        <td class="negra" style="text-align:left;">Valor Bruto</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblvalorbruto" /></td>
                    </tr>
                </table>
        </div>
        
<div id="divreturnws" class="alert alert-info" role="alert" runat="server" visible="false">
  <asp:Label ID="lblreturnws" runat="server" Text="label"/>
  <asp:Button CssClass="btn btn-default btn-sm" ID="btnContinuar" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Continuar" />
</div>
        <div style="padding-top:20px; width:100%; text-align:right;">
             <asp:Button CssClass="btn btn-primary btn-sm fi-check" ID="btnLiberar" runat="server" OnClick="btnLiberar_Click" CommandArgument="" Text="Liberar" />
             <asp:Button CssClass="btn btn-default btn-sm" ID="btnVolver" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Cancelar" />
             <asp:HiddenField ID="hfOrdenCompraIdLiberar" Value="" runat="server" />
        </div>
    </div>


 <div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
        <div class="row">
             <div class="col-lg-12 text-center">
                  <div class="form-group " id="Div1" runat="server">
                       <p> <asp:Label runat="server" ID="lblreturnws2" /> </p>
                  </div>
              </div>
        </div>
        <div id="modalfooterc" runat="server" class="modal-footer">
                       		<button id="Butt" runat="server" type="button" class="btn btn-primary btn-sm"  onserverclick="btnVolver_Click">
							&nbsp;&nbsp;Continuar</button>
       </div>
    </div>
  </div>
</div>

<script>
     function crearMandato() {
         $("#myModal").modal('show'); 
     }
</script>

<script>
    $(document).ready(function () {
        $('#contenido_gvorden').DataTable({
            searching: false
        });
    });
</script>

    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>


</asp:Content>
