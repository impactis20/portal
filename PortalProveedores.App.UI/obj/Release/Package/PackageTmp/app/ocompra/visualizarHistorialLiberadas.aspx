﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="visualizarHistorialLiberadas.aspx.cs" Inherits="PortalProveedores.App.UI.app.ocompra.visualizarHistorialLiberadas" %>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>.table th, .table td { 
     border-top: none !important; 
 }
.negra{font-weight:bold;}
    </style>
    


    <script type="text/javascript">

        $(document).ready(function () {

                $('[id*=calendarioFechaDesde]').datetimepicker({
                    useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                    format: 'DD-MM-YYYY',
                    maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                    //minDate: moment().add(-3, 'month'),
                    showClear: true,
                    ignoreReadonly: false
                });

                $('[id*=calendarioFechaHasta]').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    format: 'DD-MM-YYYY',
                    maxDate: moment().add(1, 'days'),
                    showClear: true,
                    ignoreReadonly: false
                });

                $("[id*=calendarioFechaDesde]").on("dp.change", function (e) {
                    $('[id*=calendarioFechaHasta]').data("DateTimePicker").minDate(e.date);
                });

                $("[id*=calendarioFechaHasta]").on("dp.change", function (e) {
                    $('[id*=calendarioFechaDesde]').data("DateTimePicker").maxDate(e.date);
                });

                $('.fechafiltro').datetimepicker({
                    useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                    format: 'DD-MM-YYYY',
                    minDate: moment().add(-2, 'year'),
                    showClear: true,
                    ignoreReadonly: false
                });

                //inicia combobox por defecto
                $('.selectpicker').selectpicker({
                    title: 'Seleccione...',
                    showTick: true
                });
        });
    </script>



<h2 class="page-header">Historial órdenes de compra liberadas.</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline" id="lblLabel" runat="server">
                    <p>Estimado <asp:Label runat="server" ID="lblNombreUsuarioLogueado" />, usted puede buscar órdenes de compra liberadas con los siguientes filtros. </p>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group" id="search" runat="server">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                          <%--  <div  runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
											<label class="control-label"> Sociedad</label>
											<br />
											<asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" SelectionMode="Single" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
											<asp:RequiredFieldValidator ID="rfvlstSociedades" runat="server"  Display="Dynamic" ControlToValidate="lstSociedades" validationgroup="busr"/>
										</div>
                                 </div>
                           </div>--%>
                            <div  runat="server" class="col-lg-2">
                                <div class="row">
                             <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                    <label for="lblSociedad" class="control-label">Sociedad</label>
                                    <asp:TextBox ID="lblSociedad" runat="server" ReadOnly="true" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="cvnlblSociedad" runat="server" CssClass="help-block" ErrorMessage="Ingrese una cuenta igual o menor a 15 caracteres..." Display="Dynamic" ControlToValidate="lblSociedad" SetFocusOnError="True" ValidationGroup="buscar" />
                                   <%-- <asp:RequiredFieldValidator ID="rfvnroCuenta" runat="server" CssClass="help-block" ErrorMessage="Ingrese Nro de cuenta..." Display="Dynamic" ControlToValidate="lblcuentac" SetFocusOnError="True" ValidationGroup="crear" />--%>
                                </div>
                                     </div>
                            </div>

                           <div id="filtroRut" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <%--<label class="sr-only">Ingrese rut de proveedor a consultar</label>--%>
                                        <label for="txtRutProveedor" class="control-label">Rut Proveedor</label>
                                        <br />
                                        <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" placeholder="Ingrese..."></asp:TextBox>
                                        <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true" ClientValidationFunction="customValidatorRut" ValidationGroup="buscar"></asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvRutProveedor" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." ControlToValidate="txtRutProveedor" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>

                              <div id="filtroRS" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <%--<label class="sr-only">Ingrese rut de proveedor a consultar</label>--%>
                                        <label for="txtRazonS" class="control-label">Razón Social</label>
                                        <br />
                                        <asp:TextBox ID="txtRazonS" runat="server" CssClass="form-control input-sm" MaxLength="40" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                                        <asp:CustomValidator ID="cvRazonS" runat="server" CssClass="help-block" ControlToValidate="txtRazonS" ErrorMessage="Ingrese <strong>Razón Social</strong>..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true"  ValidationGroup="buscar"></asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvRutProveedor" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." ControlToValidate="txtRutProveedor" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>

                            <div id="FiltroOC" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <%--<label class="sr-only">Ingrese rut de proveedor a consultar</label>--%>
                                        <label for="txtordenC" class="control-label">Orden de Compra</label>
                                        <br />
                                        <asp:TextBox ID="txtordenC" runat="server" CssClass="form-control input-sm" MaxLength="40" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                                        <asp:CustomValidator ID="cvordenC" runat="server" CssClass="help-block" ControlToValidate="txtordenC" ErrorMessage="Ingrese <strong>Orden de Compra</strong>..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true"  ValidationGroup="buscar"></asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvRutProveedor" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." ControlToValidate="txtRutProveedor" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>


                            <div id="filtroFechaDesde" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only">Ingrese fecha desde, para realizar la consultar</label>--%>
                                        <label for="txtFechaDesde" class="control-label">Fecha desde (Emisión)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaDesde">
                                            <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha desde..." Display="Dynamic" ControlToValidate="txtFechaDesde" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaHasta" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only">Ingrese fecha hasta, para realizar la consultar</label>--%>
                                        <label for="txtFechaHasta" class="control-label">Fecha hasta (Emisión)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaHasta">
                                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha hasta..." Display="Dynamic" ControlToValidate="txtFechaHasta" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                      
                        </ContentTemplate>
                    </asp:UpdatePanel>
                   <div class="col-lg-12" style="margin-top:20px">
                       <div class="row">
                            <asp:button id="btnBuscar2" runat="server" type="submit" class="btn btn-primary btn-sm pull-right" OnClick="btnBuscar2_Click" validationgroup="buscar" Text="Buscar">
                            </asp:button>
                       </div>
                  </div>
                </div>
            </div>
        </div>
    </div>




  <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
   </div>
   <%--Tabla usuarios --%>   
    <div class="table-responsive small">

        <asp:GridView ID="gvorden" AutoGenerateColumns="false" runat="server" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
            <Columns> 
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Detalle."></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkdetail" runat="server" Text="Detalle" OnCommand="lnkdetail_Click" CommandArgument='<%#Eval("ordencompra")%>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Orden de Comprao.">Orden de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("ordencompra") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Orden.">Fecha de Orden</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaordencompra").ToString().Substring(6,2)+"/"+Eval("fechaordencompra").ToString().Substring(4,2)+"/"+Eval("fechaordencompra").ToString().Substring(0,4)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Entrega.">Fecha de Entrega</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#  Eval("fechaentrega").ToString().Substring(6,2)+"/"+Eval("fechaentrega").ToString().Substring(4,2)+"/"+Eval("fechaentrega").ToString().Substring(0,4) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rut proveedor.">Rut Proveedor</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# formatearRut(Eval("rutproveedor").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Razon social proveedor.">Razón Social Proveedor</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <%# Eval("razonsocial") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto bruto.">Monto Bruto</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# (Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"valorbruto"))).ToString("C0") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Texto cabecera.">Texto Cabecera</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("textocabecera") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div runat="server" id="detalle">
        <div class="table-responsive small">
        
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Orden de Compra <asp:Label runat="server" id="lblordencompra2" /></h3>
              </div>
           </div>
            <table  class="table table-striped" border="0" style="width:35%;border-color: #FAFAFA;" >
                <tr><td class="negra" style="text-align:left;">Rut Proveedor:</td><td style="text-align:left;"><asp:Label runat="server" id="lblrutproveedor" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Razon Social Proveedor:</td><td style="text-align:left;"><asp:Label runat="server" id="lblrazonsocial" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Orden de Compra:</td><td style="text-align:left;"><asp:Label runat="server" id="lblordencompra" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Fecha de Orden:</td><td style="text-align:left;"><asp:Label runat="server" id="lblfechaordencompra" /></td></tr>
            </table>
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Detalle</h3>
              </div>
           </div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Posición.">Posición</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("posicion") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Material.">Material</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("material") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción.">Descripción</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("descripcion") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción centro costo.">Centro Costo</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("descripcentrocoste") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Cantidad.">Cantidad</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("cantidad") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Estatus del usuario.">Unidad Medida</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("um") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Precio neto unidad.">Precio Neto Unidad</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#  (Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"pnetounidad"))).ToString("C0") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Valor neto.">Valor Neto</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#  (Convert.ToDecimal(DataBinder.Eval(Container.DataItem,"totalbruto"))).ToString("C0") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                <table class="table table-striped pull-right" border="0" style="width:35%;border-color: #FAFAFA;">
                    <tr>
                        <td class="negra" style="text-align:left;">Precio Neto</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblprecioneto" /></td>
                    </tr>
                    <tr runat="server" id="trRecargo">
                        <td class="negra" style="text-align:left;">Recargo</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblrecargo" /></td>
                    </tr>
                    <tr runat="server" id="trImporte">
                        <td class="negra" style="text-align:left;">Descuento Importe</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lbldsctoimporte" /></td>
                    </tr>
                    <tr runat="server" id="trPorcentaje">
                        <td class="negra" style="text-align:left;">Descuento Porcentaje</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lbldsctoporcentaje" /></td>
                    </tr>
                    <tr>
                        <td class="negra" style="text-align:left;">Iva</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblivacompraclp" /></td>
                    </tr>
                    <tr runat="server" id="trRetencion">
                        <td class="negra" style="text-align:left;">Retención</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblretencion" /></td>
                    </tr>
                    <tr>
                        <td class="negra" style="text-align:left;">Valor Bruto</td>
                        <td style="text-align:right;"><asp:Label runat="server" id="lblvalorbruto" /></td>
                    </tr>
                </table>
        </div>
        
<div id="divreturnws" class="alert alert-info" role="alert" runat="server" visible="false">
  <asp:Label ID="lblreturnws" runat="server" Text="label"/>
  <asp:Button CssClass="btn btn-default btn-sm" ID="btnContinuar" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Continuar" />
</div>
        <div style="padding-top:20px; width:100%; text-align:right;">
             <asp:Button CssClass="btn btn-default btn-sm" ID="btnVolver" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Cancelar" />
             <asp:HiddenField ID="hfOrdenCompraIdLiberar" Value="" runat="server" />
        </div>
    </div>
<script>
    $(document).ready(function () {
        $('#contenido_gvorden').DataTable({
            searching: false
        });
    });
</script>

    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>

    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>


</asp:Content>
