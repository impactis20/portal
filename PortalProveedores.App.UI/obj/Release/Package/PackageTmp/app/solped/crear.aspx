﻿<%@ Page Title="Solicitud de compra" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="crear.aspx.cs" Inherits="PortalProveedores.App.UI.app.solped.crear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });
        });
    </script>
    <h2 class="page-header">Solicitud de compra</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <p>Para crear una Solicitud de compra debe agregar al menos un Material.</p>
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <label class="control-label">Sociedad</label>
                                <br />
                                <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" disabled="disabled" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4" style="margin-top:20px">
                        <div class="row">
                            <button id="btnBuscar" runat="server" type="submit" class="btn btn-default btn-sm" onserverclick="btnBuscar_ServerClick">
                                <i class="fa fa-plus"></i>
                                &nbsp;&nbsp;Agregar Material
                            </button>
                            <button id="btnCrearSolped" runat="server" class="btn btn-primary btn-sm" onserverclick="btnCrearSolped_ServerClick" validationgroup="crear">
                                <i class="fa fa-paper-plane"></i>
                                &nbsp;&nbsp;Crear Solped
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
    </div>
    <div class="table-responsive small">
        <asp:GridView ID="gvMaterial" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
             OnRowCommand="gvMaterial_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Posición del material.">N°</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="ID del material.">Material</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("id") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Descripción del material.">Descripción</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("descripcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Cantidad a solicitar.">Cantidad</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("cantidad") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Grupo de compras.">Grupo Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("grupoCompra") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Centro de costo.">Centro Costo</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("centroCosto") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Rut del usuario solicitante.">Rut Solicitante</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("rutSolicitante") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de entrega.">Fecha de Entrega</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaEntrega") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" CommandName="editar" CommandArgument='<%# Container.DataItemIndex %>' id-material='<%# Eval("id") %>' />
                        <asp:Button ID="btnBorrar" runat="server" Text="Quitar" CssClass="btn btn-danger btn-xs" CommandName="quitar" CommandArgument='<%# Container.DataItemIndex %>' id-material='<%# Eval("id") %>'/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div id="obs" runat="server" class="form-group" visible="false">
                    <label class="control-label" >Observación</label>
                    <br />
                    <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Rows="3"  MaxLength="300"  CssClass="form-control input-sm"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvObservacion" runat="server" CssClass="help-block" ErrorMessage="Ingrese Observación..." Display="Dynamic" ControlToValidate="txtObservacion" SetFocusOnError="True" ValidationGroup="crear" />
                    <p align="right" style="font-size:small;"><i>Máximo 300 carácteres</i></p>
        </div>
    
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
</asp:Content>
