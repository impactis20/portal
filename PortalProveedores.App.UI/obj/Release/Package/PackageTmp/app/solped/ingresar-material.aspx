﻿<%@ Page Title="Ingresar material" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="ingresar-material.aspx.cs" Inherits="PortalProveedores.App.UI.app.solped.ingresar_material" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('[id*=calendarioFecha]').datetimepicker({
                format: 'DD-MM-YYYY',
                showClear: true,
                ignoreReadonly: true,
                minDate: moment()
            });
        });
    </script>

    <h2 id="titulo" runat="server" class="page-header"></h2>

    <div class="form-group">
        <p>Los campos con asterisco son obligatorios, una vez agregado el material será redireccionado a la Solicitud de Compra. <a class="pull-right" href="crear.aspx">Volver a Solped</a></p>
        <div class="col-lg-4">
            <div class="row">
                <div class="form-group">
                    <label class="control-label">Material</label>
                    <br />
                    <asp:TextBox ID="txtID" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <br />
                    <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" Rows="2"  ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="control-label">Grupo de Compras</label>
                    <br />
                    <asp:TextBox ID="txtGrupoCompra" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="control-label">Solicitante</label>
                    <br />
                    <asp:TextBox ID="txtRutSolicitante" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="control-label">* Cantidad</label>
                    <br />
                    <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control input-sm" onkeyup="formatoNumerico(this);" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCantidad" runat="server" CssClass="help-block" ErrorMessage="Ingrese Cantidad..." Display="Dynamic" ControlToValidate="txtCantidad" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <label class="control-label">* Centro de Costo</label>
                    <br />
                    <asp:TextBox ID="txtCentroCosto" runat="server" CssClass="form-control input-sm" onkeyup="formatoNumerico(this, '0');" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCentroCosto" runat="server" CssClass="help-block" ErrorMessage="Ingrese Centro de Costo..." Display="Dynamic" ControlToValidate="txtCentroCosto" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <label for="txtFechaEntrega" class="control-label">* Fecha Entrega</label>
                    <br />
                    <div class="input-group date input-group" id="calendarioFechaDesde">
                        <asp:TextBox ID="txtFechaEntrega" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Seleccione fecha..."></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                    <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha de emisión..." Display="Dynamic" ControlToValidate="txtFechaEntrega" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>

                <div class="pull-right">
                    <button id="btnAgregar" runat="server" class="btn btn-primary btn-sm" onserverclick="btnAgregar_ServerClick" validationgroup="ingresar" >
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Agregar Material
                    </button>
                    <button id="btnGuardar" runat="server" class="btn btn-primary btn-sm" onserverclick="btnGuardar_ServerClick" validationgroup="ingresar" >
                        <i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;Guardar cambios
                    </button>
                    <button class="btn btn-default btn-sm" onclick="window.history.back();" >
                        <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp;Volver atrás
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
</asp:Content>
