﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="liberarSolicitudesPendientes.aspx.cs" Inherits="PortalProveedores.App.UI.app.solped.liberarSolicitudesPendientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>.table th, .table td { 
     border-top: none !important; 
 }
.negra{font-weight:bold;}
    </style>
    
<h2 class="page-header">Solicitudes pendientes de liberación.</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline" id="lblLabel" runat="server">
                    <p>Estimado <asp:Label runat="server" ID="lblNombreUsuarioLogueado" />, favor revisar y liberar las solicitudes de compra asignadas a su usuario. </p>
                </div>
            </div>
        </div>
    </div>

  <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
   </div>
   <%--Tabla usuarios --%>   
    <div class="table-responsive small">

        <asp:GridView ID="gvSolicitud" AutoGenerateColumns="false" runat="server" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
            <Columns> 
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Detalle."></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkdetail" runat="server" Text="Detalle" OnCommand="lnkdetail_Click" CommandArgument='<%#Eval("ID_PEDIDO")%>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número de solicitud.">Solicitud de Compra</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("SOLPED") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Sociedad.">Sociedad</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("NOMBRE_CENTRO") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de solicitud.">Fecha de Solicitud</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("FECHA_SOLPED")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de Entrega.">Fecha de Entrega</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#  Eval("FECHA_ENTREGA") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div runat="server" id="detalle">
        <div class="table-responsive small">
        
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Solicitud de Compra <asp:Label runat="server" id="lblordencompra2" /></h3>
              </div>
           </div>
            <table  class="table table-striped" border="0" style="width:35%;border-color: #FAFAFA;" >
                <tr><td class="negra" style="text-align:left;">Solicitud de Compra:</td><td style="text-align:left;"><asp:Label runat="server" id="lblID_PEDIDO" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Sociedad:</td><td style="text-align:left;"><asp:Label runat="server" id="lblNOMBRE_CENTRO" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Fecha Solicitud:</td><td style="text-align:left;"><asp:Label runat="server" id="lblFECHA_SOLPED" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Fecha de Entrega:</td><td style="text-align:left;"><asp:Label runat="server" id="lblFECHA_ENTREGA" /></td></tr>
                <tr><td class="negra" style="text-align:left;">Observación:</td><td style="text-align:left;"><asp:Label runat="server" id="lblObservacion" /></td></tr>
            </table>
           <div class="row">
              <div class="col-md-12">
                  <h3 style="text-align:left;">Detalle</h3>
              </div>
           </div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" EmptyDataText="No existen datos">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Posición.">Posición</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("POSICION") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Material.">Material</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("MATERIAL") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción.">Descripción</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("DESCRIPCION") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Descripción centro costo.">Centro Costo</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("CENTRO_COSTE") %> <%# Eval("DESC_CENT_COSTE") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Cantidad.">Cantidad</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("CANTIDAD") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <div class="tooltip-wrapper-header-table pull-left" title="Estatus del usuario.">Unidad Medida</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("UNIDAD_MEDIDA") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        
<div id="divreturnws" class="alert alert-info" role="alert" runat="server" visible="false">
  <asp:Label ID="lblreturnws" runat="server" Text="label"/>
  <asp:Button CssClass="btn btn-default btn-sm" ID="btnContinuar" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Continuar" />
</div>
        <div style="padding-top:20px; width:100%; text-align:right;">
             <asp:Button CssClass="btn btn-primary btn-sm fi-check" ID="btnLiberar" runat="server" OnClick="btnLiberar_Click" CommandArgument="" Text="Liberar" />
             <asp:Button CssClass="btn btn-default btn-sm" ID="btnVolver" runat="server" OnClick="btnVolver_Click" CommandArgument="" Text="Cancelar" />
             <asp:HiddenField ID="hfOrdenCompraIdLiberar" Value="" runat="server" />
        </div>
    </div>

 <div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
        <div class="row">
             <div class="col-lg-12 text-center">
                  <div class="form-group " id="Div1" runat="server">
                       <p> <asp:Label runat="server" ID="lblreturnws2" /> </p>
                  </div>
              </div>
        </div>
        <div id="modalfooterc" runat="server" class="modal-footer">
                       		<button id="Butt" runat="server" type="button" class="btn btn-primary btn-sm"  onserverclick="btnVolver_Click">
							&nbsp;&nbsp;Continuar</button>
       </div>
    </div>
  </div>
</div>

<script>
    function crearMandato() {
        $("#myModal").modal('show');
    }
</script>

<script>
    $(document).ready(function () {
        $('#contenido_gvSolicitud').DataTable(.DataTable(({
            searching: false
        });
    });
</script>

    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/correoUsuario-format.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>


</asp:Content>
