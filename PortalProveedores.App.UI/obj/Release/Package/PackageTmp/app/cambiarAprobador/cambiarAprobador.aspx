﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="cambiarAprobador.aspx.cs" Inherits="PortalProveedores.App.UI.app.cambiarAprobador.cambiarAprobador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {

            $('[id*=calendarioFechaDesde]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: moment().add(-0, 'days'), //permite hasta la fecha de hoy
                //minDate: moment().add(-3, 'month'),
                showClear: true,
                ignoreReadonly: false
            });

            $('[id*=calendarioFechaHasta]').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                //maxDate: moment().add(1, 'month'),
                showClear: true,
                ignoreReadonly: false
            });

            $("[id*=calendarioFechaDesde]").on("dp.change", function (e) {
                $('[id*=calendarioFechaHasta]').data("DateTimePicker").minDate(e.date);
            });

            $("[id*=calendarioFechaHasta]").on("dp.change", function (e) {
                $('[id*=calendarioFechaDesde]').data("DateTimePicker").maxDate(e.date);
            });

            $('.fechafiltro').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: moment().add(-2, 'year'),
                showClear: true,
                ignoreReadonly: false
            });

            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $("#<%= rbCambiar.ClientID %>").click(function () {
            var target = document.getElementById('<%=rbCambiar.ClientID %>');
            var radioButtons = target.getElementsByTagName('input');
              var $uF = $("#DocComercial");
              var is_valid;
              if (radioButtons[1].checked) {
                  $uF.show();
              }
              else if (radioButtons[0].checked) {
                  $uF.hide();
              }
            });
        });
    </script>
   <%--  <script>
         function validateRadioButtonList() {
             var target = document.getElementById('<%=rbCambiar.ClientID %>');
             var radioButtons = target.getElementsByTagName('input');
             var $uF = $("#DocComercial");
             var is_valid;
             if (radioButtons[1].checked) {
                 $uF.show();
             }
             else if (radioButtons[0].checked){
                 $uF.hide();
             }
         }
   
    </script>--%>
    <h2 class="page-header">Cambiar Aprobador</h2>
    <div class="form-group">
        <div class="col-lg-4">
            <div class="row">
                <i>Rut Aprobadores:</i>
                <hr />
                <div class="form-group">
                    <label class="control-label">* Rut Anterior</label>
                    <br />
                    <asp:TextBox ID="txtRutAnterior" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);"></asp:TextBox>
                    <asp:CustomValidator ID="cvRutAnterior" runat="server" CssClass="help-block" ControlToValidate="txtRutAnterior" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" validationgroup="cambiar" ></asp:CustomValidator>
                </div>
                <div class="form-group">
                    <label class="control-label">* Rut Nuevo</label>
                    <br />
                   <asp:TextBox ID="txtRutNuevo" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);"></asp:TextBox>
                    <asp:CustomValidator ID="cvRutNuevo" runat="server" CssClass="help-block" ControlToValidate="txtRutNuevo" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" validationgroup="cambiar"></asp:CustomValidator>
                </div>
                <div class="form-group">
                          <label class="control-label">* Seleccione Período</label> 
                    <br />
                          <asp:RadioButtonList ID="rbCambiar" runat="server"  RepeatDirection="Vertical" 
                          RepeatLayout="Flow" AutoPostBack="false">
                          <asp:ListItem  Selected="True">&nbsp;&nbsp;Definitivo</asp:ListItem>
                          <asp:ListItem >&nbsp;&nbsp;Por tiempo especifico</asp:ListItem>
                         </asp:RadioButtonList>
                 </div>
                  <div id="DocComercial" class="form-group" style="display:none;">
                       <i>Tiempo de asignación:</i>
                       <hr />
                          <div class="col-lg-6">
                              <div class="row">
                                   <div class="form-group" style="margin-right: 4px">
                                        <label for="txtFechaDesde" class="control-label">Fecha desde</label>
                                         <br />
                                         <div class="input-group date input-group" id="calendarioFechaDesde">
                                             <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                                             <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                               </span>
                                             </div>
                                       <%--<asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha desde..." Display="Dynamic" ControlToValidate="txtFechaDesde" SetFocusOnError="True" ValidationGroup="camb" />--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="row">
                                    <div class="form-group" style="margin-right: 4px">
                                        <label for="txtFechaHasta" class="control-label">Fecha hasta</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaHasta">
                                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha hasta..." Display="Dynamic" ControlToValidate="txtFechaHasta" SetFocusOnError="True" ValidationGroup="camb" />--%>
                                    </div>
                             </div>
                      </div>
                 </div>
                <div class="pull-right">
                    <button id="btnCambiar" runat="server" type="submit" onserverclick="btnCambiar_ServerClick" class="btn btn-primary btn-sm" validationgroup="cambiar" >
                        <i class="fa fa-refresh"></i>
                        &nbsp;&nbsp;Cambiar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>
