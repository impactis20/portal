﻿<%@ Page Title="Ingresar documento" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="ingresar.aspx.cs" Inherits="PortalProveedores.App.UI.app.documento.ingresar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
    <%--<link href="../../css/fileinput.min.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
<%--    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>--%>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('[id*=calendarioFecha]').datetimepicker({
                useCurrent: true, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: moment().add(-2, 'year'),
                maxDate: moment().add(0, 'days'),//.add(0, 'days'), //permite hasta la fecha de hoy
                showClear: true,
                ignoreReadonly: true
            });

            $('.selectpicker').selectpicker({
                title: 'Seleccione una opción...',
                showTick: true
            });

            $('.tooltip-wrapper-form').tooltip({ container: 'body', placement: function () { return $(window).width() < 1183 ? 'top' : 'right'; } });
        });
    </script>

    <script>
        $(document).ready(function () {
            $("#<%= txtNroDocumento.ClientID %>").focusout(function () {
                var nroDoc = $("#<%= txtNroDocumento.ClientID %>").val();
                var $uF = $("#DocComercial");

                if (nroDoc !== "") {
                    $uF.show();
                }
                else {
                    $uF.hide();
                    $("#<%= fArchivo.ClientID %>").fileinput('reset');
                }
            });

        });
    </script>

    <style type="text/css">
        .tooltip{
            max-width: 100% /* Max Width of the popover (depending on the container!) */
        }
    </style>
    
    <h2 class="page-header">Ingresar documento</h2>

    <div class="form-group">
        <div class="col-lg-4">
            <div class="row"> <%-- RequiredFieldValidator con formulario "normal" (hacia abajo) llevan Display="Dynamyc" --%>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="form-group">
                            <%--<label class="sr-only">Seleccione las sociedades a consultar</label>--%>
                            <label class="control-label">* Sociedad</label>
                            <br />
                            <div class="tooltip-wrapper-form" title="Seleccione una sociedad">
                                <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                                <asp:RequiredFieldValidator ID="rfvSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstSociedades" SetFocusOnError="True" ValidationGroup="ingresar" />

                            </div>
                           </div>
                    </div>
                </div>
                <div class="form-group">
                    <%--<label class="sr-only">Ingrese el rut del proveedor</label>--%>
                    <label class="control-label">* Rut Proveedor</label>
                    <br />
                    <div class="tooltip-wrapper-form" title="Rut del proveedor">
                        <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control input-sm" MaxLength="10" onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" autocomplete="off" placeholder="Ingrese..." data-toggle="popover" data-trigger="hover" data-content="Ingrese rut del proveedor"></asp:TextBox>
                        <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="ingresar"></asp:CustomValidator>
                    </div>
                </div>
                <div class="form-group">
                    <%--<label class="sr-only">Ingrese número de documento</label>--%>
                    <label class="control-label">* Número Documento</label>
                    <br />
                    <div class="tooltip-wrapper-form" title="Nro del documento">
                        <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="form-control input-sm" MaxLength="16" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" CssClass="help-block" ErrorMessage="Ingrese Nro de documento..." Display="Dynamic" ControlToValidate="txtNroDocumento" SetFocusOnError="True" ValidationGroup="ingresar" />
                    </div>
                </div>
                <div class="form-group">
                    <%--<label class="sr-only" for="lstSociedadesEditar">Seleccione otra sociedad si desea editar</label>--%>
                    <label for="lstTipoDocumento" class="control-label">* Tipo de documento</label>
                    <br />
                    <div class="tooltip-wrapper-form" title="Seleccione un tipo de documento">
                        <asp:ListBox ID="lstTipoDocumento" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="rfvTipoDocumento" runat="server" CssClass="help-block" ErrorMessage="Seleccione tipo documento..." Display="Dynamic" ControlToValidate="lstTipoDocumento" SetFocusOnError="True" ValidationGroup="ingresar" />
                    </div>
                </div>
                <div class="collapse" id="Referencia" runat="server">
                    <div class="form-group">
                        <%--<label class="sr-only" for="lstSociedadesEditar">Seleccione otra sociedad si desea editar</label>--%>
                        <label for="txtReferencia" class="control-label">* Referencia</label>
                        <br />
                        <div class="tooltip-wrapper-form" title="Número de referencia de la nota débito.">
                            <asp:TextBox ID="txtReferencia" runat="server" CssClass="form-control input-sm" MaxLength="16" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvReferencia" runat="server" CssClass="help-block" ErrorMessage="Ingrese referencia de documento..." Enabled="false" Display="Dynamic" ControlToValidate="txtReferencia" SetFocusOnError="True" ValidationGroup="ingresar" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <%--<label class="sr-only">Ingrese fecha del documento</label>--%>
                    <label for="txtFechaEmision" class="control-label">* Fecha Emisión Documento</label>
                    <br />
                    <div class="input-group date input-group" id="calendarioFechaDesde">
                        <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Seleccione..."></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                    <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha de emisión..." Display="Dynamic" ControlToValidate="txtFechaEmision" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group" >
                    <%--<label class="sr-only">Ingrese monto de la documento</label>--%>
                    <label class="control-label">* Monto</label>
                    <br />
                    <asp:TextBox ID="txtMonto" runat="server" CssClass="form-control input-sm" MaxLength="12" autocomplete="off" onkeyup="formatoNumerico(this);" placeholder="Ingrese..."></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvMonto" runat="server" CssClass="help-block" ErrorMessage="Ingrese monto de documento..." Display="Dynamic" ControlToValidate="txtMonto" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <%--<label class="sr-only" for="lstStatus">Seleccione otro estatus si desea editar</label>--%>
                    <label for="lstStatus" class="control-label">* Estatus</label>
                    <br />
                    <asp:ListBox ID="lstStatus" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-size="5" data-width="100%"><%--data-width="200px"--%></asp:ListBox>
                    <asp:RequiredFieldValidator ID="rfvStatus" runat="server" CssClass="help-block" ErrorMessage="Seleccione un estatus..." Display="Dynamic" ControlToValidate="lstStatus" SetFocusOnError="True" ValidationGroup="ingresar" />
                </div>
                <div class="form-group">
                    <label for="txtObservacion" class="control-label">Observación</label>
                    <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Rows="3" MaxLength="100" CssClass="form-control input-sm"></asp:TextBox>
                </div>
                
                <div id="DocComercial" class="form-group" style="display:none">
                        <label>* Adjuntar Documento</label>
                        <asp:FileUpload ID="fArchivo" runat="server" CssClass="file input-sm" AllowMultiple="false" accept="application/pdf" data-show-upload="false" data-show-caption="true" />
                        <asp:RequiredFieldValidator ID="rfvArchivo" runat="server" ErrorMessage="Campo <b>Archivo a subir</b> es obligatorio..." ControlToValidate="fArchivo" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ingresar" />
<%--                        <asp:RegularExpressionValidator ID="reArchivo" runat="server" ErrorMessage="Tipo de archivo no permitido" ControlToValidate="fArchivo" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.pdf)$" Enabled = "false" ></asp:RegularExpressionValidator> --%>

                        <div class="form-group">
                            <div class="pull-right">
                                <br />
                                <button id="btnAgregarDocComercial" runat="server" class="btn btn-primary btn-sm" type="button" style="margin-top: 5px;width:90px" onserverclick="btnAgregarDocComercial_ServerClick" validationgroup="ingresar">
                                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Guardar
                                </button>
                            </div>
                        </div>              
                 </div>
                            

                       
<%--                <div class="pull-right">
                    <button id="btnAgregar" runat="server" class="btn btn-primary btn-sm"  validationgroup="ingresar" >
                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
                    </button>
                </div>--%>
            </div>
        </div>
 </div>

    <script>
        //activa referencia
        VerificaReferencia(<%= Referencia.ClientID %>, <%= lstTipoDocumento.ClientID %>, <%= txtReferencia.ClientID %>, <%= rfvReferencia.ClientID %>)
    </script>
    
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>