﻿<%@ Page Title="Consulta de documentos" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="consultar.aspx.cs" Inherits="PortalProveedores.App.UI.app.documento.consultar" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
    <link href="../css/jquery.dataTables.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script> 
    <script src='<%= ResolveUrl("~/js/fileinput.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            cargarElementoFiltros();

            $.fn.button.Constructor.DEFAULTS = {
                loadingText: '<i class="fa fa-spin fa-spinner"></i> Chargement…'
            };

            $('[data-toggle="popover"]').popover({
                html: true
            });

            //Función que cierra popover desde el boton "x"
            $(document).on("click", ".popover .close", function () {
                $(this).parents(".popover").popover('hide');
            });

            $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });

            $("#<%= rfvSociedades.ClientID %>").hide();
            $('.tooltip-wrapper-form').tooltip({ container: 'body', placement: function () { return $(window).width() < 1183 ? 'top' : 'right'; } });


        });
    </script>
    
    <script type="text/javascript">
        function cargarElementoFiltros() {
            $('.filtro').selectpicker({
                title: 'Filtro...',
                showTick: true,
                countSelectedText: function (numSelected, numTotal) {
                    return (numSelected == 1) ? "Mostrando {0} filtro" : "Mostrando {0} filtros";
                },
                style: 'btn-default btn-xs'
            });

            $('[id*=calendarioFechaDesde]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                //minDate: moment().add(-3, 'month'),
                showClear: true,
                ignoreReadonly: false
            });

            $('[id*=calendarioFechaHasta]').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'),
                showClear: true,
                ignoreReadonly: false
            });

            $("[id*=calendarioFechaDesde]").on("dp.change", function (e) {
                $('[id*=calendarioFechaHasta]').data("DateTimePicker").minDate(e.date);
            });

            $("[id*=calendarioFechaHasta]").on("dp.change", function (e) {
                $('[id*=calendarioFechaDesde]').data("DateTimePicker").maxDate(e.date);
            });

            $('.fechafiltro').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: moment().add(-2, 'year'),
                showClear: true,
                ignoreReadonly: false
            });

            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });
        }
    </script>



    <h2 class="page-header">Consultar documentos</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                            <p>Para realizar una búsqueda debe rellenar los campos con asterisco. 
                                <asp:ListBox ID="lbFiltro" runat="server" CssClass="filtro" AutoPostBack="true" OnSelectedIndexChanged="lbFiltro_SelectedIndexChanged" data-selected-text-format="count > 0" data-width="135px" SelectionMode="Multiple">
                                    <asp:ListItem Value="1" Selected="True">Sociedades</asp:ListItem>
                                </asp:ListBox><%-- Para ver un documento en detalle, haga clic sobre el número de documento.--%>
                            </p>
                            <%--<p><span class="label label-danger"><samp>IMPORTANTE</samp></span> El rango entre fechas puede ser de 3 meses como máximo (90 días).</p>--%>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only" for="lstSociedades">Seleccione las sociedades a consultar</label>--%>
                                        <label class="control-label">* Sociedad</label>
                                        <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                                        <asp:RequiredFieldValidator ID="rfvSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstSociedades" SetFocusOnError="True" ValidationGroup="buscar" />
                                    </div>
                                </div>
                            </div>
                            <div id="filtroRut" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <%--<label class="sr-only">Ingrese rut de proveedor a consultar</label>--%>
                                        <label for="txtRutProveedor" class="control-label">Rut Proveedor</label>
                                        <br />
                                        <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" placeholder="Ingrese..."></asp:TextBox>
                                        <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true" ClientValidationFunction="customValidatorRut" ValidationGroup="buscar"></asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvRutProveedor" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." ControlToValidate="txtRutProveedor" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroTipoDoc" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px; /*margin-bottom: 35px*/">
                                        <label for="lbTipoDoc" class="control-label">Tipo Documento</label>
                                        <br />
                                        <asp:ListBox ID="lbTipoDoc" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaDesde" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only">Ingrese fecha desde, para realizar la consultar</label>--%>
                                        <label for="txtFechaDesde" class="control-label">Fecha desde (Emisión)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaDesde">
                                            <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha desde..." Display="Dynamic" ControlToValidate="txtFechaDesde" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaHasta" runat="server" class="col-lg-2">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <%--<label class="sr-only">Ingrese fecha hasta, para realizar la consultar</label>--%>
                                        <label for="txtFechaHasta" class="control-label">Fecha hasta (Emisión)</label>
                                        <br />
                                        <div class="input-group date input-group" id="calendarioFechaHasta">
                                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha hasta..." Display="Dynamic" ControlToValidate="txtFechaHasta" SetFocusOnError="True" ValidationGroup="buscar" />--%>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFolio" runat="server" class="col-lg-2"  visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label class="control-label">Folio</label>
                                        <asp:TextBox ID="txtFolio" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroEstado" runat="server" class="col-lg-2"  visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="lbEstado" class="control-label">Estado</label>
                                        <br />
                                        <asp:ListBox ID="lbEstado" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaRecepcion" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="txtFechaRecepcion" class="control-label">Fecha Recepción</label>
                                        <br />
                                        <div class="input-group date input-group fechafiltro" id="calendarioFechaRecepcion">
                                            <asp:TextBox ID="txtFechaRecepcion" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="filtroFechaCobro" runat="server" class="col-lg-2" visible="false">
                                <div class="row">
                                    <div class="form-group" style="margin-right: 7px">
                                        <label for="txtFechaCobro" class="control-label">Fecha Cobro</label>
                                        <br />
                                        <div class="input-group date input-group fechafiltro" id="calendarioFechaCobro">
                                            <asp:TextBox ID="txtFechaCobro" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <button id="btnBuscar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnBuscar_Click" ValidationGroup="buscar">
                                <i class="fa fa-search"></i>
                                &nbsp;&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
      <div class="col-md-3 text-right">
          <button id="btnExportarDataExcel" runat="server" type="button" class="btn btn-success btn-xs" onserverclick="btnExportarDataExcel_ServerClick" visible="false"><i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp;&nbsp;Exportar a excel</button>
      </div>
    </div>
    <div class="table-responsive small">
        <asp:GridView ID="gvDocumentos" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
            OnRowDataBound="gvDocumentos_RowDataBound">
            <Columns>
                <asp:TemplateField> <%--HeaderText="<div class='tooltip-wrapper-header-table' title='Fecha en que se creó el documento...'>Fecha Emisión</div>"--%>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha en la cual fue emitido el documento.">F. Emisión</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaEmision") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número correlativo del documento.">N° Doc.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a id="nroDoc" runat="server" href="#" data-toggle="modal" data-target=".modalDocumento" data-rut='<%# Eval("rutProveedor") %>' data-idtipodocumento='<%# Eval("tipoDocumento.id") %>' data-nrodocto='<%# Eval("nroDocumento") %>' data-monto='<%# Eval("monto") %>' data-fechadocumento='<%# Eval("fechaEmision") %>' data-idsociedad='<%# Eval("sociedad.id") %>' data-idestado='<%# Eval("status.id") %>' data-obs='<%# Eval("observacion") %>' data-referencia='<%# Eval("referencia") %>' data-bandera='<%# Eval("bandera") %>' data-modalidadpago='<%# Eval("modalidadPago") %>' data-fechapago='<%# Eval("fechaPago") %>' data-banco='<%# Eval("banco") %>' data-monto2='<%# Eval("monto2") %>' data-nrocuenta='<%# Eval("nroCuenta") %>' data-motivo='<%# Eval("motivoRechazo.id") %>' data-ruta='<%# Eval("nombreDoc") %>'>
                            <%# Eval("nroDocumento") %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Razón social del proveedor.">Razón Social</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("razonSocial") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-CssClass="text-right">
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto total del documento.">Monto</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblMonto" runat="server" Text='<%# Eval("monto") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha en la cual es recibido el documento en nuestras oficinas.">F. Recepción</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaRecepcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha límite planificada para el pago del documento.">F. Probable Pago</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaProbablePago") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha en la que fue cobrado el documento.">F. Cobro</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaCobro") %>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Estado">
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Estatus en el cual se encuentra el documento">Estado</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("status.descripcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TipoDoc">
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Tipo de Documento registrado">Tipo Documento</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("tipoDocumento.descripcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderText="PDF">
                    <ItemTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Visualizar documento">
                            <asp:HyperLink ID="doc" runat="server" target="_blank" Text="<i class='fa fa-file-pdf-o' aria-hidden='true'></i>"></asp:HyperLink>
                        </div> 
                    </ItemTemplate>
                </asp:TemplateField>
             <asp:TemplateField HeaderText="OCS">
                 <ItemTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Visualizar OCS">
                         <asp:HyperLink ID="ocs" runat="server" target="_blank" Text="<i class='fa fa-file-pdf-o' aria-hidden='true'></i>"></asp:HyperLink>
                        </div> 
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div class="modal modalDocumento fade" role="dialog" aria-labelledby="tituloDoc" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="edicion" runat="server" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="tituloDoc"></h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="lstEditarSociedades" class="control-label">Sociedad</label>
                                    <br />
                                    <div class="tooltip-wrapper-form" title="Seleccione una sociedad">
                                        <asp:ListBox ID="lstEditarSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"><%--data-width="200px"--%>
                                        </asp:ListBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvEditarSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstEditarSociedades" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="tooltip-wrapper-form" title="Rut del proveedor">
                                        <label for="txtRutProveedorEditar" class="control-label">Rut Proveedor</label>
                                        <asp:TextBox ID="txtRutProveedorEditar" runat="server" CssClass="form-control input-sm" onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" onchange="javascript:textboxRutFormat(this);"></asp:TextBox>
                                    </div>
                                    <%--<asp:RequiredFieldValidator ID="rfvRutProveedorEditar" runat="server" CssClass="help-block" ErrorMessage="Ingrese un RUT..." Display="Dynamic" ControlToValidate="txtRutProveedorEditar" SetFocusOnError="True" ValidationGroup="editar" />--%>
                                    <asp:CustomValidator ID="cvRutProveedorEditar" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedorEditar" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="editar"></asp:CustomValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="txtNroDocumentoEditar" class="control-label">Número Documento</label>
                                    <asp:TextBox ID="txtNroDocumentoEditar" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvNroDocumentoEditar" runat="server" CssClass="help-block" ErrorMessage="Ingrese Nro de documento..." Display="Dynamic" ControlToValidate="txtNroDocumentoEditar" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="lstEditarTipoDocumento" class="control-label">Tipo de documento</label>
                                    <br />
                                    <asp:ListBox ID="lstEditarTipoDocumento" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="rfvEditarTipoDocumento" runat="server" CssClass="help-block" ErrorMessage="Seleccione tipo documento..." Display="Dynamic" ControlToValidate="lstEditarTipoDocumento" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="collapse" id="ReferenciaEditar" runat="server">
                                    <div class="form-group">
                                        <label for="txtReferenciaEditar" class="control-label">* Referencia</label>
                                        <br />
                                        <div class="tooltip-wrapper-form" title="Número de referencia de la nota débito.">
                                            <asp:TextBox ID="txtReferenciaEditar" runat="server" CssClass="form-control input-sm" MaxLength="16" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="rfvReferenciaEditar" runat="server" CssClass="help-block" ErrorMessage="Ingrese referencia de documento..." Enabled="false" Display="Dynamic" ControlToValidate="txtReferenciaEditar" SetFocusOnError="True" ValidationGroup="editar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="txtFechaDocumentoEditar" class="control-label">Fecha Emisión Documento</label>
                                    <div class="input-group date input-group" id="calendarioFechaDocumentoEditar">
                                        <asp:TextBox ID="txtFechaDocumentoEditar" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox><%--placeholder="Seleccione en el calendario"--%>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvFechaDocumentoEditar" runat="server" CssClass="help-block" ErrorMessage="Seleccione fecha de emisión..." Display="Dynamic" ControlToValidate="txtFechaDocumentoEditar" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="txtMontoEditar" class="control-label">Monto</label>
                                    <asp:TextBox ID="txtMontoEditar" runat="server" CssClass="form-control input-sm" autocomplete="off" onkeyup="formatoNumerico(this);"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                                        
                        <div class="row" id="estatusSeleccion">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <%--<label class="sr-only" for="lstStatusEditar">Seleccione otro estatus si desea editar</label>--%>
                                    <label for="lstEditarStatus" class="control-label">Estatus</label>
                                    <br />
                                    <%--<asp:ListBox ID="lstEditarStatus" runat="server" CssClass="filtro" AutoPostBack="true" OnSelectedIndexChanged="lstEditarStatus_SelectedIndexChanged" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>--%>
                                    <asp:ListBox ID="lstEditarStatus" runat="server" onChange="lstEditarStatus(this.value);" CssClass="selectpicker" ViewStateMode="Enabled" EnableViewState="true" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="rfvEditarStatus" runat="server" CssClass="help-block" ErrorMessage="Seleccione un estatus..." Display="Dynamic" ControlToValidate="lstEditarStatus" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            </div>
                        </div>
                        <div  class="row" id="estatusMostrar">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="lstMostrarStatus" class="control-label" >Estatus</label>
                                    <br />
                                    <asp:ListBox ID="lstMostrarStatus" runat="server" CssClass="selectpicker" ViewStateMode="Enabled" EnableViewState="true" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
                                   <%-- <asp:RequiredFieldValidator ID="rfvMostrarStatus" runat="server" CssClass="help-block" ErrorMessage="Seleccione un estatus..." Display="Dynamic" ControlToValidate="lstEditarStatus" SetFocusOnError="True" ValidationGroup="editar" />            --%>
                                </div>
                            </div>
                         </div>   
                         <div class="row" id="motivoRechazo">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="lstMotivo" class="control-label">Motivo Rechazo</label>
                                    <asp:ListBox ID="lstMotivo" runat="server" CssClass="" ViewStateMode="Enabled" EnableViewState="true" data-selected-text-format="count > 1" data-size="5" data-width="100%"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="rfvMotivo" runat="server" CssClass="help-block" ErrorMessage="Seleccione un estatus..." Display="Dynamic" ControlToValidate="lstMotivo" SetFocusOnError="True" ValidationGroup="editar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="txtObservacionEditar" class="control-label">Observación</label>
                                    <asp:TextBox ID="txtObservacionEditar" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        
                        <div id="detalleDocumento" class="panel panel-default">
                          <div class="panel-heading"><strong>Detalle documento</strong></div>
                            <ul class="list-group">
                                <li id="modalidadPago" class="list-group-item"></li>
                                <li id="fechaPago" class="list-group-item"></li>
                                <li id="banco" class="list-group-item"></li>
                                <li id="monto2" class="list-group-item"></li>
                                <li id="nroCuenta" class="list-group-item"></li>
                            </ul>
                        </div>

               <div id="uploadFile" class="form-group" style="display:none">
                    <label class="control-label">* Adjuntar documento</label>
                    <br />
                    <asp:FileUpload ID="fArchivo" runat="server" CssClass="file-loading" AllowMultiple="false"  />
                    
                </div>
                    </div>
                    <div id="modalFooter" runat="server" class="modal-footer">
                        <button id="btnHabilitarEdicion" runat="server" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Habilitando formulario...">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Habilitar edición
                        </button>
                        <button id="btnEliminar" runat="server" class="btn btn-danger" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Eliminando documento...">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;&nbsp;Eliminar
                        </button>
                        <button id="btnGuardar" runat="server" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Editando documento..."  ValidationGroup="editar">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
                        </button>
                        <button id="btnCerrar" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function fixform() {
            document.getElementById("aspnetForm").target = '';
        }

    </script>
    <script type="text/javascript">
        //determina si debe activar el detalle de documento
        function detalleDoc($li, value) {
            if (value === '') {
                $li.hide();
            }
            else {
                $li.text(value);
                $li.show();
                $("#detalleDocumento").show();
            }
        }
    </script>

<script type="text/javascript">
        //funcion habilita o deshabilita listbox motivo rechazo si al editar un documento cambia el estatus a Rechazado

        function lstEditarStatus(val) {
            if (val == "Rechazada") {
                //alert("Hubo un cambio de valor en el elemento seleccionado.");
                document.getElementById('motivoRechazo').style.display = 'block';
            } else {
                document.getElementById('motivoRechazo').style.display = 'none';
                $('[id*=lstMotivo]').selectpicker('refresh');

            }
        }

</script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('[id*=calendarioFechaDocumento]').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'),
                showClear: true,
                ignoreReadonly: false
            });

            //activa referencia
            VerificaReferencia(<%= ReferenciaEditar.ClientID %>, <%= lstEditarTipoDocumento.ClientID %>, <%= txtReferenciaEditar.ClientID %>, <%= rfvReferenciaEditar.ClientID %>)

            //carga modal al abrirlo
            $('.modalDocumento').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var bandera = button.data('bandera');
                var nroDocto = button.data('nrodocto');
                var idTipoDocumento = button.data('idtipodocumento');
                var rut = button.data('rut');
                var monto = button.data('monto');
                var fechaDocumento = button.data('fechadocumento');
                var idsociedad = button.data('idsociedad');
                var estatus = button.data('idestado');
                var observacion = button.data('obs');
                var rechazo = button.data('motivo');
                var referencia = button.data('referencia');
                var nombreDoc = button.data('ruta');
                var modal = $(this);

                //establece título
                $('#tituloDoc').text('Documento ' + nroDocto);
                $("#<%= btnGuardar.ClientID %>").hide();
                $("#btnCerrar").hide();
                if (estatus === "Pagada" || estatus === "Aprobada") {
                    document.getElementById('estatusSeleccion').style.display = 'none';
                    document.getElementById('estatusMostrar').style.display = 'block';
                    document.getElementById('motivoRechazo').style.display = 'none';
                } else if (estatus === "Rechazada") {
                    document.getElementById('estatusSeleccion').style.display = 'none';
                    document.getElementById('estatusMostrar').style.display = 'block';
                    document.getElementById('motivoRechazo').style.display = 'block';
                } else {
                    document.getElementById('estatusSeleccion').style.display = 'block';
                    document.getElementById('estatusMostrar').style.display = 'none';
                    document.getElementById('motivoRechazo').style.display = 'none';
                }

                $("#detalleDocumento").hide();

                //verifica si habilita editar y eliminar
                if (bandera === "X") {
                    $("#<%= btnHabilitarEdicion.ClientID %>").show();
                    $("#<%= btnEliminar.ClientID %>").show();
                }
                else {
                    $("#<%= btnHabilitarEdicion.ClientID %>").hide();
                    $("#<%= btnEliminar.ClientID %>").hide();

                    //despliega detalle de documento... si obtiene algún dato, el detalle de documento será abierto
                    detalleDoc(modal.find('#modalidadPago'), button.data('modalidadpago'));
                    detalleDoc(modal.find('#fechaPago'), button.data('fechapago'));
                    detalleDoc(modal.find('#banco'), button.data('banco'));
                    detalleDoc(modal.find('#monto2'), button.data('monto2'));
                    detalleDoc(modal.find('#nroCuenta'), button.data('nrocuenta'));
                }



                //verifica si desplegar referencia
                ShowReferencia(idTipoDocumento, <%= ReferenciaEditar.ClientID %>, <%= txtReferenciaEditar.ClientID %>, <%= rfvReferenciaEditar.ClientID %>);

                //bloquea formulario
                $('.modal-body :input').attr("readonly", "1");
                $('[id*=lstEditar]').prop('disabled', true);
                $('[id*=lstEditar]').selectpicker('refresh');
                $('[id*=lstMostrar]').prop('disabled', true);
                $('[id*=lstMostrar]').selectpicker('refresh');
                $('[id*=lstMotivo]').prop('disabled', true);
                $('[id*=lstMotivo]').selectpicker('refresh');

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

                //almacena valores antes de editar el formulario
                $(<%= hfRowId.ClientID %>).val(button.context.id)
                $(<%= hfRutProveedorAntiguo.ClientID %>).val(rut);
                $(<%= hfNroDocAntiguo.ClientID %>).val(nroDocto);
                $(<%= hfMontoAntiguo.ClientID %>).val(monto);
                $(<%= hfObsAntiguo.ClientID %>).val(observacion);
                $(<%= hfFechaDocAntiguo.ClientID %>).val(fechaDocumento);
                $(<%= hfIdSociedadAntiguo.ClientID %>).val(idsociedad);
                $(<%= hfIdTipoDocAntiguo.ClientID %>).val(idTipoDocumento);
                $(<%= hfReferenciaAntiguo.ClientID %>).val(referencia);
                $(<%= hfEstatusAntiguo.ClientID %>).val(estatus);
                $(<%= hfMotivoAntiguo.ClientID %>).val(rechazo);
                $(<%= hfnombreDoc.ClientID %>).val(nombreDoc);

                //asigna valores al formulario
                modal.find('#<%= txtRutProveedorEditar.ClientID %>').val(rut);
                textboxRutFormat(<%= txtRutProveedorEditar.ClientID %>); <%-- le da formato a rut en el textbox --%>
                modal.find('#<%= txtNroDocumentoEditar.ClientID %>').val(nroDocto);
                modal.find('#<%= txtMontoEditar.ClientID %>').val(monto);
                modal.find('#<%= txtObservacionEditar.ClientID %>').val(observacion);
                modal.find('#<%= txtReferenciaEditar.ClientID %>').val(referencia);
                $('[id*=calendarioFechaDocumento]').data("DateTimePicker").date(fechaDocumento);
                $('#<%= lstEditarSociedades.ClientID %>').selectpicker('val', idsociedad);
                $('#<%= lstEditarTipoDocumento.ClientID %>').selectpicker('val', idTipoDocumento);
                $('#<%= lstMostrarStatus.ClientID %>').selectpicker('val', estatus);
                $('#<%= lstEditarStatus.ClientID %>').selectpicker('val', estatus);
                $('#<%= lstMotivo.ClientID %>').selectpicker('val', rechazo);


            });

            $('.modalMessage').on('hidden.bs.modal', function (e) {
                var accion = $(<%= hfModalAccion.ClientID %>).val();

                if (accion !== "") {
                    var rowAfectada = $("#" + $(<%= hfRowAfectada.ClientID %>).val()).parent().parent();

                    if (accion === "Eliminar") {
                        var time = 1000;

                        $(rowAfectada).addClass("bg-danger").fadeOut(time);

                        setTimeout(function () {
                            $(rowAfectada).remove();
                            var rowCount = $('[id*=gvDocumentos] tr').length - 1;
                            var $lblInfo = $("[id*=lblInfo]");
                            var $contDoc = $lblInfo.find("strong");

                            if (rowCount === 0) {
                                $lblInfo.removeClass().addClass("alert-danger");
                                $lblInfo.html("Nos quedamos sin registros para la búsqueda realizada <i class='fa fa-frown-o' aria-hidden='true'></i>...");
                                $("[id*=btnExportarDataExcel]").hide();
                                $("[id*=gvDocumentos]").hide();
                            }
                            else if (rowCount > 1) {
                                $contDoc.text(rowCount + " documentos");
                            }
                            else if (rowCount === 1) {
                                $contDoc.text("1 documento");
                            }
                        }, time);


                    }

                    if (accion == "Editar") {
                        $(rowAfectada).addClass("bg-info").fadeOut(200).fadeIn(1000);

                        //datos nuevos de la edición
                        var idSociedad = $("[id*=lstEditarSociedades]").selectpicker('val');
                        var rutProveedor = $("[id*=txtRutProveedorEditar]").val();
                        var nroDoc = $("[id*=txtNroDocumentoEditar]").val();
                        var idTipoDoc = $("[id*=lstEditarTipoDocumento]").selectpicker('val');
                        var referencia = $("[id*=txtReferenciaEditar]").val();
                        var fechaDocumento = $("[id*=txtFechaDocumentoEditar]").val();
                        var monto = $("[id*=txtMontoEditar]").val();
                        var idStatus = $("[id*=lstEditarStatus]").selectpicker('val');
                        var obs = $("[id*=txtObservacionEditar]").val();
                        var motivo1 = $("[id*=lstMotivo]").selectpicker('val');
                        var $linkDoc = $(rowAfectada).find('td:eq(1)').find('a');

                        <%--actualiza data del vínculo del documento--%>
                        $linkDoc.data("idtipodocumento", idTipoDoc);
                        $linkDoc.data("nrodocto", nroDoc);
                        $linkDoc.data("monto", monto);
                        $linkDoc.data("fechadocumento", fechaDocumento);
                        $linkDoc.data("idsociedad", idSociedad);
                        $linkDoc.data("idestado", idStatus);
                        $linkDoc.data("motivo", motivo1);
                        $linkDoc.data("obs", obs);
                        $linkDoc.data("referencia", referencia);
                        <%--si actualiza a pagada o aprobada el documento no debe volver a editarse--%>

                        if (idStatus === "Pagada" || idStatus === "Aprobada" || idStatus === "Rechazada")
                            $linkDoc.data("bandera", "");

                        $(rowAfectada).find('td:eq(0)').text(fechaDocumento); <%--Fecha emisión--%>
                        $linkDoc.text(nroDoc); <%--N doc--%>
                        $(rowAfectada).find('td:eq(3)').text(monto); <%--Monto--%>
                        $(rowAfectada).find('td:eq(6)').text($("[id*=lstEditarStatus]").find("option:selected").text()); <%--Estado--%>


                    }

                    $(<%= hfModalAccion.ClientID %>).val("");
                }
            });

            <%--habilita formulario--%>
            $("#<%= btnHabilitarEdicion.ClientID %>").on('click', function () {
                $('#<%= btnEliminar.ClientID %>').hide();

                var $this = $(this);
                $this.button('loading');

                setTimeout(function () {
                    $('#tituloDoc').text('Editar documento ' + $(<%= hfNroDocAntiguo.ClientID %>).val());
                    var idTipoDoc = $("[id*=lstEditarTipoDocumento]").selectpicker('val');



                    //Desbloquea formulario: si documento es electrónico debe permitir sólamente modificar el estado
                    if (idTipoDoc === "KJ" || idTipoDoc === "KK" || idTipoDoc === "KG" || idTipoDoc === "KC" || idTipoDoc === "KB") {
                        $('#<%= lstEditarStatus.ClientID %>').prop('disabled', false);
                        $('#<%= lstEditarStatus.ClientID %>').selectpicker('refresh');
                        $('#<%= lstMotivo.ClientID %>').prop('disabled', false);
                        $('#<%= lstMotivo.ClientID %>').selectpicker('refresh');
                        $('#<%= txtObservacionEditar.ClientID %>').removeAttr("readonly");
                        $('#<%= fArchivo.ClientID %>').prop('disabled', false);


                    } else {
                        $('.modal-body :input').removeAttr("readonly"); //remueve readonly de textbox
                        $('[id*=lstEditar]').prop('disabled', false); //remueve disabled de listbox
                        $('[id*=lstEditar]').selectpicker('refresh');
                        $('[id*=lstMotivo]').prop('disabled', false); //remueve disabled de listbox
                        $('[id*=lstMotivo]').selectpicker('refresh');
                        $('[id*=fArchivo]').selectpicker('refresh');
                    }


                    var nDoc = $(<%= hfnombreDoc.ClientID %>).val();
                    if (nDoc != "") {
                        var $uF = $("#uploadFile");
                        $uF.show();

                        $("#<%= fArchivo.ClientID %>").fileinput('refresh', {
                        uploadUrl: "uploadFile.ashx?nroDoc=" + nDoc,
                        allowedFileExtensions: ['pdf'],
                        maxFileCount: 1,
                        uploadAsync: true,
                        showBrowse: true,
                        showUpload: false,
                        showRemove: false,
                        showUploadedThumbs: true,
                        initialPreviewShowDelete: false,
                        browseOnZoneClick: true,
                        layoutTemplates: {
                            actionUpload: ''
                        }
                    });
                    }


                    $this.button('reset');

                    $this.hide();
                    $(<%= btnEliminar.ClientID %>).hide();
                    $(<%= btnGuardar.ClientID %>).show();
                    $("#btnCerrar").show();

                }, 1500);
                return false;
            });

            //elimina documento
            $('#<%= btnEliminar.ClientID %>').on('click', function () {
                var $this = $(this);
                $this.button('loading');
                //lógica
                $.ajax({
                    type: "POST",
                    url: "consultar.aspx/SetEstadoDocumento",
                    data: "{'idSociedad': '" + $(<%= hfIdSociedadAntiguo.ClientID %>).val() + "', 'rut': '" + $(<%= hfRutProveedorAntiguo.ClientID %>).val() + "', 'nroDocumento': '" + $(<%= hfNroDocAntiguo.ClientID %>).val() + "', 'fechaDoc': '" + $(<%= hfFechaDocAntiguo.ClientID %>).val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        setTimeout(function () {
                            $('.modalDocumento').modal('hide');

                            if (response.d.codigo === "0") {
                                var idLink = $("#<%= hfRowId.ClientID %>").val();
                                $(<%= hfRowAfectada.ClientID %>).val(idLink);
                                $(<%= hfModalAccion.ClientID %>).val("Eliminar");
                            }

                            getMessage(response.d.mensaje, response.d.codigo);
                        }, 4000);
                    },
                    error: function (response) {
                        setTimeout(function () {
                            //getMessage(response.responseText, "2");
                            getMessage("No tiene Permisos para realizar esta acción", "2");
                        }, 1000);
                    }
                });

                //evita postback
                return false;
            });

            //función actualiza documento y fila de gridview
            $('#<%= btnGuardar.ClientID %>').on('click', function () {
                var $this = $(this);
                var validationGroup = $this.attr("validationgroup");
                if (Page_ClientValidate(validationGroup)) {
                    $this.button('loading');
                    var pdf = $("#<%= fArchivo.ClientID %>");
                    pdf.fileinput('upload');
                    //lógica
                    $.ajax({
                        type: "POST",
                        url: "consultar.aspx/SetDocumento",
                        data: "{'nroDocumentoAntiguo': '" + $(<%= hfNroDocAntiguo.ClientID %>).val() + "', 'idTipoDocumentoAntiguo': '" + $(<%= hfIdTipoDocAntiguo.ClientID %>).val() + "', 'referenciaAntiguo': '" + $(<%= hfReferenciaAntiguo.ClientID %>).val() + "', 'rutProveedorAntiguo': '" + $(<%= hfRutProveedorAntiguo.ClientID %>).val() + "', 'idSociedadAntigua': '" + $(<%= hfIdSociedadAntiguo.ClientID %>).val() + "', 'montoAntiguo': '" + $(<%= hfMontoAntiguo.ClientID %>).val() + "', 'fechaDocumentoAntigua': '" + $(<%= hfFechaDocAntiguo.ClientID %>).val() + "','motivoAntiguo': '" + $(<%= hfMotivoAntiguo.ClientID %>).val() + "', 'idEstadoAntiguo': '" + $(<%= hfEstatusAntiguo.ClientID %>).val() + "', 'observacionAntiguo': '" + $(<%= hfObsAntiguo.ClientID %>).val() + "', 'nroDocumentoNuevo': '" + $("#<%=txtNroDocumentoEditar.ClientID%>").val() + "', 'idTipoDocumentoNuevo': '" + $("#<%= lstEditarTipoDocumento.ClientID %>").selectpicker('val') + "', 'referenciaNuevo': '" + $("#<%=txtReferenciaEditar.ClientID%>").val() + "', 'rutProveedorNuevo': '" + $("#<%=txtRutProveedorEditar.ClientID%>").val() + "', 'montoNuevo': '" + $("#<%=txtMontoEditar.ClientID%>").val() + "', 'fechaDocumentoNuevo': '" + $("#<%=txtFechaDocumentoEditar.ClientID%>").val() + "', 'idSociedadNuevo': '" + $(<%= lstEditarSociedades.ClientID %>).selectpicker('val') + "', 'idEstadoNuevo': '" + $(<%= lstEditarStatus.ClientID %>).selectpicker('val') + "', 'observacionNuevo': '" + $(<%= txtObservacionEditar.ClientID %>).val() + "', 'motivoNuevo': '" + $(<%= lstMotivo.ClientID %>).selectpicker('val') + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            setTimeout(function () {
                                $this.button('reset');
                                $('.modalDocumento').modal('hide');

                                if (response.d.codigo === "0") {
                                    var idLink = $("#<%= hfRowId.ClientID %>").val();
                                    $(<%= hfRowAfectada.ClientID %>).val(idLink);
                                    $(<%= hfModalAccion.ClientID %>).val("Editar");
                                    pdf.fileinput('clear');
                                    $("#uploadFile").hide();
                                }

                                getMessage(response.d.mensaje, response.d.codigo);
                                pdf.fileinput('clear');
                                $("#uploadFile").hide();
                            }, 4000);
                        },
                        error: function (response) {
                            setTimeout(function () {
                                $this.button('reset');
                                //getMessage(response.responseText, "2");
                                getMessage("No tiene Permisos para realizar esta acción", "2");
                            }, 1000);
                        }
                    });

                    //evita postback
                    return false;
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.dataTable.ext.order['dom-text-numeric'] = function (settings, col) {
                return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
                    var value = $('a, span', td).text().replace(/\./g, '') * 1
                    //console.log(value);
                    return value;
                });
            }

            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-euro-pre": function (a) {
                    var x;

                    if ($.trim(a) !== '') {
                        var frDatea = $.trim(a).split(' ');
                        var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00, 00, 00];
                        var frDatea2 = frDatea[0].split('-');
                        x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + ((undefined != frTimea[2]) ? frTimea[2] : 0)) * 1;
                    }
                    else {
                        x = Infinity;
                    }
                    return x;
                },
                "date-euro-asc": function (a, b) {
                    return a - b;
                },
                "date-euro-desc": function (a, b) {
                    return b - a;
                }
            });

            //datatable
            $('#<%=gvDocumentos.ClientID%>').DataTable({
                searching: false,
                paging: false,
                info: false,
                "columns": [
                    null,
                    { "orderDataType": "dom-text-numeric" },
                    null,
                    { "orderDataType": "dom-text-numeric" },
                    null,
                    null,
                    null,
                    null,
                    null
                ],
                aoColumnDefs: [
                    { aTargets: [0], sType: "date-euro" },
                    { aTargets: [1], sType: "currency" },
                    { aTargets: [3], sType: "currency" },
                    { aTargets: [4], sType: "date-euro" },
                    { aTargets: [5], sType: "date-euro" }
                ]
            });
        });
    </script>
    <asp:HiddenField id="hfModalAccion" runat="server"/>
    <asp:HiddenField ID="hfRowAfectada" runat="server" />

    <asp:HiddenField id="hfRowId" runat="server"/>
    <asp:HiddenField id="hfRutProveedorAntiguo" runat="server"/>
    <asp:HiddenField id="hfIdTipoDocAntiguo" runat="server"/>
    <asp:HiddenField id="hfReferenciaAntiguo" runat="server"/>
    <asp:HiddenField id="hfNroDocAntiguo" runat="server"/>
    <asp:HiddenField id="hfMontoAntiguo" runat="server"/>
    <asp:HiddenField id="hfFechaDocAntiguo" runat="server"/>
    <asp:HiddenField id="hfIdSociedadAntiguo" runat="server"/>
    <asp:HiddenField id="hfEstatusAntiguo" runat="server"/>
    <asp:HiddenField id="hfMotivoAntiguo" runat="server"/>
    <asp:HiddenField id="hfObsAntiguo" runat="server"/>
    <asp:HiddenField id="hfnombreDoc" runat="server"/>
    
    
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>