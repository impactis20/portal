﻿<%@ Page Title="Pago proveedores" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="pagos.aspx.cs" Inherits="PortalProveedores.App.UI.app.reporte.pagos" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('.selectpicker').selectpicker({
                title: 'Seleccione una opción...',
                showTick: true
            });

            //datatable
            $('#<%= gvPago.ClientID%>').DataTable({
                searching: false,
                paging: false,
                info: false
            });

            $('.tooltip-wrapper-header-table').tooltip({ placement: "top" });
        });
    </script>

    <h2 class="page-header">Pago proveedores</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    <p>Para realizar una búsqueda debe rellenar los campos con asterisco.</p>
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <%--<label class="sr-only">La sociedad a consultar es Colmena Compañía Seguro de Vida</label>--%>
                                <label class="control-label">Sociedad</label>
                                <br />
                                <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" disabled="" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <%--<label class="sr-only">Ingrese el rut del proveedor</label>--%>
                                <label class="control-label">* Rut Proveedor</label>
                                <br />
                                <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control input-sm" MaxLength="10" autocomplete="off" placeholder="Ingrese..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);"></asp:TextBox>
                                <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="buscar"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <%--<label class="sr-only">Ingrese número de documento</label>--%>
                                <label class="control-label">Número Documento</label>
                                <br />
                                <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="form-control input-sm" MaxLength="16" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2" style="margin-top:20px">
                        <div class="row">
                            <button id="btnBuscar" runat="server" type="submit" class="btn btn-primary btn-sm" onserverclick="btnBuscar_ServerClick" validationgroup="buscar">
                                <i class="fa fa-search"></i>
                                &nbsp;&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-9">
          <asp:Label ID="lblInfo" runat="server"></asp:Label>
      </div>
      <div class="col-md-3 text-right">
          <button id="btnExportarDataExcel" runat="server" type="button" class="btn btn-success btn-xs" onserverclick="btnExportarDataExcel_ServerClick" visible="false"><i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp;&nbsp;Exportar a excel</button>
      </div>
    </div>
    <div class="table-responsive small">
        <asp:GridView ID="gvPago" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Folio del documento.">Factura</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("nroDocumento") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha del documento.">Fecha Doc.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaEmision") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Tipo de Documento.">Clase Doc.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("tipoDocumento.descripcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Descripción del documento.">Denominación</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("tipoDocumento.id") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto del documento.">Importe Doc.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("monto") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número de documento de pago.">N° Doc pago.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("nroDocumentoPago") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="ID Banco.">Bco. prop.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("banco") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Número de cheque.">N° Cheque.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("nroCheque") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de pago del documento.">F. Pago</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaPago") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Monto del cheque.">Importe Pag.</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("monto2") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Estado del documento.">Estado</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("status.descripcion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <div class="tooltip-wrapper-header-table pull-left" title="Fecha de cobro del documento.">F. Cobro</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("fechaCobro") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>    
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>