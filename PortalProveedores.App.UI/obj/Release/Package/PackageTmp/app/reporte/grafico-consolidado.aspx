﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="grafico-consolidado.aspx.cs" Inherits="PortalProveedores.App.UI.app.reporte.grafico_consolidado" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
           <script type="text/javascript">
     $(function () {
    $('[id*=calendarioFechaDesde]').datetimepicker({
             useCurrent: false, //mantiene la fecha ingresa después de realizar postback
             format: 'DD-MM-YYYY',
             maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
             //minDate: moment().add(-3, 'month'),
             showClear: true,
             ignoreReadonly: false
         });

         $('[id*=calendarioFechaHasta]').datetimepicker({
             useCurrent: false, //Important! See issue #1075
             format: 'DD-MM-YYYY',
             maxDate: moment().add(1, 'days'),
             showClear: true,
             ignoreReadonly: false
               });
               });
    </script>
      <h2 class="page-header">Grafico Consolidado</h2>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline">
                    
                     <div class="col-lg-4">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <label class="control-label">Fecha desde(emisión)</label>
                                <br/>
                                <div class="input-group date input-group" id="calendarioFechaDesde">
                                <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                           <span class="input-group-addon">
											<span class="fa fa-calendar"></span>
											</span>
                                </div>    
                                </div>
                        </div>
                    </div>
                        <div class="col-lg-4">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                <label class="control-label">Fecha hasta(emisión)</label>
                                 <br/>
                                <div class="input-group date input-group" id="calendarioFechaHasta">
                                <asp:TextBox ID="txtFechaHasta" runat="server"   CssClass="form-control input-sm" placeholder="Seleccione..." autocomplete="off"></asp:TextBox>
                             <span class="input-group-addon">
											<span class="fa fa-calendar"></span>
											</span>
                                      </div>
                                    </div>
                        </div>
                    </div>

                        <div class="col-lg-2">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                                    </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="form-group" style="margin-right: 7px">
                    <asp:Chart ID="Chart1" runat="server">
                        <Series>
                            <asp:Series Name="Proveedores que Ingresan" ChartType="Pie"></asp:Series>
                        </Series>
                         <Series>
                            <asp:Series Name="Proveedores que No Ingresan" ChartType="Pie"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>

                             </div>
                        </div>
                    </div>
                <div class="col-lg-6">
                        <div class="row">
                            <div class="form-group">
                                <%-- tabla gridview --%>
                                <asp:GridView ID="GridView1" runat="server" ItemType="PortalProveedores.App.UI.app.reporte.Grafico" SelectMethod="ObtenerProveedores" AutoGenerateColumns="false" >
                              
                           <Columns>
                            <asp:TemplateField HeaderText="Rut">
                                <ItemTemplate><%# Item.Rut %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Razón Social">
                                <ItemTemplate><%# Item.RazonSocial %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Correo">
                                <ItemTemplate><%# Item.Correo %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Telefono">
                                <ItemTemplate><%# Item.Telefono %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Fecha Creación">
                                <ItemTemplate><%# Item.FechaCreacion %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Fecha Ultimo Acceso">
                                <ItemTemplate><%# Item.UltimoAcceso %></ItemTemplate>
                                </asp:TemplateField>
                         </Columns>
                                    </asp:GridView>
                            </div>
                        </div>
                    </div>
                 <div class="col-lg-6">
                        <div class="row">
                            <div class="form-group">
                                <%-- tabla gridview --%>
                                <asp:GridView ID="GridView2" runat="server" ItemType="PortalProveedores.App.UI.app.reporte.Grafico" SelectMethod="ObtenerProveedores2" AutoGenerateColumns="false" >
                              
                           <Columns>
                            <asp:TemplateField HeaderText="Rut">
                                <ItemTemplate><%# Item.Rut %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Razón Social">
                                <ItemTemplate><%# Item.RazonSocial %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Correo">
                                <ItemTemplate><%# Item.Correo %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Telefono">
                                <ItemTemplate><%# Item.Telefono %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Fecha Creación">
                                <ItemTemplate><%# Item.FechaCreacion %></ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Fecha Ultimo Acceso">
                                <ItemTemplate><%# Item.UltimoAcceso %></ItemTemplate>
                                </asp:TemplateField>
                         </Columns>
                                    </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
