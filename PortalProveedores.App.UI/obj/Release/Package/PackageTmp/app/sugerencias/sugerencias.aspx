﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/entornoMenu.Master" AutoEventWireup="true" CodeBehind="sugerencias.aspx.cs" Inherits="PortalProveedores.App.UI.app.sugerencias.sugerencias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>'></script>
  <script>
      $(document).ready(function () {
          $("#<%= txtNombreDocumento.ClientID %>").focusout(function () {
                var nroDoc = $("#<%= txtNombreDocumento.ClientID %>").val();
                var $uF = $("#DocComercial");

                if (nroDoc !== "") {
                    $uF.show();
                }
                else {
                    $uF.hide();
                    $("#<%= fArchivo.ClientID %>").fileinput('reset');
                }
          });

      });
    </script>
    <script type="text/javascript">
     $(function () {
         //inicia combobox por defecto
         $('.selectpicker').selectpicker({
             title: 'Seleccione...',

             showTick: true
         });
         $('.tooltip-wrapper-form').tooltip({ container: 'body', placement: function () { return $(window).width() < 1183 ? 'top' : 'right'; } });
     });
    </script>
    <style type="text/css">
        .tooltip{
            max-width: 100% /* Max Width of the popover (depending on the container!) */
        }
    </style>
    <h2 class="page-header">Contáctanos</h2>
      <div class="form-group">
        <div class="col-lg-4">
            <div class="row">
               <div class="form-group">
                    <label class="control-label">* Sociedad</label>
                     <br />
                     <div id="" class="tooltip-wrapper-form" title="Seleccione una sociedad">
                    <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                   </div>
                    <asp:RequiredFieldValidator ID="rfvSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstSociedades" SetFocusOnError="True" ValidationGroup="enviar" />
                </div>
                <div class="form-group">
                    <label class="control-label">* Tipo de Solicitud</label>
                    <br />
                    <div id="" class="tooltip-wrapper-form" title="Seleccione un tipo de cuenta">
                     <asp:ListBox ID="lstTipoSolicitud" runat="server"   CssClass="selectpicker"  data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                 </div>
                    <asp:RequiredFieldValidator ID="rfvTipoSolicitudes" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstTipoSolicitud" SetFocusOnError="True" ValidationGroup="enviar" />
                </div>
                    <br />
                <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <br />
                      <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine" Rows="3" MaxLength="1500" CssClass="form-control input-sm"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" CssClass="help-block" ErrorMessage="Ingrese descripción..." Display="Dynamic" ControlToValidate="txtDescripcion" SetFocusOnError="True" ValidationGroup="enviar" />
                </div>
                 <h4>¿Desea ingresar imagen?</h4>
                  <div class="form-group">
                    <label class="control-label">* Nombre Imagen</label>
                    <br />
                    <div id="" class="tooltip-wrapper-form" title="Ingrese imagen">
                      <asp:TextBox ID="txtNombreDocumento" runat="server" cssClass="form-control input-sm" ></asp:TextBox>
                    </div>
                </div>
                   <br />
                 <div id="DocComercial" class="form-group" style="display:none">
                        <label>* Adjuntar Documento</label>
                        <asp:FileUpload ID="fArchivo" runat="server" CssClass="file input-sm" AllowMultiple="false" accept="image/jpeg" data-validation-allowing="jpg" data-validation-error-msg="Elija una imagen con formato JPG." data-validation="required" data-show-upload="false" data-show-caption="true" />
                        <asp:RegularExpressionValidator ID="reArchivo" runat="server" ErrorMessage="Tipo de archivo no permitido" ControlToValidate="fArchivo" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.png|.PNG|.jpeg|.JPEG)$" Enabled = "true" ValidationGroup="enviar" ></asp:RegularExpressionValidator> 
                 </div>
                <div  class="form-group">
                        <div class="col-lg-8" style="margin-left: 50px">
                        <button id="btnEnviar" runat="server" type="submit" onserverclick="btnEnviar_ServerClick" validationgroup="enviar" class="btn btn-primary  btn-block" >
                            Enviar
                        </button>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    <script>
        $.validate({
            modules: 'fArchivo'
        });
</script>
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/textbox-format.numeral.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>

    
</asp:Content>
