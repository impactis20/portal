﻿<%@ Page Title="" Language="C#" MasterPageFile="~/headerFooter.Master" AutoEventWireup="true" CodeBehind="sugerencias-externas.aspx.cs" Inherits="PortalProveedores.App.UI.sugerencias_externas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/bootstrap-select.css" rel="stylesheet" />
    <link href="css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("js/bootstrap-select.js") %>' type="text/javascript"></script>
     <script src='<%= ResolveUrl("js/fileinput.min.4.4.2.js") %>'></script>
    <script type="text/javascript">
        $(function () {
            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',

                showTick: true
            });
            $('.tooltip-wrapper-form').tooltip({ container: 'body', placement: function () { return $(window).width() < 1183 ? 'top' : 'right'; } });
        });
    </script>
    <style type="text/css">
        .tooltip{
            max-width: 100% /* Max Width of the popover (depending on the container!) */
        }
    </style>
     <div  style="margin-top: 50px;" class="mainbox col-md-4 col-md-offset-4 col-sm-12 col-sm-offset-2">
         <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="text-center">
                        <h4>Contáctanos</h4>
                    </div>
                </div>
            </div>
             <div style="padding-top: 30px" class="panel-body">
               <div class="form-group">
                    <label class="control-label">* Sociedad</label>
                     <br />
                     <div id="" class="tooltip-wrapper-form" title="Seleccione una sociedad">
                    <asp:ListBox ID="lstSociedades" runat="server" CssClass="selectpicker" data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                   </div>
                    <asp:RequiredFieldValidator ID="rfvSociedades" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstSociedades" SetFocusOnError="True" ValidationGroup="enviar"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <label class="control-label">* Rut Proveedor</label>
                    <br />
                    <div id="" class="tooltip-wrapper-form" title="Ingrese rut proveedor">
                      <asp:TextBox ID="txtRutProveedor" runat="server" cssClass="form-control input-sm" onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" placeholder="Ingrese..."></asp:TextBox>
                    </div>
                      <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="enviar"></asp:CustomValidator>
                </div>
                <div class="form-group">
                    <label class="control-label">* Razón Social</label>
                    <br />
                    <div id="" class="tooltip-wrapper-form" title="Ingrese nombre">
                      <asp:TextBox ID="txtNombre" runat="server"  CssClass="form-control input-sm" placeholder="Ingrese..."></asp:TextBox>
                   </div>
                     <asp:RequiredFieldValidator ID="rfvNombre" runat="server" CssClass="help-block" ErrorMessage="Ingrese nombre..." Display="Dynamic" ControlToValidate="txtNombre" SetFocusOnError="True" ValidationGroup="enviar"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <label class="control-label">* Tipo de Solicitud</label>
                    <br />
                    <div id="" class="tooltip-wrapper-form" title="Seleccione un tipo de cuenta">
                     <asp:ListBox ID="lstTipoSolicitud" runat="server"   CssClass="selectpicker"  data-selected-text-format="count > 1" data-width="100%"></asp:ListBox>
                 </div>
                    <asp:RequiredFieldValidator ID="rfvTipoSolicitudes" runat="server" CssClass="help-block" ErrorMessage="Seleccione..." Display="Dynamic" ControlToValidate="lstTipoSolicitud" SetFocusOnError="True" ValidationGroup="enviar"></asp:RequiredFieldValidator>
                </div>
                    <br />
                <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <br />
                      <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine" Rows="3" MaxLength="1500" CssClass="form-control input-sm"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" CssClass="help-block" ErrorMessage="Ingrese descripción..." Display="Dynamic" ControlToValidate="txtDescripcion" SetFocusOnError="True" ValidationGroup="enviar"></asp:RequiredFieldValidator>
                </div>
                <div  class="form-group">
                        <div class="col-lg-8" style="margin-left: 50px">
                        <button id="btnEnviar" runat="server" type="submit" onserverclick="btnEnviar_ServerClick" validationgroup="enviar" class="btn btn-primary  btn-block" >
                            Enviar
                        </button>
                        <div class="text-center" style="margin-top:15px">
                                    <a href="login.aspx" target="_self">Volver al Login</a>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
          </div>


    <script src='<%= ResolveUrl("js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("js/app/bootstrap-showMessageModal.v1.js") %>'></script>
    <script src='<%= ResolveUrl("js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>
