﻿function extendedValidatorUpdateDisplay(obj) {
    // Appelle la méthode originale
    if (typeof originalValidatorUpdateDisplay === "function") {
        originalValidatorUpdateDisplay(obj);
    }

    // Récupère l'état du control (valide ou invalide) 
    // et ajoute ou enlève la classe has-error
    var control = document.getElementById(obj.controltovalidate);
    if (control) {
        var isValid = true;
        for (var i = 0; i < control.Validators.length; i += 1) {
            if (!control.Validators[i].isvalid) {
                isValid = false;
                break;
            }
        }

        if (isValid) {
            removerErrorControl(control);
        } else {
            errorControl(control);
        }

        return isValid;
    }
}

// Remplace la méthode ValidatorUpdateDisplay
var originalValidatorUpdateDisplay = window.ValidatorUpdateDisplay;
window.ValidatorUpdateDisplay = extendedValidatorUpdateDisplay;

function errorControl(control) {
    $(control).closest(".form-group").addClass("has-error");
    var fieldValidator = $(control).parent().parent().parent().find("span[id*=rfv]");
    fieldValidator.isvalid = false;
    $(fieldValidator).show();
    //$('span[style*="visibility:hidden"],span[style*="visibility: hidden"][id*=rfv]').css("position", "absolute");
    //var fieldValidator = $(control).parent().parent().parent().find("span[id*=rfv]");
    //$(fieldValidator).css("position", "absolute");
}

function removerErrorControl(control) {
    $(control).closest(".form-group").removeClass("has-error");
    control.isvalid = true;
    //$('span[style*="visibility:visible"],span[style*="visibility: visible"][id*=rfv]').css("position", "");
    var fieldValidator = $(control).parent().parent().parent().find("span[id*=rfv]");
    fieldValidator.isvalid = true;
    $(fieldValidator).css("position", "");
    //$(fieldValidator).css("display", "none");
    $(fieldValidator).hide();
}