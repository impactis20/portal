﻿//formatea textbox numéricos
function formatoNumerico(control, event) {
    var e = window.event;
    if (e) {
        var key = e.keyCode;
        if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key === 16 || key === 86 || key === 36 || key === 35 || key === 188 || key === 8 || key === 46 || key == 190) {
            numeral.locale('es'); //asigna idioma a numeral    
            var valor = numeral(control.value).format('0,0');
            control.value = valor;
        } else if (!(key >= 37 && key <= 40)) {
            control.value = control.value.replace(new RegExp(window.event.key, 'g'), '');
        }
    }
}

function formatoNumerico(control, formato, event) {
    var e = window.event
    if (e) {
        var key = e.keyCode;
        if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key === 16 || key === 86 || key === 36 || key === 35 || key === 188 || key === 8 || key === 46 || key == 190) {
            numeral.locale('es'); //asigna idioma a numeral    
            var valor = numeral(control.value).format(formato);
            control.value = valor;
        } else if (!(key >= 37 && key <= 40)) {
            control.value = control.value.replace(new RegExp(window.event.key, 'g'), '');
        }
    }
}