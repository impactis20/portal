'use strict';


self.addEventListener('push', function(event) {
	var data = {};
	if (event.data) {
		data = event.data.json();
	}
		data = data.data;
		var title = data.title
		var message = data.message ;
		var idOrden = data.idOrden ;
  
  var options = {
    body: title,
    icon: '/img/ic_colmena_128.png',
     data: {
         //url: 'http://www.colmenaproveedores.cl/app/ocompra/liberarOrdenesPendientes.aspx',
         url: 'http://4.0.76.202/app/ocompra/liberarOrdenesPendientes.aspx',
    },
  };
  event.waitUntil(
    self.registration.showNotification(message, options)
  );
});


self.addEventListener('notificationclick', function(event) {
  var url = event.notification.data.url;
  event.preventDefault(); // Previene al buscador de mover el foco a la pestaña del Notification
  //window.open(url, '_blank');
  event.waitUntil(clients.openWindow(url));
});
