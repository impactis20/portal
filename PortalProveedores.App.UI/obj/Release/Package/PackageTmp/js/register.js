var registrationId = "";
var hubName        = "", connectionString = "";
var originalUri    = "", targetUri = "", endpoint = "", sasKeyName = "", sasKeyValue = "", sasToken = "";
var senderId    = "1028206679647";
var originalUri    = "", targetUri = "", endpoint = "", sasKeyName = "", sasKeyValue = "", sasToken = "";
var originalUri    = "", targetUri = "", endpoint = "", sasKeyName = "", sasKeyValue = "", sasToken = "";
var toeknID=""
var userId="";
window.onload = function() { 
  // registerWithGCM();  
   updateLog("You have not registered yet. Please provider sender ID and register with GCM and then Notifications Hub."); 
} 

function updateLog(status) {
  console.log(status);
}


async  function registerWithNH(token,user) {
  toeknID=token;
  hubName = "laboratorionh";
  userId=user;
  connectionString = "Endpoint=sb://laboratorionh-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=5wrgSk8UmwADKW8xWChNLR36/oNvEHHVBeS1Bf4aqqc=";
  try {
      splitConnectionString();
      generateSaSToken();
      await sendNHRegistrationRequest();
  }
  catch (error)
  {
      throw error;
  }
}

// From http://msdn.microsoft.com/en-us/library/dn495627.aspx 
function splitConnectionString()
{
  var parts = connectionString.split(';');
  if (parts.length != 3)
  throw "Error parsing connection string";

  parts.forEach(function(part) {
    if (part.indexOf('Endpoint') == 0) {
    endpoint = 'https' + part.substring(11);
    } else if (part.indexOf('SharedAccessKeyName') == 0) {
    sasKeyName = part.substring(20);
    } else if (part.indexOf('SharedAccessKey') == 0) {
    sasKeyValue = part.substring(16);
    }
  });

  originalUri = endpoint + hubName;
}

function generateSaSToken()
{
  targetUri = encodeURIComponent(originalUri.toLowerCase()).toLowerCase();
  var expiresInMins = 10; // 10 minute expiration

  // Set expiration in seconds
  var expireOnDate = new Date();
  expireOnDate.setMinutes(expireOnDate.getMinutes() + expiresInMins);
  var expires = Date.UTC(expireOnDate.getUTCFullYear(), expireOnDate
    .getUTCMonth(), expireOnDate.getUTCDate(), expireOnDate
    .getUTCHours(), expireOnDate.getUTCMinutes(), expireOnDate
    .getUTCSeconds()) / 1000;
  var tosign = targetUri + '\n' + expires;

  // using CryptoJS
  var signature = CryptoJS.HmacSHA256(tosign, sasKeyValue);
  var base64signature = signature.toString(CryptoJS.enc.Base64);
  var base64UriEncoded = encodeURIComponent(base64signature);

  // construct authorization string
  sasToken = "SharedAccessSignature sr=" + targetUri + "&sig="
                  + base64UriEncoded + "&se=" + expires + "&skn=" + sasKeyName;
}

function sendNHRegistrationRequest()
{
    return new Promise((resolve, reject) => {
          var registrationPayload = 
          "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
          "<entry xmlns=\"http://www.w3.org/2005/Atom\">" + 
              "<content type=\"application/xml\">" + 
                  "<GcmRegistrationDescription xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.microsoft.com/netservices/2010/10/servicebus/connect\">" +
                      "<Tags>{UserId}</Tags>"+
			          "<GcmRegistrationId>{GCMRegistrationId}</GcmRegistrationId>" +
                  "</GcmRegistrationDescription>" +
              "</content>" +
          "</entry>";

        //  registrationId= token;
          registrationPayload = registrationPayload.replace("{GCMRegistrationId}", toeknID);
          registrationPayload = registrationPayload.replace("{UserId}", userId);

          var url = originalUri + "/registrations/?api-version=2014-09";
          var client = new XMLHttpRequest();

          client.onload = function () {
            if (client.readyState == 4) {
              if (client.status == 200) {
                updateLog("Notification Hub Registration succesful!");
              /*  updateLog(client.responseText); 
                updateLog("Resulto!!!");*/
                resolve('ok');
                return;
              } else {
                  updateLog("Notification Hub Registration did not succeed!");
                  reject('error');
                   throw "NO es posible suscribirse al servicio Notification Hub ";  
                } 
            } 
          };

          client.onerror = function () {
              updateLog("ERROR - Notification Hub Registration did not succeed!");

              reject('error');
              throw "NO es posible suscribirse al servicio Notification Hub ";  
          }

          client.open("POST", url, true);
          client.setRequestHeader("Content-Type", "application/atom+xml;type=entry;charset=utf-8");
          client.setRequestHeader("Authorization", sasToken);
          client.setRequestHeader("x-ms-version", "2014-09");
          updateLog("client.setRequestHeader");
          try {
              client.send(registrationPayload);

          }
          catch (err) {

              reject('error');
              throw "NO es posible suscribirse al servicio Notification Hub ";  
              updateLog(err.message);
          }

    });
}


