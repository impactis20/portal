﻿<%@ Page Title="Recuperar clave" Language="C#" MasterPageFile="~/headerFooter.Master" AutoEventWireup="true" CodeBehind="recuperar-clave.aspx.cs" Inherits="PortalProveedores.App.UI.recuperar_clave" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div id="loginbox" style="margin-top: 50px;" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="text-center">
                        <h4>Recuperar clave en Portal Proveedores</h4>
                    </div>
                </div>
            </div>

            <div style="padding-top: 30px" class="panel-body">
                <div id="error" runat="server" class="alert alert-danger col-sm-12" visible="false"></div>

                <div id="loginform" class="form-horizontal">
                    <p>Ingrese su RUT y le enviaremos un correo electrónico con su contraseña.</p>
                    <div class="form-group" style="margin-top: 20px">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <asp:TextBox ID="txtRutProveedor" runat="server" CssClass="form-control" MaxLength="10" placeholder="Ingrese RUT..." onkeyup="javascript:textboxRutOnKeyUp(this);" onfocus="javascript:textboxRutOnFocus(this);" onblur="javascript:textboxRutFormat(this);" autocomplete="off"></asp:TextBox>
                            </div>
                            <asp:CustomValidator ID="cvRutProveedor" runat="server" CssClass="help-block" ControlToValidate="txtRutProveedor" ErrorMessage="Ingrese un <strong>RUT</strong> válido..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="customValidatorRutObligatorio" ValidateEmptyText="true" ValidationGroup="recuperar"></asp:CustomValidator>
                        </div>
                    </div>
                    <div style="margin-top: 30px" class="form-group">
                        <div class="col-sm-12">
                            <asp:Button ID="btnEntrar" runat="server" Text="Recuperar" CssClass="btn btn-primary btn-block" OnClick="btnEntrar_Click" ValidationGroup="recuperar" />
                            <div class="text-center" style="margin-top:15px">
                                <a href="login.aspx" target="_self">Volver al Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>    
    <script src='<%= ResolveUrl("~/js/numeral/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral/es.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/rut-format.numeral.v1.js") %>'></script>
</asp:Content>
