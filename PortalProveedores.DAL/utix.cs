﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

public class utix
{
    public static NetworkCredential GetCredencial()
    {
        string Ambiente = ConfigurationManager.AppSettings["ambiente"].ToString();

        switch (Ambiente)
        {
            case "qas":
                return new NetworkCredential(ConfigurationManager.AppSettings["user_pi_qas"].ToString(), ConfigurationManager.AppSettings["pass_pi_qas"].ToString());
            case "prd":
                return new NetworkCredential(ConfigurationManager.AppSettings["user_pi_prd"].ToString(), ConfigurationManager.AppSettings["pass_pi_prd"].ToString());
        }

        return null;
    }

    public static string GetUrlServicio(string urlOriginal)
    {
        string Ambiente = ConfigurationManager.AppSettings["ambiente"].ToString();
        string DNS = string.Empty;

        switch (Ambiente)
        {
            case "qas":
                DNS = ConfigurationManager.AppSettings["dns_pi_qas"].ToString();
                break;
            case "prd":
                DNS = ConfigurationManager.AppSettings["dns_pi_prd"].ToString();
                break;
        }

        var builder = new UriBuilder(urlOriginal);
        builder.Host = DNS;
        return builder.Uri.ToString();
    }
}
