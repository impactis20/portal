﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL
{
    public class SociedadDAL
    {
        public static List<Entities.SociedadEntity> GetAllSociedades()
        {
            List<Entities.SociedadEntity> Lst = new List<Entities.SociedadEntity>();
            Lst.Add(new Entities.SociedadEntity("1001", "Colmena Golden Cross S. A."));
            Lst.Add(new Entities.SociedadEntity("2001", "Serv. Farmacéuticos Dial"));
            Lst.Add(new Entities.SociedadEntity("3000", "Inmobiliaria Golden Cross"));
            Lst.Add(new Entities.SociedadEntity("7000", "Colmena Salud S.A."));
            Lst.Add(new Entities.SociedadEntity("8100", "Servicios Medicos Dial S. A."));
            Lst.Add(new Entities.SociedadEntity("8150", "Laboratorio Dial Spa."));
            Lst.Add(new Entities.SociedadEntity("8200", "Laboratorio IEM Ltda."));
            Lst.Add(new Entities.SociedadEntity("8300", "Inmobiliaria IEM Ltda."));
            Lst.Add(new Entities.SociedadEntity("9000", "Colmena Cia. Seg. de Vida"));

            return Lst;
        }
    }
}
