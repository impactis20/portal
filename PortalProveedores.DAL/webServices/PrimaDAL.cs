﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL.webServices
{
    public class PrimaDAL
    {
        public static devolucionPrima.dt_pifi503_res GetPrimas(string idSociedad, string año, string fechaDesdeR, string fechaHastaR, string fechaDesdeV, string fechaHastaV, string rut, string estado)
        {
            devolucionPrima.dt_pifi503_req request = new devolucionPrima.dt_pifi503_req();
            request.I_SOCIEDAD = idSociedad;
            request.I_FECHA_INI = fechaDesdeR;
            request.I_FECHA_FIN = fechaHastaR;
            request.I_PERIODO = año;
            request.I_FECHA_INI_VEN = fechaDesdeV;
            request.I_FECHA_FIN_VEN = fechaHastaV;
            request.I_RUT = rut;
            request.I_ESTADO_DOC = estado;
            //request.I_PERIODO = string.Empty;

            devolucionPrima.os_pifi503Service service = new devolucionPrima.os_pifi503Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi503(request);
        }
    }
}
