﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL.mandato;
using PortalProveedores.DAL.mandatoBanco;
using PortalProveedores.Entities;

namespace PortalProveedores.DAL.webServices
{
    public class MandatoDAL
    {

        public static List<Entities.MandatoEntity.Banco> GetListBanco()
        {
            List<Entities.MandatoEntity.Banco> Lst = new List<MandatoEntity.Banco>();
            return Lst;
        }

        public static dt_pifi517_res GetListBan(string claveP, string accion)
        {
            mandatoBanco.dt_pifi517_req request = new mandatoBanco.dt_pifi517_req();
            request.I_ACCION = accion;
            request.I_CLAVE_PAIS = claveP;

            mandatoBanco.os_pifi517Service service = new mandatoBanco.os_pifi517Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();
            return service.os_pifi517(request);
        }

        public static mandato.dt_pifi516_res AddMandato(string rut, string banco, string cuenta, string tipoCuenta, string referencia, string correo, string ruta, string accion)
        {
            mandato.dt_pifi516_req acceso = new mandato.dt_pifi516_req();

            List<Entities.MandatoEntity> lstData = new List<MandatoEntity>();
            dt_pifi516_reqItem[] nodo = new dt_pifi516_reqItem[1];

            nodo[0] = new dt_pifi516_reqItem();
            nodo[0].RUT_PROVEEDOR = rut;
            nodo[0].CLAVE_BANCO = banco;
            nodo[0].CUENTA_BANCARIA = cuenta;
            nodo[0].TIPO_CUENTA = tipoCuenta;
            nodo[0].REFERENCIA = referencia;
            nodo[0].CORREO = correo;
            nodo[0].RUTA_PDF = ruta;

            acceso.I_ACCION = accion;
            acceso.I_USUARIO = rut;
            acceso.T_MANDATO_I = nodo;

            mandato.os_pifi516Service service = new mandato.os_pifi516Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi516(acceso);
        }

        public static mandato.dt_pifi516_res GetMandato(string rut, string accion)
        {
            mandato.dt_pifi516_req acceso = new mandato.dt_pifi516_req();

            List<Entities.MandatoEntity> lstData = new List<MandatoEntity>();
            dt_pifi516_reqItem[] nodo = new dt_pifi516_reqItem[1];
            nodo[0] = new dt_pifi516_reqItem();
            nodo[0].RUT_PROVEEDOR = rut;
            acceso.I_ACCION = accion;
            acceso.T_MANDATO_I = nodo;
            acceso.I_USUARIO = rut;

            mandato.os_pifi516Service service = new mandato.os_pifi516Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi516(acceso);
        }
        public static List<Entities.MandatoEntity.Banco> GetBancoDescripcion()
        {
            List<Entities.MandatoEntity.Banco> Lst = new List<Entities.MandatoEntity.Banco>();
            return Lst;
        }

    }
}
