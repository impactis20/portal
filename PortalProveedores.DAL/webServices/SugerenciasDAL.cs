﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL.webServices
{
  public  class SugerenciasDAL
    {
        public static sugerencias.dt_pifi524_res GetSugerencias(string accion, string rutProveedor, string idSoli, string idOrigen, string descripcion, string razon, string idsociedad, string ruta)
        {
            sugerencias.dt_pifi524_req request = new sugerencias.dt_pifi524_req();
            request.I_ACCION = accion;
            request.I_RUT_PROVEEDOR = rutProveedor;
            request.I_ID_SOLI = idSoli;
            request.I_ORIGEN = idOrigen;
            request.I_DESCRIPSOLI = descripcion;
            request.I_RAZON = razon;
            request.I_SOCIEDAD = idsociedad;
            request.I_RUTA_IMAGEN = ruta;
            sugerencias.os_pifi524Service service = new sugerencias.os_pifi524Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi524(request);
        }

        public static sugerencias.dt_pifi524_res GetTipoSugerencia(string accion, string origen)
        {
            sugerencias.dt_pifi524_req request = new sugerencias.dt_pifi524_req();
            request.I_ACCION = accion;
            request.I_ORIGEN = origen;

            sugerencias.os_pifi524Service service = new sugerencias.os_pifi524Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi524(request);
        }
        public static sugerencias.dt_pifi524_res GetSugerenciasExternas(string accion, string rutProveedor, string idSoli, string idOrigen, string descripcion, string razon, string idsociedad)
        {
            sugerencias.dt_pifi524_req request = new sugerencias.dt_pifi524_req();
            request.I_ACCION = accion;
            request.I_RUT_PROVEEDOR = rutProveedor;
            request.I_ID_SOLI = idSoli;
            request.I_ORIGEN = idOrigen;
            request.I_DESCRIPSOLI = descripcion;
            request.I_RAZON = razon;
            request.I_SOCIEDAD = idsociedad;

            sugerencias.os_pifi524Service service = new sugerencias.os_pifi524Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi524(request);
        }
    }
}
