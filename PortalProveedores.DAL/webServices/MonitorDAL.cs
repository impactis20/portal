﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.Entities;
namespace PortalProveedores.DAL.webServices
{
    public class MonitorDAL
    {

        public static monitorOC.dt_pifi523_res GetMonitor(string sociedad, string rutproveedor, string fechaemisionini, string fechaemisionfin, string accion, string idDoc, string id)
        {
            monitorOC.dt_pifi523_req request = new monitorOC.dt_pifi523_req();

            request.I_SOCIEDAD = sociedad;
            request.ID_TIPO_DOC = idDoc;
            request.I_RUT_PROVEEDOR = rutproveedor;
            request.I_FECHA_EMISION_INI = fechaemisionini;
            request.I_FECHA_EMISION_FIN = fechaemisionfin;
            request.I_ACCION = accion;
            request.I_ID_DOCUMENTO = id;

            monitorOC.os_pifi523Service service = new monitorOC.os_pifi523Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            //monitorOC.dt_pifi523_res resp = new monitorOC.dt_pifi523_res();
            //resp = service.os_pifi523(request);

            //return resp;
            return service.os_pifi523(request);
        }
        public static monitorOC.dt_pifi523_res GetDetalleMonitor(string idmoni, string accion, string idsociedad)
        {
            monitorOC.dt_pifi523_req request = new monitorOC.dt_pifi523_req();

            request.I_ID_DOCUMENTO = idmoni;
            request.I_RUT_PROVEEDOR = "";
            request.I_SOCIEDAD = idsociedad;
            request.I_FECHA_EMISION_INI = "";
            request.I_FECHA_EMISION_FIN = "";
            request.I_ACCION = accion;

            monitorOC.os_pifi523Service service = new monitorOC.os_pifi523Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi523(request);
        }
    }
}