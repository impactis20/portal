﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL.webServices
{
    public class SolpedDAL
    {
        public static void AddMaterial(string id, string descripcion, string grupoCompra, string cantidad, string centroCosto, string fechaEntrega, string rutUsuarioResponsable, ref List<Entities.SolpedEntity> lstSolped)
        {
            lstSolped.Add(new Entities.SolpedEntity{
                id = id,
                cantidad = cantidad,
                descripcion=descripcion,
                grupoCompra=grupoCompra,
                rutSolicitante= rutUsuarioResponsable,
                centroCosto = centroCosto,
                fechaEntrega = fechaEntrega
            });
        }

        public static void delMaterial(string id, ref List<Entities.SolpedEntity> lstSolped)
        {
            lstSolped = lstSolped.Where(d => d.id != id).ToList();
        }

        public static buscarMaterial.dt_pifi508_res GetMaterial(string prefix)
        {
            buscarMaterial.dt_pifi508_req request = new buscarMaterial.dt_pifi508_req();
            request.I_DESCRIPCION = prefix;

            buscarMaterial.os_pifi508Service service = new buscarMaterial.os_pifi508Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi508(request);
        }

        public static crearSolped.dt_pifi509_res AddSolped(crearSolped.dt_pifi509_reqItem[] arrMaterial)
        {
            crearSolped.dt_pifi509_req request = new crearSolped.dt_pifi509_req();
            request.I_SOLPED = arrMaterial;

            crearSolped.os_pifi509Service service = new crearSolped.os_pifi509Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi509(request);
        }
    }
}
