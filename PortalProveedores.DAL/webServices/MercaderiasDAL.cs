﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL.mercaderias;
using PortalProveedores.Entities;

namespace PortalProveedores.DAL.webServices
{
    public class MercaderiasDAL
    {
        public static mercaderias.dt_pifi520_res GetOrdenes(string rut, string accion)
        {
            mercaderias.dt_pifi520_req acceso = new mercaderias.dt_pifi520_req();

            List<Entities.MercaderiasEntity> lstData = new List<MercaderiasEntity>();
            dt_pifi520_reqItem[] nodo = new dt_pifi520_reqItem[1];

            acceso.I_USUARIO = rut;
            acceso.I_ACCION = accion;

            mercaderias.os_pifi520Service service = new mercaderias.os_pifi520Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi520(acceso);
        }

        public static mercaderias.dt_pifi520_res GetDetalle(string rut, string accion, string orden)
        {
            mercaderias.dt_pifi520_req acceso = new mercaderias.dt_pifi520_req();

            List<Entities.MercaderiasEntity> lstData = new List<MercaderiasEntity>();
            dt_pifi520_reqItem[] nodo = new dt_pifi520_reqItem[1];

            acceso.I_USUARIO = rut;
            acceso.I_ACCION = accion;
            acceso.I_ORDEN_COMPRA = orden;

            mercaderias.os_pifi520Service service = new mercaderias.os_pifi520Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi520(acceso);
        }

        public static mercaderias.dt_pifi520_res AddContabilizacion(string tipoM, string orden, string posicion, string cantidadR, string fechaD, string observaciones, string fechaS, string accion, string rut, string GuiaD)
        {
            mercaderias.dt_pifi520_req acceso = new mercaderias.dt_pifi520_req();

            List<Entities.MercaderiasEntity> lstData = new List<MercaderiasEntity>();
            dt_pifi520_reqItem[] nodo = new dt_pifi520_reqItem[1];

            nodo[0] = new dt_pifi520_reqItem();
            nodo[0].ZTIPO_MATERIAL = tipoM;
            nodo[0].ZFECHA_CONTAB = fechaS; 
            nodo[0].ZFECHA_DOC = fechaD;
            nodo[0].ZPEDIDO = orden;
            nodo[0].ZPOSICION = posicion;
            nodo[0].ZTEXTO_CABECERA = observaciones;
            nodo[0].ZCANTIDAD = cantidadR;
            nodo[0].ZNOTA_ENTREGA = GuiaD;

            acceso.I_ACCION = accion;
            acceso.I_USUARIO = rut;
            acceso.I_ORDEN_COMPRA = orden;
            acceso.IT_ENTRADA = nodo;

            mercaderias.os_pifi520Service service = new mercaderias.os_pifi520Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi520(acceso);
        }

    }
}
