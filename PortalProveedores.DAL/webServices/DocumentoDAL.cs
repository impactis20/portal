﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL.buscarDocumento;
using PortalProveedores.DAL.motivoRechazo;

namespace PortalProveedores.DAL.webServices
{
    public class DocumentoDAL
    {
        //public static dt_pifi504_res GetDocumento(string idSociedad, string rutProveedor, string fechaDesde, string fechaHasta, string folio, string estado, string fechaRecepcion, string fechaCobro, tipoDoc)
        public static dt_pifi504_res GetDocumento(string idSociedad, string rutProveedor, string fechaDesde, string fechaHasta, string folio, string estado, string fechaRecepcion, string fechaCobro, string tipoDoc)
        {
            buscarDocumento.dt_pifi504_req request = new buscarDocumento.dt_pifi504_req();
            request.I_SOCIEDAD = idSociedad;
            request.I_RUT_EMPRESA = rutProveedor.ToUpper();
            request.I_FECHA_INI = fechaDesde;
            request.I_FECHA_FIN = fechaHasta;
            request.I_FOLIO = folio;
            request.I_ESTADO = estado.ToUpper();
            request.I_FECHA_RECEP = fechaRecepcion;
            request.I_FECHA_COBRO = fechaCobro;
            request.I_TIPO_DOC = tipoDoc;

            buscarDocumento.os_pifi504Service service = new buscarDocumento.os_pifi504Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi504(request);
        }

        public static dt_pifi504_res GetDocumento(string idSociedad, string rutProveedor, string fechaDesde, string fechaHasta, string folio, string estado, string fechaRecepcion, string fechaCobro, string rutaPDF, string idtipoDoc, string motivoRechazo)
        {
            throw new NotImplementedException();
        }

        public static dt_pifi518_res GetMotivo(string accion, string motivo)
        {
            motivoRechazo.dt_pifi518_req request = new motivoRechazo.dt_pifi518_req();
            request.I_ACCION = accion;
            request.I_ID_MOTIVO = motivo;

            motivoRechazo.os_pifi518Service service = new motivoRechazo.os_pifi518Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();
            return service.os_pifi518(request);
        }

        public static ingresarDocumento.dt_pifi501_res AddDocumento(string nroDocumento, string rutProveedor, string idTipoDocumento, string ReferenciaNC, string fechaFactura, string monto, string idSociedad, string observacion, string idEstatus, string rutUsuarioResponsable, string fullPathPDF)
        {
            ingresarDocumento.dt_pifi501_req request = new ingresarDocumento.dt_pifi501_req();
            //request.I_ESTADO = "Recibida";
            request.I_ESTADO = idEstatus.ToUpper();
            request.I_FECHA_FACTURA = fechaFactura;
            request.I_FOLIO_DOCUMENTO = nroDocumento;
            request.I_MONTO_FACTURA = monto;
            request.I_REFERENCIA = ReferenciaNC;
            request.I_RUT_PROVEEDOR = rutProveedor;
            request.I_SOCIEDAD = idSociedad;
            request.I_TIPO_DOC = idTipoDocumento;
            request.I_OBSERVACION = observacion;
            request.I_USUARIO = rutUsuarioResponsable;
            request.I_RUTA_PDF = fullPathPDF;

            ingresarDocumento.os_pifi501Service service = new ingresarDocumento.os_pifi501Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi501(request);
        }

        public static editarDocumento.dt_pifi511_res SetDocumento(Entities.DocumentoEntity documentoAntiguo, Entities.DocumentoEntity documentoNuevo, string rutUsuarioResponsable)
        {
            editarDocumento.dt_pifi511_req request = new editarDocumento.dt_pifi511_req();

            editarDocumento.st_pifi511 requestFactAntigua = new editarDocumento.st_pifi511();

            requestFactAntigua.I_TIPO_DOC = documentoAntiguo.tipoDocumento.id;
            requestFactAntigua.I_SOCIEDAD = documentoAntiguo.sociedad.id;
            requestFactAntigua.I_RUT_PROVEEDOR = documentoAntiguo.rutProveedor;
            requestFactAntigua.I_REFERENCIA = documentoAntiguo.referencia;
            requestFactAntigua.I_MONTO_FACTURA = documentoAntiguo.monto;
            requestFactAntigua.I_FOLIO_DOCUMENTO = documentoAntiguo.nroDocumento;
            requestFactAntigua.I_FECHA_FACTURA = documentoAntiguo.fechaEmision;
            requestFactAntigua.I_OBSERVACION = documentoAntiguo.observacion;
            requestFactAntigua.I_ESTADO = documentoAntiguo.status.id.ToUpper();
            requestFactAntigua.I_MOT_RECHA = documentoAntiguo.motivoRechazo.id;

            editarDocumento.st_pifi511 requestFactNueva = new editarDocumento.st_pifi511();

            requestFactNueva.I_TIPO_DOC = documentoNuevo.tipoDocumento.id;
            requestFactNueva.I_SOCIEDAD = documentoNuevo.sociedad.id;
            requestFactNueva.I_RUT_PROVEEDOR = documentoNuevo.rutProveedor;
            requestFactNueva.I_REFERENCIA = documentoNuevo.referencia;
            requestFactNueva.I_MONTO_FACTURA = documentoNuevo.monto;
            requestFactNueva.I_FOLIO_DOCUMENTO = documentoNuevo.nroDocumento;
            requestFactNueva.I_FECHA_FACTURA = documentoNuevo.fechaEmision;
            requestFactNueva.I_OBSERVACION = documentoNuevo.observacion;
            requestFactNueva.I_ESTADO = documentoNuevo.status.id.ToUpper();
            requestFactNueva.I_MOT_RECHA = documentoNuevo.motivoRechazo.id;

            request.I_FACTURA_ANTIGUA = requestFactAntigua;
            request.I_FACTURA_NUEVA = requestFactNueva;
            request.I_USUARIO_MODIF = rutUsuarioResponsable;

            editarDocumento.os_pifi511Service service = new editarDocumento.os_pifi511Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi511(request);
        }

        public static umbralDocVencimiento.dt_pifi514_res GetUmbralDocumentoVencimiento(string peticion)
        {
            umbralDocVencimiento.dt_pifi514_req request = new umbralDocVencimiento.dt_pifi514_req();
            request.I_PETICION = peticion;

            umbralDocVencimiento.os_pifi514Service service = new umbralDocVencimiento.os_pifi514Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi514(request);
        }

        public static pagoProveedor.dt_pifi510_res GetPago(string idSociedad, string rut, string nroFactura)
        {
            pagoProveedor.dt_pifi510_req request = new pagoProveedor.dt_pifi510_req();
            request.I_SOCIEDAD = idSociedad;
            request.I_RUT_PROVEEDOR = rut.ToUpper();
            request.I_FACTURA = nroFactura;

            pagoProveedor.os_pifi510Service service = new pagoProveedor.os_pifi510Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi510(request);
        }

        public static cambioEstadoDocumento.dt_pifi505_res SetEstadoDocumento(string idSociedad, string rut, string nroDocumento, string fechaDoc, string estado, string rutUsuario)
        {
            cambioEstadoDocumento.dt_pifi505_req request = new cambioEstadoDocumento.dt_pifi505_req();
            request.I_ESTADO = estado.ToUpper();
            request.I_FECHA_FACTURA = fechaDoc;
            request.I_NUMERO_DOCUMENTO = nroDocumento;
            request.I_RUT_EMPRESA = rut;
            request.I_SOCIEDAD = idSociedad;
            request.I_USER_MODIF = rutUsuario;

            cambioEstadoDocumento.os_pifi505Service service = new cambioEstadoDocumento.os_pifi505Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi505(request);
        }

        public static crearProveedor.dt_pifi513_res AddProveedor(string rut, string nombre, string region, string poblacion, string calle, string sociedad, string correo)
        {
            crearProveedor.dt_pifi513_req request = new crearProveedor.dt_pifi513_req();
            request.P_NAME1 = nombre;
            request.P_ORT01 = poblacion;
            request.P_REGIO = region;
            request.P_RUT = rut;
            request.P_SORTL = rut;
            request.P_STRAS = calle;
            request.P_SOCIEDAD = sociedad;
            request.P_CORREO = correo;
            crearProveedor.os_pifi513Service service = new crearProveedor.os_pifi513Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi513(request);
        }

        public static List<Entities.DocumentoEntity.Status> GetStatus()
        {
            List<Entities.DocumentoEntity.Status> Lst = new List<Entities.DocumentoEntity.Status>();
            Lst.Add(new Entities.DocumentoEntity.Status("Recibida", "Recibida", "fa fa-folder-open-o", true, false));
            Lst.Add(new Entities.DocumentoEntity.Status("Pagada", "Pagada", "fa fa-diamond", true, true));
            Lst.Add(new Entities.DocumentoEntity.Status("Aprobada", "Aprobada", "fa fa-check", true, true));
            Lst.Add(new Entities.DocumentoEntity.Status("En Revision", "En revisión", "fa fa-eye", true, false));
            Lst.Add(new Entities.DocumentoEntity.Status("Rechazada", "Rechazada", "fa fa-ban", true, false)); //fa fa-times
            Lst.Add(new Entities.DocumentoEntity.Status("Eliminar", "Eliminar", "fa fa-trash-o", false, false));


            return Lst;
        }

        public static List<Entities.DocumentoEntity.Status> GetStatusMostrar()
        {
            List<Entities.DocumentoEntity.Status> Lst = new List<Entities.DocumentoEntity.Status>();
            Lst.Add(new Entities.DocumentoEntity.Status("Recibida", "Recibida", "fa fa-folder-open-o", true, false));
            Lst.Add(new Entities.DocumentoEntity.Status("Pagada", "Pagada", "fa fa-diamond", true, false));
            Lst.Add(new Entities.DocumentoEntity.Status("Aprobada", "Aprobada", "fa fa-check", true, false));
            Lst.Add(new Entities.DocumentoEntity.Status("En Revision", "En revisión", "fa fa-eye", true, false));
            Lst.Add(new Entities.DocumentoEntity.Status("Rechazada", "Rechazada", "fa fa-ban", true, false)); //fa fa-times
            Lst.Add(new Entities.DocumentoEntity.Status("Eliminar", "Eliminar", "fa fa-trash-o", false, false));


            return Lst;
        }

        public static Entities.DocumentoEntity.TipoDocumento GetTipoDocumento(string idTipoDocumento)
        {
            if (GetTipoDocumento().Exists(x => x.id == idTipoDocumento))
            {
                return GetTipoDocumento().Where(x => x.id == idTipoDocumento).ToList()[0];
            }
            else
            {
                return new Entities.DocumentoEntity.TipoDocumento("XX", "Documento desconocido", false);
            }
        }

        public static List<Entities.DocumentoEntity.MotivoRechazo> GetMotivoDescripcion()
        {
            List<Entities.DocumentoEntity.MotivoRechazo> Lst = new List<Entities.DocumentoEntity.MotivoRechazo>();
            return Lst;
        }


        public static List<Entities.DocumentoEntity.TipoDocumento> GetTipoDocumento()
        {
            List<Entities.DocumentoEntity.TipoDocumento> Lst = new List<Entities.DocumentoEntity.TipoDocumento>();
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KB", "NC Fact Afect Elect", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KC", "NC Fact Excet Elect", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KD", "NC Factura Exenta", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KE", "NC factura Nacional", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KF", "ND Factura Nacional", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KG", "ND Fact Electrónica", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KH", "ND Electro. Exenta", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KI", "ND Factura Exenta", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KJ", "Factura Electrónica", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KK", "Fact Electro Exenta", true));
            //Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KL", "Boleta Honor Electro", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KM", "Factura Exenta", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KR", "Factura Nacional", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("KW", "Boleta de Honorario", true));
            Lst.Add(new Entities.DocumentoEntity.TipoDocumento("FA", "Fact. Elect Afecta", true));


            return Lst;
        }

        //public static List<Entities.>
    }
}
