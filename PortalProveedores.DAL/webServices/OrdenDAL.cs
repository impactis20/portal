﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.Entities;

namespace PortalProveedores.DAL.webServices
{
    public class OrdenDAL
    {

        public static solicitudPedido.dt_pifi519_res GetSolicitud(string USUARIO, string PENDIENTE_LIBERAR, string fechaDesde, string fechaHasta, string SOLPED, string action)
        {
            string fechadesde2 = "";
            string fechahasta2 = "";
            if (fechaDesde.Trim() != "")
            {
                fechadesde2 = fechaDesde.Substring(6, 4) + fechaDesde.Substring(3, 2) + fechaDesde.Substring(0, 2);
            }
            if (fechaHasta.Trim() != "")
            {
                fechahasta2 = fechaHasta.Substring(6, 4) + fechaHasta.Substring(3, 2) + fechaHasta.Substring(0, 2);
            }
            solicitudPedido.dt_pifi519_req request = new solicitudPedido.dt_pifi519_req();
            request.I_USUARIO = USUARIO;
            //request.I_USUARIO = "16619948-4";
            request.I_PENDIENTES_LIBERAR = PENDIENTE_LIBERAR;
            request.I_ACCION = action;
            request.I_FECHA_ORDEN_DESDE = fechadesde2;
            request.I_FECHA_ORDEN_HASTA = fechahasta2;
            request.I_SOLPED = SOLPED;
            
            solicitudPedido.os_pifi519Service service = new solicitudPedido.os_pifi519Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi519(request);
        }

        public static consultarOrden.OrdenConsultarMsgResp GetOrden(string USUARIO, string PENDIENTE_LIBERAR, string rut, string fechaDesde, string fechaHasta, string razonS, string ordenC)
        {
            string fechadesde2 = "";
            string fechahasta2 = "";
            if (fechaDesde.Trim() != "") {
                fechadesde2 = fechaDesde.Substring(6, 4) + fechaDesde.Substring(3, 2) + fechaDesde.Substring(0, 2);
            }
            if (fechaHasta.Trim() != "")
            {
                fechahasta2 = fechaHasta.Substring(6, 4) + fechaHasta.Substring(3, 2) + fechaHasta.Substring(0, 2);
            }
          

            consultarOrden.OredenConsultarMsgReq request = new consultarOrden.OredenConsultarMsgReq();
            request.cabecera = new consultarOrden.cabeceraRequest();
            request.cabecera.idProceso = "OC";

            request.payloadReq = new consultarOrden.RequestType();
            request.payloadReq.usuario = USUARIO;
            request.payloadReq.estado = PENDIENTE_LIBERAR;
            //request.payloadReq.empresa = sociedad.Trim();
            request.payloadReq.rutProveedor = rut.Trim();
            request.payloadReq.fechaDesde = fechadesde2;
            request.payloadReq.fechaHasta = fechahasta2;
            request.payloadReq.nombreProveedor = razonS;
            request.payloadReq.nroOrden = ordenC;
           
            consultarOrden.consultarOrdenService service = new consultarOrden.consultarOrdenService();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.consultarOrden(request);
        }
        public static consultarOrden.OrdenConsultarMsgResp GetOrden(string USUARIO, string PENDIENTE_LIBERAR, string ordenC)
        {
            consultarOrden.OredenConsultarMsgReq request = new consultarOrden.OredenConsultarMsgReq();
            request.cabecera = new consultarOrden.cabeceraRequest();
            request.cabecera.idProceso = "OC";

            request.payloadReq = new consultarOrden.RequestType();
            request.payloadReq.usuario = USUARIO;
            request.payloadReq.estado = PENDIENTE_LIBERAR;
            request.payloadReq.nroOrden = ordenC;
            
            consultarOrden.consultarOrdenService service = new consultarOrden.consultarOrdenService();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.consultarOrden(request);
        }
        public static consultarOrden.OrdenConsultarMsgResp GetOrden(string USUARIO, string PENDIENTE_LIBERAR)
        {
            //consultarOrden. requestCabecera = new consultarOrden.cabeceraRequest();
            consultarOrden.OredenConsultarMsgReq request = new consultarOrden.OredenConsultarMsgReq();

            request.cabecera = new consultarOrden.cabeceraRequest();
            request.cabecera.idProceso = "OC";

            request.payloadReq = new consultarOrden.RequestType();
            request.payloadReq.usuario = USUARIO;
            request.payloadReq.estado = PENDIENTE_LIBERAR;
        

            consultarOrden.consultarOrdenService service = new consultarOrden.consultarOrdenService();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.consultarOrden(request);
        }


        public static validaUsuario.ValidaUsuarioMsgResp GetValidausuario(string USUARIO, string CONTRASENA)
        {
            validaUsuario.ValidaUsuarioMsgReq request = new validaUsuario.ValidaUsuarioMsgReq();
            request.payloadReq = new validaUsuario.RequestType();
            request.payloadReq.empresa = "CO";
            request.payloadReq.usuario = new validaUsuario.RequestTypeUsuario();
            request.payloadReq.usuario.usuario = USUARIO;
            request.payloadReq.usuario.contrasena = CONTRASENA;

            validaUsuario.validaUsuarioService service = new validaUsuario.validaUsuarioService();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.validaUsuario(request);
        }

        public static modificarOrden.OrdenModificarMsgResp GetModificaOrden(string codigoLiberacion, string ordenId, string rut)
        {
            modificarOrden.OredenModificarMsgReq request = new modificarOrden.OredenModificarMsgReq();
            request.cabecera = new modificarOrden.cabeceraRequest();
            request.cabecera.idProceso = "OC";
            request.cabecera.idUsuario = rut;
            request.payloadReq = new modificarOrden.RequestType();
            request.payloadReq.orden = new modificarOrden.OrdenType();
            request.payloadReq.orden.estado = new modificarOrden.TipoType();
            request.payloadReq.orden.nroOrden = ordenId;
            request.payloadReq.orden.estado.id = codigoLiberacion;


            modificarOrden.modificarOrdenService service = new modificarOrden.modificarOrdenService();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.modificarOrden(request);
        }
    }
}
