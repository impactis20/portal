﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL.webServices
{
    public class CambiarAprobadorDAL
    {
        public static cambiarAprobador.dt_pifi525_res GetCambiarAprobador(string rutAnterior, string rutNuevo, string idParametro, string fechaDesde, string fechaHasta)
        {
            cambiarAprobador.dt_pifi525_req request = new cambiarAprobador.dt_pifi525_req();
            request.I_RUT_ANTE = rutAnterior;
            request.I_RUT_NUEVO = rutNuevo;
            request.I_PARAM = idParametro;
            request.I_FECHA_DESDE = fechaDesde;
            request.I_FECHA_HASTA = fechaHasta;
            cambiarAprobador.os_pifi525Service service = new cambiarAprobador.os_pifi525Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi525(request);
        }
    }
}
