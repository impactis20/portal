﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.Entities;
using PortalProveedores.DAL.Roldes;
using PortalProveedores.servicioAppMovil.AddUsuario.Caller;
using PortalProveedores.servicioAppMovil.AddUsuario.Vo;

namespace PortalProveedores.DAL.webServices
{
    public class UsuarioDAL

    {
        public static loginUsuario.dt_pifi502_res GetAutenticacionUsuario(string usuario, string clave)
        {
            loginUsuario.dt_pifi502_req request = new loginUsuario.dt_pifi502_req();
            request.I_USUARIO = usuario;
            request.I_CLAVE = clave;

            loginUsuario.os_pifi502Service service = new loginUsuario.os_pifi502Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            var response = service.os_pifi502(request);
            if (response.E_CORREO != string.Empty && response.E_COD_ERROR == "0")
            {
                try
                {
                    AddUsuarioIN addUsuarioReques = new AddUsuarioIN();
                    addUsuarioReques.Activo = 1;// resposne.payloadResp.estado;
                    addUsuarioReques.Correo = "" + response.E_CORREO;
                    addUsuarioReques.Direccion = "NA";
                    addUsuarioReques.IdCompania = "" + response.E_SOCIEDAD;
                    addUsuarioReques.IdEmpresa = "CO";
                    try
                    {
                        addUsuarioReques.IdRol = response.E_ROL;
                        addUsuarioReques.NombreRol = "";


                    }
                    catch (Exception e)
                    {
                        addUsuarioReques.IdRol = "";
                        addUsuarioReques.NombreRol = "";
                    }

                    try
                    {
                        addUsuarioReques.IdGrupo = response.E_ROL;
                    }
                    catch (Exception e)
                    {
                        addUsuarioReques.IdGrupo = "";
                    }

                    addUsuarioReques.IdUsuario = usuario;
                    addUsuarioReques.Nombre = response.E_NAME;
                    addUsuarioReques.NombreCompania = response.E_SOCIEDAD;
                    addUsuarioReques.Perfil = "";
                    addUsuarioReques.Telefono = "NA";

                    AddUsuarioClient client = new AddUsuarioClient();
                    Task.Run(async () =>
                    {
                        try
                        {
                            await client.addUsuario(addUsuarioReques);
                        }
                        catch (Exception e)
                        {
                            Console.Write("Error en el Servicio Add Usaurio App Movil" + e.Message);
                        }

                    });

                }
                catch (Exception e)
                {
                    Console.Write("Error al llamar al Servicio Add Usaurio App Movil" + e.Message);
                }
               
            }
            return response;
            
        }

        public static cambiarDatos.dt_pifi507_res SetClave(string usuario, string clave, string claveNueva)
        {
            cambiarDatos.dt_pifi507_req request = new cambiarDatos.dt_pifi507_req();
            request.I_Usuario = usuario;
            request.I_Clave = clave;
            request.I_Clave_new = claveNueva;

            cambiarDatos.os_pifi507Service service = new cambiarDatos.os_pifi507Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi507(request);
        }

        public static cambiarDatos.dt_pifi507_res SetMisDatos(string usuario, string clave, string nombre, string email, string emailNuevo)
        {
            cambiarDatos.dt_pifi507_req request = new cambiarDatos.dt_pifi507_req();
            request.I_Usuario = usuario;
            request.I_Clave = clave;
            request.I_Clave_new = clave;
            request.I_NOMBRE = nombre;
            request.I_CORREO_ELECT = email;

            if(!string.IsNullOrEmpty(emailNuevo))
                request.I_CORREO_ELECT_NEW = emailNuevo;

            cambiarDatos.os_pifi507Service service = new cambiarDatos.os_pifi507Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi507(request);
        }

        public static recuperarClave.dt_pifi512_res GetClave(string rut)
        {
            recuperarClave.dt_pifi512_req request = new recuperarClave.dt_pifi512_req();
            request.I_STCD1 = rut;

            recuperarClave.os_pifi512Service service = new recuperarClave.os_pifi512Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi512(request);
        }

        public static usuario.dt_pifi515_res AddUsuario(string rut, string nombre, string correo, string clave, string rol, string accion)
        {
            usuario.dt_pifi515_req request = new usuario.dt_pifi515_req();
            request.I_NOMBRE = nombre;
            request.I_CORREO = correo;
            request.I_CLAVE1 = clave;
            request.I_USUARIO = rut;
            request.I_ROL = rol;
            request.I_ACCION = accion;
          

            usuario.os_pifi515Service service = new usuario.os_pifi515Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi515(request);
        }

        public static usuario.dt_pifi515_res GetUsuario(string rut, string rol, string accion, string fechai, string fechaf, string rutm)
        {
            usuario.dt_pifi515_req request = new usuario.dt_pifi515_req();
            request.I_USUARIO = rut;
            request.I_ROL = rol;
            request.I_ACCION = accion;
            request.I_FECHA_INI = fechai;
            request.I_FECHA_FIN = fechaf;
            request.I_USUARIO_M = rutm;
            
            usuario.os_pifi515Service service = new usuario.os_pifi515Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi515(request);
        }

        public static usuario.dt_pifi515_res SetEditarUsuario(string nombre, string rut,  string rol, string correo, string usuModf, string accion)
        {
            usuario.dt_pifi515_req request = new usuario.dt_pifi515_req();

            request.I_USUARIO = rut;
            request.I_NOMBRE = nombre;
            request.I_ROL = rol;
            request.I_CORREO = correo;
            request.I_USUARIO_M = usuModf;
            request.I_ACCION = accion;


            usuario.os_pifi515Service service = new usuario.os_pifi515Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi515(request);
        }

        public static usuario.dt_pifi515_res SetEditarEstado(string rut, string accion)
        {
            usuario.dt_pifi515_req request = new usuario.dt_pifi515_req();

            request.I_USUARIO = rut;
            request.I_ACCION = accion;

            usuario.os_pifi515Service service = new usuario.os_pifi515Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi515(request);
        }

        public static Roles.dt_pifi521_res GetRolesn(string rut, string accion)
        {
            Roles.dt_pifi521_req request = new Roles.dt_pifi521_req();
            request.I_RUT = rut;
            request.I_ID_ACCION = accion;

            Roles.os_pifi521Service service = new Roles.os_pifi521Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();

            return service.os_pifi521(request);
        }

        public static dt_pifi522_res GetListRol()
        {
            Roldes.dt_pifi522_req request = new Roldes.dt_pifi522_req();
            request.I_ID_ACCION = "1";


            Roldes.os_pifi522Service service = new Roldes.os_pifi522Service();
            service.Url = utix.GetUrlServicio(service.Url);
            service.Credentials = utix.GetCredencial();
            return service.os_pifi522(request);
        }
    }
}
