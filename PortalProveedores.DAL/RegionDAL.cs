﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL
{
    public class RegionDAL
    {
        public static List<Entities.RegionEntity> GetRegion()
        {
            List<Entities.RegionEntity> lst = new List<Entities.RegionEntity>();
            lst.Add(new Entities.RegionEntity("01", "I - Iquique"));
            lst.Add(new Entities.RegionEntity("02", "II - Antofagasta"));
            lst.Add(new Entities.RegionEntity("03", "III - Copiapó"));
            lst.Add(new Entities.RegionEntity("04", "IV - La Serena"));
            lst.Add(new Entities.RegionEntity("05", "V - Valparaiso"));
            lst.Add(new Entities.RegionEntity("06", "VI - Rancagua"));
            lst.Add(new Entities.RegionEntity("07", "VII - Talca"));
            lst.Add(new Entities.RegionEntity("08", "VIII - Concepción"));
            lst.Add(new Entities.RegionEntity("09", "IX - Temuco"));
            lst.Add(new Entities.RegionEntity("10", "X - Puerto Montt"));
            lst.Add(new Entities.RegionEntity("11", "XI - Coyhaique"));
            lst.Add(new Entities.RegionEntity("12", "XII - Punta Arenas"));
            lst.Add(new Entities.RegionEntity("13", "XIII - Metropolitana"));
            lst.Add(new Entities.RegionEntity("14", "XIV - Los Ríos"));
            lst.Add(new Entities.RegionEntity("15", "XV - Arica y Parinacota"));

            return lst;
        }
    }
}
