/***************************************************************
*                                                              *
*              C�digo Generado Automaticamente                 *
*                Generado Con WitiCode v1.0.0                  *
*                       Por WiTI SpA.                          *
*                     http://www.witi.cl                       *
*                                                              *
***************************************************************/

using System;
using System.Runtime.Serialization;


namespace PortalProveedores.servicioAppMovil.AddUsuario.Vo
{ 
    public class Error
    {
        public string error { get; set; }
        public string error_description { get; set; }
     
    }

}

