/***************************************************************
*                                                              *
*              C�digo Generado Automaticamente                 *
*                Generado Con WitiCode v1.0.0                  *
*                       Por WiTI SpA.                          *
*                     http://www.witi.cl                       *
*                                                              *
***************************************************************/

using System;
using System.Runtime.Serialization;

namespace PortalProveedores.servicioAppMovil.AddUsuario.Vo
{ 

	public class AddUsuarioIN  {


		public string IdUsuario
		{
			get; 
			set; 
		}


		public string IdEmpresa
		{
			get; 
			set; 
		}


		public string IdGrupo
		{
			get; 
			set; 
		}


		public string IdRol
		{
			get; 
			set; 
		}


		public string IdCompania
		{
			get; 
			set; 
		}


		public string Nombre
		{
			get; 
			set; 
		}


		public string Correo
		{
			get; 
			set; 
		}


		public string Telefono
		{
			get; 
			set; 
		}


		public string Direccion
		{
			get; 
			set; 
		}


		public string Perfil
		{
			get; 
			set; 
		}


		public int Activo
		{
			get; 
			set; 
		}


		public string NombreCompania
		{
			get; 
			set; 
		}


		public string NombreRol
		{
			get; 
			set; 
		}

	}
}
