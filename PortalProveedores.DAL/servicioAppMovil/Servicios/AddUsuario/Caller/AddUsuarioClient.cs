/***************************************************************
*                                                              *
*              Codigo Generado Automaticamente                 *
*                Generado Con WitiCode v1.0.0                  *
*                       Por WiTI SpA.                          *
*                     http://www.witi.cl                       *
*                                                              *
***************************************************************/


using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace PortalProveedores.servicioAppMovil.AddUsuario.Caller
{
    public class AddUsuarioClient
    {

     public AddUsuarioClient() {}

        
        public async Task<PortalProveedores.servicioAppMovil.AddUsuario.Vo.AddUsuarioOUT> addUsuario(PortalProveedores.servicioAppMovil.AddUsuario.Vo.AddUsuarioIN msgIn)
        {
 
            //string urlServicio = System.Configuration.ConfigurationManager.AppSettings["url_app_movil"] + "/Usuario"+"/addUsuario";
            string urlServicio = System.Configuration.ConfigurationManager.AppSettings["url_app_movil_" + System.Configuration.ConfigurationManager.AppSettings["ambiente"]] + "/Usuario" + "/addUsuario";

            HttpClient httpClient = new HttpClient();
            string json = JsonConvert.SerializeObject(msgIn);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            using (httpClient)
            {
                         
                try
                {
                    StringContent requestMsg = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage respuestaServicio = await httpClient.PostAsync(urlServicio, requestMsg);
                    if (respuestaServicio.StatusCode == HttpStatusCode.OK || respuestaServicio.StatusCode == HttpStatusCode.Created)
                    {
                        string responseBody = await respuestaServicio.Content.ReadAsStringAsync();
                        PortalProveedores.servicioAppMovil.AddUsuario.Vo.AddUsuarioOUT respuestaDeserializada = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<PortalProveedores.servicioAppMovil.AddUsuario.Vo.AddUsuarioOUT>(responseBody));
                        return respuestaDeserializada;
                    }
                    else if ( respuestaServicio.StatusCode == HttpStatusCode.NoContent )
                    {
                        return new PortalProveedores.servicioAppMovil.AddUsuario.Vo.AddUsuarioOUT();
                    }
                    else
                    {
                        string responseBody = await respuestaServicio.Content.ReadAsStringAsync();
                        Vo.Error respuestaDeserializada = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<Vo.Error>(responseBody));
                        throw new Exception(respuestaDeserializada.error_description);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Problemas con el servicio: " + e.Message);

                }
            }
        }



	}
}