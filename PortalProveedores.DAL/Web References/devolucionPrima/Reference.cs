﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// Microsoft.VSDesigner generó automáticamente este código fuente, versión=4.0.30319.42000.
// 
#pragma warning disable 1591

namespace PortalProveedores.DAL.devolucionPrima {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="os_pifi503Binding", Namespace="unr:colmena-cl:sportal:pifi503")]
    public partial class os_pifi503Service : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback os_pifi503OperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public os_pifi503Service() {
            this.Url = global::PortalProveedores.DAL.Properties.Settings.Default.PortalProveedores_DAL_devolucionPrima_os_pifi503Service;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event os_pifi503CompletedEventHandler os_pifi503Completed;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://sap.com/xi/WebService/soap1.1", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("mt_pifi503_res", Namespace="unr:colmena-cl:swebservice:pifi:fi503")]
        public dt_pifi503_res os_pifi503([System.Xml.Serialization.XmlElementAttribute(Namespace="unr:colmena-cl:swebservice:pifi:fi503")] dt_pifi503_req mt_pifi503_req) {
            object[] results = this.Invoke("os_pifi503", new object[] {
                        mt_pifi503_req});
            return ((dt_pifi503_res)(results[0]));
        }
        
        /// <remarks/>
        public void os_pifi503Async(dt_pifi503_req mt_pifi503_req) {
            this.os_pifi503Async(mt_pifi503_req, null);
        }
        
        /// <remarks/>
        public void os_pifi503Async(dt_pifi503_req mt_pifi503_req, object userState) {
            if ((this.os_pifi503OperationCompleted == null)) {
                this.os_pifi503OperationCompleted = new System.Threading.SendOrPostCallback(this.Onos_pifi503OperationCompleted);
            }
            this.InvokeAsync("os_pifi503", new object[] {
                        mt_pifi503_req}, this.os_pifi503OperationCompleted, userState);
        }
        
        private void Onos_pifi503OperationCompleted(object arg) {
            if ((this.os_pifi503Completed != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.os_pifi503Completed(this, new os_pifi503CompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="unr:colmena-cl:sportal:pifi503")]
    public partial class dt_pifi503_req {
        
        private string i_FECHA_FINField;
        
        private string i_FECHA_INIField;
        
        private string i_PERIODOField;
        
        private string i_SOCIEDADField;
        
        private string i_RUTField;
        
        private string i_ESTADO_DOCField;
        
        private string i_FECHA_INI_VENField;
        
        private string i_FECHA_FIN_VENField;
        
        private dt_pifi503_reqItem[] e_REPORTEField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_FECHA_FIN {
            get {
                return this.i_FECHA_FINField;
            }
            set {
                this.i_FECHA_FINField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_FECHA_INI {
            get {
                return this.i_FECHA_INIField;
            }
            set {
                this.i_FECHA_INIField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_PERIODO {
            get {
                return this.i_PERIODOField;
            }
            set {
                this.i_PERIODOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_SOCIEDAD {
            get {
                return this.i_SOCIEDADField;
            }
            set {
                this.i_SOCIEDADField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_RUT {
            get {
                return this.i_RUTField;
            }
            set {
                this.i_RUTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_ESTADO_DOC {
            get {
                return this.i_ESTADO_DOCField;
            }
            set {
                this.i_ESTADO_DOCField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_FECHA_INI_VEN {
            get {
                return this.i_FECHA_INI_VENField;
            }
            set {
                this.i_FECHA_INI_VENField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_FECHA_FIN_VEN {
            get {
                return this.i_FECHA_FIN_VENField;
            }
            set {
                this.i_FECHA_FIN_VENField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public dt_pifi503_reqItem[] E_REPORTE {
            get {
                return this.e_REPORTEField;
            }
            set {
                this.e_REPORTEField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="unr:colmena-cl:sportal:pifi503")]
    public partial class dt_pifi503_reqItem {
        
        private string nOMBRE_EMPRESAField;
        
        private string nUMERO_DOCUMENTOField;
        
        private string mONTOField;
        
        private string eSTADOField;
        
        private string rUTField;
        
        private string fECHA_RECEPCIONField;
        
        private string fECHA_VENCIMIENTOField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NOMBRE_EMPRESA {
            get {
                return this.nOMBRE_EMPRESAField;
            }
            set {
                this.nOMBRE_EMPRESAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NUMERO_DOCUMENTO {
            get {
                return this.nUMERO_DOCUMENTOField;
            }
            set {
                this.nUMERO_DOCUMENTOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MONTO {
            get {
                return this.mONTOField;
            }
            set {
                this.mONTOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ESTADO {
            get {
                return this.eSTADOField;
            }
            set {
                this.eSTADOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string RUT {
            get {
                return this.rUTField;
            }
            set {
                this.rUTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FECHA_RECEPCION {
            get {
                return this.fECHA_RECEPCIONField;
            }
            set {
                this.fECHA_RECEPCIONField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FECHA_VENCIMIENTO {
            get {
                return this.fECHA_VENCIMIENTOField;
            }
            set {
                this.fECHA_VENCIMIENTOField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="unr:colmena-cl:sportal:pifi503")]
    public partial class dt_pifi503_res {
        
        private string e_MENSAJEField;
        
        private string e_COD_ERRORField;
        
        private dt_pifi503_resItem[] e_REPORTEField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string E_MENSAJE {
            get {
                return this.e_MENSAJEField;
            }
            set {
                this.e_MENSAJEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string E_COD_ERROR {
            get {
                return this.e_COD_ERRORField;
            }
            set {
                this.e_COD_ERRORField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public dt_pifi503_resItem[] E_REPORTE {
            get {
                return this.e_REPORTEField;
            }
            set {
                this.e_REPORTEField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="unr:colmena-cl:sportal:pifi503")]
    public partial class dt_pifi503_resItem {
        
        private string nOMBRE_EMPRESAField;
        
        private string nUMERO_DOCUMENTOField;
        
        private string mONTOField;
        
        private string eSTADOField;
        
        private string rUTField;
        
        private string fECHA_RECEPCIONField;
        
        private string fECHA_VENCIMIENTOField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NOMBRE_EMPRESA {
            get {
                return this.nOMBRE_EMPRESAField;
            }
            set {
                this.nOMBRE_EMPRESAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NUMERO_DOCUMENTO {
            get {
                return this.nUMERO_DOCUMENTOField;
            }
            set {
                this.nUMERO_DOCUMENTOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MONTO {
            get {
                return this.mONTOField;
            }
            set {
                this.mONTOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ESTADO {
            get {
                return this.eSTADOField;
            }
            set {
                this.eSTADOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string RUT {
            get {
                return this.rUTField;
            }
            set {
                this.rUTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FECHA_RECEPCION {
            get {
                return this.fECHA_RECEPCIONField;
            }
            set {
                this.fECHA_RECEPCIONField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FECHA_VENCIMIENTO {
            get {
                return this.fECHA_VENCIMIENTOField;
            }
            set {
                this.fECHA_VENCIMIENTOField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void os_pifi503CompletedEventHandler(object sender, os_pifi503CompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class os_pifi503CompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal os_pifi503CompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public dt_pifi503_res Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((dt_pifi503_res)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591