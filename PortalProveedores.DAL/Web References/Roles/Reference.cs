﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// Microsoft.VSDesigner generó automáticamente este código fuente, versión=4.0.30319.42000.
// 
#pragma warning disable 1591

namespace PortalProveedores.DAL.Roles {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="os_pifi521Binding", Namespace="unr:colmena-cl:sportal:pifi521")]
    public partial class os_pifi521Service : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback os_pifi521OperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public os_pifi521Service() {
            this.Url = global::PortalProveedores.DAL.Properties.Settings.Default.PortalProveedores_DAL_Roles_os_pifi521Service;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event os_pifi521CompletedEventHandler os_pifi521Completed;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://sap.com/xi/WebService/soap1.1", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("mt_pifi521_res", Namespace="unr:colmena-cl:sportal:pifi521")]
        public dt_pifi521_res os_pifi521([System.Xml.Serialization.XmlElementAttribute(Namespace="unr:colmena-cl:sportal:pifi521")] dt_pifi521_req mt_pifi521_req) {
            object[] results = this.Invoke("os_pifi521", new object[] {
                        mt_pifi521_req});
            return ((dt_pifi521_res)(results[0]));
        }
        
        /// <remarks/>
        public void os_pifi521Async(dt_pifi521_req mt_pifi521_req) {
            this.os_pifi521Async(mt_pifi521_req, null);
        }
        
        /// <remarks/>
        public void os_pifi521Async(dt_pifi521_req mt_pifi521_req, object userState) {
            if ((this.os_pifi521OperationCompleted == null)) {
                this.os_pifi521OperationCompleted = new System.Threading.SendOrPostCallback(this.Onos_pifi521OperationCompleted);
            }
            this.InvokeAsync("os_pifi521", new object[] {
                        mt_pifi521_req}, this.os_pifi521OperationCompleted, userState);
        }
        
        private void Onos_pifi521OperationCompleted(object arg) {
            if ((this.os_pifi521Completed != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.os_pifi521Completed(this, new os_pifi521CompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="unr:colmena-cl:sportal:pifi521")]
    public partial class dt_pifi521_req {
        
        private string i_ID_ACCIONField;
        
        private string i_RUTField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_ID_ACCION {
            get {
                return this.i_ID_ACCIONField;
            }
            set {
                this.i_ID_ACCIONField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string I_RUT {
            get {
                return this.i_RUTField;
            }
            set {
                this.i_RUTField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="unr:colmena-cl:sportal:pifi521")]
    public partial class dt_pifi521_res {
        
        private string o_CODIGO_MENSField;
        
        private string o_MENSAJEField;
        
        private dt_pifi521_resItem[] t_ROLESField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string O_CODIGO_MENS {
            get {
                return this.o_CODIGO_MENSField;
            }
            set {
                this.o_CODIGO_MENSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string O_MENSAJE {
            get {
                return this.o_MENSAJEField;
            }
            set {
                this.o_MENSAJEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public dt_pifi521_resItem[] T_ROLES {
            get {
                return this.t_ROLESField;
            }
            set {
                this.t_ROLESField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="unr:colmena-cl:sportal:pifi521")]
    public partial class dt_pifi521_resItem {
        
        private string z_ID_ROLField;
        
        private string z_DESC_ROLField;
        
        private string z_URL_ROLSOLPEDField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Z_ID_ROL {
            get {
                return this.z_ID_ROLField;
            }
            set {
                this.z_ID_ROLField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Z_DESC_ROL {
            get {
                return this.z_DESC_ROLField;
            }
            set {
                this.z_DESC_ROLField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Z_URL_ROLSOLPED {
            get {
                return this.z_URL_ROLSOLPEDField;
            }
            set {
                this.z_URL_ROLSOLPEDField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void os_pifi521CompletedEventHandler(object sender, os_pifi521CompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class os_pifi521CompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal os_pifi521CompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public dt_pifi521_res Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((dt_pifi521_res)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591