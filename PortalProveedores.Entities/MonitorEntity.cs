﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class MonitorEntity
    {
        public MonitorEntity()
        {
        }

        public MonitorEntity(string idMoni, string razon, string fecha, string idProceso, string idSolped, string rutLiberador, string fechaLib,
            string horaLib, string idOrden, string liberadores, string folio, string fechaEmision,
            string fechaPago, string tipoPago, string monto ,string montoPago, string fechaProba, string idCoti,
            string idMercaderia)
        {
            this.idMoni = idMoni;
            this.razon = razon;
            this.fecha = fecha;
            this.idProceso = idProceso;
            this.sociedad = sociedad;
            this.nrodoc = nrodoc;
            this.rutproveedor = rutproveedor;
            this.fechaemisionini = fechaemisionini;
            this.fechaemisionfin = fechaemisionfin;
            this.accion = accion;
            this.idSolped = idSolped;
            this.rutLiberador = rutLiberador;
            this.fechaLib = fechaLib;
            this.horaLib = horaLib;
            this.idOrden = idOrden;
            this.liberadores = liberadores;
            this.folio = folio;
            this.fechaEmision = fechaEmision;
            this.fechaPago = fechaPago;
            this.tipoPago = tipoPago;
            this.montoPago = monto;
            this.montoPago = montoPago;
            this.fechaProba = fechaProba;
            this.idCoti = idCoti;
            this.idMercaderia = idMercaderia;

        }


        public string idSolped { get; set; }
        public string rutLiberador { get; set; }
        public string fechaLib { get; set; }
        public string horaLib { get; set; }
        public string idOrden { get; set; }
        public string liberadores { get; set; }
        public string folio { get; set; }
        public string fechaEmision { get; set; }
        public string fechaPago { get; set; }
        public string tipoPago { get; set; }
        public string monto { get; set; }
        public string montoPago { get; set; }
        public string fechaProba { get; set; }
        public string idCoti { get; set; }
        public string idMercaderia { get; set; }

        public string nrodoc { get; set; }
        public string sociedad { get; set; }
        public string rutproveedor { get; set; }
        public string fechaemisionini { get; set; }
        public string fechaemisionfin { get; set; }
        public string accion { get; set; }
        public string idMoni { get; set; }
        public string razon { get; set; }
        public string fecha { get; set; }
        public string idProceso { get; set; }
        public string idStatus { get; set; }
        public List<MonitorEntity> monitor { get; set; }
    }
}
