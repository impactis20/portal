﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class SistemaEntity
    {
        public class Pagina
        {
            public Pagina(){}

            public Pagina(string path, List<UsuarioEntity.Rol> lstAccesoRol)
            {
                this.path = path;
                accesoRol = lstAccesoRol;
            }

            public string path { get; set; }
            public List<UsuarioEntity.Rol> accesoRol { get; set; }
        }
        
        public class Menu
        {
            public Menu(int id, string descripcion, bool esPadre, int idPadre)
            {
                this.id = id;
                url = string.Empty;
                this.descripcion = esPadre ? descripcion + "<span class='fa arrow'>" : descripcion;
                this.esPadre = esPadre;
                this.idPadre = idPadre;
                css = esPadre ? "nav nav-second-level" : string.Empty;
            }

            public Menu(int id, string descripcion, bool esPadre, int idPadre, string url)
            {
                this.id = id;
                this.url = url;
                this.descripcion = descripcion;
                this.esPadre = esPadre;
                this.idPadre = idPadre;
                css = esPadre ? "nav nav-second-level" : string.Empty;
            }

            public int id { get; set; }
            public string url { get; set; }
            public int idPadre { get; set; }
            public bool esPadre { get; set; }
            public string descripcion { get; set; }
            public string css { get; set; }
        }
    }
}
