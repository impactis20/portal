﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class DocumentoEntity
    {
        public DocumentoEntity()
        {

        }

        //public DocumentoEntity(string nroDocumento)
        //{
        //    this.nroDocumento = nroDocumento;
        //}

        public DocumentoEntity(string nroDocumento, string idTipoDocumento, string referencia, string rutProveedor, string idSociedad, string monto, string fechaEmision, string idEstatus, string observacion, string rechazo)
        {
            this.nroDocumento = nroDocumento;
            this.rutProveedor = rutProveedor;
            tipoDocumento = new TipoDocumento(idTipoDocumento);
            this.referencia = referencia;
            sociedad = new SociedadEntity(idSociedad);
            this.monto = monto;
            this.fechaEmision = fechaEmision;
            status = new Status(idEstatus);
            this.observacion = observacion;
            status.id = idEstatus;
            motivoRechazo = new MotivoRechazo();
            motivoRechazo.id = rechazo;

        }

        public DocumentoEntity(string nroDocumento, string rutProveedor, string razonSocial, string idSociedad, string monto, string fechaEmision, string idEstatus, string idTipoDocumento, string referencia, string fechaRecepcion, string fechaProbablePago, string fechaCobro, string observacion, string bandera, string modalidadPago, string fechaPago, string banco, string monto2, string nroCuenta, string urlDoc, string rechazo, string nombreDoc, string urlOC)
        {
            this.nroDocumento = nroDocumento;
            this.rutProveedor = rutProveedor;
            this.razonSocial = razonSocial;
            sociedad = new SociedadEntity(idSociedad);
            tipoDocumento = new TipoDocumento();
            tipoDocumento.id = idTipoDocumento;
            this.referencia = referencia;
            this.monto = monto;
            this.fechaEmision = fechaEmision;
            this.fechaRecepcion = fechaRecepcion;
            this.fechaProbablePago = fechaProbablePago;
            this.fechaCobro = fechaCobro;
            this.observacion = observacion;
            status = new Status();
            status.id = idEstatus;
            motivoRechazo = new MotivoRechazo();
            motivoRechazo.id = rechazo;


            this.bandera = bandera;

            this.modalidadPago = modalidadPago;
            this.fechaPago = fechaPago;
            this.banco = banco;
            this.monto2 = monto2;
            this.nroCuenta = nroCuenta;
            this.urlDoc = urlDoc;
            this.nombreDoc = nombreDoc;
            this.urlOC = urlOC;
        }

        public DocumentoEntity(string nroDocumento, string rutProveedor, string razonSocial, string idSociedad, string monto, string fechaEmision, string idEstatus, string idTipoDocumento, string referencia, string fechaRecepcion, string fechaProbablePago, string fechaCobro, string observacion, string bandera, string modalidadPago, string fechaPago, string banco, string monto2, string nroCuenta, string urlDoc, string rechazo)
        {
            this.nroDocumento = nroDocumento;
            this.rutProveedor = rutProveedor;
            this.razonSocial = razonSocial;
            sociedad = new SociedadEntity(idSociedad);
            tipoDocumento = new TipoDocumento();
            tipoDocumento.id = idTipoDocumento;
            this.referencia = referencia;
            this.monto = monto;
            this.fechaEmision = fechaEmision;
            this.fechaRecepcion = fechaRecepcion;
            this.fechaProbablePago = fechaProbablePago;
            this.fechaCobro = fechaCobro;
            this.observacion = observacion;
            status = new Status();
            status.id = idEstatus;
            motivoRechazo = new MotivoRechazo();
            motivoRechazo.id = rechazo;

            this.bandera = bandera;

            this.modalidadPago = modalidadPago;
            this.fechaPago = fechaPago;
            this.banco = banco;
            this.monto2 = monto2;
            this.nroCuenta = nroCuenta;
            this.urlDoc = urlDoc;
        }

        public string nroDocumento { get; set; }
        public string rutProveedor { get; set; }
        public string razonSocial { get; set; }
        public SociedadEntity sociedad { get; set; }
        public TipoDocumento tipoDocumento { get; set; }
        public MotivoRechazo motivoRechazo { get; set; }
        public string monto { get; set; }
        public string nroDocumentoPago { get; set; }
        public string nroCheque { get; set; }
        public string fechaEmision { get; set; }
        public string fechaRecepcion { get; set; }
        public string fechaProbablePago { get; set; }
        public string fechaCobro { get; set; }
        public string observacion { get; set; }
        public Status status { get; set; }
        public string referencia { get; set; }
        public string urlDoc { get; set; }
        public string urlOC { get; set; }

        //cuando bandera tiene valor "X", debe habilitar las opciones "Rechazar", "En revisión", "Eliminar" y "Editar" en el documento.
        //cuando bandera tiene valor "" (string.Empty), debe habilitar el detalle del documento en un botón.
        public string bandera { get; set; }

        //Detalle de documentoEntity
        public string modalidadPago { get; set; }
        public string fechaPago { get; set; }
        public string banco { get; set; }
        public string monto2 { get; set; }
        public string nroCuenta { get; set; }
        public string nombreDoc { get; set; }

        public class Status
        {
            public Status() { }
            public Status(string id)
            {
                this.id = id;
            }
            public Status(string id, string descripcion)
            {
                this.id = id;
                this.descripcion = descripcion;
            }
            public Status(string id, string descripcion, string icono, bool seleccionable, bool estadoAutomatico)
            {
                this.id = id;
                this.descripcion = descripcion;
                this.icono = icono;
                this.seleccionable = seleccionable;
                this.estadoAutomatico = estadoAutomatico;
            }


            public string id { get; set; }
            public string descripcion { get; set; }
            public bool seleccionable { get; set; }
            public bool estadoAutomatico { get; set; }
            public string icono { get; set; }
        }

        public class TipoDocumento
        {
            public TipoDocumento() { }
            public TipoDocumento(string id)
            {
                this.id = id;
            }
            public TipoDocumento(string id, string descripcion)
            {
                this.id = id;
                this.descripcion = descripcion;
            }
            public TipoDocumento(string id, string descripcion, bool seleccionable)
            {
                this.id = id;
                this.descripcion = descripcion;
                this.seleccionable = seleccionable;
            }

            public string id { get; set; }
            public string descripcion { get; set; }
            public bool seleccionable { get; set; }
        }

        public class MotivoRechazo
        {
            public MotivoRechazo() { }
            public MotivoRechazo(string id)
            {
                this.id = id;
            }
            public MotivoRechazo(string id, string descripcion)
            {
                this.id = id;
                this.descripcion = descripcion;
            }

            public string id { get; set; }
            public string descripcion { get; set; }
        }
    }
}
