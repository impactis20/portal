﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class SociedadEntity
    {
        public SociedadEntity() { }
        public SociedadEntity(string id)
        {
            this.id = id;
        }
        public SociedadEntity(string id, string nombre)
        {
            this.id = id;
            this.nombre = nombre;
        }

        public string id { get; set; }
        public string nombre { get; set; }
    }
}
