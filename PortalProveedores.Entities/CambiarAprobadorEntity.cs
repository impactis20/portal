﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class CambiarAprobadorEntity
    {
        public CambiarAprobadorEntity()
        {

        }

        public CambiarAprobadorEntity(string descripcion, string id)
        {
            this.descripcion = descripcion;
            this.id = id;

        }

        public string descripcion { get; set; }
        public string id { get; set; }
    }
}
