﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class UsuarioEntity
    {
        public UsuarioEntity() { }

        public UsuarioEntity(Rol rol)
        {
            this.rol = rol;
        }

        public UsuarioEntity(string rut, string clave)
        {
            this.rut = rut;
            this.clave = clave;
        }

       
        public UsuarioEntity(string nombre, string correo, string clave, string rut, Rol rol, string fechac, string fecham)
        {
            this.nombre = nombre;
            this.correo = correo;
            this.clave = clave;
            this.rut = rut;
            this.rol = rol;
            this.fechac = fechac;
            this.fecham = fecham;
            this.estado = estado;
        }

        public UsuarioEntity(string nombre, string correo, string clave, string rut, string idRol, string nombreRol, string fechac, string fecham)
        {
            this.nombre = nombre;
            this.correo = correo;
            this.clave = clave;
            this.rut = rut;
            this.idRol = idRol;
            this.nombreRol = nombreRol;
            this.fechac = fechac;
            this.fecham = fecham;
            this.estado = estado;
        }

        public string estado { get; set; }
        public string fecham { get; set; }
        public string fechac { get; set; }
        public string correo { get; set; }
        public string clave { get; set; }
        public string rut { get; set; }
        public string idRol { get; set; }
        public string nombreRol { get; set; }
        public Rol rol { get; set; }
        public string nombre { get; set; } //nombre o razon social
        public string email { get; set; }
        public SociedadEntity sociedad { get; set; }

        public class Rol
        {
            public Rol() { }
            public Rol(string id)
            {
                this.id = id;
            }
            public Rol(string id, string nombre)
            {
                this.id = id;
                this.nombre = nombre;
            }

            public string id { get; set; }
            public string nombre { get; set; }
        }


    }

}
