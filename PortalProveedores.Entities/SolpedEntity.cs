﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class SolpedEntity
    {
        public string id { get; set; }
        public string descripcion { get; set; }
        public string cantidad { get; set; }
        public string grupoCompra { get; set; }
        public string centroCosto { get; set; } 
        public string rutSolicitante { get; set; }
        public string fechaEntrega { get; set; }
    }
}
