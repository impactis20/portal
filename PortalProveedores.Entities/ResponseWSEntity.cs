﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class ResponseWSEntity
    {
        public ResponseWSEntity() { }

        public ResponseWSEntity(string codigo, string mensaje)
        {
            this.codigo = codigo;
            this.mensaje = mensaje;
        }

        public string codigo { get; set; }
        public string mensaje { get; set; }
    }
}
