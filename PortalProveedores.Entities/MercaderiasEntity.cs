﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class MercaderiasEntity
    {
        public MercaderiasEntity() { }

        public MercaderiasEntity( string orden, string fechaO, string rutP, string razonP, string precioN, string iva, string retencion, string montoB)
        {
            this.orden = orden;
            this.fechaO = fechaO;
            this.rutP = rutP;
            this.razonP = razonP;
            this.precioN = precioN;
            this.iva = iva;
            this.retencion = retencion;
            this.montoB = montoB;
        }
  
        public MercaderiasEntity(string orden, string posicion, string material, string descripcion, string cantidad, string unidadM, string entregaF, string cantidadR, string cantidadP, string tipoM, string descripcionT)
        {
            this.orden = orden;
            this.posicion = posicion;
            this.material = material;
            this.descripcion = descripcion;
            this.cantidad = cantidad;
            this.unidadM = unidadM;
            this.entregaF = entregaF;
            this.cantidadR = cantidadR;
            this.cantidadP = cantidadP;
            this.tipoM = tipoM;
            this.descripcionT = descripcionT;
        }
        public MercaderiasEntity(string rut, string orden, string tipoM, string fechaC, string fechaD, string numeroG, string texto, string cantidad, string pedido, string posicion)
        {
            this.rut = rut;
            this.orden = orden;
            this.tipoM = tipoM;
            this.fechaC = fechaC;
            this.fechaD = fechaD;
            this.numeroG = numeroG;
            this.texto = texto;
            this.cantidad = cantidad;
            this.pedido = pedido;
            this.posicion = posicion;
        }
            public string rut { get; set; }
            public string orden { get; set; }
            public string fechaO { get; set; }
            public string rutP { get; set; }
            public string razonP { get; set; }
            public string precioN { get; set; }
            public string iva { get; set; }
            public string retencion { get; set; }
            public string montoB { get; set; }
            public string posicion { get; set; }
            public string material { get; set; }
            public string descripcion { get; set; }
            public string cantidad { get; set; }
            public string unidadM { get; set; }
            public string entregaF { get; set; }
            public string cantidadR { get; set; }
            public string cantidadP { get; set; }
            public string tipoM { get; set; }
            public string descripcionT { get; set; }
            public string fechaC { get; set; }
            public string fechaD { get; set; }
            public string numeroG { get; set; }
            public string texto { get; set; }
            public string pedido { get; set; }
    }
    
}


