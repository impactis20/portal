﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class OrdenEntity
    {
        public OrdenEntity() { }

        public OrdenEntity(string MAN, string IDPED, string USUARIO, string ordencompra, string fechaordencompra, string idproveedor, string razonsocial, 
            string rutproveedor, string pendienteliberar, string orgcompras, string nameorgcompras, string SOCIEDAD, string sociedad, 
            string fechaentrega, string moneda, string precioneto, string recargo, string dsctoimporte, string dsctoporcentaje, 
            string ivacompraclp, string retencion, string valorbruto, string textocabecera, List<DetalleOrdenEntity> detalleOrden)
        {
            this.MAN = MAN;
            this.IDPED = IDPED;
            this.USUARIO = USUARIO;
            this.ordencompra = ordencompra;
            this.fechaordencompra = fechaordencompra;
            this.idproveedor = idproveedor;
            this.razonsocial = razonsocial;
            this.rutproveedor = rutproveedor;
            this.pendienteliberar = pendienteliberar;
            this.orgcompras = orgcompras;
            this.nameorgcompras = nameorgcompras;
            this.SOCIEDAD = SOCIEDAD;
            this.sociedad = sociedad;
            this.fechaentrega = fechaentrega;
            this.moneda = moneda;
            this.precioneto = precioneto;
            this.recargo = recargo;
            this.dsctoimporte = dsctoimporte;
            this.dsctoporcentaje = dsctoporcentaje;
            this.ivacompraclp = ivacompraclp;
            this.retencion = retencion;
            this.valorbruto = valorbruto;
            this.textocabecera = textocabecera;
            this.detalleOrden = detalleOrden;

        }

        public string MAN { get; set; }
        public string IDPED { get; set; }
        public string USUARIO { get; set; }

        public string ordencompra { get; set; }
        public string fechaordencompra { get; set; }
        public string idproveedor { get; set; }
        public string razonsocial { get; set; }
        public string rutproveedor { get; set; }
        public string pendienteliberar { get; set; }
        public string orgcompras { get; set; }
        public string nameorgcompras { get; set; }

        public string SOCIEDAD { get; set; }

        public string sociedad { get; set; }
        public string fechaentrega { get; set; }
        public string moneda { get; set; }
        public string precioneto { get; set; }
        public string recargo { get; set; }
        public string dsctoimporte { get; set; }
        public string dsctoporcentaje { get; set; }
        public string ivacompraclp { get; set; }
        public string retencion { get; set; }
        public string valorbruto { get; set; }
        public string textocabecera { get; set; }
        public List<DetalleOrdenEntity> detalleOrden { get; set; }
    }

    public class DetalleOrdenEntity
    {
        public DetalleOrdenEntity() { }

        public DetalleOrdenEntity(string posicion, string material, string descripcion,
            string centrocoste, string descripcentrocoste, string cantidad, string um, string pnetounidad, string valorneto, string totalbruto)
        {
            this.posicion = posicion;
            this.material = material;
            this.descripcion = descripcion;
            this.centrocoste = centrocoste;
            this.descripcentrocoste = descripcentrocoste;
            this.cantidad = cantidad;
            this.um = um;
            this.pnetounidad = pnetounidad;
            this.valorneto = valorneto;
            this.totalbruto = totalbruto;
       

        }
        public string posicion { get; set; }
        public string material { get; set; }
        public string descripcion { get; set; }
        public string centrocoste { get; set; }
        public string descripcentrocoste { get; set; }
        public string cantidad { get; set; }
        public string um { get; set; }
        public string pnetounidad { get; set; }
        public string valorneto { get; set; }
        public string totalbruto { get; set; }
    }
}
