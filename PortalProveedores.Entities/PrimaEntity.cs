﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class PrimaEntity
    {
        public PrimaEntity() { }

        public PrimaEntity(string empresa, string nroDocumento, string monto, string estado,string rut, string fechaR, string fechaV)
        {
            this.empresa = empresa;
            this.nroDocumento = nroDocumento;
            this.monto = monto;
            this.estado = estado;
            this.rut = rut;
            this.fechaR = fechaR;
            this.fechaV = fechaV;
        }

        public string empresa { get; set; }
        public string nroDocumento { get; set; }
        public string monto { get; set; }
        public string estado { get; set; }
        public string rut { get; set; }
        public string fechaR { get; set; }
        public string fechaV { get; set; }
    }
}
