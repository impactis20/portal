﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class RegionEntity
    {
        public RegionEntity() { }
        public RegionEntity(string id, string descripcion)
        {
            this.id = id;
            this.descripcion = descripcion;
        }
        public string id { get; set; }
        public string descripcion { get; set; }
    }
}
