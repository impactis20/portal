﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
   public class SugerenciasEntity
    {

        public SugerenciasEntity()
        {

        }
        
        public SugerenciasEntity(string descripcion, string id)
        {
            this.descripcion = descripcion;
            this.id = id;

        }
       
        public string descripcion { get; set; }
        public string id { get; set; }
    }
    //public class SugerenciasExternasEntity
    //{

    //    public SugerenciasExternasEntity()
    //    {

    //    }

    //    public SugerenciasExternasEntity(string descripcion, string id)
    //    {
    //        this.descripcion = descripcion;
    //        this.id = id;

    //    }

    //    public string descripcion { get; set; }
    //    public string id { get; set; }
    //}
}
