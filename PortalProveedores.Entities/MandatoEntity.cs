﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class MandatoEntity
    {
        public MandatoEntity() { }

        public MandatoEntity(Tcuenta tipoCuenta)
        {
            this.tipoCuenta = tipoCuenta;
        }
        public MandatoEntity(string rut, Tcuenta tipoCuenta, string banco, string desBanco, string bancoDes, string desCuenta, string bancod, string cuenta, string nombre, string referencia, string correo, string ruta, string accion)
        {
            this.rut = rut;
            this.bancoDes = bancoDes;
            this.tipoCuenta = tipoCuenta;
            this.nombre = nombre;
            this.desCuenta = desCuenta;
            this.desBanco = desBanco;
            bancon = new Banco();
            bancon.id = banco;
            bancon.descripcion = bancod;
            this.cuenta = cuenta;
            this.referencia = referencia;
            this.correo = correo;
            this.ruta = ruta;
            this.accion = accion;

        }

        public Tcuenta tipoCuenta { get; set; }
        public string rut { get; set; }
        public string bancoDes { get; set; }
        public string nombre { get; set; }
        public string desCuenta { get; set; }
        public string desBanco { get; set; }
        public Banco bancon { get; set; }
        public string cuenta { get; set; }
        public string referencia { get; set; }
        public string correo { get; set; }
        public string ruta { get; set; }
        public string accion { get; set; }

        public class Banco
        {
            public Banco() { }
            public Banco(string id)
            {
                this.id = id;
            }
            public Banco(string id, string descripcion)
            {
                this.id = id;
                this.descripcion = descripcion;
            }

            public string id { get; set; }
            public string descripcion { get; set; }
        }

        public class Tcuenta
        {
            public Tcuenta() { }
            public Tcuenta(string id)
            {
                this.id = id;
            }
            public Tcuenta(string id, string nombre)
            {
                this.id = id;
                this.nombre = nombre;
            }

            public string id { get; set; }
            public string nombre { get; set; }
        }
    }
}
