﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    public class SolicitudEntity
    {
        public SolicitudEntity() { }

        public SolicitudEntity(string ID_PEDIDO, string USUARIO, string SOLPED, string ESTADO_LIB, string CENTRO, string NOMBRE_CENTRO, string FECHA_SOLPED,
            string FECHA_ENTREGA, List<DetalleSolicitudEntity> detallesolicitud, string POSICION, string MATERIAL, string DESCRIPCION,
            string CENTRO_COSTE, string DESC_CENT_COSTE, string CANTIDAD, string UNIDAD_MEDIDA, string COMENTARIO)
        {
            this.ID_PEDIDO = ID_PEDIDO;
            this.USUARIO = USUARIO;
            this.SOLPED = SOLPED;
            this.ESTADO_LIB = ESTADO_LIB;
            this.CENTRO = CENTRO;
            this.NOMBRE_CENTRO = NOMBRE_CENTRO;
            this.FECHA_SOLPED = FECHA_SOLPED;
            this.FECHA_ENTREGA = FECHA_ENTREGA;
            this.detallesolicitud = detallesolicitud;


            this.POSICION = POSICION;
            this.MATERIAL = MATERIAL;
            this.DESCRIPCION = DESCRIPCION;
            this.CENTRO_COSTE = CENTRO_COSTE;
            this.DESC_CENT_COSTE = DESC_CENT_COSTE;
            this.CANTIDAD = CANTIDAD;
            this.UNIDAD_MEDIDA = UNIDAD_MEDIDA;
            this.COMENTARIO = COMENTARIO;
        }

        public string ID_PEDIDO { get; set; }
        public string USUARIO { get; set; }
        public string SOLPED { get; set; }

        public string ESTADO_LIB { get; set; }
        public string CENTRO { get; set; }
        public string NOMBRE_CENTRO { get; set; }
        public string FECHA_SOLPED { get; set; }
        public string FECHA_ENTREGA { get; set; }
        public List<DetalleSolicitudEntity> detallesolicitud { get; set; }
        public string POSICION { get; set; }
        public string MATERIAL { get; set; }
        public string DESCRIPCION { get; set; }
        public string CENTRO_COSTE { get; set; }
        public string DESC_CENT_COSTE { get; set; }
        public string CANTIDAD { get; set; }
        public string UNIDAD_MEDIDA { get; set; }
        public string COMENTARIO { get; set; }
    }

    public class DetalleSolicitudEntity
    {
        public DetalleSolicitudEntity() { }

        public DetalleSolicitudEntity(string POSICION, string MATERIAL, string DESCRIPCION,
            string CENTRO_COSTE, string DESC_CENT_COSTE, string CANTIDAD, string UNIDAD_MEDIDA)
        {

            this.POSICION = POSICION;
            this.MATERIAL = MATERIAL;
            this.DESCRIPCION = DESCRIPCION;
            this.CENTRO_COSTE =CENTRO_COSTE;
            this.DESC_CENT_COSTE = DESC_CENT_COSTE;
            this.CANTIDAD = CANTIDAD;
            this.UNIDAD_MEDIDA = UNIDAD_MEDIDA;

        }
        public string POSICION { get; set; }
        public string MATERIAL { get; set; }
        public string DESCRIPCION { get; set; }
        public string CENTRO_COSTE { get; set; }
        public string DESC_CENT_COSTE { get; set; }
        public string CANTIDAD { get; set; }
        public string UNIDAD_MEDIDA { get; set; }
    }
}