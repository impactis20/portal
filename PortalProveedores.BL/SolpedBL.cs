﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.buscarMaterial;
using PortalProveedores.DAL.crearSolped;

namespace PortalProveedores.BL
{
    public class SolpedBL
    {
        public static List<Entities.SolpedEntity> GetMaterial(string prefix, ref Entities.ResponseWSEntity mensajeOperacion)
        {
            dt_pifi508_res resp = SolpedDAL.GetMaterial(prefix);
            mensajeOperacion = new Entities.ResponseWSEntity(resp.E_COD_ERROR, resp.E_MENSAJE);
            List<Entities.SolpedEntity> lstData = new List<Entities.SolpedEntity>();

            for (int i = 0; i < Convert.ToInt32(resp.E_MATERIALES.Length); i++)
            {
                Entities.SolpedEntity solped = new Entities.SolpedEntity {
                    id = resp.E_MATERIALES[i].MATERIAL,
                    descripcion = resp.E_MATERIALES[i].DESCRIPCION
                };

                lstData.Add(solped);
            }

            return lstData;
        }

        public static Entities.ResponseWSEntity AddSolped(string planta, string observacion ,List<Entities.SolpedEntity> lstData)
        {
            try
            {
                dt_pifi509_reqItem[] arrMaterial = new dt_pifi509_reqItem[lstData.Count];

                int x = 0;
                foreach (Entities.SolpedEntity item in lstData)
                {
                    dt_pifi509_reqItem material = new dt_pifi509_reqItem();
                    material.OBSERVACION = observacion;
                    material.CANTIDAD = item.cantidad.Replace(".", string.Empty);
                    material.CENTRO_COSTO = item.centroCosto;
                    material.DESCRIPCION = item.descripcion;
                    material.FECHA_ENTREGA = item.fechaEntrega;
                    material.GRUPO_COMPRAS = item.grupoCompra;
                    material.MATERIAL = item.id;
                    material.SOLICITANTE = item.rutSolicitante;
                    string plantav;
                    if (planta == "1001")
                    {
                        plantav = "CG01";
                    }
                    else
                    {
                        plantav = "9000";
                    }
                    material.PLANTA = plantav;

                    arrMaterial[x] = material;
                    x++;
                }

                dt_pifi509_res respWS = SolpedDAL.AddSolped(arrMaterial);

                return new Entities.ResponseWSEntity(respWS.E_COD_ERROR, respWS.E_MENSAJE);
            }
            catch
            {
                return new Entities.ResponseWSEntity("1", "[Error 521] Ha ocurrido un error al intentar ingresar la solicitud de compra. Inténtelo más tarde o contactese con su administrador.");
            }
        }
    }
}
