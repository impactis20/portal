﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.mandato;
using PortalProveedores.DAL.mandatoBanco;
using PortalProveedores.Entities;

namespace PortalProveedores.BL
{
    public class MandatoBL
    {
        public static List<MandatoEntity.Banco> GetListBanco(ref ResponseWSEntity mensajeOperacion)
        {
            try
            {
                string claveP = "CL";
                string accion = "1";

                List<MandatoEntity.Banco> lstData = new List<MandatoEntity.Banco>();
                dt_pifi517_res respWS = MandatoDAL.GetListBan(claveP, accion);

                mensajeOperacion = new ResponseWSEntity(respWS.O_CODIGO_MENS, respWS.O_MENSAJE);

                for (int i = 0; i < Convert.ToInt32(respWS.T_BANCOS.Length); i++)
                {
                    MandatoEntity.Banco bancoL = new MandatoEntity.Banco(
                        respWS.T_BANCOS[i].ZNOMBRE_BANCO,
                        respWS.T_BANCOS[i].ZCLAVE_BANCO);

                    lstData.Add(bancoL);
                }

                return lstData;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("0", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static List<Entities.MandatoEntity.Tcuenta> GetNumero()
        {
            List<Entities.MandatoEntity.Tcuenta> lstNum = new List<Entities.MandatoEntity.Tcuenta>();
            lstNum.Add(new Entities.MandatoEntity.Tcuenta("CC", "Cuenta Corriente"));
            lstNum.Add(new Entities.MandatoEntity.Tcuenta("CV", "Cuenta Vista"));
            lstNum.Add(new Entities.MandatoEntity.Tcuenta("CA", "Cuenta Ahorro"));
            return lstNum;
        }

        public static ResponseWSEntity AddMandato(string rut, string banco, string cuenta, string tipoCuenta, string referencia, string correo, string ruta, string accion)
        {
            try
            {
                tipoCuenta = GetNumero().FirstOrDefault(x => x.nombre == tipoCuenta).id;
                rut = rut.Replace(".", string.Empty);
                dt_pifi516_res resp = MandatoDAL.AddMandato(rut, banco, cuenta, tipoCuenta, referencia, correo, ruta, accion);
                return new ResponseWSEntity(resp.O_CODIGO, resp.O_MENSAJE);

            }
            catch
            {
                return new ResponseWSEntity("3", "<strong>[Error 232]</strong> Ha ocurrido un error.");
            }
        }

        public static MandatoEntity GetMandato(string rut, string accion, ref ResponseWSEntity mensajeOperacion)
        {
            try
            {
                //List<MandatoEntity> lstData = new List<MandatoEntity>();
                dt_pifi516_res respWS = MandatoDAL.GetMandato(rut, accion);

                mensajeOperacion = new ResponseWSEntity(respWS.O_CODIGO, respWS.O_MENSAJE);
                MandatoEntity mandato = new MandatoEntity();

                mandato.bancon = new MandatoEntity.Banco(respWS.T_MANDATO_O[0].ZCODBANCO);
                mandato.tipoCuenta = new MandatoEntity.Tcuenta(respWS.T_MANDATO_O[0].ZTIPOCUENTA);
                mandato.cuenta = respWS.T_MANDATO_O[0].ZNUMCUENTA;
                mandato.referencia = respWS.T_MANDATO_O[0].ZMANDATO;
                mandato.desCuenta = respWS.T_MANDATO_O[0].ZTIPOCUENTA;
                mandato.desBanco = respWS.T_MANDATO_O[0].ZCODBANCO;
                mandato.ruta = respWS.T_MANDATO_O[0].ZRUTA_PDF;
                mandato.nombre = respWS.O_NOMBRE;
                mandato.correo = respWS.O_CORREOS;
                mandato.bancoDes = respWS.T_MANDATO_O[0].ZDESBANCO;



                mandato.tipoCuenta.nombre = GetNumero().FirstOrDefault(x => x.id == mandato.tipoCuenta.id).nombre;
                return mandato;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> El proveedor no posee mandato asociado.");
                return null;

            }
        }
    }
}
