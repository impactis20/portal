﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.devolucionPrima;

namespace PortalProveedores.BL
{
    public class PrimaBL
    {
        public static List<Entities.PrimaEntity> GetPrimas(string idSociedad, string año, string fechaDesdeR, string fechaHastaR, string fechaDesdeV, string fechaHastaV, string rut, string estado, ref Entities.ResponseWSEntity respWS)
        {
            
            dt_pifi503_res respuesta = PrimaDAL.GetPrimas(idSociedad, año, fechaDesdeR, fechaHastaR, fechaDesdeV, fechaHastaV, rut, estado);

            List<Entities.PrimaEntity> LstPrimas = new List<Entities.PrimaEntity>();
            
            for (int i = 0; i < Convert.ToInt32(respuesta.E_REPORTE.Length); i++)
            {
                LstPrimas.Add(new Entities.PrimaEntity(respuesta.E_REPORTE[i].NOMBRE_EMPRESA,

                    respuesta.E_REPORTE[i].NUMERO_DOCUMENTO,
                    respuesta.E_REPORTE[i].MONTO,
                    respuesta.E_REPORTE[i].ESTADO,
                    respuesta.E_REPORTE[i].RUT,
                    respuesta.E_REPORTE[i].FECHA_RECEPCION,
                    respuesta.E_REPORTE[i].FECHA_VENCIMIENTO
                    ));
            }

            respWS.codigo = respuesta.E_COD_ERROR;
            respWS.mensaje = respuesta.E_MENSAJE;

            return LstPrimas;
        }
    }
}
