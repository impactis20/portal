﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.loginUsuario;
using PortalProveedores.DAL.cambiarDatos;
using PortalProveedores.DAL.recuperarClave;
using PortalProveedores.DAL.usuario;
using PortalProveedores.DAL.Roles;
using PortalProveedores.DAL.Roldes;
using PortalProveedores.Entities;


namespace PortalProveedores.BL
{
    public static class UsuarioBL
    {
        public static Entities.ResponseWSEntity GetAutenticacionUsuario(ref Entities.UsuarioEntity usuario)
        {
            usuario.rut = usuario.rut.Replace(".", string.Empty);
            dt_pifi502_res resp = UsuarioDAL.GetAutenticacionUsuario(usuario.rut, usuario.clave);
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity(resp.E_COD_ERROR, resp.E_MENSAJE);
            //actualiza por referencia
            usuario.rol = new Entities.UsuarioEntity.Rol(resp.E_ROL);
            usuario.nombre = resp.E_NAME;
            usuario.email = resp.E_CORREO.ToLower();
            usuario.sociedad = new Entities.SociedadEntity(resp.E_SOCIEDAD);

            return respWS;
        }

        public static Entities.ResponseWSEntity SetClave(string usuario, string clave, string claveNueva, string claveNuevaRepetida)
        {
            Entities.ResponseWSEntity respWS;

            if (claveNuevaRepetida == claveNueva)
            {
                dt_pifi507_res resp = UsuarioDAL.SetClave(usuario, clave, claveNueva);
                respWS = new Entities.ResponseWSEntity(resp.E_COD_ERROR, resp.mensaje);
            }
            else
            {
                //error
                respWS = new Entities.ResponseWSEntity("1", "La clave nueva debe coincidir con el campo <strong>Repetir nueva clave</strong>.");
            }
            return respWS;
        }

        public static Entities.ResponseWSEntity SetMisDatos(string usuario, string clave, string nombre, string email, string emailNuevo)
        {
            dt_pifi507_res resp = UsuarioDAL.SetMisDatos(usuario, clave, nombre, email, emailNuevo);
            Entities.ResponseWSEntity respWS = new Entities.ResponseWSEntity(resp.E_COD_ERROR, resp.mensaje);

            return respWS;
        }

        public static Entities.ResponseWSEntity GetClave(string rut)
        {
            try
            {
                dt_pifi512_res resp = UsuarioDAL.GetClave(rut);
                return new Entities.ResponseWSEntity(resp.E_COD_ERROR, resp.I_MENSAJE);
            }
            catch
            {
                return new Entities.ResponseWSEntity("1", "<strong>[Error 293]</strong> Ha ocurrido un error interno. Por favor inténtelo más tarde.");
            }
        }

        public static List<Entities.SistemaEntity.Menu> GetMenu(string idRol)
        {
            List<Entities.SistemaEntity.Menu> lstMenu = new List<Entities.SistemaEntity.Menu>();

            lstMenu.Add(new Entities.SistemaEntity.Menu(1, "Inicio", false, 0, "/app/inicio.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(2, "Reportes", true, 0));
            lstMenu.Add(new Entities.SistemaEntity.Menu(3, "Pagos", false, 2, "/app/reporte/pagos.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(4, "Devolución prima", false, 2, "/app/reporte/devolucion-prima.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(6, "Proveedores", true, 0));
            lstMenu.Add(new Entities.SistemaEntity.Menu(7, "Crear", false, 6, "/app/proveedor/crear.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(8, "Documentos", true, 0));
            lstMenu.Add(new Entities.SistemaEntity.Menu(9, "Consultar", false, 8, "/app/documento/consultar.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(10, "Ingresar", false, 8, "/app/documento/ingresar.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(11, "Recepción de mercadería", false, 0, "/app/mercaderias/mercaderias.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(12, "Mandato", false, 0, "/app/mandato/madato.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(13, "Usuarios", false, 0, "/app/usuario/consultar.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(14, "Solicitud de Compra", true, 0));
            lstMenu.Add(new Entities.SistemaEntity.Menu(15, "Crear solicitud", false, 14, "/app/solped/crear.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(16, "Liberar solicitudes pendientes", false, 14, "/app/solped/liberarSolicitudesPendientes.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(17, "Visualizar solicitudes liberadas", false, 14, "/app/solped/visualizarSolicitudesPendientes.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(18, "Orden de compra", true, 0));
            lstMenu.Add(new Entities.SistemaEntity.Menu(19, "Liberar ordenes pendientes", false, 18, "/app/ocompra/liberarOrdenesPendientes.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(20, "Visualizar historial liberadas", false, 18, "/app/ocompra/visualizarHistorialLiberadas.aspx"));
            //lstMenu.Add(new Entities.SistemaEntity.Menu(21, "Grafico Consolidado", false, 2, "/app/reporte/grafico-consolidado.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(21, "Monitor de Documentos", false, 0, "/app/monitor/monitor.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(22, "Contáctanos", false, 0, "/app/sugerencias/sugerencias.aspx"));
            lstMenu.Add(new Entities.SistemaEntity.Menu(23, "Cambiar Aprobador", false, 0, "/app/cambiarAprobador/cambiarAprobador.aspx"));
            List<Entities.SistemaEntity.Pagina> lstPaginasRol = GetPaginasRol(idRol);
            List<Entities.SistemaEntity.Menu> lstMenuUsuario = new List<Entities.SistemaEntity.Menu>();

            foreach (Entities.SistemaEntity.Menu menu in lstMenu.OrderBy(x => x.esPadre))
                if (lstPaginasRol.Exists(x => x.path == menu.url))
                    lstMenuUsuario.Add(menu);
                else if (menu.esPadre && lstMenuUsuario.Exists(a => a.idPadre == menu.id))
                    lstMenuUsuario.Add(menu);

            return lstMenuUsuario.OrderBy(x => x.id).ToList();
        }

      
        public static Entities.UsuarioEntity.Rol GetRol(string idRol)
        {
            return GetListRol().FirstOrDefault(x => x.id == idRol);
        }

        private static List<Entities.UsuarioEntity.Rol> GetRoles(params string[] idRol)
        {
            List<Entities.UsuarioEntity.Rol> lstTodosLosRoles = GetListRol();
            List<Entities.UsuarioEntity.Rol> lstRolesFiltrados = new List<Entities.UsuarioEntity.Rol>();

            foreach (string id in idRol)
                lstRolesFiltrados.Add(lstTodosLosRoles.First(x => x.id == id));

            return lstRolesFiltrados;
        }

        public static ResponseWSEntity AddUsuario(string rut, string nombre, string correo, string clave, string rol, string accion)
        {
            try
            {
                rol = GetListRol().FirstOrDefault(x => x.nombre == rol).id;
                rut = rut.Replace(".", string.Empty);
                dt_pifi515_res resp = UsuarioDAL.AddUsuario(rut, nombre, correo, clave, rol, accion);
                return new ResponseWSEntity("0", resp.O_MENSAJE);
            }
            catch
            {
                return new ResponseWSEntity("3", "<strong>[Error 232]</strong> Ha ocurrido un error.");
            }
        }

        public static List<UsuarioEntity> GetUsuario(string rut, string rol, string accion, string fechai, string fechaf, string rutm, ref ResponseWSEntity mensajeOperacion)
        {
            rut = rut.Replace(".", string.Empty);

            try
            {
                if (rol != "")
                { rol = GetListRol().FirstOrDefault(x => x.nombre == rol).id; }
                dt_pifi515_res respWS = UsuarioDAL.GetUsuario(rut, rol, accion, fechai, fechaf, rutm);
                mensajeOperacion = new ResponseWSEntity(respWS.O_COD_MENSAJE, respWS.O_MENSAJE);

                List<UsuarioEntity> LstUsrs = new List<UsuarioEntity>();

                for (int i = 0; i < Convert.ToInt32(respWS.T_USUARIOS.Length); i++)
                {
                    UsuarioEntity usuario = new UsuarioEntity();

                    usuario.nombre = respWS.T_USUARIOS[i].NOMBRE;
                    usuario.rut = respWS.T_USUARIOS[i].USUARIO;
                    usuario.idRol = respWS.T_USUARIOS[i].ROL;
                    usuario.nombreRol = respWS.T_USUARIOS[i].NOMBREROL;
                    usuario.correo = respWS.T_USUARIOS[i].SMTP_ADDR;
                    usuario.fechac = respWS.T_USUARIOS[i].FECHA_CRE;
                    usuario.fecham = respWS.T_USUARIOS[i].FECHA_MOD;
                    usuario.estado = respWS.T_USUARIOS[i].ESTATUS;

                    if (usuario.estado == "0")
                    {
                        usuario.estado = "Activo";
                    }
                    else if (usuario.estado == "1")
                    {
                        usuario.estado = "Inactivo";
                    }

                    /*usuario.rol.nombre = GetListRol().FirstOrDefault(x => x.id == usuario.rol.id).nombre;*/ //obtengo descripción del rol
                    LstUsrs.Add(usuario);
                }

                return LstUsrs;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }


        public static ResponseWSEntity SetEditarUsuario(string nombre, string rut, string rol, string correo, string usuModf, string accion)
        {
            try
            {
                rut = rut.Replace(".", string.Empty);
                rol = GetListRol().FirstOrDefault(x => x.nombre == rol).id;
                dt_pifi515_res respWS = UsuarioDAL.SetEditarUsuario(nombre, rut, rol, correo, usuModf, accion);
                return new ResponseWSEntity(respWS.O_COD_MENSAJE, respWS.O_MENSAJE);
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>[Error 232]</strong> Se generó un problema interno al intentar editar el usuario.");
            }
        }

        public static ResponseWSEntity SetEditarEstado(string rut, string accion)
        {
            try
            {
                rut = rut.Replace(".", string.Empty);
                dt_pifi515_res respWS = UsuarioDAL.SetEditarEstado(rut, accion);
                return new ResponseWSEntity(respWS.O_COD_MENSAJE, respWS.O_MENSAJE);
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>[Error 232]</strong> Se generó un problema interno al intentar editar el estatus.");
            }
        }

        public static List<Entities.SistemaEntity.Pagina> GetPaginasRol(string rut)
        {
            
            string accion = "1";
            dt_pifi521_res respWS = UsuarioDAL.GetRolesn(rut, accion);
            List<Entities.SistemaEntity.Pagina> lstPagina = new List<Entities.SistemaEntity.Pagina>();
            List<Entities.UsuarioEntity.Rol> lstRol = new List<Entities.UsuarioEntity.Rol>();

            if (respWS.O_CODIGO_MENS == "0")
            {
                lstRol.Add(new Entities.UsuarioEntity.Rol(respWS.T_ROLES[0].Z_ID_ROL, respWS.T_ROLES[0].Z_DESC_ROL));
                for (int i = 0; i < Convert.ToInt32(respWS.T_ROLES.Length); i++)
                {
                    lstPagina.Add(new Entities.SistemaEntity.Pagina(respWS.T_ROLES[i].Z_URL_ROLSOLPED, lstRol));
                }

            }
            return lstPagina;
        }
        public static List<UsuarioEntity.Rol> GetListRol()
        {

            dt_pifi522_res respWS = UsuarioDAL.GetListRol();

            List<UsuarioEntity.Rol> LstUsrs = new List<UsuarioEntity.Rol>();

            for (int i = 0; i < Convert.ToInt32(respWS.T_DES_ROLES.Length); i++)
            {
                UsuarioEntity.Rol roldes = new UsuarioEntity.Rol();

                roldes.id = respWS.T_DES_ROLES[i].ZID_ROL;
                roldes.nombre = respWS.T_DES_ROLES[i].ZDESC_ROL;
                LstUsrs.Add(roldes);
            }

            return LstUsrs;
        }

    }
}
