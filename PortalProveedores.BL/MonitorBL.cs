﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.Entities;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.monitorOC;
namespace PortalProveedores.BL
{
    public class MonitorBL
    {

        public static List<MonitorEntity> GetMonitor(string sociedad, string rutproveedor, string fechaemisionini, string fechaemisionfin, string accion, string idDoc, string id, ref ResponseWSEntity mensajeOperacion)
        {
            
            try
            {
                dt_pifi523_res respWS = MonitorDAL.GetMonitor(sociedad, rutproveedor, fechaemisionini, fechaemisionfin, accion, idDoc, id);

                List<MonitorEntity> ListadoMonitor = new List<MonitorEntity>();
                
                dt_pifi523_resItem resquest = new dt_pifi523_resItem();
                mensajeOperacion = new ResponseWSEntity(respWS.O_COD_MENSAJE, respWS.O_MENSAJE);

                for (int i = 0; i < Convert.ToInt32(respWS.T_SALIDA_CABE.Length); i++)
                {
                    for (int j = 0; j < Convert.ToInt32(respWS.T_SALIDA_DETA.Length); j++)
                    {
                        if (i == j)
                        {
                            MonitorEntity monitor = new MonitorEntity();

                            monitor.idMoni = respWS.T_SALIDA_CABE[i].I_ID_MONI;
                            monitor.razon = respWS.T_SALIDA_CABE[i].I_RAZON_SOCIAL;
                            monitor.fecha = respWS.T_SALIDA_CABE[i].I_FECHA;
                            monitor.idProceso = respWS.T_SALIDA_CABE[i].I_PROCESO;
                            monitor.idStatus = respWS.T_SALIDA_CABE[i].I_ESTATUS;
                            monitor.monto = respWS.T_SALIDA_CABE[i].I_MONTO;
                            monitor.sociedad = respWS.T_SALIDA_CABE[i].I_SOCIEDAD;
                            monitor.idSolped = respWS.T_SALIDA_DETA[i].I_ID_SOLPED;
                            monitor.rutLiberador = respWS.T_SALIDA_DETA[i].I_RUT_LIBER;
                            monitor.fechaLib = respWS.T_SALIDA_DETA[i].I_FECHA_LIBER;
                            monitor.horaLib = respWS.T_SALIDA_DETA[i].I_HORA_LIBER;
                            monitor.idOrden = respWS.T_SALIDA_DETA[i].I_ID_ORDEN_C;
                            monitor.liberadores = respWS.T_SALIDA_DETA[i].I_LIBERADORES;
                            monitor.folio = respWS.T_SALIDA_DETA[i].I_FOLIO_FACT;
                            monitor.fechaEmision = respWS.T_SALIDA_DETA[i].I_FECHA_EM_FACT;
                            monitor.fechaPago = respWS.T_SALIDA_DETA[i].I_FECHA_PAGO_FAC;
                            monitor.tipoPago = respWS.T_SALIDA_DETA[i].I_TIPO_PAGO;
                            monitor.montoPago = respWS.T_SALIDA_DETA[i].I_MONTO_PAGO;
                            monitor.fechaProba = respWS.T_SALIDA_DETA[i].I_FECHA_PROBA;
                            monitor.idCoti = respWS.T_SALIDA_DETA[i].I_ID_COTI;
                            monitor.idMercaderia = respWS.T_SALIDA_DETA[i].I_ID_ENTRADA_MER;
                            ListadoMonitor.Add(monitor);
                        }
                    }
                }
                return ListadoMonitor;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static MonitorEntity GetDetalleMonitor(string idmoni, string accion, string idsociedad)
        {

            try
            {
                //List<MonitorEntity> lstData = new List<MonitorEntity>();
                dt_pifi523_res respWS = MonitorDAL.GetDetalleMonitor(idmoni, accion, idsociedad);

                //for (int i = 0; i < Convert.ToInt32(respWS.T_SALIDA_DETA.Length); i++) { 

                MonitorEntity monitor = new MonitorEntity();

                monitor.idMoni = respWS.T_SALIDA_DETA[0].I_ID_MONI;
                monitor.idSolped = respWS.T_SALIDA_DETA[0].I_ID_SOLPED;
                monitor.rutLiberador = respWS.T_SALIDA_DETA[0].I_RUT_LIBER;
                monitor.fechaLib = respWS.T_SALIDA_DETA[0].I_FECHA_LIBER;
                monitor.horaLib = respWS.T_SALIDA_DETA[0].I_HORA_LIBER;
                monitor.idOrden = respWS.T_SALIDA_DETA[0].I_ID_ORDEN_C;
                monitor.liberadores = respWS.T_SALIDA_DETA[0].I_LIBERADORES;
                monitor.folio = respWS.T_SALIDA_DETA[0].I_FOLIO_FACT;
                monitor.fechaEmision = respWS.T_SALIDA_DETA[0].I_FECHA_EM_FACT;
                monitor.fechaPago = respWS.T_SALIDA_DETA[0].I_FECHA_PAGO_FAC;
                monitor.tipoPago = respWS.T_SALIDA_DETA[0].I_TIPO_PAGO;
                monitor.montoPago = respWS.T_SALIDA_DETA[0].I_MONTO_PAGO;
                monitor.fechaProba = respWS.T_SALIDA_DETA[0].I_FECHA_PROBA;
                monitor.idCoti = respWS.T_SALIDA_DETA[0].I_ID_COTI;
                monitor.idMercaderia = respWS.T_SALIDA_DETA[0].I_ID_ENTRADA_MER;
                //    lstData.Add(monitor);
                //}

                //    return lstData;
                return monitor;


            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
