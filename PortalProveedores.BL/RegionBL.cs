﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL;

namespace PortalProveedores.BL
{
    public class RegionBL
    {
        public static List<Entities.RegionEntity> GetRegion()
        {
            return RegionDAL.GetRegion();
        }
    }
}
