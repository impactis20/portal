﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.Entities;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.sugerencias;
namespace PortalProveedores.BL
{
    public class SugerenciasBL
    {
        public static List<SugerenciasEntity> GetSugerencias(string accion, string rutProveedor, string idSoli, string idOrigen, string descripcion, string razon, string idsociedad, string ruta, ref ResponseWSEntity mensajeOperacion)

        {
            List<SugerenciasEntity> ListadoSugerencias = new List<SugerenciasEntity>();
            try
            {
                dt_pifi524_res respWS = SugerenciasDAL.GetSugerencias(accion, rutProveedor, idSoli, idOrigen, descripcion, razon, idsociedad, ruta );

                mensajeOperacion = new ResponseWSEntity(respWS.o_id_mensaje, respWS.o_des_mensaje);

                //for (int i = 0; i < Convert.ToInt32(respWS.TI_ID_SUGERE.Length); i++)
                //{
                //    SugerenciasEntity sugerencias = new SugerenciasEntity();

                //    sugerencias.descripcion = respWS.TI_ID_SUGERE[i].Z_DESCRIPCION;
                //    sugerencias.id = respWS.TI_ID_SUGERE[i].Z_TIPO;

                //    ListadoSugerencias.Add(sugerencias);

                //}
                return ListadoSugerencias;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static List<SugerenciasEntity> GetTipoSugerencia(string accion, string origen, ref ResponseWSEntity mensajeOperacion)
        {
            try
            {
              
                List<SugerenciasEntity> ListSugerencias = new List<SugerenciasEntity>();
                dt_pifi524_res respWS = SugerenciasDAL.GetTipoSugerencia(accion, origen);

                mensajeOperacion = new ResponseWSEntity(respWS.o_id_mensaje, respWS.o_des_mensaje);

                for (int i = 0; i < Convert.ToInt32(respWS.TI_ID_SUGERE.Length); i++)
                {
                    SugerenciasEntity sugerencias = new SugerenciasEntity();

                    sugerencias.descripcion = respWS.TI_ID_SUGERE[i].Z_DESCRIPCION;
                    sugerencias.id = respWS.TI_ID_SUGERE[i].Z_TIPO;
                    ListSugerencias.Add(sugerencias);
                }

                return ListSugerencias;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("0", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static List<SugerenciasEntity> GetSugerenciasExternas(string accion, string rutProveedor, string idSoli, string idOrigen, string descripcion, string razon, string idsociedad, ref ResponseWSEntity mensajeOperacion)

        {
            List<SugerenciasEntity> ListadoSugerencias = new List<SugerenciasEntity>();
            try
            {
                dt_pifi524_res respWS = SugerenciasDAL.GetSugerenciasExternas(accion, rutProveedor, idSoli, idOrigen, descripcion, razon, idsociedad);

                mensajeOperacion = new ResponseWSEntity(respWS.o_id_mensaje, respWS.o_des_mensaje);

                //for (int i = 0; i < Convert.ToInt32(respWS.TI_ID_SUGERE.Length); i++)
                //{
                //   SugerenciasExternasEntity sugerencias = new SugerenciasExternasEntity();

                //    sugerencias.descripcion = respWS.TI_ID_SUGERE[i].Z_DESCRIPCION;
                //    sugerencias.id = respWS.TI_ID_SUGERE[i].Z_TIPO;

                //    ListadoSugerencias.Add(sugerencias);

                //}
                return ListadoSugerencias;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
    }
}
