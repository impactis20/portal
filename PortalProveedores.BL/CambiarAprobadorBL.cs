﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.Entities;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.cambiarAprobador;
namespace PortalProveedores.BL
{
  public class CambiarAprobadorBL
    {
        //public static List<CambiarAprobadorEntity> GetCambiarAprobador(string rutAnterior, string rutNuevo, string idParametro, string fechaDesde, string fechaHasta, ref ResponseWSEntity mensajeOperacion)

        //{
        //    List<CambiarAprobadorEntity> ListadoAprobador = new List<CambiarAprobadorEntity>();
        //    try
        //    {
        //        dt_pifi525_res respWS = CambiarAprobadorDAL.GetCambiarAprobador(rutAnterior, rutNuevo, idParametro, fechaDesde, fechaHasta);

        //        mensajeOperacion = new ResponseWSEntity(respWS.o_id_mensaje, respWS.o_des_mensaje);

        //        return ListadoAprobador;
        //    }
        //    catch (Exception e)
        //    {
        //        mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

        //        return null;
        //    }
        //}


        public static ResponseWSEntity GetCambiarAprobador(string rutAnterior, string rutNuevo, string idParametro, string fechaDesde, string fechaHasta)
        {
            try
            {
                dt_pifi525_res resp = CambiarAprobadorDAL.GetCambiarAprobador(rutAnterior, rutNuevo, idParametro, fechaDesde, fechaHasta);
                return new ResponseWSEntity(resp.o_id_mensaje, resp.o_des_mensaje);
            }
            catch (Exception ex)
            {
                return new ResponseWSEntity("1", "Se generó un problema interno al intentar enviar los datos. Favor contacte a su administrador." + ex.Message);
            }
        }
    }
}
