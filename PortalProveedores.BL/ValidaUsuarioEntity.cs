﻿namespace PortalProveedores.BL
{
    public class ValidaUsuarioEntity
    {
        public ValidaUsuarioEntity() { }

        public ValidaUsuarioEntity(string rolid, string rolDescripcion, string grupoLiberadorId, string grupoLiberadorDescripcion, string organizaciondeComprasaId, 
            string organizacionDeComprasaDescripcion)
        {
            this.rolid = rolid;
            this.rolDescripcion = rolDescripcion;
            this.grupoLiberadorId = grupoLiberadorId;
            this.grupoLiberadorDescripcion = grupoLiberadorDescripcion;
            this.organizaciondeComprasaId = organizaciondeComprasaId;
            this.organizacionDeComprasaDescripcion = organizacionDeComprasaDescripcion;

        }

        public string rolid { get; set; }
        public string rolDescripcion { get; set; }
        public string grupoLiberadorId { get; set; }
        public string grupoLiberadorDescripcion { get; set; }

        public string organizaciondeComprasaId { get; set; }
        public string organizacionDeComprasaDescripcion { get; set; }
    }
}