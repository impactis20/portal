﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL;

namespace PortalProveedores.BL
{
    public class SociedadBL
    {
        public static List<Entities.SociedadEntity> GetAllSociedades()
        {
            return SociedadDAL.GetAllSociedades();
        }

        public static Entities.SociedadEntity GetSociedad(string idSociedad)
        {
            return SociedadDAL.GetAllSociedades().FirstOrDefault(x => x.id == idSociedad);
        }
    }
}
