﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.mercaderias;
using PortalProveedores.Entities;


namespace PortalProveedores.BL
{
    public class MercaderiasBL
    {
        public static List<MercaderiasEntity> GetOrdenes(string rut, ref ResponseWSEntity mensajeOperacion)
        {
            try
            {
                string accion = "1";

                List<MercaderiasEntity> lstData = new List<MercaderiasEntity>();
                dt_pifi520_res respWS = MercaderiasDAL.GetOrdenes(rut, accion);

                mensajeOperacion = new ResponseWSEntity(respWS.O_COD_MENSA, respWS.O_MENSAJE);

                for (int i = 0; i < Convert.ToInt32(respWS.IT_CABECERA.Length); i++)
                {
                    MercaderiasEntity ordenL = new MercaderiasEntity();

                    ordenL.orden = respWS.IT_CABECERA[i].ZORDEN_COMPRA;
                    ordenL.fechaO = respWS.IT_CABECERA[i].ZFECHA_OC;
                    ordenL.rutP = respWS.IT_CABECERA[i].ZRUT_PROVEEDOR;
                    ordenL.razonP = respWS.IT_CABECERA[i].ZRAZON_SOCIAL;
                    ordenL.precioN = respWS.IT_CABECERA[i].ZPRECIO_NETO;
                    ordenL.iva = respWS.IT_CABECERA[i].ZIVA;
                    ordenL.retencion = respWS.IT_CABECERA[i].ZRETENCION;
                    ordenL.montoB = respWS.IT_CABECERA[i].ZVALOR_BRUTO;

                    lstData.Add(ordenL);
                }
                return lstData;
                
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("0", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static List<MercaderiasEntity> GetDetalle(string rut, string orden, ref ResponseWSEntity mensajeOperacion)
        {
            try
            {
                string accion = "2";
                
                List<MercaderiasEntity> lstData = new List<MercaderiasEntity>();
                dt_pifi520_res respWS = MercaderiasDAL.GetDetalle(rut, accion, orden);
                mensajeOperacion = new ResponseWSEntity(respWS.O_COD_MENSA, respWS.O_MENSAJE);

                for (int i = 0; i < Convert.ToInt32(respWS.IT_DETALLE.Length); i++)
                {
                    MercaderiasEntity ordenD = new MercaderiasEntity();

                    ordenD.orden = respWS.IT_DETALLE[i].ZORDEN_COMPRA;
                    ordenD.posicion = respWS.IT_DETALLE[i].ZPOSICION;
                    ordenD.material = respWS.IT_DETALLE[i].ZMATERIAL;
                    ordenD.descripcion = respWS.IT_DETALLE[i].ZDESCRIPCION;
                    ordenD.cantidad = respWS.IT_DETALLE[i].ZCANTIDAD;
                    ordenD.unidadM = respWS.IT_DETALLE[i].ZUM;
                    ordenD.entregaF = respWS.IT_DETALLE[i].ZENTREGA_FINAL;
                    ordenD.cantidadR = respWS.IT_DETALLE[i].ZCANT_RECIBIDA;
                    ordenD.cantidadP = respWS.IT_DETALLE[i].ZCANT_PENDIENTE;
                    ordenD.tipoM = respWS.IT_DETALLE[i].ZTIPO;
                    ordenD.descripcionT = respWS.IT_DETALLE[i].ZDESCRIP_TIPO;

                    lstData.Add(ordenD);
                }
                return lstData;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("0", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static ResponseWSEntity AddContabilizacion(string tipoM, string orden, string posicion, string cantidadR, string fechaD, string observaciones, string fechaS, string accion, string rut, string GuiaD)
        {
            try
            {
                dt_pifi520_res respWS = MercaderiasDAL.AddContabilizacion(tipoM, orden, posicion, cantidadR, fechaD, observaciones, fechaS, accion, rut, GuiaD);
                return new ResponseWSEntity(respWS.O_COD_MENSA, respWS.O_MENSAJE);
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>[Error 232]</strong> Se generó un problema interno al intentar contabilizar la mercancía.");
            }
        }

    }
}
