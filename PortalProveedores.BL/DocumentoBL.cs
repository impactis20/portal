﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.editarDocumento;
using PortalProveedores.DAL.buscarDocumento;
using PortalProveedores.DAL.ingresarDocumento;
using PortalProveedores.DAL.umbralDocVencimiento;
using PortalProveedores.DAL.pagoProveedor;
using PortalProveedores.DAL.cambioEstadoDocumento;
using PortalProveedores.DAL.crearProveedor;
using PortalProveedores.DAL.motivoRechazo;
using PortalProveedores.Entities;

namespace PortalProveedores.BL
{
    public class DocumentoBL
    {
        public static List<DocumentoEntity> GetDocumento(string idSociedad, string rutProveedor, string fechaDesde, string fechaHasta, string folio, string estado, string fechaRecepcion, string fechaCobro, ref ResponseWSEntity mensajeOperacion, string tipoDoc)
        {
            List<DocumentoEntity> LstDctos = new List<DocumentoEntity>();
            rutProveedor = rutProveedor.Replace(".", string.Empty);

            try
            {
                //obtiene documentos del proveedor
                /*dt_pifi504_res respWS = DocumentoDAL.GetDocumento(idSociedad, rutProveedor, fechaDesde, fechaHasta, folio, estado, fechaRecepcion, fechaCobro, tipoDoc);*/ //llama a la capa DAL función "GetDocumento"
                dt_pifi504_res respWS = DocumentoDAL.GetDocumento(idSociedad, rutProveedor, fechaDesde, fechaHasta, folio, estado, fechaRecepcion, fechaCobro, tipoDoc); //llama a la capa DAL función "GetDocumento"


                mensajeOperacion = new ResponseWSEntity(respWS.CODIGO_E, respWS.MENSAJE);

                String nombreDoc;

                for (int i = 0; i < Convert.ToInt32(respWS.REPORTE.Length); i++)
                {
                    if (respWS.REPORTE[i].RUTA_PDF == "")
                    { nombreDoc = respWS.REPORTE[i].RUTA_PDF; }
                    else
                    {
                        nombreDoc = System.IO.Path.GetFileNameWithoutExtension(respWS.REPORTE[i].RUTA_PDF);
                    }
                    DocumentoEntity documento = new DocumentoEntity(respWS.REPORTE[i].NUMERO_DOCUMENTO,
                                                          respWS.REPORTE[i].RUT_EMPRESA,
                                                          Initcap(respWS.REPORTE[i].RAZON_SOCIAL),
                                                          idSociedad,
                                                          respWS.REPORTE[i].MONTO,
                                                          respWS.REPORTE[i].FECHA_FACTURA.Replace(".", "-"),
                                                          Initcap(respWS.REPORTE[i].ESTADO),
                                                          respWS.REPORTE[i].TIPO_DOC,
                                                          respWS.REPORTE[i].REFERENCIA,
                                                          respWS.REPORTE[i].FECHA_RECEPCION.Replace(".", "-"),
                                                          respWS.REPORTE[i].FECHA_PROB_PAGO.Replace(".", "-"),
                                                          respWS.REPORTE[i].FECHA_COBRO.Replace(".", "-"),
                                                          respWS.REPORTE[i].OBSERVACION,
                                                          respWS.REPORTE[i].FLAG_PORTAL,
                                                          respWS.REPORTE[i].CAMPO1,
                                                          respWS.REPORTE[i].CAMPO2,
                                                          respWS.REPORTE[i].CAMPO3,
                                                          respWS.REPORTE[i].CAMPO4,
                                                          respWS.REPORTE[i].CAMPO5,
                                                          respWS.REPORTE[i].RUTA_PDF,
                                                          respWS.REPORTE[i].RECHAZO,
                                                          nombreDoc,
                                                            respWS.REPORTE[i].RUTA_OC_PDF); //agregar campo ruta pdf <- Esto era lo que faltaba recibir, por eso tuvimos que actualizar el servicio (antes no venia este campo)
                                                                      //Se supone que con esta ruta ahora deberiamos ser capaz de ir a buscar el pdf para que el usuario lo pueda descargar

                    documento.status.descripcion = GetStatus().FirstOrDefault(x => x.id == documento.status.id).descripcion; //obtengo descripción del estatus
                    documento.tipoDocumento.descripcion = GetTipoDocumento().FirstOrDefault(x => x.id == documento.tipoDocumento.id).descripcion; // obtengo descripción del tipo de documento
                    /*documento.motivoRechazo.descripcion = GetMotivoDescripcion().FirstOrDefault(x => x.id == documento.motivoRechazo.id).descripcion;*/// obtengo descripción del tipo de documento

                    LstDctos.Add(documento);
                }
            }
            catch (Exception ex)
            {
                mensajeOperacion = new ResponseWSEntity("2", "<strong>Error 230.</strong> Se generó un problema interno al intentar obtener los documentos. Favor contacte a su administrador.<br><br>" + ex.Message);
            }

            return LstDctos;
        }


        public static ResponseWSEntity AddDocumento(string nroDocumento, string rutProveedor, string idTipoDocumento, string ReferenciaNC, string fechaDocumento, string monto, string idSociedad, string observacion, string idEstatus, string rutUsuarioResponsable, string fullPathPDF)
        {
            try
            {
                double num;
                if (double.TryParse(monto, out num))
                {
                    rutProveedor = rutProveedor.Replace(".", string.Empty);
                    monto = monto.Replace(".", string.Empty);

                    dt_pifi501_res respWS = DocumentoDAL.AddDocumento(nroDocumento, rutProveedor, idTipoDocumento, ReferenciaNC, fechaDocumento, monto, idSociedad, observacion, idEstatus, rutUsuarioResponsable, fullPathPDF);
                    ResponseWSEntity resp = new ResponseWSEntity(respWS.E_CODIGO, string.Format("{0};{1}", respWS.E_MENSAJE, DateTime.Now.ToShortDateString()));
                    return resp;
                }
                else
                    return new ResponseWSEntity("2", "El monto debe ser un valor numérico.");
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>Error 220.</strong> Se generó un problema interno al intentar agregar el documento. Favor contacte a su administrador.");
            }
        }



        public static ResponseWSEntity SetDocumento(DocumentoEntity documentoAntiguo, DocumentoEntity documentoNuevo, string rutUsuarioResponsable)
        {
            try
            {
                //limpia los puntos en rut y monto
                documentoAntiguo.monto = documentoAntiguo.monto.Replace(".", string.Empty);
                documentoAntiguo.rutProveedor = documentoAntiguo.rutProveedor.Replace(".", string.Empty);

                documentoNuevo.monto = documentoNuevo.monto.Replace(".", string.Empty);
                documentoNuevo.rutProveedor = documentoNuevo.rutProveedor.Replace(".", string.Empty);

                dt_pifi511_res respWS = DocumentoDAL.SetDocumento(documentoAntiguo, documentoNuevo, rutUsuarioResponsable);
                return new ResponseWSEntity(respWS.E_CODIGO_E, respWS.E_MENSAJE);
            }
            catch (Exception ex)
            {
                return new ResponseWSEntity("2", "<strong>[Error 231]</strong> Se generó un problema interno al intentar editar el documento. Favor contacte a su administrador." + "<br>" + ex.Message);
            }
        }

        public static ResponseWSEntity GetUmbralDocumentoVencimiento()
        {
            try
            {
                string peticion = "1";
                dt_pifi514_res respWS = DocumentoDAL.GetUmbralDocumentoVencimiento(peticion);

                return new ResponseWSEntity(respWS.O_COD_ERROR, respWS.O_VALOR);
            }
            catch
            {
                return new ResponseWSEntity("1", "4");
            }
        }

        public static List<DocumentoEntity> GetPago(string idSociedad, string rut, string nroFactura, ref ResponseWSEntity mensajeOperacion)
        {
            rut = rut.Replace(".", string.Empty);

            try
            {
                dt_pifi510_res respWS = DocumentoDAL.GetPago(idSociedad, rut, nroFactura);
                mensajeOperacion = new ResponseWSEntity(respWS.E_COD_ERROR, respWS.E_MENSAJE);

                List<DocumentoEntity> LstDctos = new List<DocumentoEntity>();

                for (int i = 0; i < Convert.ToInt32(respWS.E_REPORTE.Length); i++)
                {
                    DocumentoEntity factura = new DocumentoEntity();

                    factura.nroDocumento = respWS.E_REPORTE[i].XBLNR;
                    factura.fechaEmision = respWS.E_REPORTE[i].BLDAT;
                    factura.tipoDocumento = new DocumentoEntity.TipoDocumento(respWS.E_REPORTE[i].LTEXT, respWS.E_REPORTE[i].BLART);
                    factura.monto = respWS.E_REPORTE[i].WRBTR;
                    factura.nroDocumentoPago = respWS.E_REPORTE[i].VBLNR;
                    factura.banco = respWS.E_REPORTE[i].HBKID;
                    factura.nroCheque = respWS.E_REPORTE[i].CHECT;
                    factura.fechaPago = respWS.E_REPORTE[i].ZALDT;
                    factura.monto2 = respWS.E_REPORTE[i].RWBTR;
                    factura.status = new DocumentoEntity.Status(respWS.E_REPORTE[i].ESTADO);
                    factura.fechaCobro = respWS.E_REPORTE[i].FECHA;

                    LstDctos.Add(factura);
                }

                return LstDctos;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static ResponseWSEntity SetEstadoDocumento(string idSociedad, string rut, string nroDocumento, string fechaDoc, string estado, string rutUsuario)
        {
            try
            {
                dt_pifi505_res respWS = DocumentoDAL.SetEstadoDocumento(idSociedad, rut, nroDocumento, fechaDoc, estado, rutUsuario);
                return new ResponseWSEntity(respWS.E_COD_ERROR, respWS.E_MENSAJE); //respWS.E_COD_ERROR
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>[Error 232]</strong> Se generó un problema interno al intentar eliminar el documento. Favor contacte a su administrador.");
            }
        }

        public static ResponseWSEntity AddProveedor(string rut, string nombre, string region, string poblacion, string calle, string sociedad, string correo)
        {
            try
            {
                rut = rut.Replace(".", string.Empty);
                dt_pifi513_res resp = DocumentoDAL.AddProveedor(rut, nombre, region, poblacion, calle, sociedad, correo);
                return new ResponseWSEntity("0", resp.I_MENSAJE);
            }
            catch
            {
                return new ResponseWSEntity("2", "<strong>[Error 882]</strong> Se generó un problema interno al intentar ingresar a un proveedor. Favor contacte a su administrador.");
            }
        }

        public static List<DocumentoEntity.Status> GetStatus()
        {
            List<DocumentoEntity.Status> lstData = DocumentoDAL.GetStatus();

            return lstData;
        }

        public static List<DocumentoEntity.Status> GetStatus(bool seleccionable)
        {
            return GetStatus().Where(x => x.seleccionable == seleccionable).ToList();
        }

        public static List<DocumentoEntity.Status> GetStatus(bool seleccionable, bool estadoAutomatico)
        {
            return GetStatus().Where(x => x.seleccionable == seleccionable && x.estadoAutomatico == estadoAutomatico).ToList();
        }
        public static List<DocumentoEntity.Status> GetStatusMostrar()
        {
            List<DocumentoEntity.Status> lstData1 = DocumentoDAL.GetStatusMostrar();

            return lstData1;
        }
        public static List<DocumentoEntity.Status> GetStatusMostrar(bool seleccionable, bool estadoAutomatico)
        {
            return GetStatusMostrar().Where(x => x.seleccionable == seleccionable && x.estadoAutomatico == estadoAutomatico).ToList();
        }

        public static List<DocumentoEntity.TipoDocumento> GetTipoDocumento()
        {
            return DocumentoDAL.GetTipoDocumento();
        }

        public static string GetNombreTipoDocumento(string id)
        {
            return DocumentoDAL.GetTipoDocumento().FirstOrDefault(x => x.id == id).descripcion;
        }

        public static List<DocumentoEntity.MotivoRechazo> GetMotivoDescripcion(ref ResponseWSEntity mensajeOperacion)
        {
            try
            {
                string peticion = "1";
                string accion = "";

                List<DocumentoEntity.MotivoRechazo> lstData = new List<DocumentoEntity.MotivoRechazo>();
                dt_pifi518_res respWS = DocumentoDAL.GetMotivo(peticion, accion);

                mensajeOperacion = new ResponseWSEntity(respWS.O_CODIGO_MENS, respWS.O_MENSAJE);

                for (int i = 0; i < Convert.ToInt32(respWS.T_RECHAZO.Length); i++)
                {
                    DocumentoEntity.MotivoRechazo motivoR = new DocumentoEntity.MotivoRechazo(
                        respWS.T_RECHAZO[i].DESCRIPCION_R,
                        respWS.T_RECHAZO[i].ID_RECHAZO);

                    lstData.Add(motivoR);
                }

                return lstData;
            }
            catch
            {
                mensajeOperacion = new ResponseWSEntity("0", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static Entities.DocumentoEntity.MotivoRechazo GetMotivo(string motivo)
        {
            return DocumentoDAL.GetMotivoDescripcion().FirstOrDefault(x => x.id == motivo);
        }

        private static string Initcap(string texto)
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(texto.ToLower());
        }

    }
}
