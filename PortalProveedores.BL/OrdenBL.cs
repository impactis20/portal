﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalProveedores.DAL.webServices;
using PortalProveedores.DAL.consultarOrden;
using PortalProveedores.Entities;
using PortalProveedores.DAL.validaUsuario;
using PortalProveedores.DAL.modificarOrden;
using PortalProveedores.DAL.solicitudPedido;

namespace PortalProveedores.BL
{
    public static class OrdenBL
    {
        private static ResponseWSEntity mensajeOperacion;

        public static dt_pifi519_res GetSolicitud2(string USUARIO, string PENDIENTE_LIBERAR, string fechaDesde, string fechaHasta, string SOLPED, string action)
        {
            try
            {
                dt_pifi519_res respWS = OrdenDAL.GetSolicitud(USUARIO, PENDIENTE_LIBERAR, fechaDesde, fechaHasta, SOLPED, action);
                return respWS;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static List<SolicitudEntity> GetSolicitud(string USUARIO, string PENDIENTE_LIBERAR, string fechaDesde, string fechaHasta, string SOLPED, string action)
        {
            try
            {
                dt_pifi519_res respWS = OrdenDAL.GetSolicitud(USUARIO, PENDIENTE_LIBERAR, fechaDesde, fechaHasta, SOLPED, action);

                List<SolicitudEntity> ListadoSolicitud = new List<SolicitudEntity>();
                string idped = "0";

                SolicitudEntity solicitud = new SolicitudEntity();
                DetalleSolicitudEntity detalle = new DetalleSolicitudEntity();
                for (int i = 0; i < Convert.ToInt32(respWS.T_SOLPED.Length); i++)
                {

                    if (idped == respWS.T_SOLPED[i].ID_PEDIDO)
                    {
                        detalle = new DetalleSolicitudEntity();
                        detalle.CANTIDAD = respWS.T_SOLPED[i].CANTIDAD;
                        detalle.CENTRO_COSTE = respWS.T_SOLPED[i].CENTRO_COSTE;
                        detalle.DESCRIPCION = respWS.T_SOLPED[i].DESCRIPCION;
                        detalle.DESC_CENT_COSTE = respWS.T_SOLPED[i].DESC_CENT_COSTE;
                        detalle.MATERIAL = respWS.T_SOLPED[i].MATERIAL;
                        detalle.POSICION = respWS.T_SOLPED[i].POSICION;
                        detalle.UNIDAD_MEDIDA = respWS.T_SOLPED[i].UNIDAD_MEDIDA;
                        solicitud.detallesolicitud.Add(detalle);
                    }
                    else
                    {
                        solicitud = new SolicitudEntity();
                        solicitud.CENTRO = respWS.T_SOLPED[i].CENTRO;
                        solicitud.ESTADO_LIB = respWS.T_SOLPED[i].ESTADO_LIB;
                        solicitud.FECHA_ENTREGA = respWS.T_SOLPED[i].FECHA_ENTREGA;
                        solicitud.FECHA_SOLPED = respWS.T_SOLPED[i].FECHA_SOLPED;
                        solicitud.ID_PEDIDO = respWS.T_SOLPED[i].ID_PEDIDO;
                        solicitud.NOMBRE_CENTRO = respWS.T_SOLPED[i].NOMBRE_CENTRO;
                        solicitud.SOLPED = respWS.T_SOLPED[i].SOLPED;
                        solicitud.USUARIO = respWS.T_SOLPED[i].USUARIO;
                        solicitud.detallesolicitud = new List<DetalleSolicitudEntity>();

                        detalle = new DetalleSolicitudEntity();
                        detalle.CANTIDAD = respWS.T_SOLPED[i].CANTIDAD;
                        detalle.CENTRO_COSTE = respWS.T_SOLPED[i].CENTRO_COSTE;
                        detalle.DESCRIPCION = respWS.T_SOLPED[i].DESCRIPCION;
                        detalle.DESC_CENT_COSTE = respWS.T_SOLPED[i].DESC_CENT_COSTE;
                        detalle.MATERIAL = respWS.T_SOLPED[i].MATERIAL;
                        detalle.POSICION = respWS.T_SOLPED[i].POSICION;
                        detalle.UNIDAD_MEDIDA = respWS.T_SOLPED[i].UNIDAD_MEDIDA;
                        solicitud.detallesolicitud.Add(detalle);
                    }
                    idped = respWS.T_SOLPED[i].ID_PEDIDO;
                    solicitud.ESTADO_LIB = solicitud.detallesolicitud.Count.ToString();
                    ListadoSolicitud.Add(solicitud);

                }
                return ListadoSolicitud;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static List<SolicitudEntity> GetSolicitudD(string USUARIO, string PENDIENTE_LIBERAR, string fechaDesde, string fechaHasta, string SOLPED, string action)
        {
            try
            {
                dt_pifi519_res respWS = OrdenDAL.GetSolicitud(USUARIO, PENDIENTE_LIBERAR, fechaDesde, fechaHasta, SOLPED, action);

                List<SolicitudEntity> ListadoSolicitud = new List<SolicitudEntity>();
              
                SolicitudEntity solicitud = new SolicitudEntity();
                DetalleSolicitudEntity detalle = new DetalleSolicitudEntity();


                for (int i = 0; i < Convert.ToInt32(respWS.T_SOLPED.Length); i++)
                {

                    solicitud = new SolicitudEntity();
                    solicitud.CENTRO = respWS.T_SOLPED[i].CENTRO;
                    solicitud.ESTADO_LIB = respWS.T_SOLPED[i].ESTADO_LIB;
                    solicitud.FECHA_ENTREGA = respWS.T_SOLPED[i].FECHA_ENTREGA;
                    solicitud.FECHA_SOLPED = respWS.T_SOLPED[i].FECHA_SOLPED;
                    solicitud.ID_PEDIDO = respWS.T_SOLPED[i].ID_PEDIDO;
                    solicitud.NOMBRE_CENTRO = respWS.T_SOLPED[i].NOMBRE_CENTRO;
                    solicitud.SOLPED = respWS.T_SOLPED[i].SOLPED;
                    solicitud.USUARIO = respWS.T_SOLPED[i].USUARIO;

                    solicitud.CANTIDAD = respWS.T_SOLPED[i].CANTIDAD;
                    solicitud.CENTRO_COSTE = respWS.T_SOLPED[i].CENTRO_COSTE;
                    solicitud.DESCRIPCION = respWS.T_SOLPED[i].DESCRIPCION;
                    solicitud.DESC_CENT_COSTE = respWS.T_SOLPED[i].DESC_CENT_COSTE;
                    solicitud.MATERIAL = respWS.T_SOLPED[i].MATERIAL;
                    solicitud.POSICION = respWS.T_SOLPED[i].POSICION;
                    solicitud.UNIDAD_MEDIDA = respWS.T_SOLPED[i].UNIDAD_MEDIDA;
                    solicitud.COMENTARIO = respWS.T_SOLPED[i].COMENTARIO;                                                       
                    //solicitud.ESTADO_LIB = solicitud.detallesolicitud.Count.ToString();
                    ListadoSolicitud.Add(solicitud);

                }
                return ListadoSolicitud;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }

        public static List<OrdenEntity> GetOrden(string USUARIO, string PENDIENTE_LIBERAR,  string rut, string fechaDesde, string fechaHasta, string razonS, string ordenC)
        {
            try
            {
                OrdenConsultarMsgResp respWS = OrdenDAL.GetOrden(USUARIO, PENDIENTE_LIBERAR, rut, fechaDesde, fechaHasta, razonS, ordenC);

                List<OrdenEntity> ListadoOrdenes = new List<OrdenEntity>();

                for (int i = 0; i < Convert.ToInt32(respWS.payloadResp.orden.Length); i++)
                {
                    OrdenEntity orden = new OrdenEntity();

                    orden.MAN = respWS.payloadResp.orden[i].cabecera.nroOrden;
                    orden.IDPED = respWS.payloadResp.orden[i].cabecera.idGrupoAurotizador;
                    orden.USUARIO = USUARIO;
                    orden.ordencompra = respWS.payloadResp.orden[i].cabecera.nroOrden;
                    orden.fechaordencompra = respWS.payloadResp.orden[i].cabecera.fechaOrden;
                    orden.idproveedor = respWS.payloadResp.orden[i].cabecera.codProveedor;
                    orden.razonsocial = respWS.payloadResp.orden[i].cabecera.razonSocial;
                    orden.rutproveedor = respWS.payloadResp.orden[i].cabecera.rutProveedor;
                    orden.pendienteliberar = respWS.payloadResp.orden[i].cabecera.tipoOrden.id;//falta
                    orden.orgcompras = respWS.payloadResp.orden[i].cabecera.organizacionDeCompra.id;
                    orden.nameorgcompras = respWS.payloadResp.orden[i].cabecera.organizacionDeCompra.descripcion;
                    orden.SOCIEDAD = respWS.payloadResp.orden[i].cabecera.rutProveedor;//falta
                    orden.sociedad = respWS.payloadResp.orden[i].cabecera.rutProveedor;//falta
                    orden.fechaentrega = respWS.payloadResp.orden[i].cabecera.fechaEntrega;
                    orden.moneda = respWS.payloadResp.orden[i].cabecera.tipoOrden.id;//falta
                    orden.precioneto = respWS.payloadResp.orden[i].footer.neto.ToString();
                    orden.recargo = respWS.payloadResp.orden[i].footer.cargo.ToString();
                    orden.dsctoimporte = respWS.payloadResp.orden[i].adicional[1].valor.ToString();//falta
                    orden.dsctoporcentaje = respWS.payloadResp.orden[i].adicional[0].valor.ToString();//falta
                    orden.ivacompraclp = respWS.payloadResp.orden[i].footer.impuesto.ToString();
                    orden.retencion = respWS.payloadResp.orden[i].footer.retenciones.ToString();
                    orden.valorbruto = respWS.payloadResp.orden[i].footer.montoTotal.ToString();
                    orden.textocabecera = respWS.payloadResp.orden[i].cabecera.observacion;

                    List<DetalleOrdenEntity> ListadoDetalleOrden = new List<DetalleOrdenEntity>();
                    for (int j = 0; j < Convert.ToInt32(respWS.payloadResp.orden[i].detalle.Length); j++)
                    {
                        DetalleOrdenEntity detalle = new DetalleOrdenEntity();
                        detalle.cantidad = respWS.payloadResp.orden[i].detalle[j].cantidad.ToString();
                        detalle.centrocoste = respWS.payloadResp.orden[i].detalle[j].centroCosto;
                        detalle.descripcentrocoste = respWS.payloadResp.orden[i].detalle[j].centroCosto;//falta
                        detalle.descripcion = respWS.payloadResp.orden[i].detalle[j].nombreProducto;
                        detalle.material = respWS.payloadResp.orden[i].detalle[j].sku;//falta
                        detalle.pnetounidad = respWS.payloadResp.orden[i].detalle[j].neto.ToString();
                        detalle.posicion = respWS.payloadResp.orden[i].detalle[j].idItem.ToString();//falta
                        detalle.totalbruto = respWS.payloadResp.orden[i].detalle[j].precio.ToString();
                        detalle.um = respWS.payloadResp.orden[i].detalle[j].unidadMedida;
                        detalle.valorneto = respWS.payloadResp.orden[i].detalle[j].neto.ToString();

                        ListadoDetalleOrden.Add(detalle);
                    }

                    orden.detalleOrden = ListadoDetalleOrden;

                    ListadoOrdenes.Add(orden);

                }
                return ListadoOrdenes;
            }
            catch(Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static List<OrdenEntity> GetOrden(string USUARIO, string PENDIENTE_LIBERAR, string ordenC)
        {
            try
            {
                OrdenConsultarMsgResp respWS = OrdenDAL.GetOrden(USUARIO, PENDIENTE_LIBERAR, ordenC);

                List<OrdenEntity> ListadoOrdenes = new List<OrdenEntity>();

                for (int i = 0; i < Convert.ToInt32(respWS.payloadResp.orden.Length); i++)
                {
                    OrdenEntity orden = new OrdenEntity();

                    orden.MAN = respWS.payloadResp.orden[i].cabecera.nroOrden;
                    orden.IDPED = respWS.payloadResp.orden[i].cabecera.idGrupoAurotizador;
                    orden.USUARIO = USUARIO;
                    orden.ordencompra = respWS.payloadResp.orden[i].cabecera.nroOrden;
                    orden.fechaordencompra = respWS.payloadResp.orden[i].cabecera.fechaOrden;
                    orden.idproveedor = respWS.payloadResp.orden[i].cabecera.codProveedor;
                    orden.razonsocial = respWS.payloadResp.orden[i].cabecera.razonSocial;
                    orden.rutproveedor = respWS.payloadResp.orden[i].cabecera.rutProveedor;
                    orden.pendienteliberar = respWS.payloadResp.orden[i].cabecera.tipoOrden.id;//falta
                    orden.orgcompras = respWS.payloadResp.orden[i].cabecera.organizacionDeCompra.id;
                    orden.nameorgcompras = respWS.payloadResp.orden[i].cabecera.organizacionDeCompra.descripcion;
                    orden.SOCIEDAD = respWS.payloadResp.orden[i].cabecera.rutProveedor;//falta
                    orden.sociedad = respWS.payloadResp.orden[i].cabecera.rutProveedor;//falta
                    orden.fechaentrega = respWS.payloadResp.orden[i].cabecera.fechaEntrega;
                    orden.moneda = respWS.payloadResp.orden[i].cabecera.tipoOrden.id;//falta
                    orden.precioneto = respWS.payloadResp.orden[i].footer.neto.ToString();
                    orden.recargo = respWS.payloadResp.orden[i].footer.cargo.ToString();
                    orden.dsctoimporte = respWS.payloadResp.orden[i].adicional[1].valor.ToString();//falta
                    orden.dsctoporcentaje = respWS.payloadResp.orden[i].adicional[0].valor.ToString();//falta
                    orden.ivacompraclp = respWS.payloadResp.orden[i].footer.impuesto.ToString();
                    orden.retencion = respWS.payloadResp.orden[i].footer.retenciones.ToString();
                    orden.valorbruto = respWS.payloadResp.orden[i].footer.montoTotal.ToString();
                    orden.textocabecera = respWS.payloadResp.orden[i].cabecera.observacion;

                    List<DetalleOrdenEntity> ListadoDetalleOrden = new List<DetalleOrdenEntity>();
                    for (int j = 0; j < Convert.ToInt32(respWS.payloadResp.orden[i].detalle.Length); j++)
                    {
                        DetalleOrdenEntity detalle = new DetalleOrdenEntity();
                        detalle.cantidad = respWS.payloadResp.orden[i].detalle[j].cantidad.ToString();
                        detalle.centrocoste = respWS.payloadResp.orden[i].detalle[j].centroCosto;
                        detalle.descripcentrocoste = respWS.payloadResp.orden[i].detalle[j].centroCosto;//falta
                        detalle.descripcion = respWS.payloadResp.orden[i].detalle[j].nombreProducto;
                        detalle.material = respWS.payloadResp.orden[i].detalle[j].sku;//falta
                        detalle.pnetounidad = respWS.payloadResp.orden[i].detalle[j].neto.ToString();
                        detalle.posicion = respWS.payloadResp.orden[i].detalle[j].idItem.ToString();//falta
                        detalle.totalbruto = respWS.payloadResp.orden[i].detalle[j].precio.ToString();
                        detalle.um = respWS.payloadResp.orden[i].detalle[j].unidadMedida;
                        detalle.valorneto = respWS.payloadResp.orden[i].detalle[j].neto.ToString();

                        ListadoDetalleOrden.Add(detalle);
                    }

                    orden.detalleOrden = ListadoDetalleOrden;

                    ListadoOrdenes.Add(orden);

                }
                return ListadoOrdenes;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static List<OrdenEntity> GetOrden(string USUARIO, string PENDIENTE_LIBERAR)
        {
            try
            {
                OrdenConsultarMsgResp respWS = OrdenDAL.GetOrden(USUARIO, PENDIENTE_LIBERAR);

                List<OrdenEntity> ListadoOrdenes = new List<OrdenEntity>();

                for (int i = 0; i < Convert.ToInt32(respWS.payloadResp.orden.Length); i++)
                {
                    OrdenEntity orden = new OrdenEntity();

                    orden.MAN = respWS.payloadResp.orden[i].cabecera.nroOrden;
                    orden.IDPED = respWS.payloadResp.orden[i].cabecera.idGrupoAurotizador;
                    orden.USUARIO = USUARIO;
                    orden.ordencompra = respWS.payloadResp.orden[i].cabecera.nroOrden;
                    orden.fechaordencompra = respWS.payloadResp.orden[i].cabecera.fechaOrden;
                    orden.idproveedor = respWS.payloadResp.orden[i].cabecera.codProveedor;
                    orden.razonsocial = respWS.payloadResp.orden[i].cabecera.razonSocial;
                    orden.rutproveedor = respWS.payloadResp.orden[i].cabecera.rutProveedor;
                    orden.pendienteliberar = respWS.payloadResp.orden[i].cabecera.tipoOrden.id;//falta
                    orden.orgcompras = respWS.payloadResp.orden[i].cabecera.organizacionDeCompra.id;
                    orden.nameorgcompras = respWS.payloadResp.orden[i].cabecera.organizacionDeCompra.descripcion;
                    orden.SOCIEDAD = respWS.payloadResp.orden[i].cabecera.rutProveedor;//falta
                    orden.sociedad = respWS.payloadResp.orden[i].cabecera.rutProveedor;//falta
                    orden.fechaentrega = respWS.payloadResp.orden[i].cabecera.fechaEntrega;
                    orden.moneda = respWS.payloadResp.orden[i].cabecera.tipoOrden.id;//falta
                    orden.precioneto = respWS.payloadResp.orden[i].footer.neto.ToString();
                    orden.recargo = respWS.payloadResp.orden[i].footer.cargo.ToString();
                    orden.dsctoimporte = respWS.payloadResp.orden[i].adicional[1].valor.ToString();//falta
                    orden.dsctoporcentaje = respWS.payloadResp.orden[i].adicional[0].valor.ToString();//falta
                    orden.ivacompraclp = respWS.payloadResp.orden[i].footer.impuesto.ToString();
                    orden.retencion = respWS.payloadResp.orden[i].footer.retenciones.ToString();
                    orden.valorbruto = respWS.payloadResp.orden[i].footer.montoTotal.ToString();
                    orden.textocabecera = respWS.payloadResp.orden[i].cabecera.observacion;

                    List<DetalleOrdenEntity> ListadoDetalleOrden = new List<DetalleOrdenEntity>();
                    for (int j = 0; j < Convert.ToInt32(respWS.payloadResp.orden[i].detalle.Length); j++)
                    {
                        DetalleOrdenEntity detalle = new DetalleOrdenEntity();
                        detalle.cantidad = respWS.payloadResp.orden[i].detalle[j].cantidad.ToString();
                        detalle.centrocoste = respWS.payloadResp.orden[i].detalle[j].centroCosto;
                        detalle.descripcentrocoste = respWS.payloadResp.orden[i].detalle[j].centroCosto;//falta
                        detalle.descripcion = respWS.payloadResp.orden[i].detalle[j].nombreProducto;
                        detalle.material = respWS.payloadResp.orden[i].detalle[j].sku;//falta
                        detalle.pnetounidad = respWS.payloadResp.orden[i].detalle[j].neto.ToString();
                        detalle.posicion = respWS.payloadResp.orden[i].detalle[j].idItem.ToString();//falta
                        detalle.totalbruto = respWS.payloadResp.orden[i].detalle[j].precio.ToString();
                        detalle.um = respWS.payloadResp.orden[i].detalle[j].unidadMedida;
                        detalle.valorneto = respWS.payloadResp.orden[i].detalle[j].neto.ToString();

                        ListadoDetalleOrden.Add(detalle);
                    }

                    orden.detalleOrden = ListadoDetalleOrden;

                    ListadoOrdenes.Add(orden);

                }
                return ListadoOrdenes;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static ValidaUsuarioEntity GetValidaUsuario(string USUARIO, string CONTRASENA)
        {
            try
            {
                ValidaUsuarioMsgResp respWS = OrdenDAL.GetValidausuario(USUARIO, CONTRASENA);
                ValidaUsuarioEntity validaUsuario = new ValidaUsuarioEntity();

                validaUsuario.rolid = respWS.payloadResp.rol.id;
                validaUsuario.rolDescripcion = respWS.payloadResp.rol.descripcion;
                validaUsuario.grupoLiberadorId = respWS.payloadResp.grupoLiberador.id;
                validaUsuario.grupoLiberadorDescripcion = respWS.payloadResp.grupoLiberador.descripcion;
                validaUsuario.organizaciondeComprasaId = respWS.payloadResp.organizacionDeComprasa.id;
                validaUsuario.organizacionDeComprasaDescripcion = respWS.payloadResp.organizacionDeComprasa.descripcion;

                return validaUsuario;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
        public static OrdenModificarMsgResp GetModificaOrden(string codigoLiberacion, string ordenId, string rut)
        {
            try
            {
                OrdenModificarMsgResp respWS = OrdenDAL.GetModificaOrden(codigoLiberacion, ordenId, rut);
                return respWS;
            }
            catch (Exception e)
            {
                mensajeOperacion = new ResponseWSEntity("1", "<strong>[Error 281]</strong> Se generó un problema interno al intentar realizar la consulta. Favor contactar a su administrador.");

                return null;
            }
        }
    }
}
